//
//  AppDelegate.swift
//  OOTTBusinessApp
//
//  Created by Santosh on 06/06/17.
//  Copyright © 2017 KonstantInfoSolutions. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift
import UserNotifications
import FBSDKCoreKit
import GoogleMaps
import GooglePlaces
import Fabric
import Crashlytics
import PKHUD
import AdSupport
import LyftSDK
import YandexMobileMetrica

var appDelegate = UIApplication.shared.delegate
var isCheckinPopupVisible = false

let NotificationInternetConnection = "internetConnection"

enum LogoutCaseEnum : Int {
    case singleLogout
    case groupLogout
    case addAccountLogout
    case switchAccount
    case none
}

var logoutCase = LogoutCaseEnum.none

class GlobalBusinessUser
{
    var userID : String
    
    init(id : String)
    {
        self.userID = id
    }
}

var global_FavouriteBusinessArray = [GlobalBusinessUser]()

enum AppUserTypeEnum : String
{
    case user = "Customer"
    case provider = "Business"
    case employee = "Employee"
}

//MARK:-
//MARK:-
@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    var window: UIWindow?
    
    static let shared: AppDelegate = {
        return UIApplication.shared.delegate
        }() as! AppDelegate
    
    var internetReachability : Reachability? = Reachability .networkReachabilityForInternetConnection()
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        
        NotificationCenter.default.addObserver(self, selector: #selector(reachabilityDidChange(_:)), name: NSNotification.Name(rawValue: ReachabilityDidChangeNotificationName), object: nil)
        
        _ = internetReachability?.startNotifier()
        
        checkReachability()
        
        //        PKHUD.sharedHUD.gracePeriod = 0.3
        
        

        
        if (UtilityClass.getUserSidData()) != nil
        {
            if UtilityClass.getUserTypeData() != nil
            {
                let userType = UtilityClass.getUserTypeData()!
                
                if userType == AppUserTypeEnum.user.rawValue
                {
                    //User home page
                    let businessVC = UIStoryboard.getUserTabsStoryboard().instantiateInitialViewController()
                    
                    UtilityClass.changeRootViewController(with: businessVC!)
                }
                else if userType == AppUserTypeEnum.provider.rawValue
                {
                    //business home page
                    let isProfileUpdateDone = UtilityClass.getUserInfoData()["isProfileDone"] as! Bool
                    
                    if !isProfileUpdateDone //Show login again
                    {
                        let loginVC = UIStoryboard.getLoginFlowStoryboard().instantiateInitialViewController()
                        
                        UtilityClass.changeRootViewController(with: loginVC!)
                    }
                    else
                    {
                        let businessVC = UIStoryboard.getBusinessTabBarFlowStoryboard().instantiateInitialViewController()
                        
                        UtilityClass.changeRootViewController(with: businessVC!)
                    }
                }
                else
                {
                    // Bouncer home page
                    let dashboardVC = UIStoryboard.getBusinessHomeFlowStoryboard().instantiateInitialViewController()
                    
                    UtilityClass.changeRootViewController(with: dashboardVC!)
                }
            }
            else {
                let loginVC = UIStoryboard.getUserTabsStoryboard().instantiateInitialViewController()
                UtilityClass.changeRootViewController(with: loginVC!)
            }
        }
        else
        {
            let loginVC = UIStoryboard.getLoginFlowStoryboard().instantiateInitialViewController()
            UtilityClass.changeRootViewController(with: loginVC!)
        }
        
        
        IQKeyboardManager.sharedManager().enable = true
        IQKeyboardManager.sharedManager().keyboardDistanceFromTextField = 50
        IQKeyboardManager.sharedManager().shouldResignOnTouchOutside = true
        UtilityClass.updateNavigationBarForApp()
        
        registerForPushNotifications()
        
        GMSServices.provideAPIKey(Google_Api_Key)
        GMSPlacesClient.provideAPIKey(Google_Api_Key)
        
        _ = LocationManager.sharedInstance()
        
        Fabric.with([Crashlytics.self])
        
        FBSDKApplicationDelegate.sharedInstance().application(application, didFinishLaunchingWithOptions: launchOptions)
        
        // Handle push notification, when app is launched...
        if let pushInfo = launchOptions?[.remoteNotification] as? [String:Any]
        {
            print(pushInfo)
        }
        
//        CFNotificationCenterAddObserver(CFNotificationCenterGetDarwinNotifyCenter(),
//                                        nil,
//                                        { (_, observer, name, _, _) in
//                                            print("received notification: \(String(describing: name))")
//                                            SocketManager.sharedInstance().removeIncomingMessagesListener()
//        },
//                                        "com.apple.springboard.lockcomplete" as CFString,
//                                        nil,
//                                        .deliverImmediately)
        
        //        // Keyobard frame change notification
        //        NotificationCenter.default.addObserver(self, selector: #selector(keyboardDidChangeFrame), name: NSNotification.Name.UIKeyboardDidChangeFrame, object: nil)
        
        AmazonServiceManager.defaultServiceManager().setDefaultServiceConfiguration(forRegion: .USEast1, poolID: "us-east-1:64e433a8-8578-48fd-9583-3cb6f6496f2f")
        
        //Lyft
        let token = "UFtxHg5xzJ2/tqSMq0BP1gl+5vo3v+fqLSvqUuGYvM6pmcrX89QgBrCF/E2OC3bTnCCHGfGDHbzad7zwVMmCR7Me797RMyzfLzFH5ronx7Pyw9aH4AnswtM="
        let clientId = "kMxOf_C-jrZq"
        LyftConfiguration.developer = (token: token, clientId: clientId)

        //Yandex Matric Config
        let config = YMMYandexMetricaConfiguration.init(apiKey:"288e0dbe-34f6-4806-a306-b02387b0ddd0")
        guard config != nil else {
            return true
        }
        YMMYandexMetrica.activate(with: config!)

        return true
    }
    
    //MARK:-
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }
    
    //MARK:-
    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
        
        //        SocketManager.sharedInstance().disconnectSocket()
        if UtilityClass.getUserSidData() != nil
        {
            SocketManager.sharedInstance().setIsInBackground(true)
        }
        //SocketManager.sharedInstance().removeIncomingMessagesListener()
    }
    
    //MARK:-
    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
        if UtilityClass.getUserSidData() != nil
        {
            SocketManager.sharedInstance().setIsInBackground(false)
            
            if let chatViewC = appDelegate?.window??.visibleViewController() as? GroupChatViewController
            {
                chatViewC.viewWillAppear(true)
            }
        }
    }
    
    //MARK:-
    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
        
        // If location is disabled, then adding popup view...
        
        application.applicationIconBadgeNumber = 0
        
        if LocationManager.sharedInstance().isLocationAccessAllowed()
        {
            if let view = window?.viewWithTag(5001) as? LocationEnablePopup
            {
                view.removeFromSuperview()
            }
        }
        else
        {
            if (window?.viewWithTag(5001) as? LocationEnablePopup) != nil
            {
                // Already added
            }
            else
            {
                let popup : LocationEnablePopup = LocationEnablePopup (frame: (window?.frame)!)
                window?.addSubview(popup)
            }
        }
    }
    
    //MARK:-
    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
    
    //MARK:-
    func application(_ app: UIApplication, open url: URL, options: [UIApplicationOpenURLOptionsKey : Any] = [:]) -> Bool {
        
        let handled = FBSDKApplicationDelegate.sharedInstance().application(app, open: url, options: options)
        
        return handled
    }
    
    //MARK:-
    func registerForPushNotifications() {
        if #available(iOS 10.0, *) {
            UNUserNotificationCenter.current().requestAuthorization(options: [.alert, .sound, .badge]) {
                (granted, error) in
                print("Permission granted: \(granted)")
                guard granted else { appDeviceToken = ""
                    return }
                self.getNotificationSettings()
            }
        } else {
            // Fallback on earlier versions
        }
    }
    
    //MARK:-
    func getNotificationSettings() {
        if #available(iOS 10.0, *) {
            UNUserNotificationCenter.current().getNotificationSettings { (settings) in
                print("Notification settings: \(settings)")
                guard settings.authorizationStatus == .authorized else { appDeviceToken = ""
                    return }
                DispatchQueue.main.async {
                    UIApplication.shared.registerForRemoteNotifications()
                }
            }
        } else {
            // Fallback on earlier versions
        }
    }
    
    //MARK:-
    func application(_ application: UIApplication,
                     didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        let tokenParts = deviceToken.map { data -> String in
            return String(format: "%02.2hhx", data)
        }
        
        let token = tokenParts.joined()
        appDeviceToken = token
        print("Device Token: \(token)")
    }
    
    //MARK:-
    func application(_ application: UIApplication,
                     didFailToRegisterForRemoteNotificationsWithError error: Error) {
        print("Failed to register: \(error)")
        appDeviceToken = ""
    }
    
    //MARK:-
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any], fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        
        if UtilityClass.getUserSidData() == nil
        {
            return
        }
        
        // Inactive state, coming from background to foreground by tapping on notification
        if application.applicationState == .inactive
        {
            print("***\n")
            print(userInfo)
            
            // If user is logged in and fetching user type
            if let userType = UtilityClass.getUserTypeData() {
                
                // If user is of type Customer
                if userType == AppUserTypeEnum.user.rawValue
                {
                    if let pushType = userInfo["type"] as? String
                    {
                        if pushType == "chat"
                        {
                            if let dataDict = userInfo["data"] as? [String:Any]
                            {
                                self.handleChatMessage(usingDictionary: dataDict)
                            }
                            return
                        }
                    }
                    if let tabbarController = appDelegate?.window??.rootViewController as? UserTabsController
                    {
                        tabbarController.updateTabBar(withIndex: 3)
                        if let activityControllerNav = tabbarController.selectedViewController as? UINavigationController
                        {
                            if let activityController = activityControllerNav.viewControllers[0] as? UserActivityViewController
                            {
                                //                            DispatchQueue.main.asyncAfter(deadline: DispatchTime .now() + .seconds(1))
                                //                            {
                                activityController.autoSelectYouTab()
                                //                            }
                            }
                        }
                    }
                }
            }
        }
    }
    
    //MARK:-
    func handleChatMessage(usingDictionary dictionary : [String:Any])
    {
        let model = self.addNewModel(usingDictionary: dictionary)
        
        // If chat view is open...
        if let groupChatVC = appDelegate?.window??.visibleViewController() as? GroupChatViewController
        {
            // If same group chat is open...
            if model.roomID == currentUserID
            {
                // Simply add new message model...
                groupChatVC.notificationSocketConnected()
            }
            else if model.senderID == currentUserID, model.roomID.isEmpty // Single chat is open
            {
                // Simply add new message model...
                groupChatVC.notificationSocketConnected()
            }
            else
            {
                // open screen
                
                let isGroup = !model.roomID.isEmpty
                
                let chatViewController = UIStoryboard.getChatViewStoryboard().instantiateInitialViewController() as! GroupChatViewController
                
                chatViewController.hidesBottomBarWhenPushed = true
                
                chatViewController.isGroupChat = isGroup
                
                chatViewController.chatUsername.text = isGroup ? model.groupName : model.senderName
                
                chatViewController.isBusinessChat = model.isBusinessGroup
                
                if isGroup
                {
                    chatViewController.chatPicture = model.groupPicture
                    
                    chatViewController.chatRoomID = model.roomID
                }
                else
                {
                    chatViewController.chatUserID = model.senderID
                }
                
                if let tabbarVC = self.window?.rootViewController as? UserTabsController
                {
                    if let navVC = tabbarVC.selectedViewController as? UINavigationController
                    {
                        navVC.pushViewController(chatViewController, animated: true)
                    }
                }
            }
        }
        else
        {
            
            // open screen
            
            let isGroup = !model.roomID.isEmpty
            
            let chatViewController = UIStoryboard.getChatViewStoryboard().instantiateInitialViewController() as! GroupChatViewController
            
            chatViewController.hidesBottomBarWhenPushed = true
            
            chatViewController.isGroupChat = isGroup
            
            chatViewController.chatUsername.text = isGroup ? model.groupName : model.senderName
            
            chatViewController.isBusinessChat = model.isBusinessGroup
            
            if isGroup
            {
                chatViewController.chatPicture = model.groupPicture
                
                chatViewController.chatRoomID = model.roomID
            }
            else
            {
                chatViewController.chatUserID = model.senderID
            }
            
            if let tabbarVC = self.window?.rootViewController as? UserTabsController
            {
                if let navVC = tabbarVC.selectedViewController as? UINavigationController
                {
                    navVC.pushViewController(chatViewController, animated: true)
                }
            }
        }
    }
    
    //MARK:-
    func addNewModel(usingDictionary dictionary : [String:Any]) -> ModelChatMessage
    {
        let model = ModelChatMessage () // Model creation
        model.updateModel(usingDictionary: dictionary) // Updating model
        return model
    }
    
    //MARK:-
    func checkReachability() {
        guard let r = internetReachability else { return }
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: NotificationInternetConnection), object: r.isReachable)
    }
    
    //MARK:-
    func isInternetAvailable() -> Bool
    {
        guard let r = internetReachability else { return false}
        
        return r.isReachable
    }
    
    //MARK:-
    func reachabilityDidChange(_ notification: Notification) {
        checkReachability()
    }
    
    //MARK:-
    func handleSessionExpiredSituation()
    {
        if let topViewController = window?.visibleViewController()
        {
            UtilityClass.showAlertWithTitle(title: App_Name, message: "Session expired, your current  account has been connected to another device. Please login again", onViewController: topViewController, withButtonArray: nil, dismissHandler:
                {[weak self] (buttonIndex) in
                    guard let `self` = self else {return}
                    self.performForceLogout()
            })
        }
    }
    
    //MARK:-
    func performForceLogout()
    {
        HUD.show(.systemActivity, onView: self.window!)
        
        let urlToHit = EndPoints.logout(UtilityClass.getUserSidData()!).path
        
        AppWebHandler.sharedInstance().fetchData(fromURL: urlToHit, httpMethod: .get, parameters: nil, shouldDeserialize: true)
        {[weak self] (data, dictionary, statusCode, error) in
            
            HUD.hide()
            
            guard let `self` = self else { return }
            
            if statusCode == 200
            {
                let responseDictionary = dictionary!
                let type = responseDictionary["type"] as! Bool
                
                if type
                {
                    logoutCase = .singleLogout
                    if logoutCase == .singleLogout // Removing current user from stack list...
                    {
                        _ = UtilityClass.removeCurrentUserFromStack()
                    }
                    
                    UtilityClass.deleteDataOnLogout()
                    
                    currentUserID = nil
                    SocketManager.sharedInstance().removeIncomingMessagesListener()
                    SocketManager.sharedInstance().disconnectSocket()
                    
                    if UtilityClass.getFirstUserFromStackedList() != nil
                    {
                        self.updateRootViewController()
                        return
                    }
                    
                    let loginVC = UIStoryboard.getLoginFlowStoryboard().instantiateInitialViewController()
                    
                    UtilityClass.changeRootViewController(with: loginVC!)
                }
            }
        }
    }
    
    func updateRootViewController()
    {
        if logoutCase == .singleLogout
        {
            UtilityClass.saveUserInfoData(userDict: UtilityClass.getFirstUserFromStackedList()!)
            
            if UtilityClass.getUserTypeData() == AppUserTypeEnum.employee.rawValue
            {
                let employeeVC = UIStoryboard.getBusinessHomeFlowStoryboard().instantiateInitialViewController()
                
                UtilityClass.changeRootViewController(with: employeeVC!)
                
                return
            }
            
            if UtilityClass.getUserTypeData() == AppUserTypeEnum.provider.rawValue
            {
                let businessVC = UIStoryboard.getBusinessTabBarFlowStoryboard().instantiateInitialViewController()
                
                UtilityClass.changeRootViewController(with: businessVC!)
                return
            }
            
            let userHomeVC = UIStoryboard.getUserTabsStoryboard().instantiateInitialViewController()
            
            UtilityClass.changeRootViewController(with: userHomeVC!)
            
            return
        }
        let loginVC = UIStoryboard.getLoginFlowStoryboard().instantiateInitialViewController()
        
        UtilityClass.changeRootViewController(with: loginVC!)
    }
}

//MARK:-
//MARK:-
extension AppDelegate
{
    func showCheckinPopup(withDetails checkinDetails: [String:Any], type: CheckinPopupViewController.CheckinPopupType, delegate: UIViewController? = nil, pickupLocation: CLLocation? = nil, dropoffLocation: CLLocation? = nil) {
        
        if let rootVC = window?.rootViewController as? UserTabsController {
            rootVC.definesPresentationContext = true
            let vc: CheckinPopupViewController = UIStoryboard.getCheckinPopupStoryboard().instantiateInitialViewController() as! CheckinPopupViewController
            vc.popupType = type
            vc.pickupLocation = pickupLocation
            vc.dropoffLocation = dropoffLocation
            vc.delegate = delegate as? CheckinPopupViewControllerDelegate
            vc.modalPresentationStyle = .overCurrentContext
            
            vc.updateModel(usingDictionary: checkinDetails)
            
            rootVC.present(vc, animated: true, completion: nil)
        }
    }
}
//MARK:-
//MARK:-

extension UIDevice {
    static var isIphoneX: Bool {
        var modelIdentifier = ""
        if isSimulator {
            modelIdentifier = ProcessInfo.processInfo.environment["SIMULATOR_MODEL_IDENTIFIER"] ?? ""
        } else {
            var size = 0
            sysctlbyname("hw.machine", nil, &size, nil, 0)
            var machine = [CChar](repeating: 0, count: size)
            sysctlbyname("hw.machine", &machine, &size, nil, 0)
            modelIdentifier = String(cString: machine)
        }
        
        return modelIdentifier == "iPhone10,3" || modelIdentifier == "iPhone10,6"
    }
    
    static var isSimulator: Bool {
        return TARGET_OS_SIMULATOR != 0
    }
}
extension UIWindow {
    
    //MARK:-
    func visibleViewController() -> UIViewController? {
        if let rootViewController: UIViewController  = self.rootViewController {
            return UIWindow.getVisibleViewControllerFrom(vc: rootViewController)
        }
        return nil
    }
    
    //MARK:-
    class func getVisibleViewControllerFrom(vc:UIViewController) -> UIViewController {
        
        if vc.isKind(of: UINavigationController.self)
        {
            let navigationController = vc as! UINavigationController
            return UIWindow.getVisibleViewControllerFrom( vc: navigationController.visibleViewController!)
        }
        else if vc.isKind(of: UITabBarController.self){
            
            let tabBarController = vc as! UITabBarController
            return UIWindow.getVisibleViewControllerFrom(vc: tabBarController.selectedViewController!)
            
        } else {
            
            if let presentedViewController = vc.presentedViewController {
                
                return UIWindow.getVisibleViewControllerFrom(vc: presentedViewController)
                
            } else {
                
                return vc
            }
        }
    }
}
