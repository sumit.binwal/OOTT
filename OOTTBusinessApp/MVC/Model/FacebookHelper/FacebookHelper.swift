//
//  FacebookHelper.swift
//  OOTTBusinessApp
//
//  Created by Santosh on 20/06/17.
//  Copyright © 2017 KonstantInfoSolutions. All rights reserved.
//

import Foundation
import UIKit
import FBSDKLoginKit
import PKHUD

class FacebookHelper
{
    class func loginFacebook(withReadPermissions readPermission:[String], completionHandler:@escaping ([String:Any])->()) -> Void
    {
        let visibleController : UIViewController = (appDelegate?.window??.rootViewController!)!
        
        visibleController.view.isUserInteractionEnabled = false

        
        let loginManager = FBSDKLoginManager()
        
        loginManager.logOut()
        
        loginManager.logIn(withReadPermissions: readPermission, from: visibleController) { (result, error) in
            
            visibleController.view.isUserInteractionEnabled = true
            
            if error != nil
            {
                completionHandler(["error" : error!.localizedDescription])
                return
            }
            let finalResult = result!
            
            if finalResult.isCancelled
            {
                completionHandler(["error" : "User cancelled the dialog"])
                return
            }
            // result
            
            getFBUserData(completionHandler: completionHandler)
        }
    }
    
    class func getFBUserData(completionHandler:@escaping ([String:Any])-> ()){
        if((FBSDKAccessToken.current()) != nil){
            FBSDKGraphRequest(graphPath: "me", parameters: ["fields": "id, name, first_name, last_name, picture.type(large), email, birthday"]).start(completionHandler: { (connection, result, error) -> Void in
                if (error == nil){
                    completionHandler(result as! [String : Any])
                }
                else
                {
                    completionHandler(["error" : "Some error occured"])
                }
            })
        }
    }
}
