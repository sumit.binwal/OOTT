//
//  ModelActivity.swift
//  OOTTBusinessApp
//
//  Created by Santosh on 19/07/17.
//  Copyright © 2017 KonstantInfoSolutions. All rights reserved.
//

import Foundation

/*
"data": [
{
"_id": "596c99896b5d9d43c613fe88",
"activityType": "follow",
"isFollow": 0,
"user": {
"_id": "5940e97ea17d461a094b1e4e",
"name": "new profile",
"picture": "http://192.168.0.131/oott/dev/public/uploads/users/http://192.168.0.131/oott/dev/public/uploads/users/http://192.168.0.131/oott/dev/public/uploads/users/http://192.168.0.131/oott/dev/public/uploads/users/http://192.168.0.131/oott/dev/public/uploads/users/vtDPlb7QaPWY8l7rRdsjGQAYd34D3Hgh.jpg",
"userType": "Business"
},
"message": "new profile started follow you"
},
 */

class LastCheckinData
{
    var checkinID : String?
    var checkinStatus : String?
    
    func updateModel(usingDictionary dictionary : [String:Any])
    {
        let newDictionary = dictionary
        if let id = newDictionary["_id"] as? String
        {
            self.checkinID = id
        }
        
        if let status = newDictionary["status"] as? String
        {
            self.checkinStatus = status
        }
    }
}

class OtherUserActivityData
{
    var userID : String?
    var userName : String?
    var pictureString : String?
    var userType : String?
    var isPrivateUser = false
    var isAccountDeleted = false
    
    var lastCheckIn : LastCheckinData?
    
    func updateModel(usingDictionary dictionary : [String:Any])
    {
        let newDictionary = dictionary
        if let id = newDictionary["user_id"] as? String
        {
            self.userID = id
        }
        
        if let isPrivateAccount = newDictionary["isPrivateAccount"] as? Bool
        {
            self.isPrivateUser = isPrivateAccount
        }
        
        if let isArchive = newDictionary["isArchive"] as? Bool
        {
            self.isAccountDeleted = isArchive
        }
        
        if let name = newDictionary["username"] as? String
        {
            self.userName = name
        }
        
        if let picture = newDictionary["picture"] as? String
        {
            self.pictureString = picture
        }
        
        if let userType = newDictionary["userType"] as? String
        {
            self.userName = userType
        }
        
        if let checkin = newDictionary["lastCheckin"] as? [String:Any]
        {
            if self.lastCheckIn == nil
            {
                self.lastCheckIn = LastCheckinData ()
            }
            self.lastCheckIn?.updateModel(usingDictionary: checkin)
        }
    }
}

class ModelMyActivity
{
    var activityID : String?
    var activityType : String?
    var otherUser : OtherUserActivityData?
    var activityMessage : String?
    var isFollowed : Bool = false
    var isRequestedToFollow : Bool = false
    var shouldDisplayFollowUnfollowButton = false
    var isMakeMoveInvitation = false
    var isFollowRequestActivity = false
    var isCheckinLikeActivity = false
    var isCheckinCommentActivity = false
    var activityTime : String?
    
    var businessID : String?
    var groupID : String?
    
    var checkinID : String?
    
    func updateModel(usingDictionary dictionary : [String:Any])
    {
        let newDictionary = dictionary
        if let id = newDictionary["_id"] as? String
        {
            self.activityID = id
        }
        
        if let created = newDictionary["updated"] as? String
        {
            self.activityTime = created
        }
        
        if let checkin_id = newDictionary["checkin_id"] as? String
        {
            self.checkinID = checkin_id
        }
        
        if let business_id = newDictionary["business_id"] as? String
        {
            self.businessID = business_id
        }
        
        if let group_id = newDictionary["group_id"] as? String
        {
            self.groupID = group_id
        }
        
        if let activityTyp = newDictionary["activityType"] as? String
        {
            self.activityType = activityTyp
            
            if self.activityType == "follow"
            {
                self.shouldDisplayFollowUnfollowButton = true
            }
            else
            {
                self.shouldDisplayFollowUnfollowButton = false
            }
            
            if self.activityType == "make_move"
            {
                self.isMakeMoveInvitation = true
            }
            
            if self.activityType == "followReq"
            {
                self.isFollowRequestActivity = true
            }
            
            if self.activityType == "checkinLike"
            {
                self.isCheckinLikeActivity = true
            }
            
            if self.activityType == "checkinComment"
            {
                self.isCheckinCommentActivity = true
            }
        }
        
        if let user = newDictionary["user"] as? [String : Any]
        {
            if self.otherUser == nil
            {
                self.otherUser = OtherUserActivityData ()
            }
            self.otherUser?.updateModel(usingDictionary: user)
        }
        
        if let activityMsg = newDictionary["message"] as? String
        {
            self.activityMessage = activityMsg
        }
        
        if let followed = newDictionary["isFollowed"] as? Bool
        {
            self.isFollowed = followed
        }
        
        if let isRequested = newDictionary["isRequested"] as? Bool
        {
            self.isRequestedToFollow = isRequested
        }
        
        if (otherUser?.isAccountDeleted)! {
            self.shouldDisplayFollowUnfollowButton = true
        }
    }
}


class ModelFollowingActivity
{
    var activityID : String?
    var activityType : String?
    var activityBy : OtherUserActivityData?
    var activityMessage : String?
    var activityTime : String?
    
    func updateModel(usingDictionary dictionary : [String:Any])
    {
        let newDictionary = dictionary
        if let id = newDictionary["_id"] as? String
        {
            self.activityID = id
        }
        
        if let activityTyp = newDictionary["activityType"] as? String
        {
            self.activityID = activityTyp
        }
        
        if let created = newDictionary["updated"] as? String
        {
            self.activityTime = created
        }
        
        if let user = newDictionary["user"] as? [String : Any]
        {
            if self.activityBy == nil
            {
                self.activityBy = OtherUserActivityData ()
            }
            self.activityBy?.updateModel(usingDictionary: user)
        }
        
        if let activityMsg = newDictionary["message"] as? String
        {
            self.activityMessage = activityMsg
        }
    }
}





