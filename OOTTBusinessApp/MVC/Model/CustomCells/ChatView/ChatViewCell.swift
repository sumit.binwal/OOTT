//
//  ChatViewCell.swift
//  OOTTBusinessApp
//
//  Created by Santosh on 13/07/17.
//  Copyright © 2017 KonstantInfoSolutions. All rights reserved.
//

import UIKit
import SDWebImage
let kCellIdentifier_GroupCell_Other = "otherGroupCell"

let kCellIdentifier_GroupCell_OtherMedia = "otherGroupCellMedia"

let kCellIdentifier_GroupCell_Own = "ownCell"

let kCellIdentifier_GroupCell_OwnLike = "ownCellLike"

let kCellIdentifier_GroupCell_OwnLikeMedia = "ownCellLikeMedia"

let kCellIdentifier_GroupCell_Single = "otherSingleCell"

let kCellIdentifier_GroupCell_Business = "BusinessGroupCell"
let kCellIdentifier_GroupCell_BusinessMedia = "BusinessGroupCellMedia"

let kCellIdentifier_GroupCell_OwnMedia = "ownCellMedia"
let kCellIdentifier_GroupCell_SingleMedia = "otherSingleCellMedia"
let kCellIdentifier_Chat_NotifyCell = "chatNotifyCell"


enum ChatCellType {
    case groupOther
    case singleOther
    case user
    case userLike
    case notify
    case none
}

protocol ChatViewCellDelegate : class
{
    func chatCell(_ cell : ChatViewCell, didTapOnLikeUnLikeButton likeUnLikeButton : UIButton)
    func chatCellDidDownloadTheMedia(_ cell: ChatViewCell)
    func chatCell(_ cell: ChatViewCell, didTapOnMedia mediaButton: UIButton)
    func chatCell(_ cell: ChatViewCell, didTapOnLikeUser userModel: ModelGroupChatLikesMessage)
}

class ChatViewCell: UITableViewCell,TapLinksLabelDelegate {

    @IBOutlet weak var imageviewUser: UIImageView!
    
    @IBOutlet weak var userName: UILabel!
     
    @IBOutlet weak var userLocation: UILabel!
    
    @IBOutlet weak var conatinerTextView: UIView!
    
    @IBOutlet weak var messageLabel: TapLinksLabel!
    
    @IBOutlet weak var messageTime: UILabel!
    
    @IBOutlet weak var locationImage: UIImageView!
    
    @IBOutlet weak var buttonLikeUnLike: UIButton!
    
    @IBOutlet weak var employeePositionLabel: UILabel!
    
    @IBOutlet weak var buttonMediaView: UIButton!
    
    
    @IBOutlet weak var buttonDownloadMedia: UIButton!
    
    @IBOutlet weak var constraintLocationHeight: NSLayoutConstraint!
    
    
    @IBOutlet weak var loaderView: BRCircularProgressView!
    
    @IBOutlet weak var playIcon: UIImageView!
    
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var constraintCollectionViewHeight: NSLayoutConstraint!
    
    var cellType = ChatCellType.none
    
    weak var delegate : ChatViewCellDelegate?
    
    var mediaString = ""
    
    var isDownloading = false
    
    var likesArray: [ModelGroupChatLikesMessage] = [ModelGroupChatLikesMessage]() {
        didSet {
//            likesArray.removeAll()
            collectionView.reloadData()
        }
    }
    
    //MARK: - deinit
    deinit {
        print("ChatViewCell Deinit")
    }
    
    //MARK: - awakeFromNib
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        initialCellSetup()
        
        if self.conatinerTextView != nil {
            self.conatinerTextView.layer.cornerRadius = 5 * scaleFactorX
        }
        self.disbaleSelection()
        
        if collectionView != nil {
            collectionView.backgroundColor = .clear
            
            collectionView.register(UINib (nibName: "ChatViewCollectionLikeCell", bundle: nil), forCellWithReuseIdentifier: "likeCollection")
            collectionView.delegate = self
            collectionView.dataSource = self
        }
    }

    //MARK: -
    func initialCellSetup()
    {
        self.backgroundColor = UIColor.clear
        
        if self.imageviewUser != nil
        {
            self.imageviewUser.layer.cornerRadius = self.imageviewUser.frame.size.width/2 * scaleFactorX
            self.imageviewUser.clipsToBounds = true
            self.imageviewUser.createBorder(withColor: UIColor (RED: 29, GREEN: 158, BLUE: 240, ALPHA: 1), andBorderWidth: 0.5 * scaleFactorX)
        }
        
        if messageLabel != nil {
            self.messageLabel.delegate = self
        }
        if self.userName != nil
        {
            self.userName.textColor = UIColor.white
        }
        
        if buttonMediaView != nil {
            buttonMediaView.imageView?.contentMode = .scaleAspectFill
            buttonMediaView.imageView?.clipsToBounds = true
            buttonMediaView.layer.cornerRadius = 5 * scaleFactorX
        }
        
        if loaderView != nil {
            loaderView.isHidden = true
            loaderView.setCircleStrokeWidth(5)
            loaderView.setCircleStrokeColor(UIColor (RED: 0, GREEN: 0, BLUE: 0, ALPHA: 0.5), circleFillColor: UIColor (RED: 0, GREEN: 0, BLUE: 0, ALPHA: 0.5), progressCircleStrokeColor: color_pink, progressCircleFillColor: UIColor (RED: 0, GREEN: 0, BLUE: 0, ALPHA: 0.5))
        }
    }
    
    //MARK: - layoutSubviews
    override func layoutSubviews() {
        if messageLabel != nil {
            self.messageLabel.setNeedsDisplay()
        }
        
    }
    
    func linkWasTapped(url: URL) {
        print("view controller, tapped link: \(url)")
        UIApplication.shared.open(url)
    }
    //MARK: - Update cell using Model
    func updateCell(usingModel model : ModelChatMessage, cellIndex : Int, folderPath: String)
    {
        
        if messageLabel != nil {
            self.messageLabel.text = model.message
            
            self.messageLabel.sizeToFit()
        }
        
        if model.isLeftUser || model.isGroupName || model.isGroupPhoto || model.isAddedInGroup || model.isInvitemove {
            if let container = self.contentView.viewWithTag(2000) {
                container.layer.cornerRadius = 5
            }
            return;
        }
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ" //Your date format\
        dateFormatter.locale = NSLocale(localeIdentifier: "en_US_POSIX") as Locale!
        dateFormatter.timeZone = NSTimeZone(name: "UTC")! as TimeZone
        let date : NSDate = dateFormatter.date(from: model.messageTime )! as NSDate //according to date format your date string
        
        self.messageTime.text = UtilityClass.timeAgoSinceDate(date as Date, currentDate:NSDate() as Date , numericDates: false)
        
        if model.isLeftUser || model.isGroupName || model.isGroupPhoto || model.isAddedInGroup || model.isInvitemove {
            return;
        }
        
        if self.userName != nil
        {
            self.userName.text = model.senderName
        }
        
        if self.employeePositionLabel != nil
        {
            self.employeePositionLabel.text = model.senderPosition
            self.employeePositionLabel.textColor = color_pink
        }
        
        if self.imageviewUser != nil
        {
            self.imageviewUser.setIndicatorStyle(.white)
            self.imageviewUser.setShowActivityIndicator(true)
            self.imageviewUser.sd_setImage(with: URL (string: model.senderPicture), completed:
                { (image, error, cache, url) in
                    
            })
        }
        
        if messageLabel != nil {
            
            self.messageLabel.numberOfLines = 0
            
          
//            self.messageLabel?.enabledTypes = [.url]
//            self.messageLabel?.textColor = .white
//            self.messageLabel?.handleURLTap({ (urltapped) in
//
//                var strUrl = urltapped.absoluteString
//                if strUrl.contains("http") || strUrl.contains("https")
//                {
//                    print(strUrl)
//                }
//                else
//                {
//                    strUrl = "http://" + strUrl
//                    print(strUrl)
//                }
//                UIApplication.shared.open(URL.init(string: strUrl)!)
//
//
////                    UIApplication.shared.openURL(urltapped)
//
//
//                 //print("Success. You just tapped the \(urltapped) hashtag")
//            })
            
            
            self.messageLabel.text = model.message
            self.messageLabel.linksColor = UIColor.white
            self.messageLabel.sizeToFit()
        }
        
        if buttonLikeUnLike != nil
        {
            buttonLikeUnLike.tag = cellIndex
            if model.isMessageLikedByMe
            {
                buttonLikeUnLike.setImage(#imageLiteral(resourceName: "liked"), for: .normal)
            }
            else
            {
                buttonLikeUnLike.setImage(#imageLiteral(resourceName: "like"), for: .normal)
            }
            
            buttonLikeUnLike.setTitle(" " + UtilityClass.getFormattedNumberString(usingString: String (model.messageLikesCount)), for: .normal)
        }
        
        if model.lastCheckIn != nil, self.locationImage != nil
        {
            self.locationImage.isHidden = false
            self.userLocation.isHidden = false
            
            self.constraintLocationHeight.constant = 16;
            
            self.userLocation.text = model.lastCheckIn?.checkinStatus
            self.userLocation.textColor = color_pink
        }
        else if self.locationImage != nil
        {
            self.locationImage.isHidden = true
            self.userLocation.isHidden = true
            self.constraintLocationHeight.constant = 0;
        }
        if model.isGhostAccount
        {
            if self.locationImage != nil {
                self.locationImage.isHidden = true
                self.userLocation.isHidden = true
                self.constraintLocationHeight.constant = 0;
            }
        }
        
        // If Media view
        if model.isImageMedia || model.isVideoMedia {
            
            playIcon.isHidden = model.isImageMedia
            
            // If data is in directory...
            if let data = UtilityClass.getFileFromFolder(folderPath, fileName: model.message)
            {
                // Use it
                if model.message.contains("jpg") {
                    handleDownloadState(usingData: data, isImage: true, thumbnail: nil)
                }
                else {
                    if let thumb = UtilityClass.getThumbnailFromVideo((UtilityClass.getDocumentsFolder(withName: folderPath)?.appendingPathComponent(model.message))!)
                    {
                        handleDownloadState(usingData: data, isImage: false, thumbnail: thumb)
                    }
                }
                return
            }
            
            
            switch model.mediaDownloadStatus {
            case .isDownloading:
                handleIsDownloadingState(usingModel: model, cellIndex: cellIndex, folderPath: folderPath)
                break
            default:
                handleNoStateCondition(usingModel: model, cellIndex: cellIndex, folderPath: folderPath)
                break
            }
        }
    }
    
    //MARK: - Like button Action
    @IBAction func onButtonLikeUnLikeAction(_ sender: UIButton) {
        guard let delegate = self.delegate  else {
            return
        }
        
        delegate.chatCell(self, didTapOnLikeUnLikeButton: sender)
    }
    
    //MARK: - Media button Action
    @IBAction func onButtonMediaAction(_ sender: UIButton) {
        guard let tempDelegate = self.delegate else {return}
        tempDelegate.chatCell(self, didTapOnMedia: sender)
    }
    
    @IBAction func onButtonDownloadMediaAction(_ sender: UIButton) {
        if buttonDownloadMedia != nil {
            isDownloading = true
            buttonDownloadMedia.isHidden = true
            loaderView.isHidden = false
            
            guard let tempDelegate = self.delegate else {return}
            tempDelegate.chatCellDidDownloadTheMedia(self)
            
//            AmazonTransferManager.defaultTransferManager().downloadData(<#T##dataURL: URL##URL#>, bucketName: <#T##String#>, keyName: <#T##String#>, progressBlock: <#T##AmazonTransferManager.AmazonProgressBlock?##AmazonTransferManager.AmazonProgressBlock?##(AWSS3TransferUtilityTask, Progress) -> ()#>, completionHandler: <#T##AmazonTransferManager.AmazonDownloadCompletionBlock?##AmazonTransferManager.AmazonDownloadCompletionBlock?##(AWSS3TransferUtilityDownloadTask?, Error?) -> ()#>)
//
//            self.buttonMediaView.imageView?.setIndicatorStyle(.white)
//            self.buttonMediaView.imageView?.setShowActivityIndicator(true)
//
//            buttonMediaView.sd_setImage(with: URL (string: baseMediaURL + mediaString), for: .normal, placeholderImage: buttonMediaView.image(for: .normal), options: .highPriority, completed:
//                {[weak self] (image, error, cacheType, url) in
//                    guard let `self` = self else {return}
//                    guard let tempDelegate = self.delegate else {return}
//                    tempDelegate.chatCellDidDownloadTheMedia(self)
//            })
        }
    }
    
}

//MARK: -
//MARK: - extension -> Donwload Status Methods
extension ChatViewCell
{
    func handleDownloadState(usingData data: Data, isImage: Bool, thumbnail: UIImage?)
    {
        self.buttonDownloadMedia.isHidden = true
        self.loaderView.isHidden = true
        self.buttonMediaView.isUserInteractionEnabled = true
        
        var thumb: UIImage!
        
        if isImage {
            thumb = UIImage (data: data)
        }
        else {
//            if let img = UtilityClass
            thumb = thumbnail!
        }
        
        if self.buttonMediaView.image(for: .normal) == nil {
            UIView.transition(with: self.buttonMediaView, duration: 0.25, options: .transitionCrossDissolve, animations:
                {[weak self] in
                    guard let `self` = self else {return}
                    self.buttonMediaView.setImage(thumb, for: .normal)
                    self.buttonMediaView.setBackgroundImage(nil, for: .normal)
            }, completion: nil)
        }
        else {
            self.buttonMediaView.setImage(thumb, for: .normal)
            self.buttonMediaView.setBackgroundImage(nil, for: .normal)
        }
    }
    
    func handleIsDownloadingState(usingModel model : ModelChatMessage, cellIndex : Int, folderPath: String)
    {
        
        self.buttonDownloadMedia.isHidden = true
        self.loaderView.isHidden = false
        
        self.buttonMediaView.imageView?.setIndicatorStyle(.white)
        self.buttonMediaView.imageView?.setShowActivityIndicator(true)
        self.buttonMediaView.isUserInteractionEnabled = false
        self.buttonMediaView.setImage(nil, for: .normal)
        self.buttonMediaView.sd_setBackgroundImage(with: URL (string: baseMediaThumbURL + model.thumbString), for: .normal, placeholderImage: UIImage (), options: .retryFailed)
//        self.buttonMediaView.sd_setBackgroundImage(with: URL (string: baseMediaThumbURL + model.thumbString), for: .normal, completed: nil)
    }
    
    func handleNoStateCondition(usingModel model : ModelChatMessage, cellIndex : Int, folderPath: String)
    {
        self.buttonDownloadMedia.isHidden = false
        self.buttonDownloadMedia.setImage(#imageLiteral(resourceName: "mediaDownloadIcon"), for: .normal)
        self.loaderView.isHidden = true
        
        self.buttonMediaView.imageView?.setIndicatorStyle(.white)
        self.buttonMediaView.imageView?.setShowActivityIndicator(true)
        self.buttonMediaView.isUserInteractionEnabled = false
        self.buttonMediaView.setImage(nil, for: .normal)
        
        self.buttonMediaView.sd_setBackgroundImage(with: URL (string: baseMediaThumbURL + model.thumbString), for: .normal, placeholderImage: UIImage (), options: .retryFailed)
//        self.buttonMediaView.sd_setBackgroundImage(with: URL (string: baseMediaThumbURL + model.thumbString), for: .normal)
    }
}

//MARK: -
//MARK: - extension -> UICollectionViewDataSource
extension ChatViewCell: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize (width: 80, height: 80)
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return likesArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        var cell : ChatViewCollectionLikeCell!
        
        if let cellOther : ChatViewCollectionLikeCell = collectionView.dequeueReusableCell(withReuseIdentifier: "likeCollection", for: indexPath) as? ChatViewCollectionLikeCell
        {
            cell = cellOther
        }
//        else
//        {
//            if let cellOther : UICollectionViewCell = Bundle.main.loadNibNamed("ChatViewCell", owner: self, options: nil)?[10] as? UICollectionViewCell
//            {
//                cell = cellOther
//            }
//        }
        
        cell.backgroundColor = .clear
        
        cell.username.textColor = .white
        cell.username.text = likesArray[indexPath.item].username
        
        cell.imageViewButton.sd_setImage(with: URL (string: likesArray[indexPath.row].picture)!, for: .normal, completed: nil)
        
        cell.imageViewButton.imageView?.makeRoundWithoutScale()
        
        cell.imageViewButton.isUserInteractionEnabled = false
        
        cell.imageViewButton.layer.cornerRadius = cell.imageViewButton.frame.size.width/2
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        collectionView.deselectItem(at: indexPath, animated: true)
        
        guard let tempDelegate = delegate else { return }
        tempDelegate.chatCell(self, didTapOnLikeUser: likesArray[indexPath.item])
    }
}
