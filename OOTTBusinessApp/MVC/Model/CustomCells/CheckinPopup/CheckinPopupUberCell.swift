//
//  CheckinPopupUberCell.swift
//  OOTTBusinessApp
//
//  Created by Santosh on 29/06/18.
//  Copyright © 2018 KonstantInfoSolutions. All rights reserved.
//

import UIKit
import UberRides
import LyftSDK
import CoreLocation

let kCellIdentifier_CheckinPopupUberCell = "checkinPopupUberCell"

protocol CheckinPopupUberCellDelegate: class {
    func checkinUberCell(_ cell: CheckinPopupUberCell, didTapOnUberButton uberButton: UIButton)
    func checkinUberCell(_ cell: CheckinPopupUberCell, didTapOnLyftButton lyftButton: UIButton)
}

class CheckinPopupUberCell: UITableViewCell {
    
    weak var delegate: CheckinPopupUberCellDelegate?
    
    lazy var uberRideBuilder: RideParametersBuilder = {
        return RideParametersBuilder.init()
    }()
    
    lazy var uberRideClient: RidesClient = {
        return RidesClient.init()
    }()
    
    @IBOutlet weak var buttonContainer: UIStackView!
    
    var uberButton = RideRequestButton ()
    var lyftButton = LyftButton ()
    
    @IBAction func onButtonUberAction(_ sender: UIButton) {
        delegate?.checkinUberCell(self, didTapOnUberButton: sender)
    }
    @IBAction func onButtonLyftAction(_ sender: UIButton) {
        delegate?.checkinUberCell(self, didTapOnLyftButton: sender)
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        for button in buttonContainer.arrangedSubviews {
            buttonContainer.removeArrangedSubview(button)
        }
        
        buttonContainer.addArrangedSubview(uberButton)
        buttonContainer.addArrangedSubview(lyftButton)
        
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    func updateCell(pickupLocation: CLLocation, dropoffLocation: CLLocation) {
        uberRideBuilder.pickupLocation = pickupLocation
        uberRideBuilder.dropoffLocation = dropoffLocation
        
        
                let pickup1 = CLLocationCoordinate2D(latitude: 37.7833, longitude: -122.4167)
                let destination1 = CLLocationCoordinate2D(latitude: 37.7794703, longitude: -122.4233223)
          lyftButton.configure(rideKind: LyftSDK.RideKind.Standard ,pickup: pickup1, destination: destination1)
          lyftButton.style = .mulberryDark
        
        
//        lyftButton.configure(rideKind: LyftSDK.RideKind.Standard ,pickup: CLLocationCoordinate2DMake(pickupLocation.coordinate.latitude, pickupLocation.coordinate.longitude), destination: CLLocationCoordinate2DMake(dropoffLocation.coordinate.latitude, dropoffLocation.coordinate.longitude))
//        lyftButton.style = .mulberryDark
        
        
        self.uberButton.rideParameters = uberRideBuilder.build()
        self.uberButton.loadRideInformation()
        var productID = ""
        
        uberRideClient.fetchProducts(pickupLocation: uberRideBuilder.pickupLocation!) {[weak self] (product, response) in
            
            guard let `self` = self else {return}
            
            if !product.isEmpty {
                productID = product[0].productID ?? ""
            }
            
            print(ProductGroup.uberX)
            for productVal in product
            {
                if productVal.productGroup == ProductGroup.uberX
                {
                    //  builder.productID = uberX.productID
                    print(productVal.name)
                    if productVal.name == "UberX"
                    {
                        self.uberRideBuilder.productID = productVal.productID
                        self.uberButton.rideParameters = self.uberRideBuilder.build()
                        self.uberButton.loadRideInformation()
                    }
                }
            }
        }
    }
}
