//
//  EmployeeListCell.swift
//  OOTTBusinessApp
//
//  Created by Santosh on 16/06/17.
//  Copyright © 2017 KonstantInfoSolutions. All rights reserved.
//

import UIKit

let kCellIdentifier_employeeListing = "employeeListCell"

class EmployeeListCell: UITableViewCell {

    
    @IBOutlet weak var imageEmployee: UIImageView!
    @IBOutlet weak var labelEmployeeName: UILabel!
    @IBOutlet weak var labelEmployeeEmail: UILabel!
    @IBOutlet weak var labelEmployeeRole: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.initialSetupOfView()
        self.disbaleSelection()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    func initialSetupOfView() -> Void
    {
        self.imageEmployee.layer.cornerRadius = (self.imageEmployee.frame.size.width/2) * scaleFactorX
        self.imageEmployee.clipsToBounds = true
        
        self.labelEmployeeName.makeAdaptiveFont()
        self.labelEmployeeRole.makeAdaptiveFont()
        self.labelEmployeeEmail.makeAdaptiveFont()
    }
    
}
