//
//  LoginInputCell.swift
//  OOTTUserApp
//
//  Created by Santosh on 06/06/17.
//  Copyright © 2017 KonstantInfoSolutions. All rights reserved.
//

import UIKit
import CoreTelephony

let kCellIdentifier_login_input = "loginInputCell"

let kCellIdentifier_login_button = "loginButtonCell"

let kCellIdentifier_login_phone_cell = "loginPhoneCell"

let kCellIdentifier_login_userType_cell = "loginuserTypeCell"



//MARK:- CellType Enum Type Declaration
enum CellType : Int {
    case email
    case pass
    case fullName
    case username
    case phone
    case dob
    case userType
    case employeeName
    case employeePosition
    case confirmPassword
    case none
}

//MARK:- LoginCellDelegate Method Declaration
@objc protocol LoginCellDelegate
{
    @objc optional func loginCell(cell:LoginInputCell, didTapOnForgotPasswordButton:UIButton) -> Void
    
    @objc optional func loginCell(cell:LoginInputCell, didTapOnRememberMeButton:UIButton) -> Void
    
    @objc optional func loginCell(cell:LoginInputCell, didTapOnLoginButton:UIButton) -> Void
    
    @objc optional func loginCell(cell:LoginInputCell, didTapOnFacebookButton:UIButton) -> Void
    
    @objc optional func loginCell(cell:LoginInputCell,  didTapOnTermsAndConditionButton:UILabel, urlString:String) -> Void
    
    @objc optional func loginCell(cell:LoginInputCell, didTapOnCreateAccountButton:UIButton) -> Void
    
    @objc optional func loginCell(cell:LoginInputCell, updatedInputfieldText:String) -> Void

    @objc optional func loginCell(cell:LoginInputCell, didTapOnCreateEmployeeButton:String) -> Void
    
}

class LoginInputCell: UITableViewCell, UITextFieldDelegate, CountryCodeDelegate {

    //MARK:- Divider line
    @IBOutlet weak var bottomDividerLine: UIView!
    
    @IBOutlet weak var bottomDividerLine2: UIView!
    
    //MARK:- Input Textfield
    @IBOutlet weak var inputTextField: UITextField!
    
    @IBOutlet var labelPrivacyPolicy: UILabel!
    
    @IBOutlet weak var buttonTermsAndCondition: UIButton!
    
    @IBOutlet weak var viewCountryCode: UIView!
    
    @IBOutlet weak var labelCountryCode: UILabel!
    
    @IBOutlet weak var viewUserTypeArea1: UIView!
    @IBOutlet weak var viewUserTypeArea2: UIView!
    
    @IBOutlet weak var imageRadioBusinessUser: UIImageView!
    @IBOutlet weak var imageRadioStandardUser: UIImageView!
    
    
    @IBOutlet weak var buttonRememberMe: UIButton!
    
    //MARK:- Icon image in field
    @IBOutlet weak var imageIcon: UIImageView!
    
    var model:ModelLogin = ModelLogin(withKey: LoginEnum.none.rawValue, value: "")

    
    //MARK:- CellType Enum Instance
    var cellType = CellType.none
    
    //MARK:- LoginCellDelegate Instance
    weak var loginDelegate : LoginCellDelegate?
    
    //MARK:- Default font size
    let fontSize : CGFloat = 17 * scaleFactorX
    
    deinit {
        print("LoginInputCell deinit")
    }
    
    //MARK:- ForgotPassword Button Action
    @IBAction func onForgotPasswordAction(_ sender: UIButton) {
        guard let delegate = loginDelegate else {
            return
        }
        delegate.loginCell!(cell: self, didTapOnForgotPasswordButton: sender)
    }
    
    //MARK:- Remember me Button Action
    @IBAction func onRememberMeAction(_ sender: UIButton) {
        guard let delegate = loginDelegate else {
            return
        }
        delegate.loginCell!(cell: self, didTapOnRememberMeButton: sender)
    }
    
    //MARK:- Login Button Action
    @IBAction func onLoginAction(_ sender: UIButton) {
        guard let delegate = loginDelegate else {
            return
        }
        delegate.loginCell!(cell: self, didTapOnLoginButton: sender)
    }
    
    //MARK:- Facebook Button Action
    @IBAction func onFacebookAction(_ sender: UIButton) {
        guard let delegate = loginDelegate else {
            return
        }
        delegate.loginCell!(cell: self, didTapOnFacebookButton: sender)
    }
    
//    @IBAction func onTermsAndCondtitionsAction(_ sender: UIButton) {
//        guard let delegate = loginDelegate else {
//            return
//        }
//        delegate.loginCell!(cell: self, didTapOnTermsAndConditionButton: sender)
//    }
    
    @IBAction func onCreateAccountAction(_ sender: UIButton) {
        guard let delegate = loginDelegate else {
            return
        }
        delegate.loginCell!(cell: self, didTapOnCreateAccountButton: sender)
    }
    
    //MARK:- Initial Cell Setup
    func initialSetupOfCell() -> Void
    {
        if self.inputTextField != nil
        {
            self.inputTextField.font = UIFont(name: FONT_PROXIMA_LIGHT, size: fontSize)
            self.inputTextField.setPlaceholderColor(color_placeholderColor)
            self.inputTextField.text=""
            
            self.bottomDividerLine.backgroundColor = color_dividerLine
            
            if self.bottomDividerLine2 != nil
            {
                self.bottomDividerLine2.backgroundColor = color_dividerLine
                
//                let network_Info = CTTelephonyNetworkInfo()
//                let carrier = network_Info.subscriberCellularProvider
                
                self.labelCountryCode.text = "+1"
                self.labelCountryCode.makeAdaptiveFont()
                addTapGestureOnCountryCode()
            }
        }
        else
        {
            if self.labelPrivacyPolicy != nil
            {
                self.labelPrivacyPolicy?.adjustsFontSizeToFitWidth = true
                self.labelPrivacyPolicy?.numberOfLines = 0


                self.labelPrivacyPolicy.addTapGesture(1, selector: #selector(tapLabel(gesture:)), target: self)

                
            }
            else if self.imageRadioStandardUser != nil
            {
                imageRadioStandardUser.image = #imageLiteral(resourceName: "radioSelect")
                imageRadioBusinessUser.image = #imageLiteral(resourceName: "radioDeselected")
                
                addTapGestureOnUserTypeAreas()
            }
        }
    }
    
     @objc func tapLabel(gesture: UITapGestureRecognizer) {
        let text = (labelPrivacyPolicy.text)!
        let termsRange = (text as NSString).range(of: "Terms & Conditions")
        let privacyRange = (text as NSString).range(of: "Privacy Policy")
        
        if gesture.didTapAttributedTextInLabel(label: labelPrivacyPolicy, inRange: termsRange) {
            
            
            
            guard let delegate = loginDelegate else {
                return
            }
            
            delegate.loginCell!(cell:self,  didTapOnTermsAndConditionButton:labelPrivacyPolicy, urlString:LinksEnum.termsAndCond.path)
            
            
        } else if gesture.didTapAttributedTextInLabel(label: labelPrivacyPolicy, inRange: privacyRange) {

            guard let delegate = loginDelegate else {
                return
            }
            delegate.loginCell!(cell:self,  didTapOnTermsAndConditionButton:labelPrivacyPolicy, urlString:LinksEnum.privacyPolicy.path)

        } else {
            print("Tapped none")
        }
    }
    
    //MARK:- awakeFromNib
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        self.cellType = .none
       
        initialSetupOfCell()
        self.disbaleSelection()
    }
    
    //MARK:- layoutSubviews (Default values set)
    override func layoutSubviews()
    {
        switch self.cellType
        {
        case .email:
            self.inputTextField.placeholder = "Email Address"
            self.inputTextField.keyboardType = .emailAddress
            self.inputTextField.isSecureTextEntry = false
            self.inputTextField.isUserInteractionEnabled = true
            if (self.imageIcon != nil)
            {
                self.imageIcon.image = nil
            }
            self.inputTextField.delegate = self
            self.inputTextField.tag = CellType.email.rawValue
            
            model.keyValue = self.inputTextField.text!
            model.keyName = LoginEnum.email.rawValue
            
        case .pass:
            self.inputTextField.placeholder = "Password"
            self.inputTextField.keyboardType = .default
            self.inputTextField.isSecureTextEntry = true
            self.inputTextField.isUserInteractionEnabled = true
            if (self.imageIcon != nil)
            {
                self.imageIcon.image = nil
            }
            self.inputTextField.tag = CellType.pass.rawValue
            self.inputTextField.delegate = self
            model.keyValue = self.inputTextField.text!
            model.keyName = LoginEnum.password.rawValue
            
            
        case .fullName:
            self.inputTextField.placeholder = "Full Name or Business Name"
            self.inputTextField.keyboardType = .default
            self.inputTextField.isSecureTextEntry = false
            self.inputTextField.isUserInteractionEnabled = true
            if (self.imageIcon != nil)
            {
                self.imageIcon.image = nil
            }
            self.inputTextField.delegate = self
            self.inputTextField.tag = CellType.fullName.rawValue
            model.keyValue = self.inputTextField.text!
            model.keyName = LoginEnum.fullname.rawValue
            
        case .username:
            self.inputTextField.placeholder = "User Name"
            self.inputTextField.keyboardType = .asciiCapable
            self.inputTextField.isSecureTextEntry = false
            self.inputTextField.isUserInteractionEnabled = true
            if (self.imageIcon != nil)
            {
                if model.validUsername == .empty
                {
                    self.imageIcon.image = nil
                }
                else if model.validUsername == .valid
                {
                    self.imageIcon.image = #imageLiteral(resourceName: "availableIcon")
                }
                else
                {
                    self.imageIcon.image = #imageLiteral(resourceName: "notAvailableIcon")
                }
            }
            self.inputTextField.delegate = self
            self.inputTextField.tag = CellType.username.rawValue
            model.keyValue = self.inputTextField.text!
            model.keyName = LoginEnum.username.rawValue
            
        case .phone:
            self.inputTextField.placeholder = "Phone"
            self.inputTextField.keyboardType = .asciiCapableNumberPad
            self.inputTextField.isSecureTextEntry = false
            self.inputTextField.isUserInteractionEnabled = true
            if (self.imageIcon != nil)
            {
                self.imageIcon.image = nil
            }
            self.inputTextField.delegate = self
            self.inputTextField.tag = CellType.phone.rawValue
            model.keyValue = self.labelCountryCode.text!.appending("-"+self.inputTextField.text!)
            model.keyName = LoginEnum.mobileNo.rawValue
            
//            model.keyValue = self.labelCountryCode.text!
//            model.keyName = LoginEnum.countryCode.rawValue
            
//            guard let delegate = loginDelegate else {
//                return
//            }
//            delegate.loginCell!(cell: self, updatedInputfieldText: model.keyValue)
            
        case .dob:
            self.inputTextField.placeholder = "DOB"
            self.inputTextField.keyboardType = .phonePad
//            self.inputTextField.isUserInteractionEnabled = false
            if (self.imageIcon != nil)
            {
                self.imageIcon.image = #imageLiteral(resourceName: "dobIcon")
            }
            self.inputTextField.isSecureTextEntry = false
            self.inputTextField.delegate = self
            self.inputTextField.tag = CellType.dob.rawValue
            model.keyValue = self.inputTextField.text!
            model.keyName = LoginEnum.dob.rawValue
            
        case .userType:
            model.keyName = LoginEnum.userType.rawValue
            
        case .confirmPassword:
            self.inputTextField.placeholder = "Confirm Password"
            self.inputTextField.keyboardType = .default
            self.inputTextField.isSecureTextEntry = true
            self.inputTextField.isUserInteractionEnabled = true
            if (self.imageIcon != nil)
            {
                self.imageIcon.image = nil
            }
            self.inputTextField.delegate = self
            self.inputTextField.tag = CellType.confirmPassword.rawValue
            model.keyValue = self.inputTextField.text!
            model.keyName = LoginEnum.confirmPassword.rawValue
            
        case .employeeName:
            self.inputTextField.placeholder = "Employee Name"
            self.inputTextField.keyboardType = .default
            self.inputTextField.isSecureTextEntry = false
            self.inputTextField.isUserInteractionEnabled = true
            if (self.imageIcon != nil)
            {
                self.imageIcon.image = nil
            }
            self.inputTextField.delegate = self
            self.inputTextField.tag = CellType.employeeName.rawValue
            model.keyValue = self.inputTextField.text!
            model.keyName = LoginEnum.fullname.rawValue
            
        case .employeePosition:
            self.inputTextField.placeholder = "Position"
            self.inputTextField.keyboardType = .asciiCapable
            self.inputTextField.isSecureTextEntry = false
            self.inputTextField.isUserInteractionEnabled = true
            if (self.imageIcon != nil)
            {
                self.imageIcon.image = nil
            }
            self.inputTextField.delegate = self
            self.inputTextField.tag = CellType.employeePosition.rawValue
            model.keyValue = self.inputTextField.text!
            model.keyName = LoginEnum.employeeRole.rawValue

        default: break
            
        }
    }
    
    //MARK:- UITextField Delegate
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool
    {
//        if textField.isAskingCanBecomeFirstResponder
//        {
//            return true
//        }
        if textField.tag == CellType.dob.rawValue
        {
            textField.inputView = UtilityClass.getDOBPickerView()
            let datePicker = textField.inputView! as! UIDatePicker
            
            if (textField.text?.isEmpty)!
            {
                textField.text = datePicker.date.getStringForFormat()
                model.keyValue = self.inputTextField.text!
                
                guard let delegate = loginDelegate else {
                    return true
                }
                delegate.loginCell!(cell: self, updatedInputfieldText: model.keyValue)
            }
            else
            {
                datePicker.date = Date.getDate(fromString: model.keyValue)
            }
            
            datePicker.addTarget(self, action: #selector(onDatePickerValueChanged(sender:)), for: .valueChanged)
        }
//        else if textField.tag == CellType.employeePosition.rawValue
//        {
//            if model.keyValue.isEmpty
//            {
//                guard let delegate = loginDelegate else {
//                    return true
//                }
//                delegate.loginCell!(cell: self, updatedInputfieldText: model.keyValue)
//            }
//        }
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        // Username field, space not allowed
        if textField.tag == CellType.username.rawValue && string == " "
        {
            return false
        }
        
        var finalCount = 0
        var newString : String?
        
        if string.count == 0
        {
            if textField.text?.count==0
            {
                finalCount = 0
                newString = textField.text
            }
            else
            {
                finalCount = textField.text!.count - 1
                var newStr = textField.text! as NSString
                newStr = newStr.replacingCharacters(in: range, with: string) as NSString
                newString = newStr as String
            }
        }
        else
        {
            finalCount = textField.text!.count + 1
            var newStr = textField.text! as NSString
            
            newStr = newStr.replacingCharacters(in: range, with: string) as NSString
            
            newString = newStr as String
        }
        
        if textField.tag == CellType.phone.rawValue
        {
            if finalCount > 10
            {
                return false
            }
        }
        
        if textField.tag == CellType.username.rawValue
        {
            // check username
            if !newString!.isEmpty
            {
                checkUniqueUserName(withString: newString!, forCell: self)
            }
            else
            {
                // some error, so not availabel
                self.imageIcon.image = nil
                model.validUsername = .empty
                
                model.keyValue = newString!
                
                guard let delegate = loginDelegate else {
                    return true
                }
                delegate.loginCell!(cell: self, updatedInputfieldText: model.keyValue)
            }
        }
        else
        {
            if cellType == .phone
            {
                model.keyValue = self.labelCountryCode.text!.appending("-"+newString!)
            }
            else
            {
                model.keyValue = newString!
            }
            print("*****************" + model.keyValue)
            
            guard let delegate = loginDelegate else {
                return true
            }
            delegate.loginCell!(cell: self, updatedInputfieldText: model.keyValue)
        }
        
        return true
    }
    
    //MARK:- DOB Value Update
    func onDatePickerValueChanged(sender : UIDatePicker) -> Void {
        self.inputTextField.text = sender.date.getStringForFormat()
        model.keyValue = self.inputTextField.text!
        
        guard let delegate = loginDelegate else {
            return
        }
        delegate.loginCell!(cell: self, updatedInputfieldText: model.keyValue)
    }
    
    //MARK:- Tap Gesture On Country Code view
    func addTapGestureOnCountryCode() -> Void
    {
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(onTapGesture(tapGesture:)))
        tapGesture.numberOfTapsRequired = 1
        self.viewCountryCode.addGestureRecognizer(tapGesture)
    }
    
    //MARK:- Tap Gesture On User Type Area
    func addTapGestureOnUserTypeAreas() -> Void
    {
        let tapGesture1 = UITapGestureRecognizer(target: self, action: #selector(onTapGesture(tapGesture:)))
        tapGesture1.numberOfTapsRequired = 1
        self.viewUserTypeArea1.addGestureRecognizer(tapGesture1)
        
        let tapGesture2 = UITapGestureRecognizer(target: self, action: #selector(onTapGesture(tapGesture:)))
        tapGesture2.numberOfTapsRequired = 1
        self.viewUserTypeArea2.addGestureRecognizer(tapGesture2)
        
        self.viewUserTypeArea2.tag = 1002
        self.viewUserTypeArea1.tag = 1001
    }
    
    //MARK:- Tap Gesture Action
    func onTapGesture(tapGesture : UITapGestureRecognizer) -> Void
    {
        self.endEditing(true)
        let index = tapGesture.view?.tag
        
        switch tapGesture.state {
        case .ended:
            
            if index == 1001
            {
                imageRadioStandardUser.image = #imageLiteral(resourceName: "radioSelect")
                imageRadioBusinessUser.image = #imageLiteral(resourceName: "radioDeselected")
                
                model.keyValue = AppUserTypeEnum.user.rawValue
                print("*****************" + model.keyValue)
                
                guard let delegate = loginDelegate else {
                    return
                }
                delegate.loginCell!(cell: self, updatedInputfieldText: model.keyValue)
                
                return
            }
            else if index == 1002
            {
                imageRadioBusinessUser.image = #imageLiteral(resourceName: "radioSelect")
                imageRadioStandardUser.image = #imageLiteral(resourceName: "radioDeselected")
                
                model.keyValue = AppUserTypeEnum.provider.rawValue
                print("*****************" + model.keyValue)
                
                guard let delegate = loginDelegate else {
                    return
                }
                delegate.loginCell!(cell: self, updatedInputfieldText: model.keyValue)
                
                return
            }
            
            let picker = UIStoryboard.getCountryCodeStoryboard().instantiateInitialViewController() as! CountryCodeViewController
            picker.countryDelegate = self
            
            let navigationController = appDelegate?.window??.rootViewController as! UINavigationController
            navigationController.pushViewController(picker, animated: true)
            
        default:
            break
        }
    }
    
    //MARK:- CountryCodeViewController Delegate -> Country code selected
    func countryCodeDidSelectCountry(withCountryName countryName: String, andCountryDialCode dialCode: String) {
        self.labelCountryCode.text = dialCode
        
        model.keyValue = dialCode.appending("-"+self.inputTextField.text!)
        print("*****************" + model.keyValue)
        
        guard let delegate = loginDelegate else {
            return
        }
        delegate.loginCell!(cell: self, updatedInputfieldText: model.keyValue)
    }
    
    //MARK:- Updating Input Fields Using Model
    func updateValue(usingModel: ModelLogin) -> Void
    {
        print(usingModel.keyName)
        
        if inputTextField != nil
        {
            inputTextField.text = usingModel.keyValue
            if usingModel.keyName == LoginEnum.mobileNo.rawValue
            {
                inputTextField.text = usingModel.keyValue.components(separatedBy: "-").last!
            }
            return
        }
        
        if cellType == .userType
        {
            if usingModel.keyValue == AppUserTypeEnum.user.rawValue
            {
                imageRadioStandardUser.image = #imageLiteral(resourceName: "radioSelect")
                imageRadioBusinessUser.image = #imageLiteral(resourceName: "radioDeselected")
            }
            else
            {
                imageRadioBusinessUser.image = #imageLiteral(resourceName: "radioSelect")
                imageRadioStandardUser.image = #imageLiteral(resourceName: "radioDeselected")
            }
        }
    }
    
    //MARK:-
    func checkUniqueUserName(withString uniqueString:String, forCell cell:LoginInputCell) -> Void
    {
        AppWebHandler.sharedInstance().cancelTask(forEndpoint: "checkUser")
        
        let params = ["username" : uniqueString]
        
        let urlToHit = EndPoints.checkUser.path
        
        AppWebHandler.sharedInstance().fetchData(fromURL: urlToHit, httpMethod: .post, parameters: params)
        {[weak self] (data, dictionary, statusCode, error) in
            
            guard let `self` = self else {return}
            
            if statusCode == 200
            {
                let responseDictionary = dictionary!
                
                if let type = responseDictionary["type"] as? Bool
                {
                    if type
                    {
                        self.imageIcon.image = #imageLiteral(resourceName: "availableIcon")
                        cell.model.validUsername = .valid
                    }
                    else
                    {
                        // not availabel
                        self.imageIcon.image = #imageLiteral(resourceName: "notAvailableIcon")
                        cell.model.validUsername = .inValid
                    }
                }
                else
                {
                    // not availabel
                    self.imageIcon.image = #imageLiteral(resourceName: "notAvailableIcon")
                    cell.model.validUsername = .inValid
                }
            }
            else
            {
                // some error, so not availabel
                self.imageIcon.image = #imageLiteral(resourceName: "notAvailableIcon")
                cell.model.validUsername = .inValid
            }
            
            
            cell.model.keyValue = uniqueString
            
            guard let delegate = self.loginDelegate else {
                return
            }
            delegate.loginCell!(cell: cell, updatedInputfieldText: cell.model.keyValue)
        }
    }
}


