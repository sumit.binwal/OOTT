//
//  CreateProfileCollectionVwCell.swift
//  OOTTBusinessApp
//
//  Created by Sumit Sharma on 16/06/2017.
//  Copyright © 2017 KonstantInfoSolutions. All rights reserved.
//

import UIKit
let kCellIdentifier_add_image = "cellAddButton"
let kCellIdentifier_uploaded_image = "cellUploadedImage"
class CreateProfileCollectionVwCell: UICollectionViewCell
{
    //MARK : Collection View Cell Title Label
    @IBOutlet var labelCellTitle: UILabel!
    
    //MARK : Collection View Cell Image Button
    @IBOutlet var buttonMenuImages: UIButton!
    
    @IBOutlet weak var loader: UIActivityIndicatorView!
    
    @IBOutlet weak var buttonDeleteImage: UIButton!
    
    var isUploadInProgress = false
    var isImageUploadFailed = false
    
    //MARK:- CreateProfileCollectionVwCell
    deinit {
        print("CreateProfileCell deinit")
    }
    
    override func awakeFromNib()
    {
        super.awakeFromNib()

        labelCellTitle.makeAdaptiveFont()
        buttonMenuImages.layer.cornerRadius=5.0 * scaleFactorX
        buttonMenuImages.clipsToBounds=true
        if loader != nil
        {
            loader.isHidden = true
        }
    }
    
    func imageUploadingProperties() -> Void
    {
        loader.isHidden = false
        loader.stopAnimating()
        loader.startAnimating()
        
//        buttonMenuImages.setBackgroundImage(nil, for: .normal)
//        buttonMenuImages.setImage(nil, for: .normal)
        
        buttonDeleteImage.isHidden = true
        
        isUploadInProgress = true
    }
    
    func imageUploadSuccessProperties() -> Void
    {
        loader.isHidden = true
        loader.stopAnimating()
        isUploadInProgress = false
        buttonDeleteImage.isHidden = false
    }
    
    func imageUploadFailProperties() -> Void
    {
        loader.isHidden = true
        loader.stopAnimating()
        isUploadInProgress = false
        buttonDeleteImage.isHidden = true
        // also retry button appears
    }
}
