//
//  EditProfileCell.swift
//  OOTTBusinessApp
//
//  Created by Sumit Sharma on 06/09/2017.
//  Copyright © 2017 KonstantInfoSolutions. All rights reserved.
//

import UIKit

let kCellIdentifier_edit_profile_userDetail = "editProfileImageCell"
let kCellIdentifier_edit_profile_Input = "editProfileInputCell"
let kCellIdentifier_edit_profile_Description = "editProfileDetailCell"

//MARK:- CellType Enum Type Declaration
enum EditProfileCellType : Int {
    
    case userDetail
    case name
    case dob
    case gender
    case description
    case none
}

protocol EditProfileCellDelegate : class
{
    func editProfileCell(_ cell : EditProfileCell, didUpdatedWithValue updatedValue: String)
    func editProfileCell(_ cell : EditProfileCell, didTapOnProfileImageButton profileImageButton: UIButton)
}

class EditProfileCell: UITableViewCell
{
    @IBOutlet var buttonProfileImage: UIButton!
    @IBOutlet var labelUserName: UILabel!
    @IBOutlet var labelUserEmail: UILabel!
    
    @IBOutlet weak var inputField: UITextField!
    
    @IBOutlet weak var descriptionField: KMPlaceholderTextView!
    
    var cellType = EditProfileCellType.none
    
    weak var editProfileDelegate : EditProfileCellDelegate?
    
    override func awakeFromNib() {
        
        cellType = EditProfileCellType.none
        
        if descriptionField != nil
        {
            descriptionField.layer.borderColor = UIColor (RED: 57, GREEN: 73, BLUE: 89, ALPHA: 1).cgColor
            descriptionField.layer.borderWidth = 1
            descriptionField.layer.cornerRadius = 5
        }
        if buttonProfileImage != nil
        {
            buttonProfileImage.layer.cornerRadius = buttonProfileImage.frame.size.width/2 * scaleFactorX
            buttonProfileImage.layer.borderColor = color_cellImageBorder.cgColor
            buttonProfileImage.layer.borderWidth = CGFloat(1)
        }
        
        if inputField != nil
        {
            inputField.setPlaceholderColor(UIColor.white)
        }
        
        self.disbaleSelection()

    }
    
    
    //MARK:- layoutSubviews
    override func layoutSubviews()
    {
        if inputField != nil
        {
            inputField.setPlaceholderColor(UIColor.white)
        }
        switch self.cellType
        {
            
        case .userDetail:
            break
        case .name:
            self.inputField.keyboardType = .asciiCapable
            self.inputField.isUserInteractionEnabled = true
            self.inputField.tag = EditProfileCellType.userDetail.rawValue
            self.inputField.placeholder = "Full Name"
            
        case .dob:
            self.inputField.keyboardType = .default
            self.inputField.isUserInteractionEnabled = true
            self.inputField.tag = EditProfileCellType.dob.rawValue
            self.inputField.inputView = UtilityClass.getDOBPickerView()
            self.inputField.placeholder = "DOB"
            
        case .gender:
            self.inputField.keyboardType = .default
            self.inputField.isUserInteractionEnabled = true
            self.inputField.tag = EditProfileCellType.gender.rawValue
            self.inputField.inputView = UtilityClass.getNormalPickerView()
            self.inputField.placeholder = "Gender"
            
        case .description:
            break
            
        default:
            break
            
        }
    }
    
    func updateCell(usingModel model:EditProfileModel, forIndex index:Int)
    {
        if buttonProfileImage != nil
        {
            buttonProfileImage.imageView?.setIndicatorStyle(.white)
            buttonProfileImage.imageView?.setShowActivityIndicator(true)
            buttonProfileImage.sd_setImage(with: URL (string: model.picture), for: .normal, completed: nil)
            
            labelUserName.text = model.username
            labelUserEmail.text = model.email
        }
        
        if descriptionField != nil
        {
            descriptionField.text = model.description
            descriptionField.delegate = self
        }
        
        if cellType == .name
        {
            inputField.text = model.fullname
            inputField.delegate = self
        }
        if cellType == .dob
        {
            inputField.text = model.dob
            inputField.delegate = self
        }
        if cellType == .gender
        {
            inputField.text = model.gender
            inputField.delegate = self
        }
        
        if inputField != nil
        {
            inputField.placeholder = "Full Name"
        }
        if inputField != nil
        {
            inputField.setPlaceholderColor(UIColor.white)
        }
    }
    
    @IBAction func onProfileImageButtonAction(_ sender: UIButton) {
        guard let delegate = editProfileDelegate else
        {
            return
        }
        delegate.editProfileCell(self, didTapOnProfileImageButton: sender)
    }
    
}


extension EditProfileCell : UITextFieldDelegate, UITextViewDelegate
{
    //MARK:- UITextField Delegate
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool
    {
        if textField.tag == EditProfileCellType.dob.rawValue || textField.tag == EditProfileCellType.gender.rawValue
        {
            guard let delegate = editProfileDelegate else
            {
                return false
            }
            delegate.editProfileCell(self, didUpdatedWithValue: textField.text ?? "")
        }
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        var newString : String?
        
        if string.count == 0
        {
            if textField.text?.count==0
            {
                newString = textField.text
            }
            else
            {
                var newStr = textField.text! as NSString
                newStr = newStr.replacingCharacters(in: range, with: string) as NSString
                newString = newStr as String
            }
        }
        else
        {
            var newStr = textField.text! as NSString
            
            newStr = newStr.replacingCharacters(in: range, with: string) as NSString
            
            newString = newStr as String
        }
        
        guard let delegate = editProfileDelegate else {
            return true
        }
        
        delegate.editProfileCell(self, didUpdatedWithValue: newString!)
        
        return true
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        
        var newString : String?
        
        if text.count == 0
        {
            if textView.text?.count==0
            {
                newString = textView.text
            }
            else
            {
                var newStr = textView.text! as NSString
                newStr = newStr.replacingCharacters(in: range, with: text) as NSString
                newString = newStr as String
            }
        }
        else
        {
            var newStr = textView.text! as NSString
            
            newStr = newStr.replacingCharacters(in: range, with: text) as NSString
            
            newString = newStr as String
        }
        
        guard let delegate = editProfileDelegate else {
            return true
        }
        
        delegate.editProfileCell(self, didUpdatedWithValue: newString!)
        
        return true
    }
}

