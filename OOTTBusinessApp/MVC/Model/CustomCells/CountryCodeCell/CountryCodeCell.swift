//
//  CountryCodeCell.swift
//  OOTTBusinessApp
//
//  Created by Santosh on 21/06/17.
//  Copyright © 2017 KonstantInfoSolutions. All rights reserved.
//

import UIKit
let kCellIdentifier_countryCodeListing = "countrycodecell"
class CountryCodeCell: UITableViewCell {
    @IBOutlet weak var labelCountryName: UILabel!
    @IBOutlet weak var labelCountryCode: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.disbaleSelection()
        labelCountryCode.makeAdaptiveFont()
        labelCountryName.makeAdaptiveFont()
        
        self.contentView.viewWithTag(100)?.backgroundColor = color_dividerLine
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
