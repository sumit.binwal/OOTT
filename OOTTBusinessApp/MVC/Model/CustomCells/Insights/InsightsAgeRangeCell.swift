//
//  InsightsAgeRangeCell.swift
//  OOTTBusinessApp
//
//  Created by Santosh on 15/01/18.
//  Copyright © 2018 KonstantInfoSolutions. All rights reserved.
//

import UIKit

let kCellIdentifier_InsightsAgeRangeCell = "insightsAgeRangeCell"

class InsightsAgeRangeCell: UITableViewCell {

    
    @IBOutlet weak var labelAgeRange: UILabel!
    
    @IBOutlet weak var ageBarView: BarView!
 
    @IBOutlet weak var labelAgePercentage: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        backgroundColor = .clear
        ageBarView.backgroundColor = .clear
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    func updateCell(usingModel model: AgeRangeModel) {
        ageBarView.barColor = UIColor.MyApp.genderChartFemalecolor
        ageBarView.barValue = model.agePercentage
        ageBarView.setNeedsDisplay()
        labelAgeRange.text = model.ageRange
        labelAgePercentage.text = String (Int(model.agePercentage)) + "%"
    }
    
}
