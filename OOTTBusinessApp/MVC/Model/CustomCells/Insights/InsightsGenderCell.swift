//
//  InsightsGenderCell.swift
//  OOTTBusinessApp
//
//  Created by Santosh on 15/01/18.
//  Copyright © 2018 KonstantInfoSolutions. All rights reserved.
//

import UIKit

let kCellIdentifier_InsightsGenderCell = "insightsGenderCell"

class InsightsGenderCell: UITableViewCell {

    @IBOutlet weak var genderChartView: PieChart!
    
    @IBOutlet weak var labelMenPercentage: UILabel!
   
    @IBOutlet weak var labelWomenPercentage: UILabel!
   
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        backgroundColor = .clear
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    func updateCell(usingModel model: ModelInisightsView) {
        genderChartView.removeAllItems()
        genderChartView.addChartItem(chartItem: PieChartItem (UIColor.MyApp.genderChartMalecolor, value: model.malePercentage))
        genderChartView.addChartItem(chartItem: PieChartItem (UIColor.MyApp.genderChartFemalecolor, value: model.femalePercentage))
        genderChartView.setNeedsDisplay()
        
        labelMenPercentage.text = String (Int(model.malePercentage)) + "%"
        labelWomenPercentage.text = String (Int(model.femalePercentage)) + "%"
    }
    
}
