//
//  BusinessGroupHeaderCell.swift
//  OOTTBusinessApp
//
//  Created by Santosh on 10/01/18.
//  Copyright © 2018 KonstantInfoSolutions. All rights reserved.
//

import UIKit

let kCellIdentifier_ChangeStatusCell = "changeStatusCell"

class BusinessGroupHeaderCell: UITableViewCell {

    @IBOutlet weak var groupPicture: UIButton!
    @IBOutlet weak var GroupName: UILabel!
    @IBOutlet weak var peopleCount: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var unreadCountContainer: UIView!
    @IBOutlet weak var unreadCountLabel: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        groupPicture.layer.cornerRadius = (groupPicture.frame.size.width/2) * scaleFactorX
        groupPicture.clipsToBounds = true
    }
    
    // MARK:-
    override func layoutSubviews() {
        
        self.unreadCountContainer.layer.cornerRadius = self.unreadCountContainer.frame.size.height/2
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    func updateCell(usingModel model: GroupDetailModel) {
        GroupName.text = model.groupName
        peopleCount.text = model.groupAttendee
        
        if let url = URL (string: model.groupPictureString) {
            groupPicture.imageView?.setIndicatorStyle(.white)
            groupPicture.imageView?.setShowActivityIndicator(true)
            groupPicture.sd_setImage(with: url, for: .normal)
        }
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ" //Your date format\
        dateFormatter.locale = NSLocale(localeIdentifier: "en_US_POSIX") as Locale!
        dateFormatter.timeZone = NSTimeZone(name: "UTC")! as TimeZone
        let date : NSDate = dateFormatter.date(from: model.updatedTime)! as NSDate //according to date format your date string
        
        self.timeLabel.text = UtilityClass.timeAgoSinceDate(date as Date, currentDate:NSDate() as Date , numericDates: false)
        
        if model.unreadCount == 0
        {
            unreadCountContainer.isHidden = true
        }
        else
        {
            unreadCountContainer.isHidden = false
            unreadCountLabel.text = UtilityClass.getFormattedNumberString(usingString: String (model.unreadCount))
        }
    }
    
}
