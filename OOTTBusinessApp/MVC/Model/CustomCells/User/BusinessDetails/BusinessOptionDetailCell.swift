//
//  BusinessOptionDetailCell.swift
//  OOTTBusinessApp
//
//  Created by Bharat Kumar Pathak on 14/07/17.
//  Copyright © 2017 KonstantInfoSolutions. All rights reserved.
//

import UIKit

let kCellIdentifier_business_option_detail_cell = "businessOptionDetailCell"

class BusinessOptionDetailCell: UITableViewCell {

    @IBOutlet weak var iconImageView: UIImageView!
    
    @IBOutlet weak var labeltitle: UILabel!
    @IBOutlet weak var labelDetail: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.disbaleSelection()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func updateCell(usingModel model:ModelGroupDetail, forIndex index:Int)
    {
        switch index {
        case 0:
            labeltitle.text = "Website"
            labelDetail.text = model.groupWebsite
            iconImageView.image = #imageLiteral(resourceName: "website")
        case 1:
            labeltitle.text = "Hours"
            labelDetail.text = model.timeForToday
            iconImageView.image = #imageLiteral(resourceName: "open")
        case 2:
            labeltitle.text = "Cover Charges"
            labelDetail.text = model.coverChargeForToday
            iconImageView.image = #imageLiteral(resourceName: "chargesIcon")
        case 3:
            labeltitle.text = "Dress Code"
            labelDetail.text = model.groupDressCode
            iconImageView.image = #imageLiteral(resourceName: "dressCode")
        case 4:
            labeltitle.text = "Address"
            labelDetail.text = model.groupLocation
            iconImageView.image = #imageLiteral(resourceName: "address")
        case 5:
            labeltitle.text = "Location Size"
            
            var radiusValue = String()
            if model.radius  == "50"
            {
                radiusValue = "Small"
            }
            else if model.radius  == "100"
            {
                radiusValue = "Medium"
            }
            else if model.radius  == "150"
            {
                radiusValue = "Large"
            }
            
            labelDetail.text = radiusValue
            iconImageView.image = #imageLiteral(resourceName: "address")
        case 6:
            labeltitle.text = "Contact"
            labelDetail.text = model.groupCallingNumber
            iconImageView.image = #imageLiteral(resourceName: "contact")
        default:
            break
        }
    }
    
}
