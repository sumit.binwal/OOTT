//
//  BusinessMapCell.swift
//  OOTTBusinessApp
//
//  Created by Bharat Kumar Pathak on 14/07/17.
//  Copyright © 2017 KonstantInfoSolutions. All rights reserved.
//

import UIKit
import GoogleMaps

let kCellIdentifier_business_map_cell = "businessMapCell"

class BusinessMapCell: UITableViewCell {

    
    @IBOutlet weak var mapView: GMSMapView!
    
    var isMapDisplayed = false
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.disbaleSelection()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func updateCell(usingModel model : ModelGroupDetail, shouldReloadMap: Bool)
    {
        if !shouldReloadMap
        {
            return
        }
        
        let locationCoordinate : CLLocationCoordinate2D = CLLocationCoordinate2D (latitude: Double(model.groupLatitude)!, longitude: Double(model.groupLongitude)!)
        
        let marker = GMSMarker (position: locationCoordinate)
        
        
        mapView.camera = GMSCameraPosition (target: locationCoordinate, zoom: 16, bearing: 0, viewingAngle: 0)
        
        marker.map = mapView
        
        isMapDisplayed = true
    }

}
