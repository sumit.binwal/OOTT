//
//  BusinessCoverImageCell.swift
//  OOTTBusinessApp
//
//  Created by Bharat Kumar Pathak on 14/07/17.
//  Copyright © 2017 KonstantInfoSolutions. All rights reserved.
//

import UIKit

let kCellIdentifier_business_cover_image_cell = "businessCoverImageCell"

class BusinessCoverImageCell: UITableViewCell {

    @IBOutlet weak var coverImageView: UIImageView!
    @IBOutlet weak var businessImageView: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        self.disbaleSelection()
        self.businessImageView.makeRound()
        self.businessImageView.createBorder(withColor: UIColor.init(white: 1, alpha: 0.43), andBorderWidth: 4.5)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    override func layoutSubviews() {
//        self.businessImageView.makeRound()
//        self.businessImageView.createBorder(withColor: UIColor.init(white: 1, alpha: 0.43), andBorderWidth: 4.5)
    }
    
    //MARK:- Update Cell Using Model
    func updateData(usingModel model:ModelGroupDetail) -> Void
    {
        // Cover picture
        if !model.coverPictureString.isEmptyString()
        {
//            self.coverImageView.setIndicatorStyle(.white)
//            self.coverImageView.setShowActivityIndicator(true)
//            self.coverImageView.contentMode = .scaleAspectFill
            self.coverImageView.sd_setImage(with: URL(string: model.coverPictureString), completed: nil)
        }
        
        // Profile picture
        if !model.pictureString.isEmptyString()
        {
            self.businessImageView.setIndicatorStyle(.white)
            self.businessImageView.setShowActivityIndicator(true)
            self.businessImageView.contentMode = .scaleAspectFill
            
            self.businessImageView.sd_setImage(with: URL(string: model.pictureString), completed: { (image, error, cacheType, url) in
//                print("completed")
            })
        }
    }

}
