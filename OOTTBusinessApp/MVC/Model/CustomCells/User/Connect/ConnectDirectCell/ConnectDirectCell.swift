//
//  ConnectDirectCell.swift
//  OOTTBusinessApp
//
//  Created by Bharat Kumar Pathak on 10/07/17.
//  Copyright © 2017 KonstantInfoSolutions. All rights reserved.
//

import UIKit

let kCellIdentifier_connect_direct_cell = "connectDirectCell"

class ConnectDirectCell: UITableViewCell {

    @IBOutlet weak var userImageView: UIImageView!
    @IBOutlet weak var userNameLabel: UILabel!
    @IBOutlet weak var userMessageLabel: UILabel!
    @IBOutlet weak var unreadCountBackView: UIView!
    @IBOutlet weak var unreadCountLabel: UILabel!
    @IBOutlet weak var labelTime: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.userImageView.makeRound()
        self.userImageView.createBorder(withColor: color_cellImageBorder, andBorderWidth: 0.5)
        
        self.disbaleSelection()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    override func layoutSubviews() {
        self.unreadCountBackView.layer.cornerRadius = self.unreadCountBackView.frame.size.height/2
    }
    
    func updateCell(usingModel model : ModelChatList)
    {
        self.userNameLabel.text = model.userName
        self.userMessageLabel.text = model.lastMessage
        
        self.userImageView.setIndicatorStyle(.white)
        self.userImageView.setShowActivityIndicator(true)
        
        self.userImageView.sd_setImage(with: URL (string: model.userPicture))
        {[weak self] (image, error, cacheType, url) in
            guard let `self` = self else {return}
            self.userImageView.setShowActivityIndicator(false)
        }
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ" //Your date format\
        dateFormatter.locale = NSLocale(localeIdentifier: "en_US_POSIX") as Locale!
        dateFormatter.timeZone = NSTimeZone(name: "UTC")! as TimeZone
        let date : NSDate = dateFormatter.date(from: model.lastMessageTime)! as NSDate //according to date format your date string
        
        self.labelTime.text = UtilityClass.timeAgoSinceDate(date as Date, currentDate:NSDate() as Date , numericDates: false)
        
        if model.unreadCount == 0
        {
            unreadCountBackView.isHidden = true
        }
        else
        {
            unreadCountBackView.isHidden = false
            unreadCountLabel.text = UtilityClass.getFormattedNumberString(usingString: String (model.unreadCount)) 
        }
    }
}
