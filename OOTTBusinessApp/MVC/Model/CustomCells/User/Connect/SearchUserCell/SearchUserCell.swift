//
//  SearchUserCell.swift
//  OOTTBusinessApp
//
//  Created by Santosh on 21/09/17.
//  Copyright © 2017 KonstantInfoSolutions. All rights reserved.
//

import UIKit

let kCellIdentifier_SearchUserCell = "searchUserCell"

class SearchUserCell: UITableViewCell {

    
    @IBOutlet weak var userImageView: UIImageView!
    @IBOutlet weak var userName: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.disbaleSelection()
        self.userImageView.makeRound()
        self.userImageView.createBorder(withColor: color_cellImageBorder, andBorderWidth: 0.5)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func updateCell(usingModel model:ModelFriendsListing)
    {
        // Friend name
        self.userName.text = model.friendName
        
        // Profile picture
        if model.friendPicture != nil
        {
            self.userImageView.setIndicatorStyle(.white)
            self.userImageView.setShowActivityIndicator(true)
            self.userImageView.sd_setImage(with: URL (string: model.friendPicture!), completed: nil)
        }
    }
    
    func updateCellForMemberList(usingModel model:GroupMember)
    {
        // Friend name
        self.userName.text = model.username
        
        // Profile picture
        
        self.userImageView.setIndicatorStyle(.white)
        self.userImageView.setShowActivityIndicator(true)
        self.userImageView.sd_setImage(with: URL (string: model.picture), completed: nil)
    }
    
    func updateCellForMyGroup(usingModel model:ModelMyGroup)
    {
        // Friend name
        self.userName.text = model.groupName
        
        // Profile picture
        
        self.userImageView.setIndicatorStyle(.white)
        self.userImageView.setShowActivityIndicator(true)
        self.userImageView.sd_setImage(with: URL (string: model.groupPicture), completed: nil)
    }
}
