//
//  FollowFollowingCell.swift
//  OOTTBusinessApp
//
//  Created by Santosh on 15/11/17.
//  Copyright © 2017 KonstantInfoSolutions. All rights reserved.
//

import UIKit

let kCellIdentifier_FollowFollowingCell = "followFollowingCell"

class FollowFollowingCell: UITableViewCell {

    @IBOutlet weak var userImageView: UIImageView!
    @IBOutlet weak var userName: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.userImageView.makeRound()
        self.userImageView.createBorder(withColor: color_cellImageBorder, andBorderWidth: 0.5)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func updateCellContents(usingModel model : GroupMember)
    {
        self.userName.text = model.username
        
        self.userImageView.setIndicatorStyle(.white)
        self.userImageView.setShowActivityIndicator(true)
        
        self.userImageView.sd_setImage(with: URL (string: model.picture), completed: nil)
    }
}
