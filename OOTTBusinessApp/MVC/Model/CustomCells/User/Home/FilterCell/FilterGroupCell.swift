//
//  FilterGroupCell.swift
//  OOTTBusinessApp
//
//  Created by Santosh on 24/07/17.
//  Copyright © 2017 KonstantInfoSolutions. All rights reserved.
//

import UIKit
import GooglePlacePicker

enum FilterCellType : Int {
    case location
    case scene
    case distance
    case lineWaitTime
    case numberOfFriends
    case numberOfPeople
    case closedBar
    case none
}

//MARK:- FilterCellDelegate Method Declaration
protocol FilterGroupCellDelegate : class
{
    func filterGroupCell(cell:FilterGroupCell, updatedInputfieldText:Any, latitude:String, longitude:String) -> Void
    
    func filterGroupCell(_ cell:FilterGroupCell, didUpdatedSliderValue sliderValue:String)
    
    func filterGroupCell(_ cell:FilterGroupCell, didTapOnRadioButton value:Bool)
}

class FilterGroupCell: UITableViewCell,UITextViewDelegate, TNSliderDelegate {

    
    @IBOutlet weak var labelHeader: UILabel!
//    @IBOutlet weak var inputField: KMPlaceholderTextView!
    @IBOutlet weak var iconImage: UIImageView!
    
    @IBOutlet weak var labelFilterName: UILabel!
    @IBOutlet weak var labelFilterType: UILabel!
    @IBOutlet var buttonRadio: UIButton!

    @IBOutlet weak var inputField: UITextField!
    
    @IBOutlet weak var sliderBar: TNSlider!
    
    //MARK:- LoginCellDelegate Instance
    weak var filterDelegate : FilterGroupCellDelegate?
    
    var cellType = FilterCellType.none
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        self.cellType = .none
        if inputField != nil
        {
            inputField.text = ""
            inputField.setPlaceholderColor(UIColor.white)
        }
        self.disbaleSelection()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    func initialCellSetup()
    {
        
    }
    
    
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        
        switch cellType {
        case .location:
            labelHeader.text = "Location"
            inputField.placeholder = "Location"
            inputField.isUserInteractionEnabled = false
            if cellType ==  FilterCellType.location
            {
                addTapGesture()
            }

        
        case .scene:
            labelHeader.text = "Scene"
            inputField.placeholder = "Search Scenes"
            iconImage.isHidden = true;
            inputField.isUserInteractionEnabled = true
            
        case .distance:
            labelFilterName.text = "Distance (Mi)"
            self.sliderBar.minimum = 0
            self.sliderBar.maximum = 50
            self.sliderBar.delegate = self
            self.sliderBar.continuous = false
            
        
        case .lineWaitTime:
            labelFilterName.text = "Line Wait Time"
//            self.sliderBar.minimum = 0
//            self.sliderBar.maximum = 120
//            self.sliderBar.delegate = self
//            self.sliderBar.continuous = false
            
        case .numberOfFriends:
            labelFilterName.text = "No. of Friends"
            
        case .numberOfPeople:
            labelFilterName.text = "Crowded Rating"
            
        case .closedBar:
            labelFilterName.text = "Show Closed Locations"
            
        default:
            break
        }
    }
    
    func addTapGesture() -> Void
    {
        self.contentView.gestureRecognizers?.removeAll()
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(onTapGesture(tapGesture:)))
        tapGesture.numberOfTapsRequired = 1
        self.contentView.addGestureRecognizer(tapGesture)
    }
    
    //MARK:- Tap Gesture Action
    func onTapGesture(tapGesture : UITapGestureRecognizer) -> Void
    {
        switch tapGesture.state {
        case .ended:
            
            if cellType == FilterCellType.location
            {
                let center = CLLocationCoordinate2DMake(LocationManager.sharedInstance().newLatitude, LocationManager.sharedInstance().newLongitude)
                
                let northEast = CLLocationCoordinate2DMake(center.latitude, center.longitude)
                
                let southWest = CLLocationCoordinate2DMake(center.latitude, center.longitude)
                
                let viewPort = GMSCoordinateBounds (coordinate: northEast, coordinate: southWest)
                
                let pickerConfig = GMSPlacePickerConfig (viewport: viewPort)
                
                let placePicker = GMSPlacePicker (config: pickerConfig)
                
                placePicker.pickPlace(callback: {[weak self] (result, error) in
                    
                    guard let `self` = self else {return}
                    
                    if result != nil
                    {
                        let finalResult = result!
                        var formattedAddress = finalResult.name
                        if finalResult.formattedAddress != nil
                        {
                            formattedAddress = finalResult.formattedAddress!.isEmpty ? finalResult.name : String(finalResult.formattedAddress!)
                        }

                        
                        self.inputField.text = formattedAddress//self.model.keyValue as? String
                        
                        guard let delegate = self.filterDelegate else {
                            return
                        }
                        
                        delegate.filterGroupCell(cell: self, updatedInputfieldText: self.inputField.text!, latitude: String(finalResult.coordinate.latitude), longitude: String(finalResult.coordinate.longitude))
                    }
                })
                
                return
            }
            
            
        default:
            break
        }
    }
    @IBAction func radioButtonClickedAction(_ sender: Any)
    {
        guard let delegate = self.filterDelegate else {
            return
        }
        
        delegate.filterGroupCell(self, didTapOnRadioButton: false)
    }
    
    func updateCell(usingModel model:ModelGroupFilter, forIndex row:Int)
    {
        switch row {
        case 0:
            if model.locationName != nil
            {
                self.inputField.text = model.locationName!
            }
        case 1:
            self.inputField.text = model.scene!
        case 2:
            if model.sortType == .distance {
                self.sliderBar.value = ceilf(Float(model.sliderValue!)!)
            }
            else
            {
                self.sliderBar.value = 0
            }
        case 3:
            if model.sortType == .lineWaitTime {
                self.labelFilterType.text = model.priorityTypeValue
            }
            else
            {
                self.labelFilterType.text = "Choose"
            }
//            if model.sortType == .lineWaitTime {
//                self.sliderBar.value = ceilf(Float(model.sliderValue!)!)
//            }
//            else
//            {
//                self.sliderBar.value = 0
//            }
        case 4 :
            if model.sortType == .numberOfFriends {
                self.labelFilterType.text = model.priorityTypeValue
            }
            else
            {
                self.labelFilterType.text = "Choose"
            }
        case 5:
            if model.sortType == .numberOfPeople {
                self.labelFilterType.text = model.priorityTypeValue
            }
            else
            {
                self.labelFilterType.text = "Choose"
            }
        case 6:
            if model.closedBar
            {
                self.buttonRadio.setImage(#imageLiteral(resourceName: "radioSelect"), for: .normal)
            }
            else
            {
                self.buttonRadio.setImage(#imageLiteral(resourceName: "radioDeselected"), for: .normal)
            }
        default: break
            
        }
    }
    
    func slider(_ slider: TNSlider, displayTextForValue value: Float) {
        guard let delegate = self.filterDelegate else {
            return
        }
        
        delegate.filterGroupCell(self, didUpdatedSliderValue: String(Int (value)))
    }
}

