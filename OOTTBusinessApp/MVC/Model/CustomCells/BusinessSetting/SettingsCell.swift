//
//  SettingsCell.swift
//  OOTTBusinessApp
//
//  Created by Santosh on 15/06/17.
//  Copyright © 2017 KonstantInfoSolutions. All rights reserved.
//

import UIKit

let kCellIdentifier_setting_arrowCell = "settingsCell1"


//private struct Item {
//    var title :String?
//    var icon :UIImage?
//    init(title : String? = "", icon : UIImage? = UIImage())
//    {
//        self.title = title
//        self.icon = icon
//    }
//}
//
//struct ItemsContainer {
//    fileprivate var staticData : [Item]?
//    
//    static var cellContents : ItemsContainer =
//    {
//        let instance = ItemsContainer.init()
//        return instance
//    }()
//    
//    private init() {
//        staticData=getItems()
//    }
//    
//    private func getItems() -> [Item]
//    {
//        let item1 = Item(title: "My Profile", icon: #imageLiteral(resourceName: "setting_profile"))
//        let item2 = Item(title: "Bouncer App", icon: #imageLiteral(resourceName: "setting_bouncerIcon"))
//        let item3 = Item(title: "Add Account", icon: #imageLiteral(resourceName: "setting_addAcount"))
//        let item4 = Item(title: "Employees", icon: #imageLiteral(resourceName: "setting_employees"))
//        let item5 = Item(title: "Notification", icon: #imageLiteral(resourceName: "setting_notification"))
//        let item6 = Item(title: "Privacy Policies", icon: #imageLiteral(resourceName: "setting_info"))
//        let item7 = Item(title: "Terms & Condition", icon: #imageLiteral(resourceName: "setting_info"))
//        let item8 = Item(title: "Delete Account", icon: #imageLiteral(resourceName: "setting_deleteAccount"))
//        let item9 = Item(title: "Logout", icon: #imageLiteral(resourceName: "setting_logout"))
//        let itemArray = [item1,item2,item3,item4,item5,item6,item7,item8,item9]
//        return itemArray
//    }
//}

private protocol CellContent {
    var title : String { get }
    var icon : UIImage { get }
}

private enum SettingCellType : Int, CellContent
{
    case myProfile
    case bouncerApp
    case addAccount
    case employees
    case notification
    case privacyPolicies
    case termsAndCond
    case deleteAccount
    case logout
    
    var title: String
    {
        switch self
        {
        case .myProfile:
            return "My Profile"
            
        case .bouncerApp:
            return "Bouncer App"
            
        case .addAccount:
            return "Add Account"
            
        case .employees:
            return "Employees"
            
        case .notification:
            return "Notification"
            
        case .privacyPolicies:
            return "Privacy Policy"
            
        case .termsAndCond:
            return "Terms & Conditions"
            
        case .logout:
            return "Logout"
            
        case .deleteAccount:
            return "Delete Account"
        }
    }
    
    var icon: UIImage
    {
        switch self
        {
        case .myProfile:
            return #imageLiteral(resourceName: "setting_profile")
            
        case .bouncerApp:
            return #imageLiteral(resourceName: "setting_bouncerIcon")
            
        case .addAccount:
            return #imageLiteral(resourceName: "setting_addAcount")
            
        case .employees:
            return #imageLiteral(resourceName: "setting_employees")
            
        case .notification:
            return #imageLiteral(resourceName: "setting_notification")
            
        case .privacyPolicies:
            return #imageLiteral(resourceName: "setting_info")
            
        case .termsAndCond:
            return #imageLiteral(resourceName: "setting_info")
            
        case .logout:
            return #imageLiteral(resourceName: "setting_logout")
            
        case .deleteAccount:
            return #imageLiteral(resourceName: "setting_deleteAccount")
        }
    }
}

private enum UserSettingsCellType : Int, CellContent
{
    case editProfile
    case addAccount
    case notification
    case privacyPolicies
    case termsAndCond
    case deleteAccount
    case logout
    
    var title: String
    {
        switch self
        {
        case .editProfile:
            return "Edit Profile"
            
        case .addAccount:
            return "Add Account"
            
        case .notification:
            return "Notification"
            
        case .privacyPolicies:
            return "Privacy Policies"
            
        case .termsAndCond:
            return "Terms & Condition"
            
        case .deleteAccount:
            return "Delete Account"
        
        case .logout:
            return "Logout"
            
        }
    }
    
    var icon: UIImage
    {
        switch self
        {
        case .editProfile:
            return #imageLiteral(resourceName: "setting_profile")
            
        case .addAccount:
            return #imageLiteral(resourceName: "setting_addAcount")
            
        case .notification:
            return #imageLiteral(resourceName: "setting_notification")
            
        case .privacyPolicies:
            return #imageLiteral(resourceName: "setting_info")
            
        case .termsAndCond:
            return #imageLiteral(resourceName: "setting_info")
            
        case .deleteAccount:
            return #imageLiteral(resourceName: "setting_deleteAccount")
            
        case .logout:
            return #imageLiteral(resourceName: "setting_logout")
        }
    }
}

class SettingsCell: UITableViewCell {

    @IBOutlet weak var labelTitle: UILabel!
    @IBOutlet weak var imageRightArrow: UIImageView!
    @IBOutlet weak var imageIcon: UIImageView!
    @IBOutlet var buttonNotificationToggle: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.labelTitle.makeAdaptiveFont()
        self.disbaleSelection()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func updateTheCellContents(withIndexpath indexpath: IndexPath) -> Void
    {
//        let content = ItemsContainer.cellContents.staticData![indexpath.row]
//        
//        self.labelTitle.text = content.title
//        self.imageIcon.image = content.icon
        self.labelTitle.text = SettingCellType(rawValue: indexpath.row)?.title
        self.imageIcon.image = SettingCellType(rawValue: indexpath.row)?.icon
        self.buttonNotificationToggle.isHidden=true
    }
    
    func updateTheCellContents(usingModel model : BusinessSettings) -> Void
    {
        self.labelTitle.text = model.settingName
        self.imageIcon.image = model.settingsImage
        self.buttonNotificationToggle.isHidden=true
    }
    
    func updateUserSettingsCellContents(withIndexpath indexpath: IndexPath) -> Void
    {
        self.labelTitle.text = UserSettingsCellType(rawValue: indexpath.row)?.title
        self.imageIcon.image = UserSettingsCellType(rawValue: indexpath.row)?.icon
        self.buttonNotificationToggle.isHidden=true
    }
    
    func updateUserSettingsCellContents(usingModel model : UserSettings) -> Void
    {
        self.labelTitle.text = model.settingName
        self.imageIcon.image = model.settingsImage
        self.buttonNotificationToggle.isHidden=true
    }
}
