//
//  ModelRegister.swift
//  OOTTBusinessApp
//
//  Created by Santosh on 19/06/17.
//  Copyright © 2017 KonstantInfoSolutions. All rights reserved.
//

import Foundation
import UIKit

enum CreateProfileEnum : String
{
    case businessName = "name"
    case radius = "radius"
    case scene = "scene"
    case about = "about"
    case contact = "contact"
    case location = "location"
    case info = "info"
    case website = "website"
    case charges = "charges"
    case dressCode = "dressCode"
    case totalOccupancy = "occupancy"
    case openTill = "open"
    case latitude = "latitude"
    case longitude = "longitude"
    case none = "none"
}

class ModelCreateProfile
{
    var keyValue : Any
    var keyName : String
    
    var latitude : String?
    var longitude : String?
    
    init(withKey : CreateProfileEnum.RawValue, value : Any)
    {
        self.keyName = withKey
        self.keyValue = value
    }
    
    deinit {
        print("ModelCreateProfile deinit")
    }
}

class PhotoData
{
    var image : UIImage?
    let imageKey : String = "file"
    var imageName : String?
    var isInProgress = false
    var isImageUploadFailed = false
    var imageURLString : String?
    var imageID : String?
    
    init(image : UIImage?, imageName : String?, isUploading : Bool)
    {
        self.image = image
        self.imageName = imageName
        self.isInProgress = isUploading
    }
    
    deinit {
        print("PhotoData deinit")
    }
}
