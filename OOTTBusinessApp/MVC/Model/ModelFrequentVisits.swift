//
//  ModelFrequentVisits.swift
//  OOTTBusinessApp
//
//  Created by Bharat Kumar Pathak on 12/07/17.
//  Copyright © 2017 KonstantInfoSolutions. All rights reserved.
//

import Foundation

//MARK:-
class ModelFrequentVisits
{
    var businessID : String?
    var businessPicture: String?
    var businessName : String?
    
    init() {
        businessID = ""
        businessPicture = ""
        businessName = ""
    }
    
    deinit {
        print("ModelFrequentVisits deinit")
    }
    
    func updateModel(usingDictionary dictionary:[String:Any]) -> Void
    {
        if let businessDict = dictionary["business"] as? [String:Any]
        {
            if let id = businessDict["_id"] as? String
            {
                businessID = id
            }
            if let picture = businessDict["picture"] as? String
            {
                businessPicture = picture
            }
            if let name = businessDict["name"] as? String
            {
                businessName = name
            }
        }
    }
}
