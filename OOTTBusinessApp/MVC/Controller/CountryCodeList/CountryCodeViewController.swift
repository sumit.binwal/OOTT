//
//  CountryCodeViewController.swift
//  OOTTBusinessApp
//
//  Created by Santosh on 21/06/17.
//  Copyright © 2017 KonstantInfoSolutions. All rights reserved.
//

import UIKit

protocol CountryCodeDelegate
{
    func countryCodeDidSelectCountry(withCountryName countryName:String, andCountryDialCode dialCode:String) -> Void
}

class Country
{
    var countryName : String
    var countryCode : String
    var countryDialCode : String
    
    init(name : String, nameCode: String, dialCode:String)
    {
        self.countryCode = nameCode
        self.countryName = name
        self.countryDialCode = dialCode
    }
}

class CountryCodeViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UISearchBarDelegate {

    @IBOutlet var searchBarCountry: UISearchBar!
    
    @IBOutlet weak var tableCountryCodeListing: UITableView!
    
    var arrayCountryCodeList : [Country] = []
    
    var filteredCountryCodeList : [Country] = []
    
    var isSearchActive = false
    
    var countryDelegate : CountryCodeDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        self.setupView()
        
        self.getCountryListing()
        
        self.setupTableView()
    }
    
    deinit {
        print("CountryCodeViewController Deinit")
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK:- viewWillAppear
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.showNavigationBar()
    }
    
    //MARK:- viewWillDisappear
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        self.hideNavigationBar()
    }
    
    func setupView() -> Void
    {
        self.title = "Country Code";
        
        let backBarButton = self.setLeftBarButtonItem(withImage: #imageLiteral(resourceName: "backButton"))
        backBarButton.action = #selector(onBackButtonAction)
        backBarButton.target = self
    }
    
    //MARK:- Initial table setup
    func setupTableView() -> Void
    {
        self.tableCountryCodeListing.delegate = self
        self.tableCountryCodeListing.dataSource = self
        
        searchBarCountry.delegate = self
//        tableCountryCodeListing.tableHeaderView = searchBarCountry
    }
    
    //MARK:- Back button action
    func onBackButtonAction() -> Void {
        self.navigationController?.popViewController(animated: true)
    }
    
    func getCountryListing() -> Void
    {
        if let resourcePath = Bundle.main.path(forResource: "CallingCodes", ofType: "plist")
        {
            weak var weakSelf = self
            let arrayResource = NSArray(contentsOfFile: resourcePath)
            arrayResource?.forEach({ (item) in
                print(item)
                let newItem = item as! [String:String]
                var name = ""
                var code = ""
                var dialCode = ""
                if newItem["name"] != nil
                {
                    name = newItem["name"]!
                }
                if newItem["code"] != nil
                {
                    code = newItem["code"]!
                }
                if newItem["dial_code"] != nil
                {
                    dialCode = newItem["dial_code"]!
                }
                weakSelf?.arrayCountryCodeList.append(Country(name: name, nameCode: code, dialCode: dialCode))
            })
        }
        tableCountryCodeListing.reloadData()
    }
    
    //MARK:- UITableView Delegate and Datasource
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        return searchBarCountry
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 44
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return isSearchActive ? filteredCountryCodeList.count : arrayCountryCodeList.count
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension;
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell =  tableView.dequeueReusableCell(withIdentifier: kCellIdentifier_countryCodeListing) as! CountryCodeCell
        
        let countryModel = isSearchActive ? filteredCountryCodeList[indexPath.row] : arrayCountryCodeList[indexPath.row]
        
        cell.labelCountryName.text = countryModel.countryName
        cell.labelCountryCode.text = countryModel.countryDialCode
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        guard countryDelegate != nil else
        {
            return
        }
        
        let country = isSearchActive ? filteredCountryCodeList[indexPath.row] : arrayCountryCodeList[indexPath.row]
        
        countryDelegate?.countryCodeDidSelectCountry(withCountryName: country.countryName, andCountryDialCode: country.countryDialCode)
        
        self.navigationController?.popViewController(animated: true)
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String)
    {
        filteredCountryCodeList = arrayCountryCodeList.filter({ (country) -> Bool in
            let name = country.countryName
            let range = name.lowercased().range(of: searchText.lowercased())
            
            if range != nil
            {
                return true
            }
            return false
        })
        
        if filteredCountryCodeList.count == 0
        {
            isSearchActive = false
            if !(searchBar.text?.isEmpty)! {
                isSearchActive = true
            }
        }
        else
        {
            isSearchActive = true
        }
        tableCountryCodeListing.reloadData()
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
    }
}
