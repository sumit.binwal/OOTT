//
//  DashboardViewController.swift
//  OOTTBouncer
//
//  Created by Santosh on 07/06/17.
//  Copyright © 2017 KonstantInfoSolutions. All rights reserved.
//

import UIKit
import PKHUD
import InAppNotify

private let dummyStatus = "No current status"

private class ModelBusinessStatusData
{
    var businessStatus : String = dummyStatus
    var businessTotalCrowd : String = "0"
    var businessWaitingTime : String = "0"
    var businessID : String = "0"
    var businessOriginalWaitingTime : String = "0"
    
    var groupName = ""
    var groupPicture = ""
    var groupUnreadCount = 0
    var roomID = ""
    
    
    func resetModelValues() -> Void
    {
        self.businessID = "0"
        self.businessStatus = dummyStatus
        self.businessTotalCrowd = "0"
        self.businessWaitingTime = "0"
        self.businessOriginalWaitingTime = "0"
        
        self.groupName = ""
        self.groupPicture = ""
        self.groupUnreadCount = 0
        self.roomID = ""
    }
}

class DashboardViewController: UIViewController, ChangeStatusDelegate {
    
    //MARK: Label people header
    @IBOutlet weak var labelPeopleHeader: UILabel!
    
    //MARK: Label time header
    @IBOutlet weak var labelTimeHeader: UILabel!
    
    //MARK: Label status header
    @IBOutlet weak var labelStatusHeader: UILabel!
    
    //MARK: Font sizes
    let fontHeaders = 17 * scaleFactorX
    let fontStatusHeader = 20 * scaleFactorX
    let fontStatusButton = 16 * scaleFactorX
    
    //MARK: Label people count
    @IBOutlet weak var labelPeopleCount: UILabel!
    
    //MARK: Label time count
    @IBOutlet weak var labelTimeCount: UILabel!
    
    //MARK: Label current status
    @IBOutlet weak var labelCurrentStatus: UILabel!
    
    //MARK: Button change status
    @IBOutlet weak var buttonChangeStatus: UIButton!
    
    //MARK: Model Event Data
    fileprivate var modelEvent = ModelBusinessStatusData ()
    
    var isEventAvailabel = false
    
    //MARK:- viewDidLoad
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        let contains = UtilityClass.isCurrentUserInStackedUserInfoData()
        if !contains
        {
            UtilityClass.deleteStackedUserInfoData()
        }
        else
        {
            if let stackedUsers = UtilityClass.getStackedUserInfoData()
            {
                if stackedUsers.count == 1
                {
                    UtilityClass.deleteStackedUserInfoData()
                }
            }
        }
        
        initiateSocket()
        self.setupView()
        self.getCurrentEventStatusApi()
    }

    //MARK:- didReceiveMemoryWarning
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK:- deinit
    deinit {
//        currentUserID = nil
        SocketManager.sharedInstance().removeIncomingMessagesListener()
        SocketManager.sharedInstance().disconnectSocket()
        print("DashboardViewController deinit")
    }
    
    //MARK:- Initial setup of view
    func setupView() -> Void
    {
        self.title = "DASHBOARD"
        
        self.showNavigationBar()
        
        let chatButton = self.setRightBarButtonItem(withImage: #imageLiteral(resourceName: "groupChatBlue"))
        chatButton.action = #selector(onChatButtonAction)
        chatButton.target = self
        
        labelTimeHeader.font = UIFont(name: FONT_PROXIMA_LIGHT, size: fontHeaders)
        
        labelPeopleHeader.font = labelTimeHeader.font
        
        labelPeopleCount.font = labelTimeHeader.font
        labelTimeCount.font = labelTimeHeader.font
        labelCurrentStatus.font = labelTimeHeader.font
                
        labelStatusHeader.font = UIFont(name: FONT_PROXIMA_SEMIBOLD, size: fontStatusHeader)
        
        let yourAttributes : [String : Any] = [
            NSForegroundColorAttributeName : color_pink,
            NSFontAttributeName : UIFont(name: FONT_PROXIMA_SEMIBOLD, size: fontStatusButton)!,
            NSUnderlineStyleAttributeName : NSUnderlineStyle.styleSingle.rawValue
        ]
        
        buttonChangeStatus.setAttributedTitle(NSAttributedString(string: "CHANGE STATUS", attributes: yourAttributes), for: .normal)
        
        resetView()
    }
    
    //MARK:- Socket Connection
    func initiateSocket()
    {
        SocketManager.sharedInstance().userSID = UtilityClass.getUserSidData()!
        SocketManager.sharedInstance().connectSocket
            {[weak self] (data, ack) in
                
                guard let `self` = self else { return }
                
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: NOTIFICATION_SOCKET_CONNECTED), object: nil)
                self.applyReceiveMessageListener()
        }
    }
    
    //MARK:- Reset view details
    func resetView() -> Void
    {
        modelEvent.resetModelValues()
        labelPeopleCount.text = modelEvent.businessTotalCrowd
        labelTimeCount.text = modelEvent.businessWaitingTime
        labelCurrentStatus.text = modelEvent.businessStatus
    }
    
    //MARK:- Minus people count action
    @IBAction func onMinusPeopleCount(_ sender: UIButton) {
        if !isEventAvailabel
        {
            return
        }
        var peopleCount = Int(modelEvent.businessTotalCrowd)!
        
        if peopleCount > 0
        {
            peopleCount -= 1
        }
        modelEvent.businessTotalCrowd = String(peopleCount)
        updateView()
        updateEventStatus()
    }
    
    //MARK:- Plus people count action
    @IBAction func onPlusPeopleCount(_ sender: UIButton) {
        if !isEventAvailabel
        {
            return
        }
        var peopleCount = Int(modelEvent.businessTotalCrowd)!
        
        peopleCount += 1

        modelEvent.businessTotalCrowd = String(peopleCount)
        updateView()
        updateEventStatus()
    }
    //MARK:- Plus wait time action
    @IBAction func onPlusWaitTime(_ sender: UIButton) {
        if !isEventAvailabel
        {
            return
        }
        var numberString = String(Int (modelEvent.businessOriginalWaitingTime)! + 1)

        modelEvent.businessOriginalWaitingTime = numberString
        
        numberString = UtilityClass.checkAndConvertMinutesToHour(usingString: String(Int (numberString)!))
        
        modelEvent.businessWaitingTime = numberString
        updateView()
        updateEventStatus()
    }
    
    //MARK:- Minus wait time action
    @IBAction func onMinusWaitTime(_ sender: UIButton) {
        if !isEventAvailabel
        {
            return
        }
        if Int (modelEvent.businessOriginalWaitingTime)!>0
        {
            var numberString = String(Int (modelEvent.businessOriginalWaitingTime)!-1)
            
            modelEvent.businessOriginalWaitingTime = numberString
            
            numberString = UtilityClass.checkAndConvertMinutesToHour(usingString: String(Int (numberString)!))
            
            modelEvent.businessWaitingTime = numberString
            updateView()
            updateEventStatus()
        }
    }
    
    //MARK:- Change status action
    @IBAction func onChangeStatusAction(_ sender: UIButton) {
        
        if !isEventAvailabel
        {
//            UtilityClass.showAlertWithTitle(title: App_Name, message: "No Live Event Found", onViewController: self, withButtonArray: nil, dismissHandler: nil)
            return
        }
        
        print(modelEvent.businessStatus,modelEvent.businessTotalCrowd,modelEvent.businessWaitingTime)
        let statusVC = self.storyboard?.instantiateViewController(withIdentifier: "statusVC") as! ChangeStatusViewController
        
        statusVC.delegate = self
//        statusVC.eventID = modelEvent.eventID
        statusVC.eventStatus = modelEvent.businessStatus
        
        self.navigationController?.pushViewController(statusVC, animated: true)
    }
    
    //MARK:- Logout action
    @IBAction func onLogoutAction(_ sender: UIButton) {
        logoutAPI()
    }
    
    //MARK:-
    func updateModel(usingDictionary dictionary:[String:Any]) -> Void
    {
        if dictionary["status"] != nil
        {
            modelEvent.businessStatus = String(dictionary["status"] as! String)
        }
        if dictionary["waitingTime"] != nil
        {
            modelEvent.businessOriginalWaitingTime = String(dictionary["waitingTime"] as! Int)
            
            let numberString = UtilityClass.checkAndConvertMinutesToHour(usingString: String(Int (Float (modelEvent.businessOriginalWaitingTime)!)))
            
            modelEvent.businessWaitingTime = numberString
            
        }
        if dictionary["attended"] != nil
        {
            modelEvent.businessTotalCrowd = String(dictionary["attended"] as! Int)
        }
        if dictionary["_id"] != nil
        {
            modelEvent.businessID = String(dictionary["_id"] as! String)
        }
        
        if let groupName = dictionary["name"] as? String
        {
            modelEvent.groupName = groupName
        }
        if let picture = dictionary["picture"] as? String
        {
            modelEvent.groupPicture = picture
        }
        if let unseenCount = dictionary["unseenCount"] as? Int
        {
            modelEvent.groupUnreadCount = unseenCount
        }
        if let room_id = dictionary["room_id"] as? String
        {
            modelEvent.roomID = room_id
        }
        
        isEventAvailabel = true
        updateView()
    }
    
    //MARK:-
    func updateView() -> Void
    {
        labelCurrentStatus.text = modelEvent.businessStatus.isEmpty ? dummyStatus : modelEvent.businessStatus
        
        labelTimeCount.text = modelEvent.businessWaitingTime
        
        labelPeopleCount.text = modelEvent.businessTotalCrowd
    }
    
    //MARK:- Get Status Api
    func getCurrentEventStatusApi() -> Void
    {
        HUD.show(.systemActivity, onView: self.view.window)
        
        let urlToHit = EndPoints.getCurrentStatus(UtilityClass.getUserSidData()!).path
        
        AppWebHandler.sharedInstance().fetchData(fromURL: urlToHit, httpMethod: .get, parameters: nil)
        {[weak self] (data, dictionary, statusCode, error) in
            
            HUD.hide()
            
            guard let `self` = self else { return }
            
            if error != nil
            {
                self.resetView()
                UtilityClass.showAlertWithTitle(title: App_Name, message: error!.localizedDescription, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                
                return
            }
            
            if statusCode == 203
            {
                self.resetView()
                let responseDictionary = dictionary!
                guard (responseDictionary["message"] != nil) else
                {
                    UtilityClass.showAlertWithTitle(title: App_Name, message: App_Global_Error_Msg, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                    return
                }
                let messageStr = responseDictionary["message"] as! String
                
                UtilityClass.showAlertWithTitle(title: App_Name, message: messageStr, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                return
            }
            if statusCode == 200
            {
                let responseDictionary = dictionary!
                let messageStr = responseDictionary["message"] as! String
                let type = responseDictionary["type"] as! Bool
                if type
                {
                    // Got the event, update view...
                    let dataDictionary = responseDictionary["data"] as! [String:Any]
                    self.updateModel(usingDictionary: dataDictionary)
                }
                else
                {
                    self.resetView()
                    UtilityClass.showAlertWithTitle(title: App_Name, message: messageStr, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                }
            }
        }
    }
    
    //MARK:- Update Event Status Api
    func updateEventStatus() -> Void
    {
        let urlToHit = EndPoints.updateStatus(UtilityClass.getUserSidData()!).path
        
        let params = ["attended" : modelEvent.businessTotalCrowd,
                      "waitingTime" : modelEvent.businessOriginalWaitingTime,
                      "status" : modelEvent.businessStatus]
        
        
        AppWebHandler.sharedInstance().fetchData(fromURL: urlToHit, httpMethod: .post, parameters: params)
        { (data, dictionary, statusCode, error) in
            
        }
    }
    
    //MARK:- Logout API Call
    func logoutAPI() -> Void {
        
        HUD.show(.systemActivity, onView: self.view.window)
        
        self.view.endEditing(true)
        
        let urlToHit = EndPoints.logout(UtilityClass.getUserSidData()!).path
        
        
        AppWebHandler.sharedInstance().fetchData(fromURL: urlToHit, httpMethod: .get, parameters: nil, shouldDeserialize: true)
        {[weak self] (data, dictionary, statusCode, error) in
            
            HUD.hide()
            
            guard let `self` = self else { return }
            
            if error != nil
            {
                UtilityClass.showAlertWithTitle(title: App_Name, message: error!.localizedDescription, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                
                return
            }
            
            if dictionary == nil
            {
                UtilityClass.showAlertWithTitle(title: App_Name, message: App_Global_Error_Msg, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                
                return
            }
            
            if statusCode == 203
            {
                let responseDictionary = dictionary!
                guard (responseDictionary["message"] != nil) else
                {
                    UtilityClass.showAlertWithTitle(title: App_Name, message: App_Global_Error_Msg, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                    return
                }
                let messageStr = responseDictionary["message"] as! String
                
                UtilityClass.showAlertWithTitle(title: App_Name, message: messageStr, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                return
            }
            
            if statusCode == 200
            {
                
                let responseDictionary = dictionary!
                
                let type = responseDictionary["type"] as! Bool
                let msgStr = responseDictionary["message"] as! String
                
                if type
                {
                    UtilityClass.removeCurrentUserFromStack()
                    
                    UtilityClass.deleteDataOnLogout()
                    
                    if UtilityClass.getFirstUserFromStackedList() != nil
                    {
                        self.updateRootViewController()
                        return
                    }
                    
                    let loginVC = UIStoryboard.getLoginFlowStoryboard().instantiateInitialViewController()
                    
                    UtilityClass.changeRootViewController(with: loginVC!)
                }
                else
                {
                    UtilityClass.showAlertWithTitle(title: App_Name, message: msgStr, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                }
            }
        }
    }
    
    func updateRootViewController()
    {
        UtilityClass.saveUserInfoData(userDict: UtilityClass.getFirstUserFromStackedList()!)
        
        if UtilityClass.getUserTypeData() == AppUserTypeEnum.employee.rawValue
        {
            let employeeVC = UIStoryboard.getBusinessHomeFlowStoryboard().instantiateInitialViewController()
            
            UtilityClass.changeRootViewController(with: employeeVC!)
            
            return
        }
        
        if UtilityClass.getUserTypeData() == AppUserTypeEnum.provider.rawValue
        {
            let businessVC = UIStoryboard.getBusinessTabBarFlowStoryboard().instantiateInitialViewController()
            
            UtilityClass.changeRootViewController(with: businessVC!)
            return
        }
        
        let userHomeVC = UIStoryboard.getUserTabsStoryboard().instantiateInitialViewController()
        
        UtilityClass.changeRootViewController(with: userHomeVC!)
        
        return
    }
    
    //MARK:- ChangeStatus Delegate
    func changeStatusControllerDidChangeText(withText updatedText:String) -> Void
    {
        modelEvent.businessStatus = updatedText
        updateView()
    }
    
    //MARK:- Chat Button Action
    func onChatButtonAction()
    {
        if !isEventAvailabel
        {
            return
        }
        
        // open screen
        let chatViewController = UIStoryboard.getChatViewStoryboard().instantiateInitialViewController() as! GroupChatViewController
        
        chatViewController.hidesBottomBarWhenPushed = true
        
        chatViewController.isGroupChat = true
        
        chatViewController.isBusinessChat = true
        
        chatViewController.chatUsername.text = modelEvent.groupName
        
        chatViewController.chatRoomID = modelEvent.roomID
        
        chatViewController.chatPicture = modelEvent.groupPicture
        
        chatViewController.businessID = modelEvent.businessID
        
        self.navigationController?.pushViewController(chatViewController, animated: true)
    }
    
    // MARK:- Message Listener (New Message)
    func applyReceiveMessageListener()
    {
        // Group message listner
        SocketManager.sharedInstance().receiveIncomingGroupMessages
            {[weak self] (messageDict, statusCode) in
                if statusCode == 200
                {
                    guard let `self` = self else {return}
                    self.manageTheIncomingMessage(usingDictionary: messageDict)
                }
        }
        
    }
    
    func manageTheIncomingMessage(usingDictionary dictionary:[String:Any])
    {
        let model = self.getNewModel(usingDictionary: dictionary)
        
        // If chat view is open...
        if let groupChatVC = appDelegate?.window??.visibleViewController() as? GroupChatViewController
        {
            // Simply add new message model...
            groupChatVC.addNewModel(usingDictionary: dictionary)
        }
        else
        {
            self.createInAppNotification(usingModel: model)
        }
    }
    
    // MARK:- Message Model Creation
    func getNewModel(usingDictionary dictionary : [String:Any]) -> ModelChatMessage
    {
        let model = ModelChatMessage () // Model creation
        model.updateModel(usingDictionary: dictionary) // Updating model
        return model
    }
    
    // MARK:- InApp Notification Fire
    func createInAppNotification(usingModel model:ModelChatMessage)
    {
        let title = model.roomID.isEmpty ? model.senderName : String (model.senderName+" @ "+model.groupName)
        
        let picture = model.roomID.isEmpty ? model.senderPicture : model.groupPicture
        
        let announce = Announcement (title: title, subtitle: model.message, image: nil, urlImage: picture, duration: 5, interactionType: .none, userInfo: model)
        {[weak self] (callBackType, str, announce) in
            
            guard let `self` = self else { return }
            
            if callBackType == CallbackType.tap
            {
                if let chatInfo = announce.userInfo as? ModelChatMessage
                {
                    let isGroup = !chatInfo.roomID.isEmpty
                    
                    // open screen
                    let chatViewController = UIStoryboard.getChatViewStoryboard().instantiateInitialViewController() as! GroupChatViewController
                    
                    chatViewController.hidesBottomBarWhenPushed = true
                    
                    chatViewController.isGroupChat = isGroup
                    
                    chatViewController.isBusinessChat = chatInfo.isBusinessGroup
                    
                    chatViewController.chatUsername.text = isGroup ? chatInfo.groupName : chatInfo.senderName
                    
                    if isGroup
                    {
                        chatViewController.chatPicture = chatInfo.groupPicture
                        
                        chatViewController.chatRoomID = chatInfo.roomID
                    }
                    else
                    {
                        chatViewController.chatUserID = chatInfo.senderID
                    }
                    
                    self.navigationController?.pushViewController(chatViewController, animated: true)
                }
            }
        }
        
        InAppNotify .Show(announce, to: self.navigationController!)
    }
}
