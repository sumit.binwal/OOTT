//
//  BusinessSettingViewController.swift
//  OOTTBusinessApp
//
//  Created by Santosh on 15/06/17.
//  Copyright © 2017 KonstantInfoSolutions. All rights reserved.
//

import UIKit
import PKHUD

struct BusinessSettings {
    var settingName = ""
    var settingsImage : UIImage!
}

class BusinessSettingViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var tableSettings: UITableView!
    
    var arraySettings = [BusinessSettings]()
    
    var isSwitchModeEnabled = false
    
    var selectedIndex = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        let contains = UtilityClass.isCurrentUserInStackedUserInfoData()
        if !contains
        {
            UtilityClass.deleteStackedUserInfoData()
        }
        else
        {
            if let stackedUsers = UtilityClass.getStackedUserInfoData()
            {
                if stackedUsers.count == 1
                {
                    UtilityClass.deleteStackedUserInfoData()
                }
//                else // Check for updated name and replace
//                {
//                    let contains = stackedUsers.contains(where: { (userDict) -> Bool in
//                        return userDict["sid"] as! String == UtilityClass.getUserSidData()!
//                    })
//                    
//                    if contains
//                    {
//                        let index = stackedUsers.index(where: { (userDict) -> Bool in
//                            return userDict["sid"] as! String == UtilityClass.getUserSidData()!
//                        })
//                        
//                        var userInfo = stackedUsers[index!]
//                        userInfo["name"] = UtilityClass.getUserNameData()!
//                        stackedUsers[index!] = userInfo
//                        UtilityClass.saveStackedUserInfoArray(userArray: stackedUsers)
//                    }
//                }
            }
        }
        
        self.setupView()
        self.setupTableView()
        setupArray()

    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func setupView() -> Void
    {
        self.title = "SETTINGS"
        
        self.showNavigationBar()
        
        let leftBarButton = self.setLeftBarButtonItem(withImage: #imageLiteral(resourceName: "backButton"))
        leftBarButton.action = #selector(onBackButtonAction)
        leftBarButton.target = self
    }
    
    //MARK:- Setup Array
    func setupArray() -> Void
    {
        arraySettings.append(BusinessSettings (settingName: "Edit Profile", settingsImage: #imageLiteral(resourceName: "setting_profile")))
        arraySettings.append(BusinessSettings (settingName: "Employees", settingsImage: #imageLiteral(resourceName: "setting_employees")))
        arraySettings.append(BusinessSettings (settingName: "Notification", settingsImage: #imageLiteral(resourceName: "setting_notification")))
        arraySettings.append(BusinessSettings (settingName: "Privacy Policy", settingsImage: #imageLiteral(resourceName: "setting_info")))
        arraySettings.append(BusinessSettings (settingName: "Terms & Conditions", settingsImage: #imageLiteral(resourceName: "setting_info")))
        arraySettings.append(BusinessSettings (settingName: "Delete Account", settingsImage: #imageLiteral(resourceName: "setting_deleteAccount")))
        arraySettings.append(BusinessSettings (settingName: "Add Account", settingsImage: #imageLiteral(resourceName: "setting_addAcount")))
        arraySettings.append(BusinessSettings (settingName: "Logout", settingsImage: #imageLiteral(resourceName: "setting_logout")))
        
        if UtilityClass.getStackedUserInfoData() != nil
        {
            arraySettings.append(BusinessSettings (settingName: "Logout All Accounts", settingsImage: #imageLiteral(resourceName: "setting_logout")))
            arraySettings.append(BusinessSettings (settingName: "Switch Account", settingsImage: #imageLiteral(resourceName: "setting_addAcount")))
        }
        
        tableSettings.reloadData()
    }
    
    //MARK:- Initial table setup
    func setupTableView() -> Void
    {
        self.tableSettings.delegate = self
        self.tableSettings.dataSource = self
        
        //        self.tableBuisnessConnect.contentInset = UIEdgeInsetsMake(64 * scaleFactorX, 0, 0, 0)
    }
    
    func onBackButtonAction() -> Void
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    //MARK:- UITableView Delegate and Datasource
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 1
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let sectionHeader = UIView()
        sectionHeader.backgroundColor = UIColor(RED: 56, GREEN: 71, BLUE: 85, ALPHA: 0.2)
        return sectionHeader
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arraySettings.count
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 55
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 1 || indexPath.row == 5 {
            return 0
        }
        return 55 * scaleFactorX;
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
       
        let cell =  tableView.dequeueReusableCell(withIdentifier: kCellIdentifier_setting_arrowCell) as! SettingsCell
        
//        cell.updateTheCellContents(withIndexpath: indexPath)
        cell.updateTheCellContents(usingModel: arraySettings[indexPath.row])
        
        if indexPath.row==2 {
            cell.imageRightArrow.isHidden=true
            cell.buttonNotificationToggle.isHidden=false
            cell.buttonNotificationToggle.setImage(#imageLiteral(resourceName: "toggle_on").withRenderingMode(.alwaysOriginal), for: .normal)
            cell.buttonNotificationToggle.setImage(#imageLiteral(resourceName: "toggle_off").withRenderingMode(.alwaysOriginal), for: .selected)
            cell.buttonNotificationToggle.addTarget(self, action:#selector(toggleButtonClickedAction(_sender:)), for: .touchUpInside)
            //cell.buttonNotificationToggle.tag=1
        }
        
        if indexPath.row > 9
        {
            cell.imageRightArrow.isHidden=true
            //            cell.imageIcon.isHidden=true
        }
               
        return cell
    }
    
    @IBAction func toggleButtonClickedAction(_sender : UIButton){
        
        _sender.isSelected = !_sender.isSelected
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        selectedIndex = indexPath.row
        
        var controllerToDisplay : UIViewController? = nil
        
        
        switch indexPath.row
        {
        case 0: // Edit profile
            let createProfileVC = UIStoryboard.getCreateProfileStoryboard().instantiateViewController(withIdentifier: "createProfileVC") as! CreateProfileViewController
            
            createProfileVC.isFromSettings = true
            
            self.navigationController?.pushViewController(createProfileVC, animated: true)
        case 1: //Employees
            
            controllerToDisplay = UIStoryboard.getBusinessEmployeeListingFlowStoryboard().instantiateViewController(withIdentifier: "employeeListVC") as! EmployeeListingViewController
            
            break
        case 2: //Notification
            break
            
        case 3: //Privacy Policies
            UtilityClass.openSafariController(usingLink: LinksEnum.privacyPolicy, onViewController: self.navigationController)
            break
            
        case 4: //Terms And Condition
            UtilityClass.openSafariController(usingLink: LinksEnum.termsAndCond, onViewController: self.navigationController)
            break
            
        case 5: //Delete Account
            UtilityClass.showAlertWithTitle(title: "Confirm Delete?", message: "Your account will be deleted permanently, and can not be restored later.", onViewController: self, withButtonArray: ["Delete"], dismissHandler:
                {[weak self] (butotnIndex) in
                    guard let `self` = self else {return}
                    if butotnIndex == 0
                    {
                        self.deleteAccount()
                    }
            })
            break
            
        case 6: //Add account
            logoutCase = .addAccountLogout
            self.logoutAPI()
            break
            
        case 7: //Logout
            logoutCase = .singleLogout
            self.logoutAPI()
            break
            
        case 8: //Logout from all accounts
            logoutCase = .groupLogout
            self.logoutAPI()
            break
            
        case 9: //Switch account
            isSwitchModeEnabled = !isSwitchModeEnabled
            
            let cell = tableView.cellForRow(at: indexPath) as! SettingsCell
            
            cell.imageRightArrow.transform = isSwitchModeEnabled ? CGAffineTransform (rotationAngle: CGFloat(Double.pi)/2) : CGAffineTransform.identity
            
            if let stackedUsers = UtilityClass.getStackedUserInfoData()
            {
                if isSwitchModeEnabled
                {
                    for userInfo in stackedUsers
                    {
                        if userInfo["sid"] as! String == UtilityClass.getUserSidData()!
                        {
                            continue
                        }
                        arraySettings.append(BusinessSettings (settingName: userInfo["username"] as! String, settingsImage: #imageLiteral(resourceName: "setting_employees")))
                    }
                }
                else
                {
                    var count = 0
                    for userInfo in stackedUsers
                    {
                        if userInfo["sid"] as! String == UtilityClass.getUserSidData()!
                        {
                            continue
                        }
                        count = count + 1
                    }
                    let range = arraySettings.endIndex.advanced(by: -count)..<arraySettings.endIndex
                    arraySettings.removeSubrange(range)
                }
                tableView.reloadData()
            }
            break
            
            
        default: // For switching users
            logoutCase = .switchAccount
            logoutAPI()
            break
        }
        
        guard controllerToDisplay != nil else
        {
            return
        }
        self.navigationController?.pushViewController(controllerToDisplay!, animated: true)
    }
    
    //MARK:- Logout API Call
    func logoutAPI() -> Void {
        
        HUD.show(.systemActivity, onView: self.view.window)
        
        self.view.endEditing(true)
        
        let urlToHit = EndPoints.logout(UtilityClass.getUserSidData()!).path
        
        AppWebHandler.sharedInstance().fetchData(fromURL: urlToHit, httpMethod: .get, parameters: nil, shouldDeserialize: true)
        {[weak self] (data, dictionary, statusCode, error) in
            
            HUD.hide()
            
            guard let `self` = self else {return}
            
            if error != nil
            {
                UtilityClass.showAlertWithTitle(title: App_Name, message: error!.localizedDescription, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                
                return
            }
            
            
            
            if dictionary == nil
            {
                UtilityClass.showAlertWithTitle(title: App_Name, message: App_Global_Error_Msg, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                
                return
            }
            
            if statusCode == 203
            {
                let responseDictionary = dictionary!
                guard (responseDictionary["message"] != nil) else
                {
                    UtilityClass.showAlertWithTitle(title: App_Name, message: App_Global_Error_Msg, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                    return
                }
                let messageStr = responseDictionary["message"] as! String
                
                UtilityClass.showAlertWithTitle(title: App_Name, message: messageStr, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                return
            }
            
            if statusCode == 200
            {
                let responseDictionary = dictionary!
                let type = responseDictionary["type"] as! Bool
                let msgStr = responseDictionary["message"] as! String
                
                if type
                {
                    if logoutCase == .addAccountLogout // Adding current user to stack list...
                    {
                        UtilityClass.addCurrentUserToStack()
                    }
                    if logoutCase == .singleLogout // Removing current user from stack list...
                    {
                        UtilityClass.removeCurrentUserFromStack()
                    }
                    if logoutCase == .groupLogout // Removing all user from stack list...
                    {
                        UtilityClass.deleteStackedUserInfoData()
                    }
                    
                    UtilityClass.deleteDataOnLogout()
                    
                    currentUserID = nil
                    SocketManager.sharedInstance().removeIncomingMessagesListener()
                    SocketManager.sharedInstance().disconnectSocket()
                    
                    if UtilityClass.getFirstUserFromStackedList() != nil
                    {
                        self.updateRootViewController()
                        return
                    }
                    
                    let loginVC = UIStoryboard.getLoginFlowStoryboard().instantiateInitialViewController()
                    
                    UtilityClass.changeRootViewController(with: loginVC!)
                }
                else
                {
                    UtilityClass.showAlertWithTitle(title: App_Name, message: msgStr, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                }
            }
        }
    }
    
    func deleteAccount()
    {
        HUD.show(.systemActivity, onView: self.view.window)
        
        self.view.endEditing(true)
        
        let urlToHit = EndPoints.deleteAccount(UtilityClass.getUserSidData()!).path
        
        AppWebHandler.sharedInstance().fetchData(fromURL: urlToHit, httpMethod: .get, parameters: nil, shouldDeserialize: true)
        {[weak self] (data, dictionary, statusCode, error) in
            
            HUD.hide()
            
            guard let `self` = self else {return}
            
            if error != nil
            {
                UtilityClass.showAlertWithTitle(title: App_Name, message: error!.localizedDescription, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                
                return
            }
            
            if dictionary == nil
            {
                UtilityClass.showAlertWithTitle(title: App_Name, message: App_Global_Error_Msg, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                
                return
            }
            
            if statusCode == 203
            {
                let responseDictionary = dictionary!
                guard (responseDictionary["message"] != nil) else
                {
                    UtilityClass.showAlertWithTitle(title: App_Name, message: App_Global_Error_Msg, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                    return
                }
                let messageStr = responseDictionary["message"] as! String
                
                UtilityClass.showAlertWithTitle(title: App_Name, message: messageStr, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                return
            }
            
            if statusCode == 200
            {
                let responseDictionary = dictionary!
                let type = responseDictionary["type"] as! Bool
                let msgStr = responseDictionary["message"] as! String
                
                if type
                {
                    UtilityClass.removeCurrentUserFromStack()
                    UtilityClass.deleteDataOnLogout()
                    
                    currentUserID = nil
                    SocketManager.sharedInstance().removeIncomingMessagesListener()
                    SocketManager.sharedInstance().disconnectSocket()
                    
                    if UtilityClass.getFirstUserFromStackedList() != nil
                    {
                        self.updateRootViewController()
                        return
                    }
                    
                    let loginVC = UIStoryboard.getLoginFlowStoryboard().instantiateInitialViewController()
                    
                    UtilityClass.changeRootViewController(with: loginVC!)
                }
                else
                {
                    UtilityClass.showAlertWithTitle(title: App_Name, message: msgStr, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                }
            }
        }
    }
    
    func updateRootViewController()
    {
        if logoutCase == .switchAccount || logoutCase == .singleLogout
        {
            if logoutCase == .switchAccount
            {
                UtilityClass.switchCurrentUserWithUser(self.arraySettings[self.selectedIndex].settingName)
            }
            else
            {
                UtilityClass.saveUserInfoData(userDict: UtilityClass.getFirstUserFromStackedList()!)
            }
            
            if UtilityClass.getUserTypeData() == AppUserTypeEnum.employee.rawValue
            {
                let employeeVC = UIStoryboard.getBusinessHomeFlowStoryboard().instantiateInitialViewController()
                
                UtilityClass.changeRootViewController(with: employeeVC!)
                
                return
            }
            
            if UtilityClass.getUserTypeData() == AppUserTypeEnum.provider.rawValue
            {
                let businessVC = UIStoryboard.getBusinessTabBarFlowStoryboard().instantiateInitialViewController()
                
                UtilityClass.changeRootViewController(with: businessVC!)
                return
            }
            
            let userHomeVC = UIStoryboard.getUserTabsStoryboard().instantiateInitialViewController()
            
            UtilityClass.changeRootViewController(with: userHomeVC!)
            
            return
        }
        let loginVC = UIStoryboard.getLoginFlowStoryboard().instantiateInitialViewController()
        
        UtilityClass.changeRootViewController(with: loginVC!)
    }
}
