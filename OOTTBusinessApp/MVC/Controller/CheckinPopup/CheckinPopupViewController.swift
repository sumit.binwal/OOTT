//
//  CheckinPopupViewController.swift
//  OOTTBusinessApp
//
//  Created by Santosh on 16/01/18.
//  Copyright © 2018 KonstantInfoSolutions. All rights reserved.
//

import UIKit
import PKHUD
import CoreLocation

class ModelCheckinPopup
{
    var businessID: String?
    var businessName: String?
    var option1: String?
    var option2: String?
    
    
    init() {
        
    }
    
    func updateModel(usingDictionary dictionary: [String:Any]) {
        businessID = dictionary["_id"] as? String
        businessName = dictionary["name"] as? String
    }
    
    func getDictionary() -> [String:Any] {
        return [
            "business_id" : businessID!,
            "lineWaiting" : option1!,
            "crowded" : option2!,
            "latitude" : String (LocationManager.sharedInstance().newLatitude),
            "longitude" : String (LocationManager.sharedInstance().newLongitude)
        ]
    }
}

struct CheckinQA {
    let question: String
    let option1: String
    let option2: String
    let option3: String
    let option4: String
}

protocol CheckinPopupViewControllerDelegate: class {
    func checkinPopUp(_ controller: CheckinPopupViewController, didTapOnSoloButton soloButton: UIButton)
    func checkinPopUp(_ controller: CheckinPopupViewController, didTapOnGroupButton groupButton: UIButton)
    
    func checkinPopUp(_ controller: CheckinPopupViewController, didTapOnUberButton button: UIButton)
    func checkinPopUp(_ controller: CheckinPopupViewController, didTapOnLyftButton button: UIButton)
}

class CheckinPopupViewController: UIViewController {

    enum CheckinPopupType {
        case checkin
        case makeMove
        case uberLyftPopup
    }
    
    weak var delegate: CheckinPopupViewControllerDelegate?
    
    var popupType: CheckinPopupType = .makeMove
    
    var pickupLocation: CLLocation?
    var dropoffLocation: CLLocation?
        
    @IBOutlet weak var bottomButton: UIButton!
    @IBOutlet weak var tableCheckinPopup: UITableView!
    
    @IBOutlet weak var labelCheckinHeader: UILabel!
    var modelCheckin = ModelCheckinPopup ()
    
    @IBOutlet weak var constraintTableHeight: NSLayoutConstraint!
    let arrayQA: [CheckinQA] = [
        CheckinQA (question: "How was the line?", option1: "No line", option2: "Less than 10 min.", option3: "20 min.", option4: "30 min. or more"),
        CheckinQA (question: "How crowded?", option1: "Starting up", option2: "Lively", option3: "Crowded", option4: "Packed")
    ]
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        setupView()
        setupTableview()
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        
        tableCheckinPopup.layer.cornerRadius = 5 * scaleFactorX
//        tableCheckinPopup.layer.borderColor = UIColor.white.cgColor
//        tableCheckinPopup.layer.borderWidth = 1
        
        if popupType == .makeMove {
            tableCheckinPopup.tableHeaderView = nil
            tableCheckinPopup.tableFooterView = nil
        }
        if popupType == .uberLyftPopup {
            tableCheckinPopup.tableHeaderView = nil
            bottomButton.setTitle("No thanks", for: .normal)
        }
        constraintTableHeight.constant = tableCheckinPopup.contentSize.height
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setupView() {
        labelCheckinHeader.text = "Welcome to "
        if modelCheckin.businessName != nil {
            labelCheckinHeader.text = "Welcome to " + modelCheckin.businessName!
        }
        labelCheckinHeader.text = labelCheckinHeader.text?.uppercased()
    }
    
    func setupTableview() {
        tableCheckinPopup.delegate = self
        tableCheckinPopup.dataSource = self
        tableCheckinPopup.isScrollEnabled = false
//        tableCheckinPopup.layer.cornerRadius = 4 * scaleFactorX
    }
    
    func updateModel(usingDictionary dictionary: [String:Any]) {
        modelCheckin.updateModel(usingDictionary: dictionary)
    }

    @IBAction func onCloseButtonAction(_ sender: UIButton) {
        if popupType == .checkin {
            isCheckinPopupVisible = false
        }
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func onSubmitButtonAction(_ sender: UIButton) {
        if popupType != .checkin {
            onCloseButtonAction(sender)
            return;
        }
        if validateFields() {
            // call api
            HUD.show(.systemActivity, onView: self.view)
            SocketManager.sharedInstance().checkinSubmitOptions(modelCheckin, messageHandler:
                {[weak self] (response, statusCode) in
                    HUD.hide()
                    
                    userDefault.set(LocationManager.sharedInstance().newLatitude, forKey: UserDefaults.MyApp.checkinLatitude)
                    userDefault.set(LocationManager.sharedInstance().newLongitude, forKey: UserDefaults.MyApp.checkinLongitude)
                    
                    guard let `self` = self else {return}
                    self.onCloseButtonAction(UIButton ())
            })
        }
    }
    
    func validateFields() -> Bool {
        if modelCheckin.option1 == nil || modelCheckin.option2 == nil {
            UtilityClass.showAlertWithTitle(title: App_Name, message: "Please select both answers", onViewController: self, withButtonArray: nil, dismissHandler: nil)
            return false
        }
        return true
    }
}


//MARK: -
//MARK: - extension UITableViewDelegate
extension CheckinPopupViewController: UITableViewDelegate, UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if popupType == .makeMove || popupType == .uberLyftPopup {
            return 1
        }
        return arrayQA.count
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 44
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if popupType == .makeMove {
            return 277
        }
        if popupType == .uberLyftPopup {
            return 233
        }
        return 174 * scaleFactorX
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if popupType == .makeMove {
            let cell: CheckinPopMakeMoveCell = tableView.dequeueReusableCell(withIdentifier: kCellIdentifier_CheckinPopupMakeMoveCell) as! CheckinPopMakeMoveCell
            cell.delegate = self
            return cell
        }
        
        if popupType == .uberLyftPopup {
            let cell: CheckinPopupUberCell = tableView.dequeueReusableCell(withIdentifier: kCellIdentifier_CheckinPopupUberCell) as! CheckinPopupUberCell
            cell.delegate = self
//            let loc1 = CLLocation (latitude: LocationManager.sharedInstance().newLatitude, longitude: LocationManager.sharedInstance().newLongitude)
            cell.updateCell(pickupLocation: pickupLocation!, dropoffLocation: dropoffLocation!)
            return cell
        }
        
        let cell: CheckinPopupCell = tableView.dequeueReusableCell(withIdentifier: kCellIdentifier_CheckinPopupCell) as! CheckinPopupCell
        cell.delegate = self
        cell.tag = indexPath.row
        cell.cellType = indexPath.row == 0 ? CheckinPopupCellEnum.lineWait : CheckinPopupCellEnum.crowded
        cell.updateCell(usingModel: arrayQA[indexPath.row])
        return cell
    }
}

//MARK: -
//MARK: -
extension CheckinPopupViewController: CheckinPopupCellDelegate
{
    func checkinPopupCell(_ cell: CheckinPopupCell, didUpdateWithOption option: String)
    {
        if cell.tag == 0 {
            modelCheckin.option1 = option
        }
        else
        {
            modelCheckin.option2 = option
        }
    }
}

//MARK: -
//MARK: -
extension CheckinPopupViewController: CheckinPopMakeMoveCellDelegate, CheckinPopupUberCellDelegate
{
    func checkinUberCell(_ cell: CheckinPopupUberCell, didTapOnUberButton uberButton: UIButton) {
        delegate?.checkinPopUp(self, didTapOnUberButton: uberButton)
    }
    
    func checkinUberCell(_ cell: CheckinPopupUberCell, didTapOnLyftButton lyftButton: UIButton) {
        delegate?.checkinPopUp(self, didTapOnLyftButton: lyftButton)
    }
    
    func checkinMakeMoveCell(_ cell: CheckinPopMakeMoveCell, didTapOnSoloButton soloButton: UIButton) {
        delegate?.checkinPopUp(self, didTapOnSoloButton: soloButton)
    }
    
    func checkinMakeMoveCell(_ cell: CheckinPopMakeMoveCell, didTapOnGroupButton groupButton: UIButton) {
        delegate?.checkinPopUp(self, didTapOnGroupButton: groupButton)
    }
    
    func checkinMakeMoveCell(_ cell: CheckinPopMakeMoveCell, didTapOnCancelButton cancelButton: UIButton) {
        onCloseButtonAction(cancelButton)
    }
}
