//
//  LoginViewController.swift
//  OOTTUserApp
//
//  Created by Santosh on 06/06/17.
//  Copyright © 2017 KonstantInfoSolutions. All rights reserved.
//

import UIKit
import PKHUD

//MARK:- CLASS IMPLEMENTATION
class LoginViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, LoginCellDelegate, UITextFieldDelegate {

    //MARK:- Login TableView
    @IBOutlet weak var tableLoginView: UITableView!
    
    //MARK:- Bottom View
    @IBOutlet weak var bottomContainerView: UIView!
    
    //MARK:- Array Holding Values
    var arrayModelLogin : [ModelLogin] = []
    
    var dictFacebookData : [String:Any] = [:]
    
    var shouldRemember = false
    
    
    //MARK:- viewDidLoad
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.loadDummyData()
        self.setupTableView()
    }

    //MARK:- didReceiveMemoryWarning
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK:- deinit
    deinit {
        print("LoginViewController Deinit")
    }
    
    //MARK:- Load Dummy Data
    func loadDummyData() -> Void
    {
        var emailValue = ""
        var passValue = ""
        
        if UtilityClass.shouldRemember {
            shouldRemember = true
            emailValue = UtilityClass.savedUsername
            passValue = UtilityClass.savedPassword
        }
        else
        {
            shouldRemember = false
            UtilityClass.savedUsername = ""
            UtilityClass.savedPassword = ""
        }
        arrayModelLogin.append(ModelLogin(withKey: LoginEnum.email.rawValue, value: emailValue))
        
        arrayModelLogin.append(ModelLogin(withKey: LoginEnum.password.rawValue, value: passValue))
    }
    
    //MARK:- TableView Setup
    func setupTableView() -> Void
    {
        self.tableLoginView.delegate=self;
        self.tableLoginView.dataSource = self;
//        self.tableLoginView.contentInset=UIEdgeInsetsMake(56 * scaleFactorX, 0, 0, 0)
    }
    
    //MARK:- UITableView Datasource and Delegate
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 200
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if (indexPath.row == 2)
        {
            return 200 * scaleFactorX;
        }
        return 64 * scaleFactorX;
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell : LoginInputCell
        
        var cellIdentifier = kCellIdentifier_login_input
        
        var newCellType = CellType.none
        
        switch indexPath.row
        {
            
        case 0:
            cellIdentifier = kCellIdentifier_login_input
            newCellType = .email
            
        case 1:
            cellIdentifier = kCellIdentifier_login_input
            newCellType = .pass
            
        case 2:
            cellIdentifier = kCellIdentifier_login_button
            newCellType = .none
        default:
            break
            
        }
        
        cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier) as! LoginInputCell
        
        cell.cellType = newCellType;
        cell.loginDelegate = nil;
        cell.loginDelegate = self;
        
        if cellIdentifier !=  kCellIdentifier_login_button{
            cell.updateValue(usingModel: arrayModelLogin[indexPath.row])
        }
        else
        {
            if cell.buttonRememberMe != nil
            {
            cell.buttonRememberMe.setImage(shouldRemember ? #imageLiteral(resourceName: "radioSelect") : #imageLiteral(resourceName: "radioDeselected"), for: .normal)
            }
        }
        
        return cell
    }
    
    //MARK:- New Account Button Action
    @IBAction func onNewAccountAction(_ sender: UIButton)
    {
        let signupVC = UIStoryboard.getSignUpFlowStoryboard().instantiateInitialViewController() as! SignUpViewController
        
        self.navigationController?.pushViewController(signupVC, animated: true)
    }
    
    //MARK:- LoginInputCell Delegate -> Login Button Action
    func loginCell(cell: LoginInputCell, didTapOnLoginButton: UIButton) {
        
        let isVerified = validateTheInputFields()
        
        if isVerified
        {
           print("yes finally")
            callLoginApi()
        }
    }
    
    //MARK:- LoginInputCell Delegate -> Facebook Button Action
    func loginCell(cell: LoginInputCell, didTapOnFacebookButton: UIButton)
    {
        
        HUD.show(.systemActivity, onView: self.view)
        
        FacebookHelper.loginFacebook(withReadPermissions: ["public_profile","email"]) { (resultDict) in
            
            resultDict.forEach({ (key,value) in
                print(key,"=",value)
            })
            if resultDict["error"] != nil
            {
                HUD.hide()
                UtilityClass.showAlertWithTitle(title: App_Name, message: resultDict["error"] as? String, onViewController: self, withButtonArray: ["OK"], dismissHandler: nil)
            }
            else
            {
                self.processFacebookData(withLoginData: resultDict)
            }
        }
        
    }
    
    //MARK:- LoginInputCell Delegate -> Forgot Password Button Action
    func loginCell(cell: LoginInputCell, didTapOnForgotPasswordButton: UIButton) {
        let forgotVC = self.storyboard?.instantiateViewController(withIdentifier: "forgotVC") as! ForgotPasswordViewController
        self.navigationController?.pushViewController(forgotVC, animated: true)
    }
    
    //MARK:- LoginInputCell Delegate -> Remember me Button Action
    func loginCell(cell: LoginInputCell, didTapOnRememberMeButton: UIButton) {
        shouldRemember = !shouldRemember
        tableLoginView.reloadRows(at: [IndexPath (row: 2, section: 0)], with: .none)
    }
    
    //MARK:- LoginInputCell Delegate -> Textfield updated Action
    func loginCell(cell: LoginInputCell, updatedInputfieldText: String) {
        if cell.model.keyName != LoginEnum.none.rawValue
        {
            let isAlreadyContained = arrayModelLogin.contains { (model) -> Bool in
                return model.keyName == cell.model.keyName
            }
            if !isAlreadyContained
            {
                arrayModelLogin.append(cell.model)
            }
            else
            {
                let newModel = cell.model
                newModel.keyValue = updatedInputfieldText
                let index = arrayModelLogin.index(where: { (model) -> Bool in
                    model.keyName == newModel.keyName
                })
                arrayModelLogin[index!].keyValue = newModel.keyValue
//                arrayModelLogin.remove(at: index!)
//                arrayModelLogin.append(newModel)
            }
        }
    }
    
    //MARK:- Validate input fields
    func validateTheInputFields() -> Bool
    {
        if arrayModelLogin.count == 0
        {
            UtilityClass.showAlertWithTitle(title: "", message: "Please provide email address", onViewController: self, withButtonArray: nil, dismissHandler: nil)
            
            return false
        }
        
        for model in arrayModelLogin
        {
            switch model.keyName {
            case LoginEnum.email.rawValue:
                if model.keyValue.isEmptyString()
                {
                    UtilityClass.showAlertWithTitle(title: "", message: "Please provide email address", onViewController: self, withButtonArray: nil, dismissHandler: nil)
                    return false
                }
                
                if !model.keyValue.isValidEmailAddress()
                {
                    UtilityClass.showAlertWithTitle(title: "", message: "Please provide valid email address", onViewController: self, withButtonArray: nil, dismissHandler: nil)
                    return false
                }
                
            default:
                if model.keyValue.isEmptyString()
                {
                    UtilityClass.showAlertWithTitle(title: "", message: "Please provide password", onViewController: self, withButtonArray: nil, dismissHandler: nil)
                    return false
                }
            }
        }
        return true
    }
    
    //MARK:- Login Api Call
    func callLoginApi() -> Void
    {
        HUD.show(.systemActivity, onView: self.view.window)
        
        self.view.endEditing(true)
        
        var params = [String:String]()
        
        for model in arrayModelLogin
        {
            params[model.keyName] = model.keyValue
        }
        params["deviceToken"] = appDeviceToken
        
        let urlToHit = isFromFacebook ? EndPoints.socialLogin.path : EndPoints.login.path
        
        AppWebHandler.sharedInstance().fetchData(fromURL: urlToHit, httpMethod: .post, parameters: params, shouldDeserialize: true)
        {[weak self] (data, dictionary, statusCode, error) in
            
            HUD.hide()
            
            guard let `self` = self else {return}
            
            if error != nil
            {
                isFromFacebook = false
                UtilityClass.showAlertWithTitle(title: App_Name, message: error!.localizedDescription, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                
                return
            }
            
            if dictionary == nil
            {
                isFromFacebook = false
                UtilityClass.showAlertWithTitle(title: App_Name, message: App_Global_Error_Msg, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                
                return
            }
            
            if statusCode == 203
            {
                isFromFacebook = false
                let responseDictionary = dictionary!
                guard (responseDictionary["message"] != nil) else
                {
                    UtilityClass.showAlertWithTitle(title: App_Name, message: App_Global_Error_Msg, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                    return
                }
                let messageStr = responseDictionary["message"] as! String
                
                UtilityClass.showAlertWithTitle(title: App_Name, message: messageStr, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                return
            }
            
            if statusCode == 200
            {
                let responseDictionary = dictionary!
                let replyType = responseDictionary["type"] as! Bool
                print(responseDictionary)
                if !replyType
                {
                    //sign up screen
                    isFromFacebook = false
                    UtilityClass.showAlertWithTitle(title: "", message: "Account does not exist", onViewController: self, withButtonArray: ["Go To SignUp?"], dismissHandler: {[weak self] (buttonIndex) in
                        guard let `self` = self else {return}
                        if buttonIndex == 0
                        {
                            self.onNewAccountAction(UIButton())
                        }
                    })
                }
                else
                {
                    // login...
                    guard responseDictionary["data"] != nil else
                    {
                        isFromFacebook = false
                        UtilityClass.showAlertWithTitle(title: App_Name, message: App_Global_Error_Msg, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                        return
                    }
                    
                    let userDataDict = (responseDictionary["data"] as! [String : Any] )
                    
                    UtilityClass.saveUserInfoData(userDict: userDataDict)
                    
                    if self.shouldRemember {
                        UtilityClass.shouldRemember = true
                        UtilityClass.savedPassword = self.arrayModelLogin[1].keyValue
                        UtilityClass.savedUsername = self.arrayModelLogin[0].keyValue
                    }
                    else
                    {
                        UtilityClass.shouldRemember = false
                        UtilityClass.savedPassword = ""
                        UtilityClass.savedUsername = ""
                    }
                    
                    if logoutCase == .addAccountLogout // Adding current user to stack list...
                    {
                        logoutCase = .none
                        UtilityClass.addCurrentUserToStack()
                    }
                    
                    if userDataDict["userType"] != nil
                    {
                        let userType = userDataDict["userType"] as! String
                        
                        if userType == AppUserTypeEnum.user.rawValue
                        {
                            // User home page
                            self.goToUserHomeScreen()
                        }
                        else if userType == AppUserTypeEnum.provider.rawValue
                        {
                            // business home page
                            let isProfileUpdateDone = userDataDict["isProfileDone"] as! Bool
                            
                            if !isProfileUpdateDone
                            {
                                self.goToCreateProfileScreen()
                            }
                            else
                            {
                                self.goToBusinessHomeScreen()
                            }
                        }
                        else
                        {
                            // Bouncer home page
                            self.goToBouncerDashboard()
                        }
                    }
                }
            }
        }
    }
    
    //MARK:- Handle Facebook Data
    func processFacebookData(withLoginData loginData:[String:Any]) -> Void
    {
        isFromFacebook = true
        
        dictFacebookData = loginData
        
        if dictFacebookData["email"] != nil
        {
            arrayModelLogin[0].keyValue = dictFacebookData["email"] as! String
        }
        
        if dictFacebookData["id"] != nil
        {
            let isAlreadyContained = arrayModelLogin.contains { (model) -> Bool in
                return model.keyName == LoginEnum.socialId.rawValue
            }
            if isAlreadyContained
            {
                let index = arrayModelLogin.index(where: { (model) -> Bool in
                    model.keyName == LoginEnum.socialId.rawValue
                })
                arrayModelLogin.remove(at: index!)
                
                let index1 = arrayModelLogin.index(where: { (model) -> Bool in
                    model.keyName == LoginEnum.sourceType.rawValue
                })
                arrayModelLogin.remove(at: index1!)
            }
            
            
            let keyValue = dictFacebookData["id"] as! String
            arrayModelLogin.append(ModelLogin(withKey: LoginEnum.socialId.rawValue, value: keyValue))
            
            arrayModelLogin.append(ModelLogin(withKey: LoginEnum.sourceType.rawValue, value: "facebook"))
        }
        
        arrayModelLogin.forEach { (model) in
            print("********** ",model.keyName," = ",model.keyValue)
        }
        
        dictFacebookData.removeAll()
        
        HUD.hide()
        
        callLoginApi()
    }
    
    //MARK:- Go To Create Profile
    func goToCreateProfileScreen() -> Void
    {
        appDelegate?.window??.makeToast("Please complete profile updation", duration: 2, position: .bottom)
        
        let createProfileVC = UIStoryboard.getCreateProfileStoryboard().instantiateInitialViewController()
        
        UtilityClass.changeRootViewController(with: createProfileVC!)
    }
    
    //MARK:- Go To Business Home
    func goToBusinessHomeScreen() -> Void
    {
        let businessVC = UIStoryboard.getBusinessTabBarFlowStoryboard().instantiateInitialViewController()
        
        UtilityClass.changeRootViewController(with: businessVC!)
    }
    
    //MARK:- Go To User Home
    func goToUserHomeScreen() -> Void
    {
        let userVC = UIStoryboard.getUserTabsStoryboard().instantiateInitialViewController()
        
        UtilityClass.changeRootViewController(with: userVC!)
    }
    
    func goToBouncerDashboard() -> Void
    {
        let dashboardVC = UIStoryboard.getBusinessHomeFlowStoryboard().instantiateInitialViewController()
        
        UtilityClass.changeRootViewController(with: dashboardVC!)
    }
}
