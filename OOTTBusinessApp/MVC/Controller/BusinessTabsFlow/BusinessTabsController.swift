//
//  BusinessTabsController.swift
//  OOTTBusinessApp
//
//  Created by Santosh on 20/12/17.
//  Copyright © 2017 KonstantInfoSolutions. All rights reserved.
//

import UIKit
import PKHUD
import SDWebImage
import InAppNotify

class BusinessTabsController: UITabBarController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        setupLayouts()
        initiateSocket()
    }
    
    //MARK:- deinit
    deinit {
        print("BusinessTabsController Deinit")
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    // MARK:- Initial layout setup
    func setupLayouts() {
        
        self.tabBar.backgroundImage = #imageLiteral(resourceName: "tabsBackground").resizableImage(withCapInsets: UIEdgeInsetsMake(0, 0, 0, 0), resizingMode: .stretch)
        
        let numberOfItems = CGFloat((self.tabBar.items!.count))
        
        let tabBarItemSize = CGSize(width: (self.tabBar.frame.width) / numberOfItems,
                                    height: (self.tabBar.frame.height+50))
//        self.tabBar.selectionIndicatorImage = UIImage.imageWithColor(color: UIColor.init(white: 0, alpha: 0.3), size: tabBarItemSize).resizableImage(withCapInsets: .zero)
        
        self.tabBar.selectionIndicatorImage = UIImage.imageWithColor(color: UIColor.init(white: 0, alpha: 0.3), size: tabBarItemSize).resizableImage(withCapInsets: UIEdgeInsets.init(top: 0, left: 0, bottom: 1, right: 0))

    }
    
    // MARK:- Tab bar select index
    func updateTabBar(withIndex selectedIndex: Int)
    {
        self.selectedIndex = selectedIndex
    }

    //MARK:- Socket Connection
    func initiateSocket()
    {
        SocketManager.sharedInstance().userSID = UtilityClass.getUserSidData()!
        SocketManager.sharedInstance().connectSocket
            {[weak self] (data, ack) in
                
                guard let `self` = self else { return }
                
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: NOTIFICATION_SOCKET_CONNECTED), object: nil)
                self.applyReceiveMessageListener()
        }
    }
    
    // MARK:- Message Listener (New Message)
    func applyReceiveMessageListener()
    {
        // Group message listner
        SocketManager.sharedInstance().receiveIncomingGroupMessages
            {[weak self] (messageDict, statusCode) in
                if statusCode == 200
                {
                    guard let `self` = self else {return}
                    self.manageTheIncomingMessage(usingDictionary: messageDict)
                }
        }
        
    }
    
    func manageTheIncomingMessage(usingDictionary dictionary:[String:Any])
    {
        let model = self.getNewModel(usingDictionary: dictionary)
        
        // If chat view is open...
        if let groupChatVC = appDelegate?.window??.visibleViewController() as? GroupChatViewController
        {
            // Simply add new message model...
            groupChatVC.addNewModel(usingDictionary: dictionary)
        }
        else
        {
            self.createInAppNotification(usingModel: model)
        }
    }
    
    // MARK:- Message Model Creation
    func getNewModel(usingDictionary dictionary : [String:Any]) -> ModelChatMessage
    {
        let model = ModelChatMessage () // Model creation
        model.updateModel(usingDictionary: dictionary) // Updating model
        return model
    }
    
    // MARK:- InApp Notification Fire
    func createInAppNotification(usingModel model:ModelChatMessage)
    {
        let title = model.roomID.isEmpty ? model.senderName : String (model.senderName+" @ "+model.groupName)
        
        let picture = model.roomID.isEmpty ? model.senderPicture : model.groupPicture
        
        var message = model.message
        
        if model.isImageMedia {
            message = "Sent a Photo"
        }
        if model.isVideoMedia {
            message = "Sent a Video"
        }
        
        let announce = Announcement (title: title, subtitle: message, image: nil, urlImage: picture, duration: 5, interactionType: .none, userInfo: model)
        {[weak self] (callBackType, str, announce) in
            
            guard let `self` = self else { return }
            
            if callBackType == CallbackType.tap
            {
                if let chatInfo = announce.userInfo as? ModelChatMessage
                {
                    let isGroup = !chatInfo.roomID.isEmpty
                    
                    // open screen
                    let chatViewController = UIStoryboard.getChatViewStoryboard().instantiateInitialViewController() as! GroupChatViewController
                    
                    chatViewController.hidesBottomBarWhenPushed = true
                    
                    chatViewController.isGroupChat = isGroup
                    
                    chatViewController.isBusinessChat = chatInfo.isBusinessGroup
                    
                    chatViewController.chatUsername.text = isGroup ? chatInfo.groupName : chatInfo.senderName
                    
                    if isGroup
                    {
                        chatViewController.chatPicture = chatInfo.groupPicture
                        
                        chatViewController.chatRoomID = chatInfo.roomID
                    }
                    else
                    {
                        chatViewController.chatUserID = chatInfo.senderID
                    }
                    
                    self.navigationController?.pushViewController(chatViewController, animated: true)
                }
            }
        }
        
        InAppNotify .Show(announce, to: self.navigationController!)
    }
}
