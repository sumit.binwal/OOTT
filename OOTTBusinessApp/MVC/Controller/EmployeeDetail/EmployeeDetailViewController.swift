//
//  EmployeeDetailViewController.swift
//  OOTTBusinessApp
//
//  Created by Sumit Sharma on 26/06/2017.
//  Copyright © 2017 KonstantInfoSolutions. All rights reserved.
//

import Foundation
import UIKit
import PKHUD
class EmployeeDetailViewController: UIViewController {
    var modelEmployeeDetail : EmployeeListModel?
    
    @IBOutlet var imageEmployee: UIImageView!
    @IBOutlet var labelEmployeeName: UILabel!
    @IBOutlet var labelEmployeeEmail: UILabel!
    @IBOutlet var labelEmployeeType: UILabel!
    
    override func viewDidLoad() {
     
        self.setupView()
        
    }

    
    func setupView() -> Void
    {
        self.title = "Employee Details".uppercased()
        
        self.showNavigationBar()
        
        let leftBarButton = self.setLeftBarButtonItem(withImage: #imageLiteral(resourceName: "backButton"))
        leftBarButton.action = #selector(onBackButtonAction)
        leftBarButton.target = self
        
        
        imageEmployee.layer.cornerRadius=imageEmployee.frame.size.width/2*scaleFactorX
        imageEmployee.clipsToBounds = true
        imageEmployee .sd_setImage(with: modelEmployeeDetail!.employeeImageURL, completed: nil)
        
        labelEmployeeName.makeAdaptiveFont()
        labelEmployeeEmail.makeAdaptiveFont()
        labelEmployeeType.makeAdaptiveFont()
        
        labelEmployeeName.text=modelEmployeeDetail!.employeeName
        labelEmployeeEmail.text=modelEmployeeDetail!.employeeEmail
        labelEmployeeType.text=modelEmployeeDetail!.employeePosition
    }
    
    
    deinit {
        print("EmployeeDetailViewController Deinit")
    }
    
    //MARK:- Back Button Click Action
    func onBackButtonAction() -> Void
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    //MARK:- Delete Button Click Action
    @IBAction func deleteButtonAction(_ sender: UIButton)
    {
        UtilityClass.showAlertWithTitle(title: App_Name, message: "Are you sure you want to delete this user?", onViewController: self, withButtonArray: ["Delete"], cancelButtonTitle: "Cancel")
        {[weak self] (buttonIndex) in
            guard let `self` = self else {return}
            if buttonIndex==0
            {
                self .deleteUserApi()
            }
        }
    }
    
    //MARK:- Edit Button Click Action
    @IBAction func editButtonAction(_ sender: UIButton)
    {
        //addEmployeeVC
        let addEmployeeVC = self.storyboard?.instantiateViewController(withIdentifier: "addEmployeeVC") as! AddEmployeeViewController
        
        //employeeDetailVC.modelEmployeeDetail=arrayEmployeeList[indexPath.row]
        
        addEmployeeVC.arrayModelLogin.append(ModelLogin(withKey: LoginEnum.fullname.rawValue, value: modelEmployeeDetail!.employeeName))
        addEmployeeVC.arrayModelLogin.append(ModelLogin(withKey: LoginEnum.employeeRole.rawValue, value: modelEmployeeDetail!.employeePosition))
        addEmployeeVC.arrayModelLogin.append(ModelLogin(withKey: LoginEnum.email.rawValue, value: modelEmployeeDetail!.employeeEmail))
        addEmployeeVC.arrayModelLogin.append(ModelLogin(withKey: LoginEnum.password.rawValue, value: ""))
        addEmployeeVC.arrayModelLogin.append(ModelLogin(withKey: LoginEnum.confirmPassword.rawValue, value: ""))
        
        addEmployeeVC.profilePicButton .sd_setImage(with: modelEmployeeDetail?.employeeImageURL, for: .normal, completed: nil)
        addEmployeeVC.isImageSelected=true
        addEmployeeVC.isFromEmployeeDetail=true
        addEmployeeVC.employeeID = modelEmployeeDetail!.employeeID

        self.navigationController?.pushViewController(addEmployeeVC, animated: true)
    }
    
    //MARK:- Delete Employee Api
    func deleteUserApi() -> Void
    {
        
            HUD.show(.systemActivity, onView: self.view.window)
            
            self.view.endEditing(true)
            
            let urlToHit = EndPoints.deleteEmployee(UtilityClass.getUserSidData()!, modelEmployeeDetail!.employeeID).path
            
            AppWebHandler.sharedInstance().fetchData(fromURL: urlToHit, httpMethod: .get, parameters: nil, shouldDeserialize: true)
            {[weak self] (data, dictionary, statusCode, error) in
                
                HUD.hide()
                
                guard let `self` = self else {return}
                
                if error != nil
                {
                    UtilityClass.showAlertWithTitle(title: App_Name, message: error!.localizedDescription, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                    
                    return
                }
                
                if dictionary == nil
                {
                    UtilityClass.showAlertWithTitle(title: App_Name, message: App_Global_Error_Msg, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                    
                    return
                }
                
                if statusCode == 203
                {
                    let responseDictionary = dictionary!
                    guard (responseDictionary["message"] != nil) else
                    {
                        UtilityClass.showAlertWithTitle(title: App_Name, message: App_Global_Error_Msg, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                        return
                    }
                    let messageStr = responseDictionary["message"] as! String
                    
                    UtilityClass.showAlertWithTitle(title: App_Name, message: messageStr, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                    return
                }
                
                if statusCode == 200
                {
                    let responseDictionary = dictionary!
                    
                    let responseMsg=responseDictionary["message"] as! String
                    let type=responseDictionary["type"] as! Bool
                    if type
                    {
                        UtilityClass.removeUserFromStack(UtilityClass.getUserUsernameData()!)
                        UtilityClass.showAlertWithTitle(title: App_Name, message: responseMsg, onViewController: self, withButtonArray: nil, dismissHandler: { (buttonIndex) in
                            self.navigationController?.popViewController(animated: true)
                        })
                    }
                    else
                    {
                        UtilityClass.showAlertWithTitle(title: App_Name, message: responseMsg, onViewController: self, withButtonArray: nil, dismissHandler: { (buttonIndex) in
                        })
                    }
                }
            }
            
        
    }
}
