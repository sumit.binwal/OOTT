//
//  EventDetailViewController.swift
//  OOTTBusinessApp
//
//  Created by Sumit Sharma on 27/06/2017.
//  Copyright © 2017 KonstantInfoSolutions. All rights reserved.
//

import Foundation
import UIKit
import PKHUD
class EventDetailViewController: UIViewController {
    var modelEventList : EventListModel?
    
    
    @IBOutlet var labelEventTitle: UILabel!
    @IBOutlet var labelEventDetail: UILabel!
    @IBOutlet var labelEventDate: UILabel!
    @IBOutlet var labelEventTime: UILabel!
    @IBOutlet var labelEventEndTime: UILabel!
    @IBOutlet var labelEventEndDate: UILabel!
    @IBOutlet var viewTimeContainer: UIView!
    @IBOutlet var viewDateContainer: UIView!
    @IBOutlet var vwEndTime: UIView!
    @IBOutlet var vwEndDate: UIView!
    
    var isViewOnlyMode = false
    
    override func viewDidLoad() {
        
        self.setupView()
        self.fillDetail()
    }
    
    func setupView() -> Void
    {
        self.title = modelEventList?.eventName
        
        self.showNavigationBar()
        
        let leftBarButton = self.setLeftBarButtonItem(withImage: #imageLiteral(resourceName: "backButton"))
        leftBarButton.action = #selector(onBackButtonAction)
        leftBarButton.target = self
        
        viewTimeContainer.layer.cornerRadius=5;
        viewDateContainer.layer.cornerRadius=5;
        vwEndDate.layer.cornerRadius=5;
        vwEndTime.layer.cornerRadius=5;
        
        if isViewOnlyMode {
            self.view.viewWithTag(201)?.isHidden = true
            self.view.viewWithTag(202)?.isHidden = true
        }
       
    }
    
    func fillDetail() -> Void {
        labelEventDetail.text=modelEventList?.eventDiscription
        labelEventTitle.text=modelEventList?.eventName
        labelEventDate.text=String.getDateTimeString(fromString: (modelEventList?.eventStarTime)!, inputFormat: "d MMM yyyy, h:mm a", outputFormat: "d MMM yyyy")
        labelEventTime.text=String.getDateTimeString(fromString: (modelEventList?.eventStarTime)!, inputFormat: "d MMM yyyy, h:mm a", outputFormat: "h:mm a")
        
        labelEventEndDate.text=String.getDateTimeString(fromString: (modelEventList?.eventEndTime)!, inputFormat: "d MMM yyyy, h:mm a", outputFormat: "d MMM yyyy")
        labelEventEndTime.text=String.getDateTimeString(fromString: (modelEventList?.eventEndTime)!, inputFormat: "d MMM yyyy, h:mm a", outputFormat: "h:mm a")
    }
    
    deinit {
        print("EventDetailViewController Deinit")
    }
    
    func onBackButtonAction() -> Void
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    
    //MARK:- Delete Button Click Action

    @IBAction func deleteButtonClickAction(_ sender: UIButton) {
        
        UtilityClass.showAlertWithTitle(title: App_Name, message: "Are you sure you want to delete this event?", onViewController: self, withButtonArray: ["Delete"], cancelButtonTitle: "Cancel")
            {[weak self] (buttonIndex) in
                guard let `self` = self else {return}
                if buttonIndex==0
                {
                    self .deleteEventApi()
                }
            }
        
    }
    
    @IBAction func editButtonClickAction(_ sender: Any)
    {
        
        //addEmployeeVC
        let addEventVC = self.storyboard?.instantiateViewController(withIdentifier: "createEventVC") as! CreateEventViewController
        
        
        addEventVC.arrayModelCreateEvent.append(ModelCreateEvent(withKey: EventEnum.title.rawValue, value: modelEventList!.eventName))
        
        let startTime=modelEventList!.eventStarTime.replacingOccurrences(of: ", ", with: "divider")
        
        addEventVC.arrayModelCreateEvent.append(ModelCreateEvent (withKey: EventEnum.startTime.rawValue, value: startTime))
        
        let endTime=modelEventList!.eventEndTime.replacingOccurrences(of: ", ", with: "divider")

        addEventVC.arrayModelCreateEvent.append(ModelCreateEvent (withKey: EventEnum.endTime.rawValue, value: endTime))
        addEventVC.arrayModelCreateEvent.append(ModelCreateEvent (withKey: EventEnum.discription.rawValue, value: modelEventList!.eventDiscription))

        addEventVC.isFromEventDetail=true
        addEventVC.eventID = modelEventList!.eventID
        
        self.navigationController?.pushViewController(addEventVC, animated: true)
    }
    
    
    //MARK:- Delete Event Api
    func deleteEventApi() -> Void
    {
        
        HUD.show(.systemActivity, onView: self.view.window)
        
        self.view.endEditing(true)
        
        let urlToHit = EndPoints.deleteEvent(UtilityClass.getUserSidData()!, modelEventList!.eventID).path
        
        AppWebHandler.sharedInstance().fetchData(fromURL: urlToHit, httpMethod: .get, parameters: nil, shouldDeserialize: true)
        {[weak self] (data, dictionary, statusCode, error) in
            
            HUD.hide()
            
            guard let `self` = self else {return}
            
            if error != nil
            {
                UtilityClass.showAlertWithTitle(title: App_Name, message: error!.localizedDescription, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                
                return
            }
            
            if dictionary == nil
            {
                UtilityClass.showAlertWithTitle(title: App_Name, message: App_Global_Error_Msg, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                
                return
            }
            
            if statusCode == 203
            {
                let responseDictionary = dictionary!
                guard (responseDictionary["message"] != nil) else
                {
                    UtilityClass.showAlertWithTitle(title: App_Name, message: App_Global_Error_Msg, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                    return
                }
                let messageStr = responseDictionary["message"] as! String
                
                UtilityClass.showAlertWithTitle(title: App_Name, message: messageStr, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                return
            }
            
            if statusCode == 200
            {
                
                let responseDictionary = dictionary!
                
                let responseMsg=responseDictionary["message"] as! String
                
                UtilityClass.showAlertWithTitle(title: App_Name, message: responseMsg, onViewController: self, withButtonArray: nil, dismissHandler: { (buttonIndex) in
                    self.navigationController?.popViewController(animated: true)
                })
            }
        }        
    }
}
