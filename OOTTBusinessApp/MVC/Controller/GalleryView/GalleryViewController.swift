//
//  GalleryViewController.swift
//  OOTTBusinessApp
//
//  Created by Santosh on 13/07/17.
//  Copyright © 2017 KonstantInfoSolutions. All rights reserved.
//

import UIKit

private let kCellIdentifier_GalleryOriginal = "imageScroller"
class GalleryOriginalCell: UICollectionViewCell
{
    @IBOutlet weak var galleryImage: ZoomImageView!
    
    // MARK:-
    override func layoutSubviews() {
        
//        self.galleryImage.layer.cornerRadius = CGFloat(5)
    }
    
    func updateGalleryImage(withURL imageURL : URL)
    {
        galleryImage.imageView.setIndicatorStyle(.white)
        galleryImage.imageView.setShowActivityIndicator(true)
        galleryImage.imageView.sd_setImage(with: imageURL)
        {[weak self] (image, error, cacheType, url) in
            guard let `self` = self else {return}
            self.galleryImage.imageView.setShowActivityIndicator(false)
            self.galleryImage.zoomMode = .fit
        }
    }
    
    deinit {
        print("GalleryOriginalCell Deinit")
    }
}


private let kCellIdentifier_GalleryThumbnail = "galleryCell"
class GalleryThumbnailCell: UICollectionViewCell
{
    @IBOutlet weak var galleryImage: UIImageView!
    
    // MARK:-
    override func layoutSubviews() {
        
        self.galleryImage.layer.cornerRadius = CGFloat(5)
    }
    
    func updateGalleryImage(withURL imageURL : URL)
    {
        galleryImage.setIndicatorStyle(.white)
        galleryImage.setShowActivityIndicator(true)
        galleryImage.sd_setImage(with: imageURL)
        {[weak self] (image, error, cacheType, url) in
            guard let `self` = self else {return}
            self.galleryImage.setShowActivityIndicator(false)
        }
    }
    
    deinit {
        print("GalleryThumbnailCell Deinit")
    }
}

private class ModelGalleryThumbnail
{
    var imageURL : URL!
    
    deinit {
        print("ModelGalleryThumbnail Deinit")
    }
}

class GalleryViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDataSourcePrefetching, UICollectionViewDelegateFlowLayout {
    
    @IBOutlet weak var selectedImageView: ZoomImageView!
    @IBOutlet weak var collectionViewGalleries: UICollectionView!
    
    @IBOutlet weak var collectionViewOriginalImage: UICollectionView!
    
    
    fileprivate var arrayGallery = [ModelGalleryThumbnail]()
    
    var selectedIndex : Int = 0
    var selectedBarName : String!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
        setupCollectionView()
//        updateImageview()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    deinit {
        print("GalleryViewController Deinit")
    }
    
    func setupView() {
        
        self.title = selectedBarName.uppercased()
        
        let backBarButton = self.setLeftBarButtonItem(withImage: #imageLiteral(resourceName: "backButtonWhite"))
        backBarButton.action = #selector(onBackButtonAction)
        backBarButton.target = self
    }
    
    func onBackButtonAction() {
        self.navigationController?.popViewController(animated: true)
    }
    
    func setupCollectionView()
    {
        self.collectionViewOriginalImage.dataSource = self
        self.collectionViewOriginalImage.delegate = self
        self.collectionViewOriginalImage.prefetchDataSource = self
        
        self.collectionViewGalleries.dataSource = self
        self.collectionViewGalleries.delegate = self
        self.collectionViewGalleries.prefetchDataSource = self
    }
    
    func updateModelArray(usingArray array:[GroupImages])
    {
        if array.isEmpty
        {
            // empty
            return
        }
        for model in array
        {
            if let urlString = model.imageURLString
            {
                let modelGallery = ModelGalleryThumbnail ()
                modelGallery.imageURL = URL (string: urlString)
                arrayGallery.append(modelGallery)
            }
        }
        
//        collectionViewGalleries.reloadData()
    }
    
    private func updateImageview()
    {
        if selectedIndex <= arrayGallery.count - 1 {
            self.selectedImageView.imageView.setIndicatorStyle(.whiteLarge)
            self.selectedImageView.imageView.setShowActivityIndicator(true)
            self.selectedImageView.imageView.sd_setImage(with: arrayGallery[selectedIndex].imageURL, completed:
                {[weak self] (image, error, cacheType, url) in
                    guard let `self` = self else {return}
                    self.selectedImageView.imageView.setShowActivityIndicator(false)
                    self.selectedImageView.zoomMode = .fit
            })
            
            collectionViewGalleries.scrollToItem(at: IndexPath (item: selectedIndex, section: 0), at: .centeredHorizontally, animated: true)
        }
    }
    
    //MARK:- UICollectionView Methods
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return collectionView.tag == 100 ? 0 : 10 * scaleFactorX
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if collectionView.tag == 100
        {
            return collectionView.frame.size
        }
        return CGSize (width: 90 * scaleFactorX, height: 90 * scaleFactorX)
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrayGallery.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if collectionView.tag == 100
        {
            let cell : GalleryOriginalCell = collectionView.dequeueReusableCell(withReuseIdentifier: kCellIdentifier_GalleryOriginal, for: indexPath) as! GalleryOriginalCell
            
            cell.updateGalleryImage(withURL: arrayGallery[indexPath.row].imageURL)
            return cell
        }
        
        let cell : GalleryThumbnailCell = collectionView.dequeueReusableCell(withReuseIdentifier: kCellIdentifier_GalleryThumbnail, for: indexPath) as! GalleryThumbnailCell
        
        cell.updateGalleryImage(withURL: arrayGallery[indexPath.row].imageURL)
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, prefetchItemsAt indexPaths: [IndexPath]) {

        if collectionView.tag == 100
        {
            for indexpath in indexPaths
            {
                if let cell = collectionView.cellForItem(at: indexpath) as? GalleryOriginalCell
                {
                    cell.updateGalleryImage(withURL: arrayGallery[indexpath.row].imageURL)
                }
            }
            return
        }
        for indexpath in indexPaths
        {
            if let cell = collectionView.cellForItem(at: indexpath) as? GalleryThumbnailCell
            {
                cell.updateGalleryImage(withURL: arrayGallery[indexpath.row].imageURL)
            }
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if collectionView.tag == 100
        {
            return
        }
        
        selectedIndex = indexPath.item
        updateGalleryOriginal()
    }
    
    func updateGalleryThumbnail()
    {
        collectionViewGalleries.scrollToItem(at: IndexPath (item: selectedIndex, section: 0), at: .centeredHorizontally, animated: true)
    }
    
    func updateGalleryOriginal()
    {
        collectionViewOriginalImage.scrollToItem(at: IndexPath (item: selectedIndex, section: 0), at: .centeredHorizontally, animated: true)
    }
}


//MARK:-
//MARK:- Extension

extension GalleryViewController
{
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        if scrollView.tag != 100
        {
            return
        }
        
        let visibleRect = CGRect (origin: collectionViewOriginalImage.contentOffset, size: collectionViewOriginalImage.bounds.size)
        
        let visiblePoint = CGPoint (x: visibleRect.midX, y: visibleRect.midY)
        
        let visibleIndexpath = collectionViewOriginalImage.indexPathForItem(at: visiblePoint)
        
        selectedIndex = (visibleIndexpath?.item)!
        
        updateGalleryThumbnail()
    }
}
