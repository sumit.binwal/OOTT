//
//  CreateEventViewController.swift
//  OOTTBusinessApp
//
//  Created by Sumit Sharma on 27/06/2017.
//  Copyright © 2017 KonstantInfoSolutions. All rights reserved.
//

import Foundation
import UIKit
import PKHUD

class CreateEventViewController: UIViewController,UITableViewDataSource,UITableViewDelegate,CreateEventCellDelegate {
    
    
    //MARK: Array Holding Values
    var arrayModelCreateEvent : [ModelCreateEvent] = []
    var isFromEventDetail:Bool! = false
    var eventID : String?
    
    var eventStartDate:Date?
    var eventEndDate:Date?

    @IBOutlet var tableViewCreateEvent: UITableView!
    @IBOutlet var vwTableFooter: UIView!

    override func viewDidLoad() {
        
        self.setupView()
        
        if !isFromEventDetail {
            self.loadDummyData()
        }
    }
    
    func setupView() -> Void
    {
        if isFromEventDetail {
            self.title = "EDIT EVENTS"
        }
        else
        
        {
        self.title = "CREATE EVENTS"
        }
        
        
        self.showNavigationBar()
        
        let leftBarButton = self.setLeftBarButtonItem(withImage: #imageLiteral(resourceName: "backButton"))
        leftBarButton.action = #selector(onBackButtonAction)
        leftBarButton.target = self
        
        vwTableFooter.frame=CGRect(x: 0, y: 0, width: tableViewCreateEvent.frame.size.width*scaleFactorX, height: 80*scaleFactorX)
        vwTableFooter.backgroundColor=UIColor.clear
        tableViewCreateEvent.tableFooterView=vwTableFooter
    }
    
    deinit {
        print("CreateEventViewController Deinit")
    }
    
    
    //MARK:- Load Dummy Array Data
    func loadDummyData() -> Void
    {
        arrayModelCreateEvent.append(ModelCreateEvent (withKey: EventEnum.title.rawValue, value: ""))
        arrayModelCreateEvent.append(ModelCreateEvent (withKey: EventEnum.startTime.rawValue, value: "divider"))
        arrayModelCreateEvent.append(ModelCreateEvent (withKey: EventEnum.endTime.rawValue, value: "divider"))
        arrayModelCreateEvent.append(ModelCreateEvent (withKey: EventEnum.discription.rawValue, value: ""))
    }
    
    func onBackButtonAction() -> Void
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    //MARK:- TableView Delegate And DataSource Methods
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 4
    }

    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 64 * scaleFactorX
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if indexPath.row==3 {
        return UITableViewAutomaticDimension
        }
        else
        {
        return 64 * scaleFactorX
        }
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell : CreateEventCell
        
        var cellIdentifier = kCellIdentifier_inputevent_title_cell
        
        var newCellType = CreateEventCellType.none
        
        switch indexPath.row
        {
        case 0:
            cellIdentifier = kCellIdentifier_inputevent_title_cell

            newCellType = .eventTitle
            
        case 1:
            newCellType = .startDateTime
            cellIdentifier=kCellIdentifier_inputevent_startend_cell

            
        case 2:
            
            cellIdentifier=kCellIdentifier_inputevent_startend_cell

            newCellType = .endDateTime

            
        case 3:
            newCellType = .description
            cellIdentifier=kCellIdentifier_inputevent_detail_cell

        default:
            break
        }
        
        cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier) as! CreateEventCell
        
        cell.cellType = newCellType;
        
        cell.eventDelegate = nil;
        cell.eventDelegate = self;

        cell.updateValue(usingModel: arrayModelCreateEvent[indexPath.row])
        
        return cell
    }
    
    
    //MARK:- CreateEventCell Delegate -> Textfield updated Action
    func createEventCell(cell: CreateEventCell, updatedInputfieldText: String) {
        
        if cell.model.keyName != EventEnum.none.rawValue
        {
            let isAlreadyContained = arrayModelCreateEvent.contains { (model) -> Bool in
                return model.keyName == cell.model.keyName
            }
            if !isAlreadyContained
            {
                arrayModelCreateEvent.append(cell.model)
            }
            else
            {
                let newModel = cell.model
                newModel.keyValue = updatedInputfieldText
                let index = arrayModelCreateEvent.index(where: { (model) -> Bool in
                    model.keyName == newModel.keyName
                })
                arrayModelCreateEvent[index!].keyValue = newModel.keyValue
            }
        }
    }
    
    
    //MARK:- Validate input fields
    func validateTheInputFields() -> Bool
    {
        if arrayModelCreateEvent.count == 0
        {
            UtilityClass.showAlertWithTitle(title: "", message: "Please provide event title", onViewController: self, withButtonArray: nil, dismissHandler: nil)
            
            return false
        }
        
        for model in arrayModelCreateEvent
        {
            switch model.keyName {
            case EventEnum.title.rawValue:
                if model.keyValue.isEmptyString()
                {
                    UtilityClass.showAlertWithTitle(title: "", message: "Please provide event title", onViewController: self, withButtonArray: nil, dismissHandler: nil)
                    return false
                }
            
            case EventEnum.startTime.rawValue:
                
                if model.keyValue.components(separatedBy: "divider").first!.isEmptyString()
                {
                    UtilityClass.showAlertWithTitle(title: "", message: "Please provide event start date", onViewController: self, withButtonArray: nil, dismissHandler: nil)
                    return false
                }
                if model.keyValue.components(separatedBy: "divider").last!.isEmptyString()
                {
                    UtilityClass.showAlertWithTitle(title: "", message: "Please provide event start time", onViewController: self, withButtonArray: nil, dismissHandler: nil)
                    return false
                }
                
                
            case EventEnum.endTime.rawValue:
                
                if model.keyValue.components(separatedBy: "divider").first!.isEmptyString()
                {
                    UtilityClass.showAlertWithTitle(title: "", message: "Please provide event end date", onViewController: self, withButtonArray: nil, dismissHandler: nil)
                    return false
                }
                if model.keyValue.components(separatedBy: "divider").last!.isEmptyString()
                {
                    UtilityClass.showAlertWithTitle(title: "", message: "Please provide event end time", onViewController: self, withButtonArray: nil, dismissHandler: nil)
                    return false
                }
            case EventEnum.discription.rawValue:
                if model.keyValue.isEmptyString()
                {
                    UtilityClass.showAlertWithTitle(title: "", message: "Please provide event description", onViewController: self, withButtonArray: nil, dismissHandler: nil)
                    return false
                }
                
            default: break
                
            }
        }
        return true
    }
    
    //MARK:- Login Api Call
    func callCreateEventApi() -> Void
    {
        HUD.show(.systemActivity, onView: self.view.window)
        
        self.view.endEditing(true)
        
        var params = [String:String]()
        
        for model in arrayModelCreateEvent
        {
            if model.keyName == EventEnum.startTime.rawValue || model.keyName == EventEnum.endTime.rawValue  {
                let startString = model.keyValue.replacingOccurrences(of: "divider", with: " ")
                params[model.keyName] = startString
            }
            else
            {
                params[model.keyName] = model.keyValue
            }
        }
        
        var urlToHit = EndPoints.createEvent(UtilityClass.getUserSidData()!).path
        if isFromEventDetail {
            
            urlToHit = EndPoints.editEvent(UtilityClass.getUserSidData()!,eventID!).path
        }

        
        AppWebHandler.sharedInstance().fetchData(fromURL: urlToHit, httpMethod: .post, parameters: params, shouldDeserialize: true)
        {[weak self] (data, dictionary, statusCode, error) in
            
            HUD.hide()
            
            guard let `self` = self else {return}
            
            if error != nil
            {
                UtilityClass.showAlertWithTitle(title: App_Name, message: error!.localizedDescription, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                
                return
            }
            
            if dictionary == nil
            {
                UtilityClass.showAlertWithTitle(title: App_Name, message: App_Global_Error_Msg, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                
                return
            }
            
            if statusCode == 203
            {
                let responseDictionary = dictionary!
                guard (responseDictionary["message"] != nil) else
                {
                    UtilityClass.showAlertWithTitle(title: App_Name, message: App_Global_Error_Msg, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                    return
                }
                let messageStr = responseDictionary["message"] as! String
                
                UtilityClass.showAlertWithTitle(title: App_Name, message: messageStr, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                return
            }
            
            if statusCode == 200
            {
                let responseDictionary = dictionary!
                let replyType = responseDictionary["type"] as! Bool
                let messageStr = responseDictionary["message"] as! String

               if !replyType
                {
                    //Showing Error
                    UtilityClass.showAlertWithTitle(title: "", message: messageStr, onViewController: self, withButtonArray: nil, dismissHandler:nil)
                }
                else
               {
                UtilityClass.showAlertWithTitle(title: App_Name, message: messageStr, onViewController: self, withButtonArray: nil, dismissHandler:
                    {[weak self] (buttonIndex) in
                 
                        guard let `self` = self else {return}
                        
                    var isFoundVwCntroller:Bool = false
                        
                    for vwController in (self.navigationController?.viewControllers)!
                    {
                        if vwController.isKind(of: EventListingViewController.self){
                            self.navigationController?.popToViewController(vwController, animated: true)
                            isFoundVwCntroller = true
                            return
                        }
                        
                    }
                    
                    if !isFoundVwCntroller
                    {
                        self.navigationController?.popToRootViewController(animated: true)
                        }
                        
                })
                
                }
                
                
            }
        }
    }

   
    
    @IBAction func doneButtonAction(_ sender: Any) {
        
        
        let isVerified = validateTheInputFields()
        
        if isVerified
        {
            print("yes finally")
            for model in arrayModelCreateEvent
            {
                if model.keyName == EventEnum.startTime.rawValue  {
                    
                    eventStartDate=Date.getDateTime(fromString: model.keyValue.replacingOccurrences(of: "divider", with: " "))

                }
                else if model.keyName == EventEnum.endTime.rawValue
                {
                     eventEndDate=Date.getDateTime(fromString: model.keyValue.replacingOccurrences(of: "divider", with: " "))
                }
            }

            
            let compareDate = eventStartDate!.compare(eventEndDate!)
                
            switch compareDate {
            case .orderedSame:
                UtilityClass.showAlertWithTitle(title: App_Name, message: "Event start date and end date not be same", onViewController: self, withButtonArray: nil, dismissHandler: nil)
                break
            case .orderedAscending:
                callCreateEventApi()
                break
            case .orderedDescending:
                UtilityClass.showAlertWithTitle(title: App_Name, message: "Event end date time should not less than start date time", onViewController: self, withButtonArray: nil, dismissHandler: nil)

                break
            default:
                break
            }
        }
    }
}
