//
//  AddEmployeeViewController.swift
//  OOTTBusinessApp
//
//  Created by Santosh on 16/06/17.
//  Copyright © 2017 KonstantInfoSolutions. All rights reserved.
//

import UIKit
import PKHUD


class AddEmployeeViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UIImagePickerControllerDelegate, UINavigationControllerDelegate,LoginCellDelegate,UIPickerViewDelegate,UIPickerViewDataSource {

    @IBOutlet weak var tableAddEmployee: UITableView!
    
    var arrEmployeeType = [Dictionary<String,Any>]()
    
    var isImageSelected = false
    
    var isFromEmployeeDetail:Bool! = false
    
    var employeeID : String?
    
    
    
    
    @IBOutlet var headerViewTable: UIView!
    
    var pickerView : UIPickerView!
    
    @IBOutlet var btnCreateEmployee: UIButton!
    
    @IBOutlet weak var profilePicButton: UIButton!

    //MARK: Array Holding Values
    var arrayModelLogin : [ModelLogin] = []
    
    let dispatchGroup = DispatchGroup ()
    

   //MARK:- viewDidLoad
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.setupView()
        
        if !isFromEmployeeDetail {
        self.loadDummyData()
        }
        
        self.setupTableView()
        self.updateTableHeaderView()
//        self.getEmployeeTypeApi()
        
    }

    //MARK:- didReceiveMemoryWarning
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK:- deinit
    deinit {
        print("AddEmployeeViewController Deinit")
    }

    //MARK:-
    func setupView() -> Void
    {
        if !isFromEmployeeDetail
        {
        self.title = "ADD EMPLOYEE"
            btnCreateEmployee.setTitle("CREATE EMPLOYEE", for: .normal)
        }
        else
        {
            self.title = "EDIT EMPLOYEE"
            btnCreateEmployee.setTitle("EDIT EMPLOYEE", for: .normal)
        }
        
        self.showNavigationBar()
        
        let leftBarButton = self.setLeftBarButtonItem(withImage: #imageLiteral(resourceName: "backButton"))
        leftBarButton.action = #selector(onBackButtonAction)
        leftBarButton.target = self
        
        
        //Setup Picker View
//        pickerView=UIPickerView.init()
//        pickerView.dataSource=self
//        pickerView.delegate=self
    }
    
    
    //MARK:- Load Dummy Data
    func loadDummyData() -> Void
    {
        arrayModelLogin.append(ModelLogin(withKey: LoginEnum.fullname.rawValue, value: ""))
        
        arrayModelLogin.append(ModelLogin(withKey: LoginEnum.employeeRole.rawValue, value: ""))
        
        arrayModelLogin.append(ModelLogin(withKey: LoginEnum.email.rawValue, value: ""))
        
        arrayModelLogin.append(ModelLogin(withKey: LoginEnum.password.rawValue, value: ""))
        
        arrayModelLogin.append(ModelLogin(withKey: LoginEnum.confirmPassword.rawValue, value: ""))
    }
    
    //MARK:- Initial table setup
    func setupTableView() -> Void
    {
        self.tableAddEmployee.delegate = self
        self.tableAddEmployee.dataSource = self
        
        //        self.tableBuisnessConnect.contentInset = UIEdgeInsetsMake(64 * scaleFactorX, 0, 0, 0)
    }
    
    //MARK:- Table header setup
    func updateTableHeaderView() -> Void
    {
        self.headerViewTable.frame = CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: self.headerViewTable.frame.size.height * scaleFactorX)
        
        self.headerViewTable.backgroundColor = UIColor.clear
        self.profilePicButton.backgroundColor = UIColor.yellow
        self.profilePicButton.layer.cornerRadius = (self.profilePicButton.frame.size.width/2) * scaleFactorX
        self.profilePicButton.clipsToBounds = true
        
        self.tableAddEmployee.tableHeaderView = self.headerViewTable
    }
    
    func onBackButtonAction() -> Void
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    //MARK:- PickerView Delegate and Datasource Methods
    func numberOfComponentsInPickerView(pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return arrEmployeeType.count;
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        
        let type = arrEmployeeType[row]["name"] as! String
        
        return type

    }

    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        let type = arrEmployeeType[row]["name"] as! String
        self.arrayModelLogin[1].keyValue=type
        let activeCell = tableAddEmployee.cellForRow(at: IndexPath(row: 1, section: 0)) as! LoginInputCell
        activeCell.updateValue(usingModel: arrayModelLogin[1])
        
    }
    
    //MARK:- UITableView Delegate and Datasource
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 5
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 64
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 0
        {
            return 68 * scaleFactorX;
        }
        
        if isFromEmployeeDetail
        {
            if indexPath.row == 3 || indexPath.row == 4
            {
                return 0
            }
        }
        return 64 * scaleFactorX;
        
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell : LoginInputCell
        
        var cellIdentifier = kCellIdentifier_login_input
        
        var newCellType = CellType.none
        
        switch indexPath.row
        {
            
        case 0:
            cellIdentifier = kCellIdentifier_login_input
            newCellType = .employeeName
            
        case 1:
            cellIdentifier = kCellIdentifier_login_input
            newCellType = .employeePosition
            
            
        case 2:
            cellIdentifier = kCellIdentifier_login_input
            newCellType = .email
            
        case 3:
            cellIdentifier = kCellIdentifier_login_input
            newCellType = .pass
            
        case 4:
            cellIdentifier = kCellIdentifier_login_input
            newCellType = .confirmPassword
            
        default:
            break
        }
        
        cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier) as! LoginInputCell
        
//        if indexPath.row==1 {
//            cell.inputTextField .inputView=pickerView
//        }
        cell.cellType = newCellType;
//        cell.loginDelegate = nil;
        cell.loginDelegate = self;
        cell.updateValue(usingModel: arrayModelLogin[indexPath.row])

        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {

    }
    
    //MARK:- Profile Pic Action
    @IBAction func onProfilePicAction(_ sender: UIButton) {
        
        weak var weakSelf = self
        
        UtilityClass.showActionSheetWithTitle(title: "Add Profile Picture", message: "Select from options", onViewController: self, withButtonArray: ["Camera", "Photos"]) { (buttonIndex : Int) in
            if buttonIndex == 0
            {
                weakSelf?.openPhotoPickerForType(imageSelectType: .camera)
            }
            else
            {
                weakSelf?.openPhotoPickerForType(imageSelectType: .photos)
            }
        }
    }
    
    
    //MARK:- LoginCellDelegate -> Textfield updated Action
    func loginCell(cell: LoginInputCell, updatedInputfieldText: String) {
        if cell.model.keyName != LoginEnum.none.rawValue
        {
            let isAlreadyContained = arrayModelLogin.contains { (model) -> Bool in
                return model.keyName == cell.model.keyName
            }
            if !isAlreadyContained
            {
                arrayModelLogin.append(cell.model)
            }
            else
            {
//                if cell.inputTextField.tag == CellType.employeePosition.rawValue
//                {
//                    let newModel = cell.model
//                    if newModel.keyValue.isEmpty
//                    {
//                        newModel.keyValue = arrEmployeeType[0]["name"] as! String
//                        cell.inputTextField.text = newModel.keyValue
//                        arrayModelLogin[1].keyValue = newModel.keyValue
//                    }
//                }
//                else
//                {
                    let newModel = cell.model
                    newModel.keyValue = updatedInputfieldText
                    let index = arrayModelLogin.index(where: { (model) -> Bool in
                        model.keyName == newModel.keyName
                    })
                    arrayModelLogin[index!].keyValue = newModel.keyValue
//                }
            }
        }
    }

    
    //MARK:- Show ImagePicker
    func openPhotoPickerForType(imageSelectType : ImageSelectType) -> Void
    {
        var isCamera = false
        
        if imageSelectType == ImageSelectType.camera
        {
            if !UIImagePickerController.isSourceTypeAvailable(.camera
                ) {
                UtilityClass.showAlertWithTitle(title: "This device does not have Camera. Try Photos!", message: nil, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                return
            }
            else
            {
                isCamera = true
            }
        }
        
        let imagePicker = UIImagePickerController.init()
        imagePicker.sourceType = isCamera ? .camera : .photoLibrary
        imagePicker.allowsEditing = true
        imagePicker.delegate = self
        
        imagePicker.navigationBar.isTranslucent = false
        imagePicker.navigationBar.barTintColor = UIColor.init(RED: 10, GREEN: 23, BLUE: 42, ALPHA: 0.6)
        imagePicker.navigationBar.tintColor = UIColor.white
        imagePicker.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName : UIColor.white]
        self.present(imagePicker, animated: true, completion: nil)
    }
    
    //MARK: - UIImagePickerControllerDelegate
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
    }
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        
        if let image = info[UIImagePickerControllerEditedImage] as? UIImage
        {
            self.profilePicButton.setImage(image, for: .normal)
            isImageSelected = true
        }
        picker.dismiss(animated: true, completion: nil)
    }
    
    //MARK:- Create Employee Action
    @IBAction func onCreateEmployeeAction(_ sender: UIButton) {
        //self.navigationController?.popViewController(animated: true)
        
        
        let isVerified = validateEmployeeInputFields()
        
        if isVerified {
            self.callAddEmployeeApi()
        }
    }
    
    //MARK:- Validate input fields
    func validateEmployeeInputFields() -> Bool
    {
        if !isImageSelected {
            UtilityClass.showAlertWithTitle(title: "", message: "Please add profile picture", onViewController: self, withButtonArray: nil, dismissHandler: nil)
            return false
        }
        
        if arrayModelLogin.count == 0
        {
            UtilityClass.showAlertWithTitle(title: "", message: "Please fill up entries", onViewController: self, withButtonArray: nil, dismissHandler: nil)
            
            return false
        }
        
        var passValue : String = ""
        var confirmPassValue : String = ""
        
        for model in arrayModelLogin
        {
            switch model.keyName {
            case LoginEnum.email.rawValue:
                if model.keyValue.isEmptyString()
                {
                    UtilityClass.showAlertWithTitle(title: "", message: "Please provide email address", onViewController: self, withButtonArray: nil, dismissHandler: nil)
                    return false
                }
                
                if !model.keyValue.isValidEmailAddress()
                {
                    UtilityClass.showAlertWithTitle(title: "", message: "Please provide valid email address", onViewController: self, withButtonArray: nil, dismissHandler: nil)
                    return false
                }
                
            case LoginEnum.fullname.rawValue:
                if model.keyValue.isEmptyString()
                {
                    UtilityClass.showAlertWithTitle(title: "", message: "Please provide employee name", onViewController: self, withButtonArray: nil, dismissHandler: nil)
                    return false
                }
                
            case LoginEnum.employeeRole.rawValue:
                if model.keyValue.isEmptyString()
                {
                    UtilityClass.showAlertWithTitle(title: "", message: "Please provide employee position", onViewController: self, withButtonArray: nil, dismissHandler: nil)
                    return false
                }
            case LoginEnum.confirmPassword.rawValue:
               if !isFromEmployeeDetail
               {
                if model.keyValue.isEmptyString()
                {
                    UtilityClass.showAlertWithTitle(title: "", message: "Please provide confirm password", onViewController: self, withButtonArray: nil, dismissHandler: nil)
                    return false
                }
                else
                {
                    confirmPassValue = model.keyValue
                }
                }
                
            default:
                if !isFromEmployeeDetail
                {
                if model.keyValue.isEmptyString()
                {
                    UtilityClass.showAlertWithTitle(title: "", message: "Please provide password", onViewController: self, withButtonArray: nil, dismissHandler: nil)
                    return false
                }
                if model.keyValue.characters.count < 6
                {
                    UtilityClass.showAlertWithTitle(title: "", message: "Password should be atleast 6 character", onViewController: self, withButtonArray: ["OK"], dismissHandler: nil)
                    return false
                }
                else
                {
                    passValue = model.keyValue
                }
                }
            }
        }
        
        if !isFromEmployeeDetail
        {
        if passValue != confirmPassValue
        {
            UtilityClass.showAlertWithTitle(title: "", message: "Password and confirm password should be same", onViewController: self, withButtonArray: nil, dismissHandler: nil)
            return false
        }
        }
        return true
    }
    
    //MARK:- Add Employee API Call
    func callAddEmployeeApi() -> Void
    {
        HUD.show(.systemActivity, onView: self.view.window)
        
        self.view.endEditing(true)
        
        var params = [String:String]()
        
        for model in arrayModelLogin
        {
            params[model.keyName] = model.keyValue
        }
        
        params[LoginEnum.confirmPassword.rawValue]=nil
        

        var urlToHit = EndPoints.addEmployee(UtilityClass.getUserSidData()!).path
        
        if isFromEmployeeDetail
        {
            urlToHit = EndPoints.editEmployee(UtilityClass.getUserSidData()!, employeeID!).path
        }
        
        let imageData=UIImageJPEGRepresentation(profilePicButton.image(for: .normal)!, 0.7)
        let imageArry=[ImageDataDict(data: imageData!, name: "picture")]
        
        AppWebHandler.sharedInstance().uploadImages(fromURL: urlToHit, imagesArray: imageArry, otherParameters: params)
        {[weak self] (data, dictionary, statusCode, error) in
            
            HUD.hide()
            
            guard let `self` = self else {return}
            
            if error != nil
            {
                UtilityClass.showAlertWithTitle(title: App_Name, message: error!.localizedDescription, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                
                return
            }
            
            if dictionary == nil
            {
                UtilityClass.showAlertWithTitle(title: App_Name, message: App_Global_Error_Msg, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                
                return
            }
            
            if statusCode == 203
            {
                let responseDictionary = dictionary!
                guard (responseDictionary["message"] != nil) else
                {
                    UtilityClass.showAlertWithTitle(title: App_Name, message: App_Global_Error_Msg, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                    return
                }
                let messageStr = responseDictionary["message"] as! String
                
                UtilityClass.showAlertWithTitle(title: App_Name, message: messageStr, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                return
            }
            
            if statusCode == 200
            {
                let responseDictionary = dictionary!
                
                    // login...
                    guard responseDictionary["data"] != nil else
                    {
                        UtilityClass.showAlertWithTitle(title: App_Name, message: App_Global_Error_Msg, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                        return
                }
                
                let messageStr = responseDictionary["message"] as! String
                
                UtilityClass.showAlertWithTitle(title: App_Name, message: messageStr, onViewController: self, withButtonArray: nil, dismissHandler: { (index) in
                    for vwController in (self.navigationController?.viewControllers)!
                    {
                        if vwController.isKind(of: EmployeeListingViewController.self){
                            self.navigationController?.popToViewController(vwController, animated: true)
                            return
                        }
                        
                    }                })
            }
        }
    }
    
    //MARK:- Get Employee Type API Call
    func getEmployeeTypeApi() -> Void
    {
        UtilityClass.showNetworkActivityLoader(show: true)
        self.view.endEditing(true)
        
        let urlToHit = EndPoints.getUserRole.path
        
        AppWebHandler.sharedInstance().fetchData(fromURL: urlToHit, httpMethod: .get, parameters: nil, shouldDeserialize: true)
        {[weak self] (data, dictionary, statusCode, error) in
            
            UtilityClass.showNetworkActivityLoader(show: false)
            
            guard let `self` = self else {return}
            
            if error != nil
            {
                UtilityClass.showAlertWithTitle(title: App_Name, message: error!.localizedDescription, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                
                return
            }
            
            if dictionary == nil
            {
                UtilityClass.showAlertWithTitle(title: App_Name, message: App_Global_Error_Msg, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                
                return
            }
            
            if statusCode == 203
            {
                let responseDictionary = dictionary!
                guard (responseDictionary["message"] != nil) else
                {
                    UtilityClass.showAlertWithTitle(title: App_Name, message: App_Global_Error_Msg, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                    return
                }
                let messageStr = responseDictionary["message"] as! String
                
                UtilityClass.showAlertWithTitle(title: App_Name, message: messageStr, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                return
            }
            
            if statusCode == 200
            {
                let responseDictionary = dictionary!
                
                let type = responseDictionary["type"] as! Bool
                
                if type
                {
                    let employeeType : [[String:Any]] = responseDictionary["data"]! as! [[String : Any]]
                    
                    self.arrEmployeeType = employeeType
                }
                
//                let tempDict=employeeType[0]
                
//                weakSelf?.arrayModelLogin[1].keyValue=tempDict["name"]! as! String
//                
//            weakSelf?.tableAddEmployee.reloadData()
                
            }
        }
    }
}


