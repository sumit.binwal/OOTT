//
//  MediaViewerController.swift
//  OOTTBusinessApp
//
//  Created by Santosh on 01/02/18.
//  Copyright © 2018 KonstantInfoSolutions. All rights reserved.
//

import UIKit

class ModelMediaViewer {
    var mediaLocalURL: URL
    var isImageMedia: Bool
    
    init(mediaLocalURL: URL, isImageMedia: Bool) {
        self.mediaLocalURL = mediaLocalURL
        self.isImageMedia = isImageMedia
    }
}

class MediaViewerController: UIViewController {
    
    @IBOutlet weak var collectionMediaViewer: UICollectionView!
    
    
    var arrayMediaViewer: [ModelMediaViewer]!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        setupView()
        setupCollectionView()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setupView() {
        self.navigationItem.title = "Gallery".uppercased()
        
        let leftBarButton = self.setLeftBarButtonItem(withImage: #imageLiteral(resourceName: "backButton"))
        leftBarButton.action = #selector(onBackButtonAction)
        leftBarButton.target = self
    }
    
    func setupCollectionView() {
        collectionMediaViewer.delegate = self
        collectionMediaViewer.dataSource = self
        collectionMediaViewer.prefetchDataSource = self
    }
    
    func onBackButtonAction() {
        self.navigationController?.popViewController(animated: true)
    }
}

extension MediaViewerController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDataSourcePrefetching, UICollectionViewDelegateFlowLayout
{
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize (width: 124 * scaleFactorX, height: 124 * scaleFactorX)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 1.5 * scaleFactorX
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrayMediaViewer.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        var cell: MediaViewerCell!
        
        var cellIdentifier = kCellIdentifier_MediaViewerCellVideo
        
        if arrayMediaViewer[indexPath.row].isImageMedia {
            cellIdentifier = kCellIdentifier_MediaViewerCellImage
        }
        
        cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellIdentifier, for: indexPath) as! MediaViewerCell
        
        cell.updateCellContents(usingModel: arrayMediaViewer[indexPath.item])
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, prefetchItemsAt indexPaths: [IndexPath]) {
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        collectionView.deselectItem(at: indexPath, animated: true)
        
        let mediaDetailVC: MediaViewerDetailController = self.storyboard?.instantiateViewController(withIdentifier: "mediaViewerDetailVC") as! MediaViewerDetailController
        
        mediaDetailVC.arrayMediaDetails = arrayMediaViewer
        
        mediaDetailVC.startIndex = indexPath.item
        
        self.navigationController?.pushViewController(mediaDetailVC, animated: true)
    }
}
