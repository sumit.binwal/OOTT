//
//  MediaViewerDetailController.swift
//  OOTTBusinessApp
//
//  Created by Santosh on 01/02/18.
//  Copyright © 2018 KonstantInfoSolutions. All rights reserved.
//

import UIKit
import AVFoundation
import AVKit
import PKHUD

class MediaViewerDetailController: UIViewController {

    
    @IBOutlet weak var collectionMediaDetails: UICollectionView!
    
    var arrayMediaDetails: [ModelMediaViewer]!
    
    var startIndex = 0
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        setupView()
        setupCollectionView()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        startIndex = -1
    }
    
    override func viewWillLayoutSubviews() {
        if startIndex >= 0 {
            collectionMediaDetails.scrollToItem(at: IndexPath (item: startIndex, section: 0), at: .left, animated: false)
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setupView() {
//        self.navigationItem.title = "Gallery".uppercased()

        let leftBarButton = self.setLeftBarButtonItem(withImage: #imageLiteral(resourceName: "backButton"))
        leftBarButton.action = #selector(onBackButtonAction)
        leftBarButton.target = self
    }
    
    func setupCollectionView() {
        collectionMediaDetails.delegate = self
        collectionMediaDetails.dataSource = self
        collectionMediaDetails.prefetchDataSource = self
        
//        collectionMediaDetails.scrollToItem(at: IndexPath (item: startIndex, section: 0), at: .left, animated: true)
    }
    
    func onBackButtonAction() {
        self.navigationController?.popViewController(animated: true)
    }
}

extension MediaViewerDetailController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDataSourcePrefetching, UICollectionViewDelegateFlowLayout
{
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return collectionView.frame.size
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrayMediaDetails.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        var cell: MediaViewerDetailCell!
        
        var cellIdentifier = kCellIdentifier_MediaViewerDetailCellVideo
        
        if arrayMediaDetails[indexPath.row].isImageMedia {
            cellIdentifier = kCellIdentifier_MediaViewerDetailCellImage
        }
        
        cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellIdentifier, for: indexPath) as! MediaViewerDetailCell
        
        cell.updateCellContents(usingModel: arrayMediaDetails[indexPath.item])
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, prefetchItemsAt indexPaths: [IndexPath]) {
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        collectionView.deselectItem(at: indexPath, animated: true)
        
        if arrayMediaDetails[indexPath.item].isImageMedia {
            return
        }
        
        HUD.show(.systemActivity, onView: self.view.window)
        
        let player = AVPlayer (url: arrayMediaDetails[indexPath.item].mediaLocalURL)
        let playerController = AVPlayerViewController ()
        
        playerController.player = player
        
        self.navigationController?.present(playerController, animated: true, completion: {
            HUD.hide()
        })
        player.play()
    }
}
