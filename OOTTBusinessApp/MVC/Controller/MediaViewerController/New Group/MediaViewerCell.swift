//
//  MediaViewerCell.swift
//  OOTTBusinessApp
//
//  Created by Santosh on 01/02/18.
//  Copyright © 2018 KonstantInfoSolutions. All rights reserved.
//

import UIKit

let kCellIdentifier_MediaViewerCellImage = "mediaViewerCellImage"
let kCellIdentifier_MediaViewerCellVideo = "mediaViewerCellVideo"
class MediaViewerCell: UICollectionViewCell {
    
    @IBOutlet weak var mediaImageView: UIImageView!
    
    func updateCellContents(usingModel model: ModelMediaViewer) {
        if model.isImageMedia {
            mediaImageView.image = UIImage (data: try! Data (contentsOf: model.mediaLocalURL))
        }
        else {
            mediaImageView.image = UtilityClass.getThumbnailFromVideo(model.mediaLocalURL)
        }
    }
}
