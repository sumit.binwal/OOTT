//
//  BusinessHomeViewController.swift
//  OOTTBusinessApp
//
//  Created by Santosh on 15/06/17.
//  Copyright © 2017 KonstantInfoSolutions. All rights reserved.
//

import UIKit
import PKHUD
import SDWebImage
import InAppNotify

struct GroupDetailModel
{
    var businessID : String = ""
    var groupID : String = ""
    var groupName : String = ""
    var groupAttendee : String = ""
    var groupStatus : String = "No status"
    var groupPictureString : String = ""
    
    var groupFavouriteCount : String = ""
    var distance = ""
    
    var waitingTime : String = ""
    var groupCrowded : String = ""
    var openTill: String = "NA"
    var isBusinessClosed = false
    var isVerifiedBusiness = false
    var unreadCount = 0
    var updatedTime: String = ""
    
    init(dataDictionary:[String:Any])
    {
        if let _id = dataDictionary["leader"] as? String
        {
            self.businessID = _id
        }
        
        if let updated = dataDictionary["updated"] as? String
        {
            self.updatedTime = updated
        }
        
        if let unseenCount = dataDictionary["unseenCount"] as? Int
        {
            self.unreadCount = unseenCount
        }
        
        if let verified = dataDictionary["verified"] as? Bool
        {
            self.isVerifiedBusiness = verified
        }
        
        if let group_id = dataDictionary["group_id"] as? String
        {
            self.groupID = group_id
        }
        if let name = dataDictionary["name"] as? String
        {
            self.groupName = name
        }
        if let attendee = dataDictionary["attendee"] as? Int
        {
            self.groupAttendee = String(attendee)
        }
        if let crowded = dataDictionary["crowded"] as? Int
        {
            switch crowded {
            case 1:
                self.groupCrowded = "Starting up"
                break
            case 2:
                self.groupCrowded = "Lively"
                break
            case 3:
                self.groupCrowded = "Crowded"
                break
            case 4:
                self.groupCrowded = "Packed"
                break
            default:
                self.groupCrowded = ""
                break
            }
//            self.groupCrowded = String(crowded)
        }
        if let waitingTime = dataDictionary["waitingTime"] as? Int
        {
            switch waitingTime {
            case 1:
                self.waitingTime = "No line"
                break
            case 2:
                self.waitingTime = "Less than 10 min."
                break
            case 3:
                self.waitingTime = "20 min."
                break
            case 4:
                self.waitingTime = "30 min. or more"
                break
            default:
                self.waitingTime = ""
                break
            }
//            self.waitingTime = String(waitingTime)
        }
        if let status = dataDictionary["status"] as? String
        {
            self.groupStatus = status.count>0 ? status : "No Status"
        }
        if let picture = dataDictionary["picture"] as? String
        {
            self.groupPictureString = picture
        }
        if let likesCount = dataDictionary["likesCount"] as? Int
        {
            self.groupFavouriteCount = String(likesCount)
        }
        if let dist = dataDictionary["dist"] as? Double
        {
            distance = UtilityClass.getFormattedFloatString(dist, uptoDecimalLimit: 2) // Miles
            distance = distance.appending("mi")
        }
        if let open = dataDictionary["open"] as? [String:String]
        {
            let timeForToday = open[Date ().getStringForFormat(format: "EEEE")]!
            openTill = timeForToday.components(separatedBy: "-").last!
//            if timeForToday.isEmpty {
//                openTill = "Closed"
//            }
        }
        if let closedNow = dataDictionary["closedNow"] as? Bool
        {
            isBusinessClosed = closedNow
        }
        
        if isBusinessClosed == true {
            openTill = "Closed"
        }
    }
}

//MARK:-
//MARK:- BusinessHomeViewController
class BusinessHomeViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var tableBuisnessConnect: UITableView!
    
    @IBOutlet var headerViewTable: UIView!
    
    @IBOutlet weak var groupPicture: UIImageView!
    @IBOutlet weak var labelGroupName: UILabel!
    @IBOutlet weak var labelLineWaitTime: UILabel!
    @IBOutlet weak var labelCrowded: UILabel!
    @IBOutlet weak var labelDistance: UILabel!
    @IBOutlet weak var labelTotalFavourite: UILabel!
    @IBOutlet weak var labelOpenTill: UILabel!
    @IBOutlet weak var verifiedIcon: UIImageView!
    
    @IBAction func onTotalFavouriteButtonAction(_ sender: UIButton) {
    }
    var arrayUpcomingEventList=[EventListModel]()

    var groupStructModel : GroupDetailModel?
    
    var refreshControl: UIRefreshControl!
    
    
    //MARK:- viewDidLoad
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        if UtilityClass.getUserTypeData() == AppUserTypeEnum.employee.rawValue {
            initiateSocket()
        }
        self.setupView()
        self.setupTableView()
        self.updateTableHeaderView()
//
        addGestureInHeader()
       
        applyRefreshControl()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        refreshPage(sender: UIButton ())
    }
    
    //MARK:- didReceiveMemoryWarning
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK:- deinit
    deinit {
//        SocketManager.sharedInstance().removeIncomingMessagesListener()
//        SocketManager.sharedInstance().disconnectSocket()
        print("BusinessHomeViewController Deinit")
    }
    
    
    //MARK:- Setup Tap Gesture 
    func addGestureInHeader() -> Void {
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(headerViewTapAction))
        tapGesture.numberOfTapsRequired=1
        headerViewTable.addGestureRecognizer(tapGesture)
        headerViewTable.isUserInteractionEnabled=true
    }
    
    
    //MARK:- Tap Gesture Action method
    func headerViewTapAction() -> Void {
        if let baseTabbar = appDelegate?.window??.rootViewController as? BusinessTabsController {
            baseTabbar.updateTabBar(withIndex: 4)
        }
        else {
            let detailsViewController = UIStoryboard.getBusinessDetailsStoryboard().instantiateViewController(withIdentifier: "businessDetailsViewController") as! BusinessDetailsViewController
            detailsViewController.businessId = groupStructModel?.businessID
            detailsViewController.hidesBottomBarWhenPushed = true
            self.navigationController?.pushViewController(detailsViewController, animated: true)
        }
    }
    
    //MARK:- Initial View Setup
    func setupView() -> Void
    {
        self.navigationItem.title = "Dashboard"
        
        self.showNavigationBar()
        
        if UtilityClass.getUserTypeData() == AppUserTypeEnum.employee.rawValue {
            let rightBarButton = self.setRightBarButtonItem(withImage: #imageLiteral(resourceName: "logoutButton"))
            rightBarButton.action = #selector(onLogoutButtonAction)
            rightBarButton.target = self
            rightBarButton.tintColor = UIColor.white
        }
    }
    
    //MARK:- Initial table setup
    func setupTableView() -> Void
    {
        self.tableBuisnessConnect.alpha=0
        self.tableBuisnessConnect.delegate = self
        self.tableBuisnessConnect.dataSource = self
        
        //        self.tableBuisnessConnect.contentInset = UIEdgeInsetsMake(64 * scaleFactorX, 0, 0, 0)
    }
    
    //MARK:- Table header setup
    func updateTableHeaderView() -> Void
    {
        self.headerViewTable.frame = CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: self.headerViewTable.frame.size.height * scaleFactorX)
        
        self.groupPicture.makeRound()
        
        self.tableBuisnessConnect.tableHeaderView = self.headerViewTable
    }
    
    //MARK:- Logout button action
    func onLogoutButtonAction() -> Void {
        logoutAPI()
    }
    
    // MARK:- Refresh Control
    func applyRefreshControl() {
        
        refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(self.refreshPage), for: UIControlEvents.valueChanged)
        self.tableBuisnessConnect.addSubview(refreshControl)
    }
    
    func refreshPage(sender:AnyObject) {
        getBusinessDetailsApi()
//        getBusinessUpcomingEventApi()
    }
    
    
    //MARK:- UITableView Delegate and Datasource
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return arrayUpcomingEventList.count > 0 ? 3 : 2
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 43 * scaleFactorX
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
//        if section == 0 {
//            return nil
//        }
        let sectionHeader: UITableViewCell = tableView.dequeueReusableCell(withIdentifier: kCellIdentifier_upcomingEvent_sectionHeader)!
        
        guard let label = sectionHeader.contentView.viewWithTag(101) as? UILabel else {
            return sectionHeader
        }
        
        switch section {
        case 0:
            label.text = "Status"
        case 1:
            label.text = "Group"
        case 2:
            label.text = "Upcoming Events"
        default:
            break
        }
        
        return sectionHeader
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 || section == 1 {
            return 1
        }
        return arrayUpcomingEventList.count
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 71
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 0 {
            return UITableViewAutomaticDimension // Change status
        }
        if indexPath.section == 1 {
            return 92 * scaleFactorX // business group header
        }
        return 71 * scaleFactorX; // Upcoming events
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        switch indexPath.section {
        case 0:
            let cell =  tableView.dequeueReusableCell(withIdentifier: kCellIdentifier_ChangeStatusCell) as! ChangeStatusCell
            
            cell.delegate = self
            
            if groupStructModel != nil {
                cell.updateCell(usingModel: groupStructModel!)
            }
            
            return cell
        case 1:
            let cell =  tableView.dequeueReusableCell(withIdentifier: kCellIdentifier_businessGroup_HeaderCell) as! BusinessGroupHeaderCell
            
            if groupStructModel != nil {
                cell.updateCell(usingModel: groupStructModel!)
            }
            
            return cell
        default:
            break
        }
        
        // Upcoming Events
        let cell =  tableView.dequeueReusableCell(withIdentifier: kCellIdentifier_upcomingEvent_eventCell) as! UpcomingEventCell
        
        cell.labelEventName.text=arrayUpcomingEventList[indexPath.row].eventName
        cell.labelEventDate.text=arrayUpcomingEventList[indexPath.row].eventStarTime

        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.section == 1 {
            // open screen
            let chatViewController = UIStoryboard.getChatViewStoryboard().instantiateInitialViewController() as! GroupChatViewController
            
            chatViewController.hidesBottomBarWhenPushed = true
            
            chatViewController.isGroupChat = true
            
            chatViewController.isBusinessChat = true
            
            chatViewController.chatUsername.text = groupStructModel?.groupName
            
            chatViewController.chatRoomID = groupStructModel?.groupID
            
            chatViewController.chatPicture = groupStructModel?.groupPictureString
            
            chatViewController.businessID = groupStructModel?.businessID
            
            self.navigationController?.pushViewController(chatViewController, animated: true)
            
            return
        }
        else if indexPath.section == 2 {
            let eventDetail = UIStoryboard.getEventListStoryboard().instantiateViewController(withIdentifier: "eventDetailVC") as! EventDetailViewController
            eventDetail.hidesBottomBarWhenPushed = true
            eventDetail.modelEventList=arrayUpcomingEventList[indexPath.row]
            self.navigationController?.pushViewController(eventDetail, animated: true)
        }
    }
    
    @IBAction func addEventButtonAction(_ sender: Any) {
        let eventCreate = UIStoryboard.getEventListStoryboard().instantiateViewController(withIdentifier: "createEventVC") as! CreateEventViewController
        eventCreate.hidesBottomBarWhenPushed = true
        self.navigationController?.pushViewController(eventCreate, animated: true)
    }
    
    //MARK:- Get Business Detail Api
    func getBusinessDetailsApi() -> Void
    {
        HUD.show(.systemActivity, onView: self.view.window)
        
        let urlToHit = EndPoints.myBusinessGroupDetail(UtilityClass.getUserSidData()!, String(LocationManager.sharedInstance().newLatitude), String(LocationManager.sharedInstance().newLongitude)).path
        
        AppWebHandler.sharedInstance().fetchData(fromURL: urlToHit, httpMethod: .get, parameters: nil, completionHandler:
            {[weak self] (data, dictionary, statusCode, error) in
                
                HUD.hide()

                guard let `self` = self else {return}
                
                self.tableBuisnessConnect.beginUpdates()
                self.tableBuisnessConnect.alpha = 1
                self.tableBuisnessConnect.endUpdates()
                
                if error != nil
                {
                    UtilityClass.showAlertWithTitle(title: App_Name, message: error!.localizedDescription, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                    
                    return
                }
                
                if dictionary == nil
                {
                    UtilityClass.showAlertWithTitle(title: App_Name, message: App_Global_Error_Msg, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                    
                    return
                }
                
                if statusCode == 203
                {
                    let responseDictionary = dictionary!
                    guard (responseDictionary["message"] != nil) else
                    {
                        UtilityClass.showAlertWithTitle(title: App_Name, message: App_Global_Error_Msg, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                        return
                    }
                    let messageStr = responseDictionary["message"] as! String
                    
                    UtilityClass.showAlertWithTitle(title: App_Name, message: messageStr, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                    return
                }
                
                if statusCode == 200
                {
                    let responseDictionary = dictionary!
                    let replyType = responseDictionary["type"] as! Bool
                    let messageStr = responseDictionary["message"] as! String
                    if !replyType
                    {
                        //sign up screen
                        UtilityClass.showAlertWithTitle(title: "", message: messageStr, onViewController: self, withButtonArray: nil, dismissHandler:nil)
                    }
                    else
                    {
                        let dataDict = responseDictionary["data"] as! [String:Any]
                        self.groupStructModel = GroupDetailModel (dataDictionary: dataDict)
                        self.updateGroupView()
                        self.getBusinessUpcomingEventApi()
                    }
                }
        })
    }
    
    //MARK:- Get Upcoming Event Api
    func getBusinessUpcomingEventApi() -> Void
    {
        HUD.show(.systemActivity, onView: self.view.window)
        
        let urlToHit = EndPoints.getUpcomingEvents(UtilityClass.getUserSidData()!,(groupStructModel?.businessID)!).path
        
        AppWebHandler.sharedInstance().fetchData(fromURL: urlToHit, httpMethod: .get, parameters: nil, completionHandler:
            {[weak self] (data, dictionary, statusCode, error) in
                
                HUD.hide()

                guard let `self` = self else {return}
                
                self.refreshControl.endRefreshing()
                
                self.tableBuisnessConnect.beginUpdates()
                self.tableBuisnessConnect.alpha = 1
                self.tableBuisnessConnect.endUpdates()
                
                if error != nil
                {
                    UtilityClass.showAlertWithTitle(title: App_Name, message: error!.localizedDescription, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                    
                    return
                }
                
                if dictionary == nil
                {
                    UtilityClass.showAlertWithTitle(title: App_Name, message: App_Global_Error_Msg, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                    
                    return
                }
                
                if statusCode == 203
                {
                    let responseDictionary = dictionary!
                    guard (responseDictionary["message"] != nil) else
                    {
                        UtilityClass.showAlertWithTitle(title: App_Name, message: App_Global_Error_Msg, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                        return
                    }
//                    let messageStr = responseDictionary["message"] as! String
//                    
//                    UtilityClass.showAlertWithTitle(title: App_Name, message: messageStr, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                    return
                }
                
                if statusCode == 200
                {
                    self.arrayUpcomingEventList.removeAll()
                    self.tableBuisnessConnect.reloadData()
                    
                    let responseDictionary = dictionary!
                    let replyType = responseDictionary["type"] as! Bool
//                    let messageStr = responseDictionary["message"] as! String
                    if !replyType
                    {
                        //sign up screen
//                        UtilityClass.showAlertWithTitle(title: "", message: messageStr, onViewController: self, withButtonArray: nil, dismissHandler:nil)
                    }
                    else
                    {
                    }
                    let tempArray=responseDictionary["data"] as! [[String:Any]]
                    
                    for dict in tempArray {
                        
                        self.arrayUpcomingEventList.append(EventListModel(name: dict["title"]! as! String, discription: dict["description"]! as! String, startDate: dict["start"]! as! String, endDate: dict["end"]! as! String, id: dict["_id"]! as! String))
                    }
                    self.tableBuisnessConnect.reloadData()

                }
                        })
    }
    
    //MARK:- Update Group View
    func updateGroupView() -> Void
    {
        labelGroupName.text = groupStructModel?.groupName
      
        if let imageURL = URL (string: (groupStructModel?.groupPictureString)!)
        {
            groupPicture.setIndicatorStyle(.white)
            groupPicture.setShowActivityIndicator(true)
            groupPicture.sd_setImage(with: imageURL)
        }
        
        labelDistance.text = groupStructModel?.distance
        labelTotalFavourite.text = groupStructModel?.groupFavouriteCount
        labelCrowded.text = groupStructModel?.groupCrowded
        labelLineWaitTime.text = groupStructModel?.waitingTime
        labelOpenTill.text = groupStructModel?.openTill
        verifiedIcon.isHidden = !(groupStructModel?.isVerifiedBusiness)!
        tableBuisnessConnect.reloadData()
    }
    
    //MARK:- Socket Connection
    func initiateSocket()
    {
        SocketManager.sharedInstance().userSID = UtilityClass.getUserSidData()!
        SocketManager.sharedInstance().connectSocket
            {[weak self] (data, ack) in
                
                guard let `self` = self else { return }
                
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: NOTIFICATION_SOCKET_CONNECTED), object: nil)
                self.applyReceiveMessageListener()
        }
    }
    
    // MARK:- Message Listener (New Message)
    func applyReceiveMessageListener()
    {
        // Group message listner
        SocketManager.sharedInstance().receiveIncomingGroupMessages
            {[weak self] (messageDict, statusCode) in
                if statusCode == 200
                {
                    guard let `self` = self else {return}
                    self.manageTheIncomingMessage(usingDictionary: messageDict)
                }
        }
        
    }
    
    func manageTheIncomingMessage(usingDictionary dictionary:[String:Any])
    {
        let model = self.getNewModel(usingDictionary: dictionary)
        
        // If chat view is open...
        if let groupChatVC = appDelegate?.window??.visibleViewController() as? GroupChatViewController
        {
            // Simply add new message model...
            groupChatVC.addNewModel(usingDictionary: dictionary)
        }
        else
        {
            groupStructModel?.updatedTime = model.messageTime
            groupStructModel?.unreadCount += 1
            
            tableBuisnessConnect.reloadSections(IndexSet (integer: 1), with: .automatic)
            self.createInAppNotification(usingModel: model)
        }
    }
    
    // MARK:- Message Model Creation
    func getNewModel(usingDictionary dictionary : [String:Any]) -> ModelChatMessage
    {
        let model = ModelChatMessage () // Model creation
        model.updateModel(usingDictionary: dictionary) // Updating model
        return model
    }
    
    // MARK:- InApp Notification Fire
    func createInAppNotification(usingModel model:ModelChatMessage)
    {
        let title = model.roomID.isEmpty ? model.senderName : String (model.senderName+" @ "+model.groupName)
        
        let picture = model.roomID.isEmpty ? model.senderPicture : model.groupPicture
        
        var message = model.message
        
        if model.isImageMedia {
            message = "Sent a Photo"
        }
        if model.isVideoMedia {
            message = "Sent a Video"
        }
        
        let announce = Announcement (title: title, subtitle: message, image: nil, urlImage: picture, duration: 5, interactionType: .none, userInfo: model)
        {[weak self] (callBackType, str, announce) in
            
            guard let `self` = self else { return }
            
            if callBackType == CallbackType.tap
            {
                if let chatInfo = announce.userInfo as? ModelChatMessage
                {
                    let isGroup = !chatInfo.roomID.isEmpty
                    
                    // open screen
                    let chatViewController = UIStoryboard.getChatViewStoryboard().instantiateInitialViewController() as! GroupChatViewController
                    
                    chatViewController.hidesBottomBarWhenPushed = true
                    
                    chatViewController.isGroupChat = isGroup
                    
                    chatViewController.isBusinessChat = chatInfo.isBusinessGroup
                    
                    chatViewController.chatUsername.text = isGroup ? chatInfo.groupName : chatInfo.senderName
                    
                    if isGroup
                    {
                        chatViewController.chatPicture = chatInfo.groupPicture
                        
                        chatViewController.chatRoomID = chatInfo.roomID
                    }
                    else
                    {
                        chatViewController.chatUserID = chatInfo.senderID
                    }
                    
                    self.navigationController?.pushViewController(chatViewController, animated: true)
                }
            }
        }
        
        InAppNotify .Show(announce, to: self.navigationController!)
    }
}

//MARK: - extension -> ChangeStatusDelegate
extension BusinessHomeViewController: ChangeStatusCellDelegate, ChangeStatusDelegate
{
    func changeStatusCell(_ cell: ChangeStatusCell, didTapOnChangeStatusButton changeStatusButton: UIButton) {
        let statusVC = UIStoryboard.getEmployeeDashboardStoryboard().instantiateViewController(withIdentifier: "statusVC") as! ChangeStatusViewController
        
        statusVC.delegate = self
        //        statusVC.eventID = modelEvent.eventID
        statusVC.eventStatus = (groupStructModel?.groupStatus)!
        
        statusVC.hidesBottomBarWhenPushed = true
        
        self.navigationController?.pushViewController(statusVC, animated: true)
    }
    
    func changeStatusControllerDidChangeText(withText updatedText: String) {
        groupStructModel?.groupStatus = updatedText
        tableBuisnessConnect.reloadData()
    }
}


extension BusinessHomeViewController
{
    //MARK:- Logout API Call
    func logoutAPI() -> Void {
        
        HUD.show(.systemActivity, onView: self.view.window)
        
        self.view.endEditing(true)
        
        let urlToHit = EndPoints.logout(UtilityClass.getUserSidData()!).path
        
        
        AppWebHandler.sharedInstance().fetchData(fromURL: urlToHit, httpMethod: .get, parameters: nil, shouldDeserialize: true)
        {[weak self] (data, dictionary, statusCode, error) in
            
            HUD.hide()
            
            guard let `self` = self else { return }
            
            if error != nil
            {
                UtilityClass.showAlertWithTitle(title: App_Name, message: error!.localizedDescription, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                
                return
            }
            
            if dictionary == nil
            {
                UtilityClass.showAlertWithTitle(title: App_Name, message: App_Global_Error_Msg, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                
                return
            }
            
            if statusCode == 203
            {
                let responseDictionary = dictionary!
                guard (responseDictionary["message"] != nil) else
                {
                    UtilityClass.showAlertWithTitle(title: App_Name, message: App_Global_Error_Msg, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                    return
                }
                let messageStr = responseDictionary["message"] as! String
                
                UtilityClass.showAlertWithTitle(title: App_Name, message: messageStr, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                return
            }
            
            if statusCode == 200
            {
                
                let responseDictionary = dictionary!
                
                let type = responseDictionary["type"] as! Bool
                let msgStr = responseDictionary["message"] as! String
                
                if type
                {
                    UtilityClass.removeCurrentUserFromStack()
                    
                    UtilityClass.deleteDataOnLogout()
                    
                    currentUserID = nil
                    SocketManager.sharedInstance().removeIncomingMessagesListener()
                    SocketManager.sharedInstance().disconnectSocket()
                    
                    if UtilityClass.getFirstUserFromStackedList() != nil
                    {
                        self.updateRootViewController()
                        return
                    }
                    
                    let loginVC = UIStoryboard.getLoginFlowStoryboard().instantiateInitialViewController()
                    
                    UtilityClass.changeRootViewController(with: loginVC!)
                }
                else
                {
                    UtilityClass.showAlertWithTitle(title: App_Name, message: msgStr, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                }
            }
        }
    }
    
    func updateRootViewController()
    {
        UtilityClass.saveUserInfoData(userDict: UtilityClass.getFirstUserFromStackedList()!)
        
        if UtilityClass.getUserTypeData() == AppUserTypeEnum.employee.rawValue
        {
            let employeeVC = UIStoryboard.getBusinessHomeFlowStoryboard().instantiateInitialViewController()
            
            UtilityClass.changeRootViewController(with: employeeVC!)
            
            return
        }
        
        if UtilityClass.getUserTypeData() == AppUserTypeEnum.provider.rawValue
        {
            let businessVC = UIStoryboard.getBusinessTabBarFlowStoryboard().instantiateInitialViewController()
            
            UtilityClass.changeRootViewController(with: businessVC!)
            return
        }
        
        let userHomeVC = UIStoryboard.getUserTabsStoryboard().instantiateInitialViewController()
        
        UtilityClass.changeRootViewController(with: userHomeVC!)
        
        return
    }
}
