//
//  EmployeeListingViewController.swift
//  OOTTBusinessApp
//
//  Created by Santosh on 16/06/17.
//  Copyright © 2017 KonstantInfoSolutions. All rights reserved.
//

import UIKit
import PKHUD
import SDWebImage
import DZNEmptyDataSet

class EmployeeListModel {
    var employeeName : String
    var employeeEmail : String
    var employeePosition : String
    var employeeImageURL : URL
    var employeeID : String
    
    init(name:String = "",email:String = "",position:String = "",imageUrl:URL = URL(string: "" )!,id:String = "") {
        self.employeeName=name
        self.employeeEmail=email
        self.employeePosition=position
        self.employeeImageURL=imageUrl
        self.employeeID=id
    }
}


class EmployeeListingViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, DZNEmptyDataSetSource, DZNEmptyDataSetDelegate {
    
    @IBOutlet weak var tableEmployeeListing: UITableView!
    var arrayEmployeeList=[EmployeeListModel]()
    
    var refreshControl: UIRefreshControl!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        self.setupView()
        self.setupTableView()
        applyRefreshControl()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        employeeListApi()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    func setupView() -> Void
    {
        self.navigationItem.title = "Employees"
        
        self.showNavigationBar()
//
//        let leftBarButton = self.setLeftBarButtonItem(withImage: #imageLiteral(resourceName: "backButton"))
//        leftBarButton.action = #selector(onBackButtonAction)
//        leftBarButton.target = self
    }
    
    //MARK:- Initial table setup
    func setupTableView() -> Void
    {
        self.tableEmployeeListing.delegate = self
        self.tableEmployeeListing.dataSource = self
        self.tableEmployeeListing.emptyDataSetSource = self
        self.tableEmployeeListing.emptyDataSetDelegate = self
        //        self.tableBuisnessConnect.contentInset = UIEdgeInsetsMake(64 * scaleFactorX, 0, 0, 0)
    }
    
    // MARK:- Refresh Control
    func applyRefreshControl() {
        refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(self.refreshPage), for: UIControlEvents.valueChanged)
        self.tableEmployeeListing.addSubview(refreshControl)
    }
    
    func refreshPage(sender:AnyObject) {
        employeeListApi()
    }
    
    func onBackButtonAction() -> Void
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    
    
    //MARK:- UITableView Delegate and Datasource
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrayEmployeeList.count
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 82
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 82 * scaleFactorX;
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell =  tableView.dequeueReusableCell(withIdentifier: kCellIdentifier_employeeListing) as! EmployeeListCell
        
        cell.labelEmployeeName.text=arrayEmployeeList[indexPath.row].employeeName
        cell.labelEmployeeEmail.text=arrayEmployeeList[indexPath.row].employeeEmail
        cell.labelEmployeeRole.text=arrayEmployeeList[indexPath.row].employeePosition
        cell.imageEmployee.setShowActivityIndicator(true)
        cell.imageEmployee.setIndicatorStyle(.gray)
        cell.imageEmployee.sd_setImage(with: arrayEmployeeList[indexPath.row].employeeImageURL) { (image, error, cacheType, url) in
            cell.imageEmployee.setShowActivityIndicator(false)
            
        }
        
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    
        let employeeDetailVC = self.storyboard?.instantiateViewController(withIdentifier: "employeeDetailVC") as! EmployeeDetailViewController
        
        employeeDetailVC.hidesBottomBarWhenPushed = true
        employeeDetailVC.modelEmployeeDetail=arrayEmployeeList[indexPath.row]
        self.navigationController?.pushViewController(employeeDetailVC, animated: true)
    }
    
    //MARK:- DZNEmptyDataSetSource -> Title
    func title(forEmptyDataSet scrollView: UIScrollView!) -> NSAttributedString! {
        
        let text = "No Employees Found"
        
        let attributes = [NSFontAttributeName:UIFont .boldSystemFont(ofSize: 16*scaleFactorX), NSForegroundColorAttributeName:UIColor.white]
        
        return NSAttributedString (string: text, attributes: attributes)
    }
    
    func emptyDataSetShouldAllowScroll(_ scrollView: UIScrollView!) -> Bool {
        return true
    }
    
    @IBAction func onAddEmplyeeAction(_ sender: UIButton) {
        let addEmployeeVC = self.storyboard?.instantiateViewController(withIdentifier: "addEmployeeVC") as! AddEmployeeViewController
        
        addEmployeeVC.hidesBottomBarWhenPushed = true
        
        self.navigationController?.pushViewController(addEmployeeVC, animated: true)
    }
    
    //MARK:- List Employee API Call
    func employeeListApi() -> Void
    {
        
        HUD.show(.systemActivity, onView: self.view.window)
        
        self.view.endEditing(true)
        
        let urlToHit = EndPoints.listEmployee(UtilityClass.getUserSidData()!).path
        
        
        AppWebHandler.sharedInstance().fetchData(fromURL: urlToHit, httpMethod: .get, parameters: nil, shouldDeserialize: true)
        {[weak self] (data, dictionary, statusCode, error) in
            
            HUD.hide()
            
            guard let `self` = self else {return}
            
            self.refreshControl.endRefreshing()
            
            if error != nil
            {
                UtilityClass.showAlertWithTitle(title: App_Name, message: error!.localizedDescription, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                
                return
            }
            
            if dictionary == nil
            {
                UtilityClass.showAlertWithTitle(title: App_Name, message: App_Global_Error_Msg, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                
                return
            }
            
            if statusCode == 203
            {
                let responseDictionary = dictionary!
                guard (responseDictionary["message"] != nil) else
                {
                    UtilityClass.showAlertWithTitle(title: App_Name, message: App_Global_Error_Msg, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                    return
                }
                let messageStr = responseDictionary["message"] as! String
                
                self.arrayEmployeeList.removeAll()
                self.tableEmployeeListing.reloadData()
                
                UtilityClass.showAlertWithTitle(title: App_Name, message: messageStr, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                return
            }
            
            if statusCode == 200
            {
                self.arrayEmployeeList.removeAll()
                self.tableEmployeeListing.reloadData()

                
                let responseDictionary = dictionary!
                
                // login...
                guard responseDictionary["data"] != nil else
                {
                    if (responseDictionary["type"] as? Bool) != nil
                    {
                        
                    }
                    else
                    {
                    UtilityClass.showAlertWithTitle(title: App_Name, message: App_Global_Error_Msg, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                    }
                    
                    return
                }
                
                let tempArray=responseDictionary["data"] as! [[String:Any]]
                
                for dict in tempArray {
                    
                    let imageURL=URL(string: dict["picture"]! as! String)
                    
                    self.arrayEmployeeList.append(EmployeeListModel(name: dict["name"]! as! String, email: dict["email"]! as! String, position: dict["employeeRole"]! as! String, imageUrl: imageURL!,id:dict["_id"]! as! String))
                }
                self.tableEmployeeListing.reloadData()
            }
        }
    }
}
