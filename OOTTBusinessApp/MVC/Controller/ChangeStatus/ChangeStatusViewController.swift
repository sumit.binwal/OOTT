//
//  ChangeStatusViewController.swift
//  OOTTBouncer
//
//  Created by Santosh on 08/06/17.
//  Copyright © 2017 KonstantInfoSolutions. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift
import PKHUD
protocol ChangeStatusDelegate
{
    func changeStatusControllerDidChangeText(withText updatedText:String) -> Void
}

class ChangeStatusViewController: UIViewController, UITextFieldDelegate, UITextViewDelegate, NSLayoutManagerDelegate {

    //MARK:- Font sizes
    let fontSizeCurrentStatus = 17 * scaleFactorX
    let fontSizeStatus = 20 * scaleFactorX
    
    //MARK:- Input textfield
    @IBOutlet weak var inputField: KMPlaceholderTextView!
    
    //MARK:- Label current status
    @IBOutlet weak var labelCurrentStatus: UILabel!
    
    //MARK:- Label status header
    @IBOutlet weak var labelStatusHeader: UILabel!
    
    var delegate : ChangeStatusDelegate?
    
    var eventID : String = ""
    var eventStatus : String = ""
    
    //MARK:- viewDidLoad
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        self.setupView()
    }
    
    //MARK:- deinit
    deinit {
        print("ChangeStatusViewController deinit")
    }

    //MARK:- didReceiveMemoryWarning
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK:- Initial setup of view
    func setupView() -> Void
    {
        self.title = "STATUS"
        
        let backBarButton = self.setLeftBarButtonItem(withImage: #imageLiteral(resourceName: "backButton"))
        backBarButton.action = #selector(onBackButtonAction)
        backBarButton.target = self
        
        inputField.text = ""
        inputField.placeholder = "Write Status"
        inputField.placeholderColor = color_placeholderColor
        inputField.delegate = self
        inputField.layoutManager.delegate=self
        inputField.keyboardType = .asciiCapable
        
        labelStatusHeader.font = UIFont(name: FONT_PROXIMA_SEMIBOLD, size: fontSizeStatus)
        
        labelCurrentStatus.font = UIFont(name: FONT_PROXIMA_LIGHT, size: fontSizeCurrentStatus)
        
        labelCurrentStatus.text = eventStatus
    }
    
    //MARK:- Back button action
    func onBackButtonAction() -> Void {
        self.navigationController?.popViewController(animated: true)
    }
    
    //MARK:- Done button action
    @IBAction func onDoneButtonAction(_ sender: UIButton) {
        self.view.endEditing(true)
        if !(inputField.text?.isEmptyString())!
        {
            updateEventStatus()
        }
        else
        {
            UtilityClass.showAlertWithTitle(title: App_Name, message: "Please provide valid status", onViewController: self, withButtonArray: nil, dismissHandler: nil)
        }
    }
    
    //MARK:- NSLayoutManager Delegate
    func layoutManager(_ layoutManager: NSLayoutManager, lineSpacingAfterGlyphAt glyphIndex: Int, withProposedLineFragmentRect rect: CGRect) -> CGFloat {
        return 5
    }
    
    //MARK:- UITextView Delegate
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        
        if text == "\n"
        {
            textView.resignFirstResponder()
            return false
        }
        
        var finalCount = 0
        
        
        if text.isEmpty
        {
            if (textView.text?.isEmpty)!
            {
                finalCount = 0
            }
            else
            {
                finalCount = textView.text!.characters.count - 1
            }
        }
        else
        {
            finalCount = textView.text!.characters.count + 1
        }
        
        if (finalCount>80)
        {
            return false;
        }
        IQKeyboardManager.sharedManager().reloadLayoutIfNeeded()
//        labelCurrentStatus.text = textView.text!+text
        return true
    }
    
    //MARK:- Update Event Status
    func updateEventStatus() -> Void
    {
        HUD.show(.systemActivity, onView: self.view.window)
        
        let urlToHit = EndPoints.updateStatus(UtilityClass.getUserSidData()!).path
        
        let finalString = inputField.text!.getTrimmedText()

        let params = ["status" : finalString]
        
        
        AppWebHandler.sharedInstance().fetchData(fromURL: urlToHit, httpMethod: .post, parameters: params)
        {[weak self] (data, dictionary, statusCode, error) in
            
            HUD.hide()
            
            guard let `self` = self else { return }
            
            if error != nil
            {
                UtilityClass.showAlertWithTitle(title: App_Name, message: error!.localizedDescription, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                
                return
            }
            
            if statusCode == 203
            {
                let responseDictionary = dictionary!
                guard (responseDictionary["message"] != nil) else
                {
                    UtilityClass.showAlertWithTitle(title: App_Name, message: App_Global_Error_Msg, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                    return
                }
                let messageStr = responseDictionary["message"] as! String
                
                UtilityClass.showAlertWithTitle(title: App_Name, message: messageStr, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                return
            }
            if statusCode == 200
            {
                UtilityClass.showAlertWithTitle(title: App_Name, message: "Your status has been successfully updated.", onViewController: self, withButtonArray: nil, dismissHandler: {[weak self] (buttonIndex) in
                    
                    guard let `self` = self else {return}
                    
                    guard let delegt = self.delegate else
                    {
                        self.onBackButtonAction()
                        return
                    }
                    delegt.changeStatusControllerDidChangeText(withText: finalString)
                    self.onBackButtonAction()
                })
            }
        }
    }
}
