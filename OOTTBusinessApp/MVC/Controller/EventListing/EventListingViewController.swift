//
//  EventListingViewController.swift
//  OOTTBusinessApp
//
//  Created by Sumit Sharma on 27/06/2017.
//  Copyright © 2017 KonstantInfoSolutions. All rights reserved.
//

import Foundation
import UIKit
import PKHUD
import DZNEmptyDataSet

class EventListModel {
    var eventName : String
    var eventDiscription : String
    var eventStarTime : String
    var eventEndTime : String
    var eventID : String
    
    init(name:String = "",discription:String = "",startDate:String = "",endDate:String = "",id:String = "") {
        self.eventName=name
        self.eventDiscription=discription
        self.eventStarTime=startDate
        self.eventEndTime=endDate
        self.eventID=id
    }
}


class EventListingViewController: UIViewController,UITableViewDelegate,UITableViewDataSource, DZNEmptyDataSetSource, DZNEmptyDataSetDelegate
{
    @IBOutlet var buttonAddEvent: UIButton!
    
    @IBOutlet var tableviewEventList: UITableView!
    
    var arrayEventList=[EventListModel]()
    
    var refreshControl: UIRefreshControl!
    
    var businessID = ""
    
    var isViewOnlyMode = false
    
    var shouldShowUpcomingList = false
    

    override func viewDidLoad()
    {
        self.setupView()
        
        buttonAddEvent.isHidden = isViewOnlyMode
        
        tableviewEventList.delegate=self
        tableviewEventList.dataSource=self
        self.tableviewEventList.emptyDataSetSource = self
        self.tableviewEventList.emptyDataSetDelegate = self
        applyRefreshControl()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        refreshPage(sender: UIButton ())
    }
    
    deinit {
        print("EventListingViewController Deinit")
    }
    
    func setupView() -> Void
    {
        self.navigationItem.title = isViewOnlyMode ? "Events".uppercased() : "Events".capitalized
        
        self.showNavigationBar()
        if businessID.count == 0 {
            businessID = UtilityClass.getUserIDData()!
        }
        else
        {
            let leftBarButton = self.setLeftBarButtonItem(withImage: #imageLiteral(resourceName: "backButton"))
            leftBarButton.action = #selector(onBackButtonAction)
            leftBarButton.target = self
        }
    }
    
    // MARK:- Refresh Control
    func applyRefreshControl() {
        refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(self.refreshPage), for: UIControlEvents.valueChanged)
        self.tableviewEventList.addSubview(refreshControl)
    }
    
    func refreshPage(sender:AnyObject) {
        eventListApi()
    }
    
    func onBackButtonAction() -> Void
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    //MARK:- UITableView Delegate and Datasource
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrayEventList.count

    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 71
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 71 * scaleFactorX;
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell =  tableView.dequeueReusableCell(withIdentifier: kCellIdentifier_upcomingEvent_eventCell) as! UpcomingEventCell
        
    
        
        cell.labelEventName.text=arrayEventList[indexPath.row].eventName
        cell.labelEventDate.text=arrayEventList[indexPath.row].eventStarTime
        
        
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let eventDetail = self.storyboard?.instantiateViewController(withIdentifier: "eventDetailVC") as! EventDetailViewController
        eventDetail.hidesBottomBarWhenPushed = true
        eventDetail.isViewOnlyMode = isViewOnlyMode
        eventDetail.modelEventList=arrayEventList[indexPath.row]
        self.navigationController?.pushViewController(eventDetail, animated: true)
    }

    //MARK:- DZNEmptyDataSetSource -> Title
    func title(forEmptyDataSet scrollView: UIScrollView!) -> NSAttributedString! {
        
        let text = "No Events Found"
        
        let attributes = [NSFontAttributeName:UIFont .boldSystemFont(ofSize: 16*scaleFactorX), NSForegroundColorAttributeName:UIColor.white]
        
        return NSAttributedString (string: text, attributes: attributes)
    }
    
    func emptyDataSetShouldAllowScroll(_ scrollView: UIScrollView!) -> Bool {
        return true
    }
    
    @IBAction func createEventButtonClicked(_ sender: UIButton)
    {
        let addEventVC = self.storyboard?.instantiateViewController(withIdentifier: "createEventVC") as! CreateEventViewController
        
        addEventVC.hidesBottomBarWhenPushed = true
        
        self.navigationController?.pushViewController(addEventVC, animated: true)
    }
    
    //MARK:- List Employee API Call
    func eventListApi() -> Void
    {
        HUD.show(.systemActivity, onView: self.view.window)
        
        self.view.endEditing(true)
        var urlToHit = EndPoints.eventListing(UtilityClass.getUserSidData()!, businessID).path
        
        if shouldShowUpcomingList {
            urlToHit = EndPoints.getUpcomingEvents(UtilityClass.getUserSidData()!, businessID).path
        }
        
        AppWebHandler.sharedInstance().fetchData(fromURL: urlToHit, httpMethod: .get, parameters: nil, shouldDeserialize: true)
        {[weak self] (data, dictionary, statusCode, error) in
            
            HUD.hide()
            
            guard let `self` = self else {return}
            
            self.refreshControl.endRefreshing()
            
            if error != nil
            {
                UtilityClass.showAlertWithTitle(title: App_Name, message: error!.localizedDescription, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                
                return
            }
            
            if dictionary == nil
            {
                UtilityClass.showAlertWithTitle(title: App_Name, message: App_Global_Error_Msg, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                
                return
            }
            
            if statusCode == 203
            {
                let responseDictionary = dictionary!
                guard (responseDictionary["message"] != nil) else
                {
                    UtilityClass.showAlertWithTitle(title: App_Name, message: App_Global_Error_Msg, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                    return
                }
                let messageStr = responseDictionary["message"] as! String
                
                self.arrayEventList.removeAll()
                self.tableviewEventList.reloadData()
                
                UtilityClass.showAlertWithTitle(title: App_Name, message: messageStr, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                return
            }
            
            if statusCode == 200
            {
                self.arrayEventList.removeAll()
                self.tableviewEventList.reloadData()

                let responseDictionary = dictionary!
                
                // login...
                guard responseDictionary["data"] != nil else
                {
                    if (responseDictionary["type"] as? Bool) != nil
                    {
                        
                    }
                    else
                    {
                        UtilityClass.showAlertWithTitle(title: App_Name, message: App_Global_Error_Msg, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                    }
                    
                    return
                }
                
                let tempArray=responseDictionary["data"] as! [[String:Any]]
                
                for dict in tempArray {
                    
                    self.arrayEventList.append(EventListModel(name: dict["title"]! as! String, discription: dict["description"]! as! String, startDate: dict["start"]! as! String, endDate: dict["end"]! as! String, id: dict["_id"]! as! String))
                }
                self.tableviewEventList.reloadData()
                //    UtilityClass.saveUserInfoData(userDict: userDataDict)
                
            }
        }
    }
}
