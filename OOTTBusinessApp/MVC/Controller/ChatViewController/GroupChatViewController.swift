//
//  GroupChatViewController.swift
//  OOTTBusinessApp
//
//  Created by Santosh on 13/07/17.
//  Copyright © 2017 KonstantInfoSolutions. All rights reserved.
//

import UIKit
import PKHUD
import DZNEmptyDataSet
import UserNotifications
import IQKeyboardManagerSwift

let VIDEO_MAX_DURATION: Float64 = 10
let baseMediaURL = "https://s3.amazonaws.com/oott/"
let baseMediaThumbURL = "https://s3.amazonaws.com/oott/thumb/"
//MARK:- Model Online Status
class ModelOnlineStatus
{
    var user_id = ""
    var name = ""
    var picture = ""
    var online = false
    
    deinit {
        print("ModelOnlineStatus Deinit")
    }
    
    func updateModel(usingDictionary dictionary : [String:Any])
    {
        if let user_id = dictionary["user_id"] as? String
        {
            self.user_id = user_id
        }
        
        if let name = dictionary["name"] as? String
        {
            self.name = name
        }
        
        if let picture = dictionary["picture"] as? String
        {
            self.picture = picture
        }
        
        if let online = dictionary["online"] as? Bool
        {
            self.online = online
        }
    }
}
//MARK:- Model Chat Message
enum MediaDownloadStatus {
    case downloaded
    case isDownloading
    case error
    case none
}
class ModelChatMessage
{
    // Message
    var message = ""
    var messageID = ""
    var messageTime = ""
    
    var messageLikesCount = 0
    
    var isMessageLikedByMe = false
    
    // Sender
    var senderID = ""
    var senderName = ""
    var senderPicture = ""
    var senderPosition = ""
    
    // Receiver
    var receiverID = ""
    
    var isLoggedinUser = false
    var isGhostAccount = false
    
    var roomID = ""
    var groupName = ""
    var groupPicture = ""
    
    var isBusinessGroup = false
    
    var isVideoMedia = false
    var isImageMedia = false
    
    var thumbString = ""
    
    var lastCheckIn : LastCheckinData?
    
    var mediaDownloadStatus = MediaDownloadStatus.none
    
    var downloadProgress: CGFloat = 0
    
    /*isAddedInGroup = 0;
     isGroupName = 0;
     isGroupPhoto = 0;
     isInvitemove = 0;
     isLeftUser = 0;*/
    
    var isAddedInGroup = false
    var isGroupName = false
    var isGroupPhoto = false
    var isInvitemove = false
    var isLeftUser = false
    
    deinit {
        print("ModelChatMessage Deinit")
    }
    
    func updateModel(usingDictionary dictionary : [String:Any])
    {
        print(dictionary)
        if let isAddedInGrp = dictionary["isAddedInGroup"] as? Bool
        {
            self.isAddedInGroup = isAddedInGrp
        }
        
        
        if let isGroupName = dictionary["isGroupName"] as? Bool
        {
            self.isGroupName = isGroupName
        }
        if let isGroupPhoto = dictionary["isGroupPhoto"] as? Bool
        {
            self.isGroupPhoto = isGroupPhoto
        }
        if let isInvitemove = dictionary["isInvitemove"] as? Bool
        {
            self.isInvitemove = isInvitemove
        }
        if let isLeftUser = dictionary["isLeftUser"] as? Bool
        {
            self.isLeftUser = isLeftUser
        }
        if let msgID = dictionary["_id"] as? String
        {
            self.messageID = msgID
        }
        
        if let likesCount = dictionary["likesCount"] as? Int
        {
            self.messageLikesCount = likesCount
        }
        
        if let isLiked = dictionary["isLiked"] as? Bool
        {
            self.isMessageLikedByMe = isLiked
        }
        
        // For group
        if let room_id = dictionary["room_id"] as? String
        {
            self.roomID = room_id
        }
        
        // For group
        if let group_name = dictionary["group_name"] as? String
        {
            self.groupName = group_name
        }
        
        // For group
        if let group_picture = dictionary["group_picture"] as? String
        {
            self.groupPicture = group_picture
        }
        
        if let isBusiness = dictionary["isBusiness"] as? Bool
        {
            self.isBusinessGroup = isBusiness
        }
        
        if let message = dictionary["message"] as? String
        {
//            print(message)
            self.message = message.decodeStringFromBase64Encoding()
        }
        
        if let isMedia = dictionary["isMedia"] as? Bool, isMedia
        {
            if self.message.contains("mp4")
            {
                self.isVideoMedia = isMedia
            }
            else
            {
                self.isImageMedia = isMedia
            }
            self.message = dictionary["message"] as! String
        }
        
        if let thumb = dictionary["thumb"] as? String
        {
            self.thumbString = thumb
        }
        
        if let created = dictionary["created"] as? String
        {
            self.messageTime = created
        }
        
        if let receiver = dictionary["receiver"] as? String
        {
            self.receiverID = receiver
        }
        
        if let senderDict = dictionary["sender"] as? [String:Any]
        {
            if let id = senderDict["user_id"] as? String
            {
                self.senderID = id
            }
            
            if let ghostAccount = senderDict["isGhostAccount"] as? Bool
            {
                self.isGhostAccount = ghostAccount
            }

            if let employeeRole = senderDict["employeeRole"] as? String
            {
                self.senderPosition = employeeRole
            }
            
            if self.senderID == UtilityClass.getUserIDData()!
            {
                self.isLoggedinUser = true
            }
            
            if let name = senderDict["name"] as? String
            {
                self.senderName = name
            }
            
            if let picture = senderDict["picture"] as? String
            {
                self.senderPicture = picture
            }
            
            if let checkin = senderDict["lastCheckin"] as? [String:Any]
            {
                if self.lastCheckIn == nil
                {
                    self.lastCheckIn = LastCheckinData ()
                }
                self.lastCheckIn?.updateModel(usingDictionary: checkin)
            }
        }
    }
}


private let MaxChatWindowHeight = 160 * scaleFactorX

//MARK:-
//MARK:- GroupChatViewController
class GroupChatViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UITableViewDataSourcePrefetching, PopoverMenuDelegate
{
    //MARK:-- Instance Variables
    // Instance Variables
    @IBOutlet weak var tableChatList: UITableView!
    
    @IBOutlet weak var containerTextBox: UIView!
    
    @IBOutlet weak var inputTextView: KMPlaceholderTextView!
    
    @IBOutlet weak var buttonMediaSelect: UIButton!
    
    @IBOutlet weak var buttonSend: UIButton!
    
    @IBOutlet weak var constraintMediaSelectWidth: NSLayoutConstraint!
    @IBOutlet weak var constraintContainerTextBoxHeight: NSLayoutConstraint!
    @IBOutlet weak var constraintContainerBottom: NSLayoutConstraint!
    
    @IBOutlet weak var constraintInputTextViewHeight: NSLayoutConstraint!
    
    @IBOutlet weak var constraintSendButtonHeight: NSLayoutConstraint!
    
    @IBOutlet weak var constraintSendButtonWidth: NSLayoutConstraint!
    
    
    @IBOutlet var navTitleBar: SearchBarView!
    @IBOutlet weak var chatUsername: UILabel!
    @IBOutlet weak var chatUserOnlineLabel: UILabel!
    @IBOutlet weak var connectingView: UIView!
    
    @IBOutlet weak var groupChatMoreView: UIView!
    @IBOutlet weak var groupImageView: UIImageView!
    
    var originalContainerTextBoxHeight : CGFloat = 0
    var originalInputTextViewHeight : CGFloat = 0
    
    var popoverMenu: PopoverMenu!
    var isPopoverMenuVisible: Bool = false
    
    //-----
    var isGroupChat = false
    var isBusinessChat = false
    var isUserBlocked = false
    var chatUserID : String?
    var chatRoomID : String?
    var chatPicture: String?
    var memberPositionInGroup : String?
    
    var businessID: String?
    
    var userType = UtilityClass.getUserTypeData()
    
    var isUser = true
    
    var isAccountDeleted = false
    
    var arrayChatMessages = [ModelChatMessage]()
    
    var folderPath: String?
    
    var numberOfArrayElements = 0
    
    var fullLoaderView: UploadingView!
    
    var selectedIndex: Int = -1
    var tempSelectedIndex: Int = -1
    
    //MARK:- deinit
    deinit {
        
        UIApplication.shared.isIdleTimerDisabled = false
        
        IQKeyboardManager.sharedManager().enable = true

        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillChangeFrame, object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: NOTIFICATION_SOCKET_CONNECTED), object: nil)
        
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: NotificationInternetConnection), object: nil)
        
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UITextInputCurrentInputModeDidChange, object: nil)
        
        print("GroupChatViewController Deinit")
    }
    
    //MARK:- viewDidLoad
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        UIApplication.shared.isIdleTimerDisabled = true

        isUser = userType == AppUserTypeEnum.user.rawValue ? true : false
        
        constraintContainerTextBoxHeight.constant = constraintContainerTextBoxHeight.constant * scaleFactorX
        originalContainerTextBoxHeight = constraintContainerTextBoxHeight.constant
        
//        print(originalContainerTextBoxHeight - self.inputTextView.frame.size.height)
        
        containerTextBox.viewWithTag(10)?.layer.cornerRadius = 5 * scaleFactorX
        inputTextView.delegate = self
        inputTextView.layoutManager.delegate = self
        inputTextView.inputAccessoryView = UIView ()
        
        if  isAccountDeleted
        {
            constraintContainerTextBoxHeight.constant = 0
            constraintMediaSelectWidth.constant = 0
        }
        
        setupView()
        setupTableView()
        
        if !isGroupChat {
            getFriendOnlineStatus()
//            chatRoomID = chatUserID
        }
        
        if !isGroupChat {
            folderPath = chatUserID!
        }
        else
        {
            folderPath = chatRoomID!
        }

//        getChatHistory()
        
        IQKeyboardManager.sharedManager().enable = false
        
        NotificationCenter.default.addObserver(self, selector: #selector(notificationSocketConnected), name: NSNotification.Name(rawValue: NOTIFICATION_SOCKET_CONNECTED), object: nil)
        
        // Keyobard frame change notification
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillChangeFrame), name: NSNotification.Name.UIKeyboardWillChangeFrame, object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(internetReachabilityChanged), name: NSNotification.Name(rawValue: NotificationInternetConnection), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(textInputModeChangedNotification), name: NSNotification.Name.UITextInputCurrentInputModeDidChange, object: nil)

        fullLoaderView = UploadingView (frame: (self.navigationController?.view.bounds)!)
        fullLoaderView.isHidden = true
        self.navigationController?.view.addSubview(fullLoaderView)
    }
    
    //MARK:- didReceiveMemoryWarning
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK:- viewWillAppear
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationItem.hidesBackButton = true
        getChatHistory()
    }
    
    //MARK:- viewDidAppear
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        currentUserID = isGroupChat ? chatRoomID : chatUserID
    }
    
    override func viewWillLayoutSubviews() {
        
    }
    
    //MARK:- viewWillDisappear
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        currentUserID = nil
        
        if SocketManager.sharedInstance().isSocketConnected(), chatRoomID != nil
        {
            SocketManager.sharedInstance().userDisappearingFromChatRoom(chatRoomID!)
        }

        if self.isMovingFromParentViewController
        {
            SocketManager.sharedInstance().removeFriendOnlineStatusListener()
        }
    }
    
    // MARK:- Initial setup view
    func setupView() {
        
        chatUsername.text = chatUsername.text?.uppercased()
        chatUserOnlineLabel.isHidden = true
        self.navigationItem.hidesBackButton = true
        
//        let leftBarButton = self.setLeftBarButtonItem(withImage: #imageLiteral(resourceName: "backButton"))
//        leftBarButton.action = #selector(onBackButtonAction)
//        leftBarButton.target = self
        
        navTitleBar.frame = CGRect (x: 0, y: 0, width: self.view.frame.size.width, height: UIDevice.isIphoneX ? 44 * scaleFactorY : 44)
        self.navigationItem.titleView = navTitleBar
        
        connectingView.backgroundColor = UIColor.clear
        connectingView.isHidden = true
        
        if !SocketManager.sharedInstance().isSocketConnected()
        {
            connectingView.isHidden = false
            chatUserOnlineLabel.isHidden = true
            chatUsername.isHidden = true
            buttonSend.isEnabled = false
        }
        
//        groupChatMoreView.isHidden = !isGroupChat
        initializePopoverMenu()

        if isGroupChat {
            groupImageView.makeRoundWithoutScale()
            groupImageView.setIndicatorStyle(.white)
            groupImageView.setShowActivityIndicator(true)
            if let chatString = chatPicture, let url = URL (string: chatString) {
                groupImageView.sd_setImage(with: url, completed: nil)
            }
        }
        else
        {
//            constraintMediaSelectWidth.constant = 0
        }
    }
    
    // MARK:- On Back Action
    @IBAction func onBackButtonAction(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func onGroupMoreButtonAction(_ sender: UIButton) {
        updatePopoverMenuVisibility()
    }
    
    //MARK:- setupTableView
    func setupTableView()
    {
        tableChatList.delegate = self
        tableChatList.dataSource = self
        tableChatList.prefetchDataSource = self
        
        tableChatList.emptyDataSetSource = self
        tableChatList.emptyDataSetDelegate = self
        
        tableChatList.estimatedRowHeight = 100
        tableChatList.rowHeight = UITableViewAutomaticDimension
    }
    
    //MARK:- Pop Over More Menu
    func initializePopoverMenu(){
        
        popoverMenu = PopoverMenu().instanceFromNib() as! PopoverMenu
        
        let scaleFactorX: CGFloat = (self.view.frame.size.width/375.0)
        let menuWidth = 112.0*scaleFactorX
        let menuXOrigin = self.view.frame.size.width-menuWidth-(19.5*scaleFactorX)
//        popoverMenu.frame = CGRect(x: menuXOrigin, y: 64*scaleFactorX, width: menuWidth, height: 0)
        
            popoverMenu.frame = CGRect(x: menuXOrigin, y: UIDevice.isIphoneX ? 70*scaleFactorY : 64, width: menuWidth, height: 0)

        
        updatePopoverMenuItems()
        
        popoverMenu.menuDelegate = self
    }
    
    func updatePopoverMenuItems()
    {
        popoverMenu.menuItems = isBusinessChat ? ["Members", "Gallery"] : ["Settings", "Members", "Gallery", "Leave"]
        if isUser, !isGroupChat
        {
            popoverMenu.menuItems = isUserBlocked ? ["Unblock"] : ["Block"]
        }
        
        popoverMenu.updateView()
    }
    
    func updatePopoverMenuVisibility(){
        
        if(isPopoverMenuVisible){
            popoverMenu.removeFromSuperview()
        } else {
            self.view.addSubview(popoverMenu)
        }
        isPopoverMenuVisible = !isPopoverMenuVisible
    }
    
    //MARK:- Notification Socket Connected
    func notificationSocketConnected()
    {
        connectingView.isHidden = true
        chatUsername.isHidden = false
        buttonSend.isEnabled = true
        buttonSend.isUserInteractionEnabled = true
        getChatHistory()
    }
    
    //MARK: -
    func textInputModeChangedNotification() {
//        print(UITextInputMode .activeInputModes)
        self.inputTextView.reloadInputViews()
    }
    //MARK:- Notification Keyboard Frame Change
    func keyboardWillChangeFrame(notification: Notification)
    {
        if let userInfo = notification.userInfo {
            let endFrame = (userInfo[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue
            let duration:TimeInterval = (userInfo[UIKeyboardAnimationDurationUserInfoKey] as? NSNumber)?.doubleValue ?? 0
            let animationCurveRawNSN = userInfo[UIKeyboardAnimationCurveUserInfoKey] as? NSNumber
            let animationCurveRaw = animationCurveRawNSN?.uintValue ?? UIViewAnimationOptions.curveEaseInOut.rawValue
            let animationCurve:UIViewAnimationOptions = UIViewAnimationOptions(rawValue: animationCurveRaw)
            if (endFrame?.origin.y)! >= UIScreen.main.bounds.size.height {
                self.constraintContainerBottom.constant = 0.0
            } else {
                self.constraintContainerBottom.constant = UIDevice.isIphoneX ? (endFrame?.size.height)!-20 : endFrame?.size.height ?? 0.0
            }
            
            weak var weakSelf = self
            
            UIView.animate(withDuration: duration,
                           delay: TimeInterval(0),
                           options: animationCurve,
                           animations:
                {
                    weakSelf?.view.layoutIfNeeded()
            },
                           completion: nil)
        }
    }
    
    //MARK:- Reachability Internet Change
    func internetReachabilityChanged(notification: Notification) {
        if let isReachable = notification.object as? Bool {
            print("chatinternet ",isReachable)
            if !isReachable
            {
                connectingView.isHidden = false
                chatUserOnlineLabel.isHidden = true
                chatUsername.isHidden = true
                buttonSend.isEnabled = false
            }
        }
    }
    
    //MARK:- Group Delete Action
    func groupDeleted()
    {
        UtilityClass.showAlertWithTitle(title: chatUsername.text, message: "This group has been deleted by the leader", onViewController: self, withButtonArray: nil)
        {[weak self] (buttonIndex) in
            guard let `self` = self else {return}
            self.onBackButtonAction(UIButton ())
        }
    }
    
    //MARK:- UITableView Delegate and Datasource
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrayChatMessages.count
    }
    
//    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
//        return 101 * scaleFactorX
//    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let model = arrayChatMessages[indexPath.row]
        if (model.isImageMedia || model.isVideoMedia) {
            if selectedIndex == indexPath.row {
                return 200 * scaleFactorX + 80
            }
            return 200 * scaleFactorX
        }
        return UITableViewAutomaticDimension
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        var cell : ChatViewCell!
        
        // Checking other chat condition for other members (not logged in)...
        // If  group chat, then index 0 and identifier kCellIdentifier_GroupCell_Other
        // Else If  business chat, then index 3 and identifier kCellIdentifier_GroupCell_Business
        // Else index 2 and identifier kCellIdentifier_GroupCell_Single
        
        var index = isGroupChat ? (isBusinessChat ? 3 : 0) : 2
        var cellIdentifier = isGroupChat ? (isBusinessChat ? kCellIdentifier_GroupCell_Business : kCellIdentifier_GroupCell_Other) : kCellIdentifier_GroupCell_Single
        var cellType = isGroupChat ? (isBusinessChat ? ChatCellType.user : ChatCellType.groupOther) : ChatCellType.singleOther
        
        // Fetching model of current message
        let msgModel = arrayChatMessages[indexPath.row]
        
        // Checking other chat condition for other members (not logged in)...
        // If type is media (i.e. image or video), then
        // Index 6 and identifier kCellIdentifier_GroupCell_OtherMedia
        if msgModel.isImageMedia || msgModel.isVideoMedia
        {
            index = isBusinessChat ? 7 : 6
            cellIdentifier = isBusinessChat ? kCellIdentifier_GroupCell_BusinessMedia : kCellIdentifier_GroupCell_OtherMedia
            cellType = isBusinessChat ? ChatCellType.user : ChatCellType.groupOther
            
            if !isGroupChat {
                index = 9
                cellIdentifier = kCellIdentifier_GroupCell_SingleMedia
                cellType = .singleOther
            }
        }
        // Other chat end
        
        // Logged in user cell (own cell)
        if msgModel.isLoggedinUser
        {
            index = 1
            cellIdentifier = kCellIdentifier_GroupCell_Own
            cellType = ChatCellType.user
            if isGroupChat
            {
                index = 4
                cellIdentifier = kCellIdentifier_GroupCell_OwnLike
                cellType = ChatCellType.userLike
                
                if msgModel.isImageMedia || msgModel.isVideoMedia
                {
                    index = 5
                    cellIdentifier = kCellIdentifier_GroupCell_OwnLikeMedia
                    cellType = ChatCellType.userLike
                }
            }
            else {
                if msgModel.isImageMedia || msgModel.isVideoMedia
                {
                    index = 8
                    cellIdentifier = kCellIdentifier_GroupCell_OwnMedia
                    cellType = ChatCellType.user
                }
            }
        }
        
        if msgModel.isLeftUser || msgModel.isGroupName || msgModel.isGroupPhoto || msgModel.isAddedInGroup || msgModel.isInvitemove {
            index = 10
            cellIdentifier = kCellIdentifier_Chat_NotifyCell
            cellType = ChatCellType.notify
        }
        
        if let cellOther : ChatViewCell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier) as? ChatViewCell
        {
            cell = cellOther
        }
        else
        {
            if let cellOther : ChatViewCell = Bundle.main.loadNibNamed("ChatViewCell", owner: self, options: nil)?[index] as? ChatViewCell
            {
                cell = cellOther
            }
        }
        
        cell.tag = indexPath.row
        
        cell.cellType = cellType
        
        cell.updateCell(usingModel: arrayChatMessages[indexPath.row], cellIndex: indexPath.row, folderPath: folderPath!)
        
        cell.delegate = self
        
        if selectedIndex >= 0 {
            if selectedIndex == indexPath.row {
                cell.contentView.alpha = 1.0
                cell.isUserInteractionEnabled = true
                if cell.buttonLikeUnLike != nil {
                    cell.buttonLikeUnLike.isEnabled = false
                }
            }
            else {
                cell.contentView.alpha = 0.2
                cell.isUserInteractionEnabled = false
            }
        }
        else {
            cell.contentView.alpha = 1.0
            cell.isUserInteractionEnabled = true
            if cell.buttonLikeUnLike != nil {
                cell.buttonLikeUnLike.isEnabled = true
            }
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        tableView.deselectRow(at: indexPath, animated: true)
        let msgModel = arrayChatMessages[indexPath.row]
        if msgModel.isLeftUser || msgModel.isGroupName || msgModel.isGroupPhoto || msgModel.isAddedInGroup || msgModel.isInvitemove {
            return
        }
        
        if !isGroupChat {
            return
        }
        
        if let cell = tableView.cellForRow(at: IndexPath (row: selectedIndex, section: 0)) as? ChatViewCell {
            cell.constraintCollectionViewHeight.constant = 0
        }
        
        if selectedIndex == indexPath.row {
            selectedIndex = -1
            tempSelectedIndex = -1
            tableView.reloadData()
            return
        }
        tableView.reloadData()
        selectedIndex = -1
        tempSelectedIndex = indexPath.row
        
        getChatLikesHistory()
        
        return
        
//        let groupChatLikesViewController = self.storyboard?.instantiateViewController(withIdentifier: "GroupChatLikesVC") as! GroupChatLikesViewController
//
//        groupChatLikesViewController.message_id = arrayChatMessages[indexPath.row].messageID
//        groupChatLikesViewController.chatRoomID = chatRoomID!
//
//        self.navigationController?.pushViewController (groupChatLikesViewController, animated: true)
    }
    
    func tableView(_ tableView: UITableView, prefetchRowsAt indexPaths: [IndexPath]) {
        
    }
    
    //MARK:-
    @IBAction func onButtonMediaSelectAction(_ sender: UIButton) {
        self.view.endEditing(true)
        UtilityClass.showImagePickerController(onController: self)
    }
    
    //MARK:- Send Button Action
    @IBAction func onSendButtonAction(_ sender: UIButton) {
        
        if inputTextView.text.isEmptyString()
        {
            return
        }//9599956845
        
        //5369 0772 5004 0419 - 282 - 02/22
        
        buttonSend.isUserInteractionEnabled = false
        
        if isGroupChat
        {
            SocketManager.sharedInstance().sendGroupMessage(SocketModelSendGroupMessage (chatRoomID!, message: inputTextView.text.base64EncodedString(), isMedia: false, normalMessage: inputTextView.text, messageType: .normal), messageHandler:
                {[weak self] (messageDict, statusCode) in
                    
                    guard let `self` = self else {return}
                    
                    self.buttonSend.isUserInteractionEnabled = true
                    
//                    print(messageDict)
                    
                    if statusCode == 200
                    {
                        self.inputTextView.text = ""
                        self.textViewDidChange(self.inputTextView)
                        self.addNewModel(usingDictionary: messageDict)
                    }
            })
        }
        else
        {
            SocketManager.sharedInstance().sendMessage(SocketModelSendMessage (self.chatUserID!, message: inputTextView.text.base64EncodedString(), normalMessage: inputTextView.text, isMedia: false))
            {[weak self] (messageDict, statusCode) in
                
                guard let `self` = self else {return}
                
                self.buttonSend.isUserInteractionEnabled = true
                
//                print(messageDict)
                
                if statusCode == 200
                {
                    self.inputTextView.text = ""
                    self.textViewDidChange(self.inputTextView)
                    self.addNewModel(usingDictionary: messageDict)
                }
            }
        }
    }
    
    //MARK: - Send Message -> Media Version
    func sendImageTagToServer(imageName: String, imageData: Data) {
        
        if !SocketManager.sharedInstance().isSocketConnected()
        {
            return
        }
        HUD.show(.systemActivity, onView: self.view.window)
        
        if isGroupChat {
            SocketManager.sharedInstance().sendGroupMessage(SocketModelSendGroupMessage (chatRoomID!, message: imageName, isMedia: true, normalMessage: imageName, messageType: .normal), messageHandler:
                {[weak self] (messageDict, statusCode) in
                    
                    HUD.hide()
                    
                    guard let `self` = self else {return}
                    
                    self.buttonSend.isUserInteractionEnabled = true
                    
                    //                print(messageDict)
                    
                    if statusCode == 200
                    {
                        if self.folderPath != nil
                        {
                            let isSaved = UtilityClass.addFileToFolder(self.folderPath!, fileName: imageName, fileData: imageData)
                            if isSaved {
                                self.addNewModel(usingDictionary: messageDict)
                            }
                        }
                    }
            })
        }
        else {
            SocketManager.sharedInstance().sendMessage(SocketModelSendMessage (folderPath!, message: imageName, normalMessage: imageName, isMedia: true)) {[weak self] (messageDict, statusCode) in
                
                HUD.hide()
                
                guard let `self` = self else {return}
                
                self.buttonSend.isUserInteractionEnabled = true
                
                if statusCode == 200
                {
                    if self.folderPath != nil
                    {
                        let isSaved = UtilityClass.addFileToFolder(self.folderPath!, fileName: imageName, fileData: imageData)
                        if isSaved {
                            self.addNewModel(usingDictionary: messageDict)
                        }
                    }
                }
            }
        }
    }
    
    //MARK:- Get Chat History
    func getChatHistory()
    {
        if !SocketManager.sharedInstance().isSocketConnected()
        {
            return
        }
        
        HUD.show(.systemActivity, onView: self.view.window)
        
        if isGroupChat
        {
            SocketManager.sharedInstance().getGroupChatHistory(SocketModelGetHistory (nil, roomID: chatRoomID))
            {[weak self] (historyArray, memberPosition, statusCode) in
                HUD.hide()
                
                guard let `self` = self else {return}
                
                if statusCode == 200
                {
                    self.memberPositionInGroup = memberPosition
                    self.updateModel(usingArray: historyArray)
                }
            }
        }
        else
        {
            SocketManager.sharedInstance().getUserChatHistory(SocketModelGetHistory (chatUserID, roomID: chatRoomID))
            {[weak self] (historyArray, isBlocked, statusCode) in
                HUD.hide()
                
                guard let `self` = self else {return}
                
                if statusCode == 200
                {
                    self.isUserBlocked = isBlocked
                    self.updatePopoverMenuItems()
                    self.updateModel(usingArray: historyArray)
                }
            }
        }
    }
    
    //MARK:- Update chat model array
    func updateModel(usingArray array : [[String:Any]])
    {
        arrayChatMessages.removeAll()
       
        let dataArray = array
        
        for dict in dataArray // Iterating dictionaries
        {
            let model = ModelChatMessage () // Model creation
            model.updateModel(usingDictionary: dict) // Updating model
            arrayChatMessages.append(model) // Adding model to array
        }
        
        
        
        tableChatList.reloadData()
        
        if numberOfArrayElements == 0
        {
            numberOfArrayElements = arrayChatMessages.count
            if numberOfArrayElements > 0
            {
                tableChatList.scrollToRow(at: IndexPath (row: arrayChatMessages.count-1, section: 0), at: .top, animated: false)
            }
        }
        
        updatePopoverMenuItems()
    }
    
    //MARK:- Add New chat model
    func addNewModel(usingDictionary dictionary : [String:Any])
    {
        if let cell = tableChatList.cellForRow(at: IndexPath (row: selectedIndex, section: 0)) as? ChatViewCell {
            cell.constraintCollectionViewHeight.constant = 0
        }
        tempSelectedIndex = -1
        selectedIndex = -1
        let model = ModelChatMessage () // Model creation
        model.updateModel(usingDictionary: dictionary) // Updating model
        arrayChatMessages.append(model)
        tableChatList.reloadData()
        tableChatList.scrollToRow(at: IndexPath (row: arrayChatMessages.count-1, section: 0), at: .top, animated: true)
        
        if model.isGroupName {
            chatUsername.text = model.groupName.uppercased()
        }
        
        if model.isGroupPhoto {
            if let url = URL (string: model.groupPicture) {
                groupImageView.sd_setImage(with: url, completed: nil)
            }
        }
    }
    
    //MARK:- Get Friends Online Status
    func getFriendOnlineStatus()
    {
        SocketManager.sharedInstance().getFriendOnlineStatus
            {[weak self] (statusDict, statusCode) in
                
                guard let `self` = self else {return}
                
                if statusCode == 200
                {
                    let statusModel = ModelOnlineStatus ()
                    statusModel.updateModel(usingDictionary: statusDict)
                    
                    if statusModel.user_id == self.chatUserID!
                    {
                        self.updateNavBarView(statusModel)
                    }
                }
        }
    }
    
    func updateNavBarView(_ usingStatusModel : ModelOnlineStatus?)
    {
        chatUserOnlineLabel.isHidden = true
        if usingStatusModel != nil
        {
            if (usingStatusModel?.online)!
            {
                chatUserOnlineLabel.isHidden = isGroupChat ? true : false
            }
            chatUsername.text = usingStatusModel?.name.uppercased()
        }
    }
    
    //MARK:- More Menu Actions -> Leave Group
    func onLeaveGroupAction()
    {
        if SocketManager.sharedInstance().isSocketConnected()
        {
            self.isLeftGroupNotify()
            return;
//            SocketManager.sharedInstance().leaveGroup(chatRoomID!, messageHandler:
//                {[weak self] (dictionary, statusCode) in
//                    guard let `self` = self else {return}
//                    if statusCode == 200
//                    {
//                        self.isLeftGroupNotify()
//                        self.onBackButtonAction(UIButton ())
//                    }
//            })
        }
    }
    
    func isLeftGroupNotify() {
        if SocketManager.sharedInstance().isSocketConnected()
        {
            HUD.show(.systemActivity, onView: self.view.window)

            SocketManager.sharedInstance().sendGroupMessage(SocketModelSendGroupMessage (true, roomID: chatRoomID!, messageType: .isLeftGroup)) {[weak self] result, statusCode in
                HUD.hide()
                guard let `self` = self else {return}
                if statusCode == 200
                {
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "groupLeave"), object: ["groupID": self.chatRoomID!], userInfo: nil)
                    self.onBackButtonAction(UIButton ())
                }
            }
        }
    }
    
    //MARK:- More Menu Actions -> View Members
    func onViewMembersAction()
    {
        let followFollowingVC = UIStoryboard.getUserHomeStoryboard().instantiateViewController(withIdentifier: "groupLikesViewController") as! GroupLikesViewController
        followFollowingVC.viewType = .membersOfGroup
        
        followFollowingVC.hidesBottomBarWhenPushed = true
        followFollowingVC.isBusinessGroup = isBusinessChat
        followFollowingVC.userID = chatRoomID!
        
        self.navigationController?.pushViewController(followFollowingVC, animated: true)
        
//        let membersVC : AttendeesViewController = UIStoryboard.getBusinessHomeFlowStoryboard().instantiateViewController(withIdentifier: "attendeesVC") as! AttendeesViewController
//        membersVC.isViewMembers = true
//        membersVC.chatRoomID = chatRoomID!
//        self.navigationController?.pushViewController(membersVC, animated: true)
    }
    
    //MARK:- More Menu Actions -> View Events
    func onViewEventsAction()
    {
        let eventsVC : EventListingViewController = UIStoryboard.getEventListStoryboard().instantiateViewController(withIdentifier: "eventListVC") as! EventListingViewController
        eventsVC.businessID = businessID!
        eventsVC.isViewOnlyMode = memberPositionInGroup == "leader" ? false : true
        self.navigationController?.pushViewController(eventsVC, animated: true)
    }
    
    //MARK:- More Menu Actions -> Block User
    func onBlockUserAction()
    {
        guard let appDeleg = appDelegate as? AppDelegate else {
            return
        }
        
        if !appDeleg.isInternetAvailable()
        {
            return
        }
        
        HUD.show(.systemActivity, onView: self.view.window)
        
        SocketManager.sharedInstance().block(chatUserID!)
        {[weak self] (dictionary, statusCode) in
            
            HUD.hide()
            
            if statusCode == 200
            {
                guard let `self` = self else {return}
                
                self.isUserBlocked = dictionary["isblocked"] as! Bool
                self.updatePopoverMenuItems()
            }
        }
    }
    
    //MARK:- More Menu Actions -> UnBlock User
    func onUnBlockUserAction()
    {
        guard let appDeleg = appDelegate as? AppDelegate else {
            return
        }
        
        if !appDeleg.isInternetAvailable()
        {
            return
        }
        
        HUD.show(.systemActivity, onView: self.view.window)
        
        SocketManager.sharedInstance().unBlock(chatUserID!)
        {[weak self] (dictionary, statusCode) in
            
            HUD.hide()
            
            if statusCode == 200
            {
                guard let `self` = self else {return}
                self.isUserBlocked = dictionary["isblocked"] as! Bool
                self.updatePopoverMenuItems()
            }
        }
    }
    
    //MARK:- More Menu Actions -> Settings
    func onSettingsAction()
    {
        let editGroup : AddGroupViewController = UIStoryboard.getUserConnectStoryboard().instantiateViewController(withIdentifier: "addGroupVC") as! AddGroupViewController
        
        editGroup.isEditGroup = true
        
        editGroup.groupID = chatRoomID!
        
        self.navigationController?.pushViewController(editGroup, animated: true)
    }
    
    //MARK:- More Menu Actions -> Gallery
    func onGalleryAction()
    {
        if let mediaArray = UtilityClass.getContents(inFolder: folderPath!) {
            
            let mediaViewerVC: MediaViewerController = UIStoryboard.getMediaViewerStoryboard().instantiateViewController(withIdentifier: "mediaViewerVC") as! MediaViewerController
            
            var newFilteredArray = [ModelMediaViewer]()
            
            
            for mediaPath in mediaArray {
                if mediaPath.pathExtension == "jpg"
                {
                    let model = ModelMediaViewer (mediaLocalURL: mediaPath, isImageMedia: true)
                    newFilteredArray.append(model)
                }
                else {
                    let model = ModelMediaViewer (mediaLocalURL: mediaPath, isImageMedia: false)
                    newFilteredArray.append(model)
                }
            }
            
            mediaViewerVC.arrayMediaViewer = newFilteredArray
            
            self.navigationController?.pushViewController(mediaViewerVC, animated: true)
        }
    }
    
    //MARK: -
    func startCompressionOfVideo(videoURL: URL) {
        let compressedURL = NSURL.fileURL(withPath: NSTemporaryDirectory() + NSUUID().uuidString + ".mp4")
        UtilityClass.compressVideo(inputURL: videoURL, outputURL: compressedURL)
        {[weak self] (exportSession) in
            
            guard let `self` = self else {
                print("self is deallocated")
                return
            }
            
            guard let session = exportSession else {
                print("exportSession is deallocated")
                return
            }
            print(session.status)
            switch session.status {
            case .completed:
                print(videoURL)
                self.uploadVideo(usingURL: videoURL)
                break
            case .exporting:
                break
            case .unknown:
                break
            case .waiting:
                break
            case .failed:
                print("exportSession failed: %@", session.error ?? "")
                break
            default:
                // Cancelled or failed
                break
            }
            
        }
    }
}

//MARK:-
//MARK:- Extension
extension GroupChatViewController : UITextViewDelegate, DZNEmptyDataSetSource, DZNEmptyDataSetDelegate, NSLayoutManagerDelegate
{
    
    func textViewDidChange(_ textView: UITextView) {
        
        self.inputTextView.isScrollEnabled = false
        
        let newHeight = textView.intrinsicContentSize.height + (constraintContainerTextBoxHeight.constant - textView.frame.size.height)
        
        constraintContainerTextBoxHeight.constant = max(originalContainerTextBoxHeight, min(newHeight, MaxChatWindowHeight))
        
        if constraintContainerTextBoxHeight.constant >= MaxChatWindowHeight
        {
            self.inputTextView.isScrollEnabled = true
            return
        }
        
        UIView.animate(withDuration: 0.25, delay: 0.0, usingSpringWithDamping: 1.0, initialSpringVelocity: 1.0, options: .curveLinear, animations:
            {
                self.view.layoutIfNeeded()
        }, completion: nil)
    }
    
    //MARK:- DZNEmptyDataSetSource -> Title
    func title(forEmptyDataSet scrollView: UIScrollView!) -> NSAttributedString! {
        
        let text = isBusinessChat ? "No conversation" : "Be the first to start the conversation"
        
        let attributes = [NSFontAttributeName:UIFont .boldSystemFont(ofSize: 16*scaleFactorX), NSForegroundColorAttributeName:UIColor.white]
        
        return NSAttributedString (string: text, attributes: attributes)
    }
    
    func emptyDataSetShouldAllowScroll(_ scrollView: UIScrollView!) -> Bool {
        return true
    }
    
    func layoutManager(_ layoutManager: NSLayoutManager, lineSpacingAfterGlyphAt glyphIndex: Int, withProposedLineFragmentRect rect: CGRect) -> CGFloat {
        return 3;
    }
    
    //MARK: - PopoverMenu Delegate Methods
    func didSelectedItemAt(index: Int) {
        updatePopoverMenuVisibility();
        print("selected \(index)")
        
        guard let appDeleg = appDelegate as? AppDelegate else {
            return
        }
        
        if !appDeleg.isInternetAvailable()
        {
            return
        }
        
        if isUser, !isGroupChat // Single user chat (Block / UnBlock)
        {
            isUserBlocked ? onUnBlockUserAction() : onBlockUserAction()
            return
        }
        
        // Business Chat
        if isBusinessChat {
            
            switch index {
            case 0: // Members
                onViewMembersAction()
                break
//            case 1: // Events
//                onViewEventsAction()
//                break
            default: // Gallery
                onGalleryAction()
                break
            }
            
            return
        }
        
//        if memberPositionInGroup != "leader"
//        {
//            switch index {
//            case 0: // Members
//                onViewMembersAction()
//                break
//            case 1: // Leave
//                onLeaveGroupAction()
//                break
//            default:
//                break
//            }
//            return
//        }

        // User group chat
        switch index {
        case 0: // Settings
            onSettingsAction()
            break
        case 1: // Members
            onViewMembersAction()
            break
        case 3: // Leave
            onLeaveGroupAction()
            break
        default: // Gallery
            onGalleryAction()
            break
        }
    }
}

//MARK:-
//MARK:- Extension -> ChatViewCellDelegate
extension GroupChatViewController: ChatViewCellDelegate
{
    func chatCell(_ cell: ChatViewCell, didTapOnLikeUser userModel: ModelGroupChatLikesMessage) {
        if userModel._id == UtilityClass.getUserIDData()! {
            return
        }
        
        if userModel.userType == AppUserTypeEnum.user.rawValue { // User
            let profileViewController = UIStoryboard.getUserProfileStoryboard().instantiateViewController(withIdentifier: "userProfileViewController") as! UserProfileViewController
            
            profileViewController.targetUserID = userModel.user_id
            profileViewController.isLoggedInUser = false
            profileViewController.hidesBottomBarWhenPushed = true
            self.navigationController?.pushViewController(profileViewController, animated: true)
        }
        else if userModel.userType == AppUserTypeEnum.provider.rawValue {
            let detailsViewController = UIStoryboard.getBusinessDetailsStoryboard().instantiateViewController(withIdentifier: "businessDetailsViewController") as! BusinessDetailsViewController
            detailsViewController.businessId = userModel._id
            detailsViewController.isFromDetail = true
            detailsViewController.hidesBottomBarWhenPushed = true
            self.navigationController?.pushViewController(detailsViewController, animated: true)
        }
    }
    
    //MARK:- Like-Unlike Button
    func chatCell(_ cell: ChatViewCell, didTapOnLikeUnLikeButton likeUnLikeButton: UIButton) {
        
        likeUnLikeButton.isEnabled = false
        
        let index = cell.tag
        
        let model = arrayChatMessages[index]
        
        SocketManager.sharedInstance().likeUnLikeMessage(SocketModelLikeMessage (model.messageID, roomID: chatRoomID, like: !model.isMessageLikedByMe))
        {[weak self] (dictionary, statusCode) in
            
            guard let `self` = self else {return}
            
            likeUnLikeButton.isEnabled = true
            
            if statusCode == 200
            {
                model.isMessageLikedByMe = !model.isMessageLikedByMe
                model.messageLikesCount = model.isMessageLikedByMe ? model.messageLikesCount + 1 : model.messageLikesCount - 1
                
                self.arrayChatMessages[index] = model
                
                self.tableChatList.reloadRows(at: [IndexPath (row: index, section: 0)], with: .none)
            }
        }
    }
    
    //MARK:- Download Media
    func chatCellDidDownloadTheMedia(_ cell: ChatViewCell) {
        
        let model = arrayChatMessages[cell.tag]
        
        downloadImage(toPath: (UtilityClass.getDocumentsFolder(withName: folderPath!)?.absoluteString)!+"/"+model.message, imageKeyName: model.message, forCell: cell)
    }
    
    //MARK:- Media Detail
    func chatCell(_ cell: ChatViewCell, didTapOnMedia mediaButton: UIButton) {
        
        if let mediaArray = UtilityClass.getContents(inFolder: folderPath!) {
            
            let mediaDetailVC: MediaViewerDetailController = UIStoryboard.getMediaViewerStoryboard().instantiateViewController(withIdentifier: "mediaViewerDetailVC") as! MediaViewerDetailController
            
            var newFilteredArray = [ModelMediaViewer]()
            
            for mediaPath in mediaArray {
                if mediaPath.pathExtension == "jpg"
                {
                    let model = ModelMediaViewer (mediaLocalURL: mediaPath, isImageMedia: true)
                    newFilteredArray.append(model)
                }
                else {
                    let model = ModelMediaViewer (mediaLocalURL: mediaPath, isImageMedia: false)
                    newFilteredArray.append(model)
                }
            }
            
            mediaDetailVC.arrayMediaDetails = newFilteredArray
            
            if let index = newFilteredArray.index(where: { (model) -> Bool in
                return model.mediaLocalURL.lastPathComponent == arrayChatMessages[cell.tag].message
            })
            {
                mediaDetailVC.startIndex = index
            }
            
            self.navigationController?.pushViewController(mediaDetailVC, animated: true)
        }
    }
}

//MARK:-
//MARK:- Extension -> UIImagePickerControllerDelegate
import AVFoundation
extension GroupChatViewController : UIImagePickerControllerDelegate, UINavigationControllerDelegate
{
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        
        picker.dismiss(animated: true, completion: nil)
        
        if let mediaType = info[UIImagePickerControllerMediaType] as? String {
            
            if mediaType  == "public.image" {
                if let image = info[UIImagePickerControllerOriginalImage] as? UIImage
                {
                    self.uploadImage(usingImage: image)
                }
            }
            else if mediaType  == "public.movie" {
                
                if let url: URL = info[UIImagePickerControllerMediaURL] as? URL
                {
                    uploadVideo(usingURL: url)
//                    startCompressionOfVideo(videoURL: url)
//                    let asset = AVAsset(url: url)
//
//                    let duration = asset.duration
//                    let durationTime = CMTimeGetSeconds(duration)
//
//                    print("duration = \(durationTime)")
//
//                    if UIVideoEditorController.canEditVideo(atPath: url.path) {
//
//                        let videoEditor = UIVideoEditorController()
//
//                        videoEditor.delegate = self
//
//                        videoEditor.videoPath = url.path
//
//                        videoEditor.videoMaximumDuration = durationTime
//
//                        self.present(videoEditor, animated: true, completion: nil)
//                    }
                }
            }
        }
    }
}

//MARK:-
//MARK:- Extension -> UIVideoEditorControllerDelegate
extension GroupChatViewController : UIVideoEditorControllerDelegate {
    
    func videoEditorControllerDidCancel(_ editor: UIVideoEditorController) {
        editor.dismiss(animated: true, completion: nil)
    }
    
    func videoEditorController(_ editor: UIVideoEditorController, didFailWithError error: Error) {
        editor.dismiss(animated: true, completion: nil)
    }
    
    func videoEditorController(_ editor: UIVideoEditorController, didSaveEditedVideoToPath editedVideoPath: String) {
        
        editor.dismiss(animated: true, completion: nil)
        
        if let data = try? Data (contentsOf: URL (fileURLWithPath: editedVideoPath)) {
            AmazonTransferManager.defaultTransferManager().uploadData(data, bucketName: "oott", keyName: UtilityClass.getUniqueString(usingString: chatRoomID!) + ".mp4", contentType: "image/mp4", progressBlock: { (task, progress) in
                
                print("fraction completed video: %f",progress.fractionCompleted)
                
            }, completionHandler: { (task, error) in
                
            })
        }
    }
    
}

//MARK: -
//MARK: - extension -> Image and Video Uploading Downloading
extension GroupChatViewController
{
    //MARK: - Upload Video
    func uploadVideo(usingURL: URL) {
        
        fullLoaderView.updateLoader(withValue: 0)
        fullLoaderView.isHidden = false
        
        let keyName = UtilityClass.getUniqueString(usingString: folderPath!) + ".mp4"
        
        if let data = try? Data (contentsOf: usingURL) {
            AmazonTransferManager.defaultTransferManager().uploadData(data, bucketName: "oott", keyName: keyName, contentType: "image/mp4", progressBlock:
                {[weak self] (task, progress) in
                
                    guard let `self` = self else {return}
                    
                    print("fraction completed video: %f",progress.fractionCompleted)
                    
                    self.fullLoaderView.updateLoader(withValue: CGFloat(progress.fractionCompleted))
                
            }, completionHandler: {[weak self] (task, error) in
                
                guard let `self` = self else {return}
                
                self.fullLoaderView.isHidden = true
                
                if let err = error
                {
                    print(err)
                    return
                }
                print("Success.......")
                self.sendImageTagToServer(imageName: keyName, imageData: data)
            })
        }
        
    }
    
    //MARK: - Upload Image
    func uploadImage(usingImage: UIImage) {
        
        fullLoaderView.updateLoader(withValue: 0)
        fullLoaderView.isHidden = false
        
        let keyName = UtilityClass.getUniqueString(usingString: folderPath!) + ".jpg"
        
        let imageToUpload = UIImageJPEGRepresentation(usingImage, 0.6)!
        
        AmazonTransferManager.defaultTransferManager().uploadData(imageToUpload, bucketName: "oott", keyName: keyName, contentType: "image/jpg", progressBlock:
            {[weak self] (task, progress) in
                
                guard let `self` = self else {return}
                
                print("fraction completed video: %f",progress.fractionCompleted)
                
                self.fullLoaderView.updateLoader(withValue: CGFloat(progress.fractionCompleted))
            }, completionHandler: {[weak self] (task, error) in
                
                guard let `self` = self else {return}
                
                self.fullLoaderView.isHidden = true
                
                if let err = error
                {
                    print(err)
                    return
                }
                print("Success.......")
                self.sendImageTagToServer(imageName: keyName, imageData: imageToUpload)
        })
    }
    
    //MARK: - Image & Video
    func downloadImage(toPath downloadPath: String, imageKeyName: String, forCell: ChatViewCell) {
        
        guard let appDel = appDelegate as? AppDelegate else {
            return
        }
        if !appDel.isInternetAvailable() {
            return
        }
        
        arrayChatMessages[forCell.tag].mediaDownloadStatus = .isDownloading
        
        AmazonTransferManager.defaultTransferManager().downloadData(URL (string: downloadPath)!, bucketName: "oott", keyName: imageKeyName, progressBlock:
            {[weak forCell] (task, progress) in
                
//                guard let `forCell` = forCell else {return}
                
                print("fraction completed video: %f",progress.fractionCompleted)
                
                forCell?.loaderView.progress = CGFloat(progress.fractionCompleted)
            })
        {[weak self] (task, error) in
            
            guard let `self` = self else {return}
            
            if let err = error
            {
                print("Error.......")
                print(err)
                self.arrayChatMessages[forCell.tag].mediaDownloadStatus = .error
            }
            else
            {
                print("Success.......")
                
                self.arrayChatMessages[forCell.tag].mediaDownloadStatus = .downloaded
            }
            
            self.tableChatList.reloadData()
        }
    }
}

extension GroupChatViewController {
    //MARK:- Get Chat History
    func getChatLikesHistory()
    {
        if !SocketManager.sharedInstance().isSocketConnected()
        {
            tempSelectedIndex = -1
            return
        }
        
        let message_id = arrayChatMessages[tempSelectedIndex].messageID
        
        HUD.show(.systemActivity, onView: self.view.window)
        
        SocketManager.sharedInstance().getGroupChatLikesHistory (SocketModelGetLikesHistory(message_id, roomID: chatRoomID)) { [weak self] (historyArray, statusCode) in
            
            HUD.hide()
            
            guard let `self` = self else {return}
            
            if statusCode == 200
            {
                self.updateLikeHistoryModel(usingArray: historyArray)
            }
            else {
                self.tempSelectedIndex = -1
            }
        }
    }
    
    func updateLikeHistoryModel(usingArray array : [[String:Any]])
    {
        var newArray = [ModelGroupChatLikesMessage] ()
        
        let dataArray = array
        
        for dict in dataArray // Iterating dictionaries
        {
            let model = ModelGroupChatLikesMessage () // Model creation
            model.updateModel(usingDictionary: dict) // Updating model
            newArray.append(model) // Adding model to array
        }
        
        if let cell = tableChatList.cellForRow(at: IndexPath (row: tempSelectedIndex, section: 0)) as? ChatViewCell, newArray.count > 0 {
            selectedIndex = tempSelectedIndex
            cell.constraintCollectionViewHeight.constant = 80
            cell.likesArray = newArray
        }
        else {
            tempSelectedIndex = -1
        }
        
        tableChatList.reloadData()
    }
}
