//
//  GroupChatLikesViewController.swift
//  OOTTBusinessApp
//
//  Created by Ratina on 6/25/18.
//  Copyright © 2018 KonstantInfoSolutions. All rights reserved.
//

import UIKit
import PKHUD
import DZNEmptyDataSet

class ModelGroupChatLikesMessage
{
    // user
    var _id = ""
    var isFollowed = 0
    var isRequested = 0
    var name = ""
    var picture = ""
    var userType = ""
    var user_id = ""
    var username = ""

    deinit {
        print("ModelGroupChatLikesMessage Deinit")
    }
    
    func updateModel(usingDictionary dictionary : [String:Any])
    {
        if let msgID = dictionary["_id"] as? String
        {
            self._id = msgID
        }
        
        if let isFollowed = dictionary["isFollowed"] as? Int
        {
            self.isFollowed = isFollowed
        }
        
        if let isRequested = dictionary["isRequested"] as? Int
        {
            self.isRequested = isRequested
        }
        
        if let name = dictionary["name"] as? String
        {
            self.name = name
        }
        
        if let picture = dictionary["picture"] as? String
        {
            self.picture = picture
        }
        
        if let userType = dictionary["userType"] as? String
        {
            self.userType = userType
        }
        
        if let user_id = dictionary["user_id"] as? String
        {
            self.user_id = user_id
        }
        
        if let username = dictionary["username"] as? String
        {
            self.username = username
        }
    }
}

class GroupChatLikesViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, DZNEmptyDataSetSource, DZNEmptyDataSetDelegate, SuggestedFriendsCellDelegate {

    @IBOutlet weak var groupChatLikesTableView: UITableView!
    var arrayChatLikesListing = [ModelGroupChatLikesMessage]()
    
    var message_id = ""
    var chatRoomID = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        setupTableview()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        showNavigationBar()
        self.navigationItem.title = "Chat Likes".uppercased()
        
        let leftBarButton = self.setLeftBarButtonItem(withImage: #imageLiteral(resourceName: "backButton"))
        leftBarButton.action = #selector(onBackButtonAction)
        leftBarButton.target = self
        
        getChatLikesHistory()
    }
    
    func onBackButtonAction() {
        self.navigationController?.popViewController(animated: true)
    }
    
    func setupTableview()
    {
        groupChatLikesTableView.delegate = self
        groupChatLikesTableView.dataSource = self
        
        groupChatLikesTableView.emptyDataSetSource = self
        groupChatLikesTableView.emptyDataSetDelegate = self
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    //MARK:- Get Chat History
    func getChatLikesHistory()
    {
        if !SocketManager.sharedInstance().isSocketConnected()
        {
            return
        }
        
        HUD.show(.systemActivity, onView: self.view.window)
        
        SocketManager.sharedInstance().getGroupChatLikesHistory (SocketModelGetLikesHistory(message_id, roomID: chatRoomID)) { [weak self] (historyArray, statusCode) in
            
            HUD.hide()
            
            guard let `self` = self else {return}
            
            if statusCode == 200
            {
                self.updateModel(usingArray: historyArray)
            }
        }
    }
    
    //MARK:- Update chat model array
    func updateModel(usingArray array : [[String:Any]])
    {
        arrayChatLikesListing.removeAll()
        
        let dataArray = array
        
        for dict in dataArray // Iterating dictionaries
        {
            let model = ModelGroupChatLikesMessage () // Model creation
            model.updateModel(usingDictionary: dict) // Updating model
            arrayChatLikesListing.append(model) // Adding model to array
        }
        
        groupChatLikesTableView.reloadData()
    }

    // MARK:- UITableView Delegate and Datasource Methods
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrayChatLikesListing.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 78 * scaleFactorX
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell: SuggestedFriendsCell = tableView.dequeueReusableCell(withIdentifier: kCellIdentifier_suggested_friends_cell) as! SuggestedFriendsCell
        
        cell.cellDelegate = self
        
        cell.tag = indexPath.row
        
        cell.suggestedFriendImageView.setIndicatorStyle(.white)
        cell.suggestedFriendImageView.setShowActivityIndicator(true)
    
        cell.suggestedFriendNameLabel.text = arrayChatLikesListing[indexPath.row].name
        
        cell.suggestedFriendImageView.sd_setImage(with: URL (string: arrayChatLikesListing[indexPath.row].picture), completed: nil)
        
        cell.mutualFriendsCountLabel.isHidden = true
        cell.followUnfollowButton.isHidden = true
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        tableView.deselectRow(at: indexPath, animated: true)
        
         if UtilityClass.getUserIDData() != arrayChatLikesListing[indexPath.row]._id
         {
            if arrayChatLikesListing[indexPath.row].userType == AppUserTypeEnum.provider.rawValue
            {
                
                let detailsViewController = UIStoryboard.getBusinessDetailsStoryboard().instantiateViewController(withIdentifier: "businessDetailsViewController") as! BusinessDetailsViewController
                detailsViewController.businessId = arrayChatLikesListing[indexPath.row]._id
                detailsViewController.isFromDetail = true
                detailsViewController.hidesBottomBarWhenPushed = true
                self.navigationController?.pushViewController(detailsViewController, animated: true)
                

            }
            else
            
            {
            navigateToProfile(forUser: arrayChatLikesListing[indexPath.row])
            }
            

        }
    }
    
    //MARK:- DZNEmptyDataSetSource -> Title
    func title(forEmptyDataSet scrollView: UIScrollView!) -> NSAttributedString! {
        
        let text = "No suggestion found to follow"
        
        let attributes = [NSFontAttributeName:UIFont .boldSystemFont(ofSize: 16*scaleFactorX), NSForegroundColorAttributeName:UIColor.white]
        
        return NSAttributedString (string: text, attributes: attributes)
    }
    
    func emptyDataSetShouldAllowScroll(_ scrollView: UIScrollView!) -> Bool {
        return true
    }
    
    // MARK:- Navigation Methods
    
    func navigateToProfile(forUser userModel : ModelGroupChatLikesMessage) {
        
        let profileViewController = UIStoryboard.getUserProfileStoryboard().instantiateViewController(withIdentifier: "userProfileViewController") as! UserProfileViewController
        
        profileViewController.targetUserID = userModel.user_id
        profileViewController.isLoggedInUser = false
        profileViewController.hidesBottomBarWhenPushed = true
        
        self.navigationController?.pushViewController(profileViewController, animated: true)
    }
    
    // MARK:- SuggestedFriendsCellDelegate -> Follow Button
    
    func suggestedFriendsCell(cell: SuggestedFriendsCell, didTapOnFollowButton followButton: UIButton) {
        
        //followButton.isUserInteractionEnabled = false
        //
        //let index = cell.tag
        //
        //let userID = arrayChatLikesListing[index].friendID!
        //
        //performFollowUserApi(forUserID: userID, actionsCell: cell)
    }
}
