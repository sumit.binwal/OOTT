//
//  ConnectGroupsViewController.swift
//  OOTTBusinessApp
//
//  Created by Bharat Kumar Pathak on 10/07/17.
//  Copyright © 2017 KonstantInfoSolutions. All rights reserved.
//

import UIKit
import PKHUD
import DZNEmptyDataSet

class ModelGroupChatList
{
    /*
     
     {
     "_id": "59df21dbc7cedb14a5dc07f9",
     "name": "new by 59d4ad9e4c01ae78f7d7e9a2",
     "picture": "http://192.168.0.131/oott/dev/public/uploads/users/CNhcqn3kv6LP5tgIFiGp2EYdKEywDiKA.jpg",
     "friends": "1",
     "people": "1",
     "unseenCount": 0
     }
     
     */
    
    var roomID = ""
    var businessID = ""
    var groupName = ""
    var groupPicture = ""
    var friendsCount = 0
    var peopleCount = 0
//    var lastMessage = ""
//    var lastMessageTime = ""
    var unreadCount = 0
    var isBusinessChat = false
    var lastCheckIn : LastCheckinData?
    var groupCreateTime = ""
    
    var isMakeMoveStatus = false
    
    
    
    func updateModel(usingDictionary dictionary : [String:Any])
    {
        if let id = dictionary["_id"] as? String
        {
            self.roomID = id
        }
        
        if let leader = dictionary["leader"] as? String
        {
            self.businessID = leader
        }
        
        if let isBusiness = dictionary["isBusiness"] as? Bool
        {
            self.isBusinessChat = isBusiness
        }
        
        if let unseenCount = dictionary["unseenCount"] as? Int
        {
            self.unreadCount = unseenCount
        }
        
        if let friends = dictionary["friends"] as? Int
        {
            self.friendsCount = friends
        }
        
        if let people = dictionary["people"] as? Int
        {
            self.peopleCount = people
        }
        
        if let picture = dictionary["picture"] as? String
        {
            self.groupPicture = picture
        }
        
        if let name = dictionary["name"] as? String
        {
            self.groupName = name
        }
        
        if let created = dictionary["updated"] as? String
        {
            self.groupCreateTime = created
        }
        
        if let isMakeMoveStatus = dictionary["isMakeMoveStatus"] as? Bool
        {
            self.isMakeMoveStatus = isMakeMoveStatus
        }
        
        if let checkin = dictionary["lastCheckin"] as? [String:Any]
        {
            if self.lastCheckIn == nil
            {
                self.lastCheckIn = LastCheckinData ()
            }
            self.lastCheckIn?.updateModel(usingDictionary: checkin)
        }
        
//        if let lastMsgDict = dictionary["lastMessage"] as? [String:Any]
//        {
//            if let msg = lastMsgDict["message"] as? String
//            {
//                self.lastMessage = msg
//            }
//
//            if let created = lastMsgDict["created"] as? String
//            {
//                self.lastMessageTime = created
//            }
//        }
    }
}

class ConnectGroupsViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var tableGroupListing: UITableView!
    
    @IBOutlet weak var buttonPlusIcon: UIButton!
    
    var arrayGroupChatList = [ModelGroupChatList]()
    
    var isViewLoadedd = false
    
    var didDisappear = false
    
    // MARK:-
    override func viewDidLoad() {
        super.viewDidLoad()
        setupTableView()
//        getGroupChatList()

        NotificationCenter.default.addObserver(self, selector: #selector(onGroupLeaveNotification(_:)), name: NSNotification.Name(rawValue: "groupLeave"), object: nil)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    deinit {
        print("ConnectGroupsViewController deinit")
        NotificationCenter.default.removeObserver(self)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if !isViewLoadedd {
            getGroupChatList()
            isViewLoadedd = true
        }
        else {
            getGroupChatList()
//            if didDisappear {
//                didDisappear = false
//                getGroupChatList()
//            }
        }
    }
    
    func viewAboutToBeSelected()
    {
        getGroupChatList()
    }
    
    func setupTableView()
    {
        tableGroupListing.delegate = self
        tableGroupListing.dataSource = self
        tableGroupListing.emptyDataSetSource = self
        tableGroupListing.emptyDataSetDelegate = self
    }
    
    func onGroupLeaveNotification(_ notification: Notification) {
        if let info = notification.object as? [String:String] {
            if let groupID = info["groupID"] {
                if let index = arrayGroupChatList.index(where: { model -> Bool in
                    return model.roomID == groupID
                }) {
                    arrayGroupChatList.remove(at: index)
                    tableGroupListing.reloadData()
                }
            }
        }
    }
    
    // MARK:- UITableView Delegate and Datasource Methods
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrayGroupChatList.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 109 * scaleFactorX
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell: ConnectGroupCell = tableView.dequeueReusableCell(withIdentifier: kCellIdentifier_connect_group_cell) as! ConnectGroupCell
        
        cell.updateCell(usingModel: arrayGroupChatList[indexPath.row])
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        didDisappear = true
        
        let chatViewController = UIStoryboard.getChatViewStoryboard().instantiateInitialViewController() as! GroupChatViewController
        
        chatViewController.hidesBottomBarWhenPushed = true
        
        chatViewController.isGroupChat = true
        
        chatViewController.isBusinessChat = arrayGroupChatList[indexPath.row].isBusinessChat
        
        chatViewController.chatUsername.text = arrayGroupChatList[indexPath.row].groupName
        
        chatViewController.chatPicture = arrayGroupChatList[indexPath.row].groupPicture
        
        chatViewController.chatRoomID = arrayGroupChatList[indexPath.row].roomID
        
        chatViewController.businessID = arrayGroupChatList[indexPath.row].businessID
        
        self.navigationController?.pushViewController(chatViewController, animated: true)
    }
    
    // MARK:-
    @IBAction func onAddButtonAction(_ sender: UIButton) {
        
        let addGroupVC = self.storyboard?.instantiateViewController(withIdentifier: "addGroupNav") as! UINavigationController
        
        self.navigationController?.present(addGroupVC, animated: true, completion: nil)
        
    }
    
    //MARK:- Get Group Chat Listing Api
    func getGroupChatList()
    {
        SocketManager.sharedInstance().getGroupChatList
            {[weak self] (chatListArray, statusCode) in
                
                HUD.hide()
                
                guard let `self` = self else {return}
                
                if statusCode == 200
                {
                    self.updateModel(usingArray: chatListArray)
                }
        }
    }
    
    //MARK:- Update Model
    func updateModel(usingArray array : [[String:Any]])
    {
        arrayGroupChatList.removeAll()

        let dataArray = array

        for dict in dataArray // Iterating dictionaries
        {
            let model = ModelGroupChatList () // Model creation
            model.updateModel(usingDictionary: dict) // Updating model
            arrayGroupChatList.append(model) // Adding model to array
        }
        tableGroupListing.reloadData()
    }
}

extension ConnectGroupsViewController : DZNEmptyDataSetSource, DZNEmptyDataSetDelegate
{
    //MARK:- DZNEmptyDataSetSource -> Title
    func title(forEmptyDataSet scrollView: UIScrollView!) -> NSAttributedString! {
        
        let text = "Tap + icon to create first group"
        
        let attributes = [NSFontAttributeName:UIFont .boldSystemFont(ofSize: 16*scaleFactorX), NSForegroundColorAttributeName:UIColor.white]
        
        return NSAttributedString (string: text, attributes: attributes)
    }
    
    func emptyDataSetShouldAllowScroll(_ scrollView: UIScrollView!) -> Bool {
        return true
    }
}
