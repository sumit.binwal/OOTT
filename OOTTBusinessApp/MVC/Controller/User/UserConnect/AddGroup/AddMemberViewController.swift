//
//  AddMemberViewController.swift
//  OOTTBusinessApp
//
//  Created by Santosh on 10/10/17.
//  Copyright © 2017 KonstantInfoSolutions. All rights reserved.
//

import UIKit
import PKHUD
import DZNEmptyDataSet

protocol AddMemberViewControllerDelegate : class {
    func onAddMemberViewController(_ viewController : AddMemberViewController, didSelectMember member:GroupMember, isLeader : Bool)
}

class AddMemberViewController: UIViewController {

    
    @IBOutlet weak var tableAddMember: UITableView!
    
    @IBOutlet var headerTableView: SearchBarView!
    @IBOutlet weak var searchTextField: UITextField!
    
    weak var delegate : AddMemberViewControllerDelegate?
    
    var isSearchModeOn = false
    
    var searchedString = ""
    
    var arrayInvitedFriends = [GroupMember]()
    
    var arrayInvitedFriendsTemp = [GroupMember]()
    
    var isLeaderSelection = true
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        var currentUser = GroupMember ()
        currentUser.picture = UtilityClass.getUserPictureData()!
        currentUser.userID = UtilityClass.getUserIDData()!
        currentUser.username = UtilityClass.getUserNameData()!
        arrayInvitedFriends.insert(currentUser, at: 0)
        
        
        setupNavigationView()
        setupTableview()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    //MARK:-
    func setupNavigationView() {
        
        self.navigationController?.setNavigationBarHidden(false, animated: false)
        searchTextField.setPlaceholderColor(UIColor.init(RED: 184.0, GREEN: 187.0, BLUE: 190.0, ALPHA: 1.0))
        searchTextField.customize(withLeftIcon: #imageLiteral(resourceName: "searchIcon"))
        searchTextField.delegate = self
        searchTextField.placeholder = isLeaderSelection ? "Select Leader" : "Select Co-Leader"
        self.navigationItem.titleView = headerTableView
        
        let rightBarButton = self.setRightBarButtonItem("cancel".uppercased())
        rightBarButton.action = #selector(onCancelAction)
        rightBarButton.target = self
    }
    
    //MARK:-
    func setupTableview()
    {
        tableAddMember.delegate = self
        tableAddMember.dataSource = self
        
        tableAddMember.emptyDataSetDelegate = self
        tableAddMember.emptyDataSetSource = self
    }
    
    //MARK:-
    func onCancelAction()
    {
        self.view.endEditing(true)
        self.navigationController?.dismiss(animated: true, completion: nil)
    }
    
    //MARK:- UITableView Delegate and DataSource
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if isSearchModeOn
        {
            return arrayInvitedFriendsTemp.count
        }
        return arrayInvitedFriends.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 77 * scaleFactorX
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell : SearchUserCell = tableView.dequeueReusableCell(withIdentifier: kCellIdentifier_SearchUserCell) as! SearchUserCell
        if isSearchModeOn
        {
            cell.updateCellForMemberList(usingModel: arrayInvitedFriendsTemp[indexPath.row])
        }
        else
        {
            cell.updateCellForMemberList(usingModel: arrayInvitedFriends[indexPath.row])
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        tableView.deselectRow(at: indexPath, animated: true)
        
        guard let delegate = self.delegate else {
            return
        }
        let member = isSearchModeOn ? arrayInvitedFriendsTemp[indexPath.row] : arrayInvitedFriends[indexPath.row]
        delegate.onAddMemberViewController(self, didSelectMember: member, isLeader: isLeaderSelection)
    }
    
    //MARK:- DZNEmptyDataSetSource -> Title
    func title(forEmptyDataSet scrollView: UIScrollView!) -> NSAttributedString! {
        
        let text = "Please invite friends"
        
        let attributes = [NSFontAttributeName:UIFont .boldSystemFont(ofSize: 16*scaleFactorX), NSForegroundColorAttributeName:UIColor.white]
        
        return NSAttributedString (string: text, attributes: attributes)
    }
    
    func emptyDataSetShouldAllowScroll(_ scrollView: UIScrollView!) -> Bool {
        return true
    }

}

extension AddMemberViewController : UITextFieldDelegate, UITableViewDelegate, UITableViewDataSource, DZNEmptyDataSetDelegate, DZNEmptyDataSetSource
{
    //MARK:- UITextFieldDelegate
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func textFieldShouldClear(_ textField: UITextField) -> Bool {
        isSearchModeOn = false
        arrayInvitedFriendsTemp.removeAll()
        tableAddMember.reloadData()
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        var newString : String?
        var finalCount = 0
        
        if string.characters.count == 0
        {
            if textField.text?.characters.count==0
            {
                finalCount = 0
                newString = textField.text
            }
            else
            {
                finalCount = textField.text!.characters.count - 1
                var newStr = textField.text! as NSString
                newStr = newStr.replacingCharacters(in: range, with: string) as NSString
                newString = newStr as String
            }
        }
        else
        {
            finalCount = textField.text!.characters.count + 1
            var newStr = textField.text! as NSString
            
            newStr = newStr.replacingCharacters(in: range, with: string) as NSString
            
            newString = newStr as String
        }
        
        searchedString = newString!
        if finalCount == 0
        {
            isSearchModeOn = false
        }
        else
        {
            isSearchModeOn = true
            
            arrayInvitedFriendsTemp = arrayInvitedFriends.filter { (model) -> Bool in
                
                let isAvailable = model.username.localizedCaseInsensitiveContains(newString!)
                
                if isAvailable
                {
                    return true
                }
                return false
            }
        }
        
        self.tableAddMember.reloadData()
        
        return true
    }
}
