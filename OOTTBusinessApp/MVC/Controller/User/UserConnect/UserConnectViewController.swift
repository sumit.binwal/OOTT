//
//  UserConnectViewController.swift
//  OOTTBusinessApp
//
//  Created by Bharat Kumar Pathak on 07/07/17.
//  Copyright © 2017 KonstantInfoSolutions. All rights reserved.
//

import UIKit
import MXSegmentedPager

class UserConnectViewController: MXSegmentedPagerController {

    var segmentedViewControllers = [UIViewController]()
    var segmentTitles = ["Groups", "Direct"]
    var selectedPage: Int = 0
    
    // MARK:-
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupView()
        initializeChildViewControllers()
        segmentedPager.initializeSegments()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        showNavigationBar()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    deinit {
        print("UserConnectViewController deinit")
    }
    
    //MARK:- Initial Setup View
    
    func setupView() -> Void
    {
        self.navigationItem.title = "Connect".uppercased()
    }
    
    // MARK:- Custom Methods
    
    func initializeChildViewControllers() {
        
        segmentedViewControllers.append(self.storyboard!.instantiateViewController(withIdentifier: "connectGroupsViewController") as! ConnectGroupsViewController)
        segmentedViewControllers.append(self.storyboard!.instantiateViewController(withIdentifier: "connectDirectViewController") as! ConnectDirectViewController)
    }
    
    // MARK:- MXSegmentedPager Delegate and Datasource Methods
    
    override func numberOfPages(in segmentedPager: MXSegmentedPager) -> Int {
        return segmentedViewControllers.count
    }
    
    override func segmentedPager(_ segmentedPager: MXSegmentedPager, titleForSectionAt index: Int) -> String {
        return segmentTitles[index]
    }
    
    override func segmentedPager(_ segmentedPager: MXSegmentedPager, viewControllerForPageAt index: Int) -> UIViewController {
        let viewController = segmentedViewControllers[index]
        self.addChildViewController(viewController)
        return viewController
    }
    
    override func segmentedPager(_ segmentedPager: MXSegmentedPager, didSelectViewWith index: Int) {
      
        if index == selectedPage {
            return
        }
        switch index {
        case 0:
            if let controller = segmentedViewControllers[0] as? ConnectGroupsViewController
            {
                selectedPage = 0
                controller.viewAboutToBeSelected()
            }
        case 1:
            if let controller = segmentedViewControllers[1] as? ConnectDirectViewController
            {
                selectedPage = 1
                controller.viewAboutToBeSelected()
            }
        default:
            break
        }
    }
}
