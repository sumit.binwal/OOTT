//
//  UserActivityViewController.swift
//  OOTTBusinessApp
//
//  Created by Bharat Kumar Pathak on 07/07/17.
//  Copyright © 2017 KonstantInfoSolutions. All rights reserved.
//

import UIKit
import MXSegmentedPager

class UserActivityViewController: MXSegmentedPagerController {

    var segmentedViewControllers = [UIViewController]()
    var segmentTitles = ["Following", "You"]
    
    var selectedPage: Int = 0
    
    // MARK:-
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupView()
        initializeChildViewControllers()
        segmentedPager.initializeSegments()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        showNavigationBar()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    deinit {
        print("UserActivityViewController deinit")
    }
    
    //MARK:- Initial Setup View
    
    func setupView() -> Void
    {
        self.navigationItem.title = "Activity".uppercased()
    }
    
    //MARK:- Auto Select You Tab
    func autoSelectYouTab()
    {
        segmentedPager.segmentedControl.setSelectedSegmentIndex(1, animated: true)
        segmentedPager.pager.showPage(at: 1, animated: true)
    }
    
    // MARK:- Custom Methods
    
    func initializeChildViewControllers() {
        
        segmentedViewControllers.append(self.storyboard!.instantiateViewController(withIdentifier: "followingViewController") as! FollowingViewController)
        segmentedViewControllers.append(self.storyboard!.instantiateViewController(withIdentifier: "followersViewController") as! FollowersViewController)
    }
    
    // MARK:- MXSegmentedPager Delegate and Datasource Methods
    
    override func numberOfPages(in segmentedPager: MXSegmentedPager) -> Int {
        return segmentedViewControllers.count
    }
    
    override func segmentedPager(_ segmentedPager: MXSegmentedPager, titleForSectionAt index: Int) -> String {
        return segmentTitles[index]
    }
    
    override func segmentedPager(_ segmentedPager: MXSegmentedPager, viewControllerForPageAt index: Int) -> UIViewController {
        let viewController = segmentedViewControllers[index]
        self.addChildViewController(viewController)
        return viewController
    }
    
    override func segmentedPager(_ segmentedPager: MXSegmentedPager, didSelectViewWith index: Int) {
      
        if index == selectedPage {
            return
        }
        switch index {
        case 0:
            if let controller = segmentedViewControllers[0] as? FollowingViewController
            {
                selectedPage = 0
                controller.viewAboutToBeSelected()
            }
        case 1:
            if let controller = segmentedViewControllers[1] as? FollowersViewController
            {
                selectedPage = 1
                controller.viewAboutToBeSelected()
            }
        default:
            break
        }
    }
}
