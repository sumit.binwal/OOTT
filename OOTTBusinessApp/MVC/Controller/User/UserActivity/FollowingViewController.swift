//
//  FollowingViewController.swift
//  OOTTBusinessApp
//
//  Created by Bharat Kumar Pathak on 10/07/17.
//  Copyright © 2017 KonstantInfoSolutions. All rights reserved.
//

import UIKit
import PKHUD
import DZNEmptyDataSet

class FollowingViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, DZNEmptyDataSetSource, DZNEmptyDataSetDelegate {
    @IBOutlet weak var tableFollowingActivityList: UITableView!
    
    var arrayActivityList = [ModelFollowingActivity]()
    
    var refreshControl: UIRefreshControl!
    
    var currentPage = 0
    var limit = 0
    
    
    var isPageRefreshing = true
    var isEndOfResult = false

    var isViewLoadedd = false
    
    var didDisappear = false
    
    // MARK:-
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupView()
        setupTableview()
        
//        getFollowingActivityListApi(onRefresh: false, shouldShowHUD: true)
        applyRefreshControl()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    deinit {
        print("FollowingViewController deinit")
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if !isViewLoadedd {
            getFollowingActivityListApi(onRefresh: true, shouldShowHUD: true)
            isViewLoadedd = true
        }
        else {
            if didDisappear {
                didDisappear = false
                getFollowingActivityListApi(onRefresh: true, shouldShowHUD: false)
            }
        }
    }
    
    func viewAboutToBeSelected()
    {
        getFollowingActivityListApi(onRefresh: true, shouldShowHUD: false)
    }
    
    func setupView()
    {
        
    }
    
    func setupTableview()
    {
        tableFollowingActivityList.delegate = self
        tableFollowingActivityList.dataSource = self
        tableFollowingActivityList.emptyDataSetSource = self
        tableFollowingActivityList.emptyDataSetDelegate = self
    }
    
    // MARK:- Refresh Control
    func applyRefreshControl() {
        
        refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(self.refreshPage), for: UIControlEvents.valueChanged)
        self.tableFollowingActivityList.addSubview(refreshControl)
    }
    
    func refreshPage(sender:AnyObject) {
        currentPage = 0
        isPageRefreshing = true
        getFollowingActivityListApi(onRefresh: true, shouldShowHUD: true)
    }
    
    // MARK:- UITableView Delegate and Datasource Methods
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrayActivityList.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell: FollowingCell = tableView.dequeueReusableCell(withIdentifier: kCellIdentifier_following_cell) as! FollowingCell
        
        cell.tag = indexPath.row
        cell.activityDelegate = self
        cell.updateCell(usingModel: arrayActivityList[indexPath.row])
        
        return cell
    }
    
    //MARK:- DZNEmptyDataSetSource -> Title
    func title(forEmptyDataSet scrollView: UIScrollView!) -> NSAttributedString! {
        
        let text = "No Activity"
        
        let attributes = [NSFontAttributeName:UIFont .boldSystemFont(ofSize: 16*scaleFactorX), NSForegroundColorAttributeName:UIColor.white]
        
        return NSAttributedString (string: text, attributes: attributes)
    }
    
    func emptyDataSetShouldAllowScroll(_ scrollView: UIScrollView!) -> Bool {
        return true
    }
    
    
    //MARK:- Get Following Activity Listing Api
    func getFollowingActivityListApi(onRefresh: Bool,shouldShowHUD showHUD:Bool) -> Void
    {
        if showHUD
        {
            HUD.show(.systemActivity, onView: self.view.window)
        }
        
        let urlToHit = EndPoints.followingsActivityList(UtilityClass.getUserSidData()!).path
        
        AppWebHandler.sharedInstance().fetchData(fromURL: urlToHit, httpMethod: .get, parameters: nil)
        {[weak self] (data, dictionary, statusCode, error) in
            
            HUD.hide()
            
            guard let `self` = self else {return}
            
            self.refreshControl.endRefreshing()
            
            self.isPageRefreshing = false
            self.isEndOfResult = true
            
            if error != nil
            {
                self.currentPage = self.currentPage > 0 ? self.currentPage-1 : 0
                UtilityClass.showAlertWithTitle(title: App_Name, message: error?.localizedDescription, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                return
            }
            
            let responseDictionary = dictionary!
            let message = responseDictionary["message"] as! String
            let type = responseDictionary["type"] as! Bool
            
            if statusCode == 203
            {
                self.currentPage = self.currentPage > 0 ? self.currentPage-1 : 0
                // show alert
//                UtilityClass.showAlertWithTitle(title: App_Name, message: message, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                return
            }
            
            if statusCode == 200
            {
                if type
                {
                    // success
                    let dataArray = responseDictionary["data"] as! [[String:Any]] // As array
                    let dataLimit = responseDictionary["limit"] as! Int
                    if onRefresh {
                        self.currentPage = 0
                        self.arrayActivityList.removeAll()
                    }
                    if dataArray.count == dataLimit
                    {
                        self.isEndOfResult = false
                    }
                    self.updateModelArray(usingArray: dataArray)
                    return
                }
                else // No listing
                {
                    // alert
                    self.currentPage = self.currentPage > 0 ? self.currentPage-1 : 0
                    UtilityClass.showAlertWithTitle(title: App_Name, message: message, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                    return
                }
            }
        }
    }
    
    //MARK:- Updating Model Array
    func updateModelArray(usingArray array : [[String:Any]]) -> Void
    {
        arrayActivityList.removeAll()
        
        let dataArray = array
        
        for dict in dataArray // Iterating dictionaries
        {
            let model = ModelFollowingActivity () // Model creation
            model.updateModel(usingDictionary: dict) // Updating model
            arrayActivityList.append(model) // Adding model to array
        }
        tableFollowingActivityList.reloadData()
    }
}



extension FollowingViewController : FollowingActivityCellDelegate
{
    
    func followingCellDidTapOnImageView(_ cell: FollowingCell) {
        navigateToUserDetailPage(withIndex: cell.tag)
    }
    
    func navigateToUserDetailPage(withIndex index: Int)
    {
        didDisappear = true
        let profileViewController = UIStoryboard.getUserProfileStoryboard().instantiateViewController(withIdentifier: "userProfileViewController") as! UserProfileViewController
        profileViewController.targetUserID = arrayActivityList[index].activityBy?.userID
        profileViewController.isLoggedInUser = false
        profileViewController.hidesBottomBarWhenPushed = true
        self.navigationController?.pushViewController(profileViewController, animated: true)
    }
    
//    func scrollViewDidScroll(_ scrollView: UIScrollView) {
//        
//        if isEndOfResult
//        {
//            return
//        }
//        if tableFollowingActivityList.contentOffset.y + tableFollowingActivityList.frame.size.height >= tableFollowingActivityList.contentSize.height
//        {
//            if !isPageRefreshing
//            {
//                isPageRefreshing = true
//                
//                currentPage+=1
//                
//                getFollowingActivityListApi(onRefresh: false)
//            }
//        }
//        
//    }
}
