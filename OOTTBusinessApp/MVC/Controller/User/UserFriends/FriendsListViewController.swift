//
//  FriendsListViewController.swift
//  OOTTBusinessApp
//
//  Created by Bharat Kumar Pathak on 10/07/17.
//  Copyright © 2017 KonstantInfoSolutions. All rights reserved.
//

import UIKit
import PKHUD
import DZNEmptyDataSet

var arrayFollowingListing = [ModelFriendsListing]()

class FriendsListViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, FriendsCellDelegate, DZNEmptyDataSetSource, DZNEmptyDataSetDelegate {
    
    @IBOutlet weak var tableFriendsList: UITableView!
    var refreshControl: UIRefreshControl!
        
    var currentPage = 1
    var limit = 0
    
    
    var isPageRefreshing = true
    var isEndOfResult = false
    
    var isViewLoadedd = false
    
    var didDisappear = false
    

    // MARK:-
    override func viewDidLoad() {
        super.viewDidLoad()
        setupTableview()
        applyRefreshControl()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if !isViewLoadedd {
            getFriendsApi(onRefresh: true, shouldShowHUD: true)
            isViewLoadedd = true
        }
        else {
            if didDisappear {
                didDisappear = false
                getFriendsApi(onRefresh: true, shouldShowHUD: false)
            }
        }
        
        NotificationCenter.default.addObserver(self, selector: #selector (getFriendsSearchApi), name: NSNotification.Name(rawValue: "applySearchForFriends"), object: nil)
    }
    
    deinit {
        print("FriendsListViewController deinit")
        arrayFollowingListing.removeAll()
    }
    
    func viewAboutToBeSelected()
    {
        getFriendsApi(onRefresh: true, shouldShowHUD: false)
    }
    
    func setupTableview()
    {
        tableFriendsList.delegate = self
        tableFriendsList.dataSource = self
        tableFriendsList.emptyDataSetSource = self
        tableFriendsList.emptyDataSetDelegate = self
    }
    
    // MARK:- Refresh Control
    func applyRefreshControl() {
        
        refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(self.refreshPage), for: UIControlEvents.valueChanged)
        self.tableFriendsList.addSubview(refreshControl)
    }
    
    func refreshPage(sender:AnyObject) {
        currentPage = 0
        isPageRefreshing = true
        getFriendsApi(onRefresh: true, shouldShowHUD: true)
    }
    
    // MARK:- UITableView Delegate and Datasource Methods
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrayFollowingListing.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell: FriendsCell = tableView.dequeueReusableCell(withIdentifier: kCellIdentifier_friends_cell) as! FriendsCell
        cell.tag = indexPath.row
        cell.cellDelegate = self
        cell.updateData(usingModel: arrayFollowingListing[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if arrayFollowingListing[indexPath.row].isAccountDeleted {
            UtilityClass.showAlertWithTitle(title: "Profile Deactivated", message: "User has deactivated the profile.", onViewController: self, withButtonArray: nil, dismissHandler: nil)
            return
        }
        navigateToProfile(forUser: arrayFollowingListing[indexPath.row])
    }
    
    //MARK:- DZNEmptyDataSetSource -> Title
    func title(forEmptyDataSet scrollView: UIScrollView!) -> NSAttributedString! {
        
        let text = "You don't follow anyone, start following to see friends here"
        
        let attributes = [NSFontAttributeName:UIFont .boldSystemFont(ofSize: 16*scaleFactorX), NSForegroundColorAttributeName:UIColor.white]
        
        return NSAttributedString (string: text, attributes: attributes)
    }
    
    func emptyDataSetShouldAllowScroll(_ scrollView: UIScrollView!) -> Bool {
        return true
    }
    
    // MARK:- Navigation Methods
    
    func navigateToProfile(forUser userModel:ModelFriendsListing) {
        if userModel.isSameUser
        {
            return
        }
        didDisappear = true
        let profileViewController = UIStoryboard.getUserProfileStoryboard().instantiateViewController(withIdentifier: "userProfileViewController") as! UserProfileViewController
        profileViewController.targetUserID = userModel.friendID
        profileViewController.isLoggedInUser = false
        profileViewController.hidesBottomBarWhenPushed = true
        self.parent?.navigationController?.pushViewController(profileViewController, animated: true)
    }
    
    func navigateToCommentsListing(forUser userModel:ModelFriendsListing) {
        
        if userModel.checkInID != nil
        {
            didDisappear = true
            let commentListViewController = self.storyboard!.instantiateViewController(withIdentifier: "commentsListingViewController") as! CommentsListingViewController
            commentListViewController.checkInID = userModel.checkInID
            commentListViewController.hidesBottomBarWhenPushed = true
            self.parent?.navigationController?.pushViewController(commentListViewController, animated: true)
        }
       
    }
    
    // MARK:- FriendsCellDelegate -> Invite button
    
    func friendCell(cell: FriendsCell, didTapOnInviteButton inviteButton: UIButton) {
//        performLike(onCell: cell)
        let myGroupListNav : UINavigationController = UIStoryboard.getUserHomeStoryboard().instantiateViewController(withIdentifier: "myGroupListNav") as! UINavigationController
        
        let mygroupListVC = myGroupListNav.viewControllers.first as! MyGroupListViewController
        
        mygroupListVC.isMakeMoveInvitation = false
        
        mygroupListVC.userID = arrayFollowingListing[cell.tag].friendID
        
        self.navigationController?.present(myGroupListNav, animated: true, completion: nil)
    }
    
    // MARK:- FriendsCellDelegate -> Comment button
    func friendCell(cell: FriendsCell, didTapOnCommentButton commentButton: UIButton) {
        navigateToCommentsListing(forUser: arrayFollowingListing[cell.tag])
    }
    
    // MARK:- FriendsCellDelegate -> LikeUnLike button
    func friendCell(cell: FriendsCell, didTapOnLikeUnLikeButton likeUnLikeButton: UIButton) {
        likeUnLikeButton.isUserInteractionEnabled = false
        if arrayFollowingListing[cell.tag].isLiked
        {
            performUnLike(onCell: cell)
        }
        else
        {
            performLike(onCell: cell)
        }
    }
    
    //MARK:- Get following(friends) Listing Api
    func getFriendsApi(onRefresh: Bool, shouldShowHUD showHUD:Bool) -> Void
    {
        if showHUD
        {
            HUD.show(.systemActivity, onView: self.view.window)
        }
        
        let urlToHit = EndPoints.getFollowingList(UtilityClass.getUserSidData()!, UtilityClass.getUserIDData()!).path
        
        let params = [
            "keyword" : searchFriendsModel.searchString
        ]
        
        AppWebHandler.sharedInstance().fetchData(fromURL: urlToHit, httpMethod: .post, parameters: params)
        {[weak self] (data, dictionary, statusCode, error) in
            
            HUD.hide()
            
            guard let `self` = self else { return }
            
            self.refreshControl.endRefreshing()
            
            self.isPageRefreshing = false
            self.isEndOfResult = true
            
            if error != nil
            {
                UtilityClass.showAlertWithTitle(title: App_Name, message: error?.localizedDescription, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                return
            }
            
            
            let responseDictionary = dictionary!
            let message = responseDictionary["message"] as! String
            let type = responseDictionary["type"] as! Bool
            
            print(responseDictionary)
            
            if statusCode == 203
            {
                // show alert
//                UtilityClass.showAlertWithTitle(title: App_Name, message: message, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                return
            }
            
            if statusCode == 200
            {
                if type
                {
                    // success
                    let dataArray = responseDictionary["data"] as! [[String:Any]] // As array
                    let dataLimit = responseDictionary["limit"] as! Int
                    if onRefresh {
                        self.currentPage = 0
                        arrayFollowingListing.removeAll()
                    }
                    if dataArray.count == dataLimit
                    {
                        self.isEndOfResult = false
                    }
                    self.updateModelArray(usingArray: dataArray)
                    return
                }
                else
                {
                    // alert
                    UtilityClass.showAlertWithTitle(title: App_Name, message: message, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                    return
                }
            }
        }
    }
    
    //MARK:- Updating Model Array
    func updateModelArray(usingArray array : [[String:Any]]) -> Void
    {
        arrayFollowingListing.removeAll()
        
        let dataArray = array
        
        for dict in dataArray // Iterating dictionaries
        {
            let model = ModelFriendsListing () // Model creation
            model.updateModel(usingDictionary: dict) // Updating model
            arrayFollowingListing.append(model) // Adding model to array
        }
        tableFriendsList.reloadData()
    }
    
    //MARK:- Get following(friends) search Listing Api
    func getFriendsSearchApi() -> Void
    {
        getFriendsApi(onRefresh: true, shouldShowHUD: true)
        return
        
        let onRefresh = true
        
        let urlToHit = EndPoints.getFriendsSearchList(UtilityClass.getUserSidData()!, UtilityClass.getUserIDData()!).path
        
        let params = [
            "keyword" : searchFriendsModel.searchString
        ]
        
        AppWebHandler.sharedInstance().fetchData(fromURL: urlToHit, httpMethod: .post, parameters: params)
        {[weak self] (data, dictionary, statusCode, error) in
            
            guard let `self` = self else { return }
            
            self.refreshControl.endRefreshing()
            
            self.isPageRefreshing = false
            self.isEndOfResult = true
            
            if error != nil
            {
                UtilityClass.showAlertWithTitle(title: App_Name, message: error?.localizedDescription, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                return
            }
            
            
            let responseDictionary = dictionary!
            print("responseDictionary = ", responseDictionary)
            
            let message = responseDictionary["message"] as! String
            let type = responseDictionary["type"] as! Bool
            
            if statusCode == 203
            {
                // show alert
                //                UtilityClass.showAlertWithTitle(title: App_Name, message: message, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                return
            }
            
            if statusCode == 200
            {
                if type
                {
                    // success
                    let dataArray = responseDictionary["data"] as! [[String:Any]] // As array
                    let dataLimit = responseDictionary["limit"] as! Int
                    if onRefresh {
                        self.currentPage = 0
                        arrayFollowingListing.removeAll()
                    }
                    if dataArray.count == dataLimit
                    {
                        self.isEndOfResult = false
                    }
                    self.updateModelArray(usingArray: dataArray)
                    return
                }
                else
                {
                    // alert
                    UtilityClass.showAlertWithTitle(title: App_Name, message: message, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                    return
                }
            }
        }
    }
    
    func performLike(onCell cell:FriendsCell)
    {
        let editingCell = cell
        
        if let checkinID = arrayFollowingListing[editingCell.tag].checkInID
        {
            UtilityClass.showNetworkActivityLoader(show: true)
            
            let urlToHit = EndPoints.likeCheckin(UtilityClass.getUserSidData()!, checkinID).path
            
            AppWebHandler.sharedInstance().fetchData(fromURL: urlToHit, httpMethod: .get, parameters: nil, completionHandler:
                { [weak self] (data, dictionary, statusCode, error) in
                    
                    UtilityClass.showNetworkActivityLoader(show: false)
                    
                    guard let `self` = self else { return }
                    
                    editingCell.likeUnlikeButton.isUserInteractionEnabled = true
                    
                    if error != nil
                    {
                        UtilityClass.showAlertWithTitle(title: App_Name, message: error?.localizedDescription, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                        return
                    }
                    
                    
                    let responseDictionary = dictionary!
                    let message = responseDictionary["message"] as! String
                    let type = responseDictionary["type"] as! Bool
                    
                    if statusCode == 203
                    {
                        // show alert
                        UtilityClass.showAlertWithTitle(title: App_Name, message: message, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                        return
                    }
                    
                    if statusCode == 200
                    {
                        if type
                        {
                            // success
                            let model = arrayFollowingListing[editingCell.tag]
                            
                            model.isLiked = true
                            
                            model.likesCount = String (Int (model.likesCount!)! + 1)
                            
                            arrayFollowingListing[editingCell.tag] = model
                            
                            self.tableFriendsList.reloadData()
                            
                            return
                        }
                        else
                        {
                            // alert
                            UtilityClass.showAlertWithTitle(title: App_Name, message: message, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                            return
                        }
                    }
                    
            })
        }
    }
    
    func performUnLike(onCell cell:FriendsCell)
    {
        let editingCell = cell
        
        if let checkinID = arrayFollowingListing[editingCell.tag].checkInID
        {
            UtilityClass.showNetworkActivityLoader(show: true)
            
            let urlToHit = EndPoints.UnlikeCheckin(UtilityClass.getUserSidData()!, checkinID).path
            
            AppWebHandler.sharedInstance().fetchData(fromURL: urlToHit, httpMethod: .get, parameters: nil, completionHandler:
                { [weak self] (data, dictionary, statusCode, error) in
                    
                    UtilityClass.showNetworkActivityLoader(show: false)
                    
                    guard let `self` = self else { return }
                    
                    editingCell.likeUnlikeButton.isUserInteractionEnabled = true
                    
                    if error != nil
                    {
                        UtilityClass.showAlertWithTitle(title: App_Name, message: error?.localizedDescription, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                        return
                    }
                    
                    
                    let responseDictionary = dictionary!
                    let message = responseDictionary["message"] as! String
                    let type = responseDictionary["type"] as! Bool
                    
                    if statusCode == 203
                    {
                        // show alert
                        UtilityClass.showAlertWithTitle(title: App_Name, message: message, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                        return
                    }
                    
                    if statusCode == 200
                    {
                        if type
                        {
                            // success
                            let model = arrayFollowingListing[editingCell.tag]
                            
                            model.isLiked = false
                            
                            model.likesCount = String (Int (model.likesCount!)! - 1)
                            
                            arrayFollowingListing[editingCell.tag] = model
                            
                            self.tableFriendsList.reloadData()
                            
                            return
                        }
                        else
                        {
                            // alert
                            UtilityClass.showAlertWithTitle(title: App_Name, message: message, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                            return
                        }
                    }
                    
            })
        }
    }
}

extension FriendsListViewController
{
//    func scrollViewDidScroll(_ scrollView: UIScrollView) {
//        
//        if isEndOfResult
//        {
//            return
//        }
//        if tableFriendsList.contentOffset.y + tableFriendsList.frame.size.height >= tableFriendsList.contentSize.height
//        {
//            if !isPageRefreshing
//            {
//                isPageRefreshing = true
//                
//                currentPage+=1
//                
//                getFriendsApi(onRefresh: false)
//            }
//        }
//    }
}
