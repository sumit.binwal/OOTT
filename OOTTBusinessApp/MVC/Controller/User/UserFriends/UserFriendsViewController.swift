//
//  UserFriendsViewController.swift
//  OOTTBusinessApp
//
//  Created by Bharat Kumar Pathak on 07/07/17.
//  Copyright © 2017 KonstantInfoSolutions. All rights reserved.
//

import UIKit
import MXSegmentedPager

var searchFriendsModel = SearchFreiendsModel()

class UserFriendsViewController: MXSegmentedPagerController {
    
    var segmentedViewControllers = [UIViewController]()
    var segmentTitles = ["Friends", "Suggested"]
    var selectedPage: Int = 0
    
    @IBOutlet var navigationView: UIView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var searchButton: UIButton!
    @IBOutlet weak var searchTextField: UITextField!
    
    var pendingRequestWorkItem: DispatchWorkItem?
    
    // MARK:-
    override func viewDidLoad() {
        super.viewDidLoad()
    
        setupView()
        initializeChildViewControllers()
        segmentedPager.initializeSegments()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        showNavigationBar()
        
        if searchTextField.text == ""
        {
            searchTextField.isHidden = true
            titleLabel.isHidden = false
        }
        else
        {
            searchTextField.isHidden = false
            titleLabel.isHidden = true
        }
        
        self.view.endEditing(true)
    }
    
    deinit {
        print("UserFriendsViewController deinit")
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    //MARK:- Initial Setup View
    
    func setupView() -> Void
    {
        //self.navigationItem.title = "Friends".uppercased()
        
        self.navigationItem.titleView = navigationView
        
        self.navigationItem.titleView?.frame = CGRect(x: (self.navigationItem.titleView?.frame.origin.x)!, y: (self.navigationItem.titleView?.frame.origin.y)!, width: UIScreen.main.bounds.size.width, height: (self.navigationItem.titleView?.frame.size.height)!)
        
        let fontNavigationBarTitle = 18 * scaleFactorX
        
        titleLabel.font = UIFont (name: FONT_PROXIMA_REGULAR, size: fontNavigationBarTitle)
        titleLabel.textColor = UIColor.white
        titleLabel.text = "Friends".uppercased()
        
        searchTextField.attributedPlaceholder = NSAttributedString(string: "Search", attributes: [NSForegroundColorAttributeName: UIColor.init(RED: 255, GREEN: 255, BLUE: 255, ALPHA: 0.6)])

        searchTextField.font = UIFont (name: FONT_PROXIMA_REGULAR, size: 16 * scaleFactorX)
        searchTextField.textColor = UIColor.white
        
        searchTextField.isHidden = true
        
        searchTextField.delegate = self
    }
    
    @IBAction func searchButtonAction(_ sender: Any)
    {
        searchTextField.isHidden = false
        searchTextField.autocorrectionType = .no
        
        titleLabel.isHidden = true
        searchTextField.becomeFirstResponder()
    }
    
    func callSearchNotification() {
        
        if selectedPage == 0
        {
            // post a notification
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "applySearchForFriends"), object: nil, userInfo: nil)
        }
        else
        {
            // post a notification
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "applySearchForSuggestedFriends"), object: nil, userInfo: nil)
        }
    }
    
    // MARK:- Custom Methods
    
    func initializeChildViewControllers() {
        
        segmentedViewControllers.append (self.storyboard!.instantiateViewController(withIdentifier: "friendsListViewController") as! FriendsListViewController)
        
        segmentedViewControllers.append (self.storyboard!.instantiateViewController(withIdentifier: "suggestedFriendsViewController") as! SuggestedFriendsViewController)
    }
    
    // MARK:- MXSegmentedPager Delegate and Datasource Methods
    
    override func numberOfPages(in segmentedPager: MXSegmentedPager) -> Int {
        return segmentedViewControllers.count
    }
    
    override func segmentedPager(_ segmentedPager: MXSegmentedPager, titleForSectionAt index: Int) -> String {
        return segmentTitles[index]
    }
    
    override func segmentedPager(_ segmentedPager: MXSegmentedPager, viewControllerForPageAt index: Int) -> UIViewController {
        let viewController = segmentedViewControllers[index]
        self.addChildViewController(viewController)
        return viewController
    }
    
    override func segmentedPager(_ segmentedPager: MXSegmentedPager, didSelectViewWith index: Int) {
        
        if index == selectedPage {
            return
        }
        
        switch index {
        case 0:
            if let controller = segmentedViewControllers[0] as? FriendsListViewController
            {
                selectedPage = 0
                
                searchFriendsModel.searchString = ""
                searchTextField.isHidden = true
                titleLabel.isHidden = false
                searchTextField.text = ""
                
                controller.viewAboutToBeSelected()
            }
            
        case 1:
            if let controller = segmentedViewControllers[1] as? SuggestedFriendsViewController
            {
                selectedPage = 1
                
                searchFriendsModel.searchString = ""
                searchTextField.isHidden = true
                titleLabel.isHidden = false
                searchTextField.text = ""
                
                controller.viewAboutToBeSelected()
            }
            
        default:
            break
        }
    }
}

extension UserFriendsViewController : UITextFieldDelegate
{
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        let name = textField.text! as NSString
        
        // Cancel the currently pending item
        pendingRequestWorkItem?.cancel()
        
        // Wrap our request in a work item
        let requestWorkItem = DispatchWorkItem { [weak self] in
            
            searchFriendsModel.searchString = name as String
            
            self?.callSearchNotification()
        }
        
        // Save the new work item and execute it after 250 ms
        pendingRequestWorkItem = requestWorkItem
        DispatchQueue.main.asyncAfter(deadline: .now() + .milliseconds(250), execute: requestWorkItem)
        
        return true
    }
    
    func textFieldShouldClear(_ textField: UITextField) -> Bool {
        
        // Cancel the currently pending item
        pendingRequestWorkItem?.cancel()
        
        // Wrap our request in a work item
        let requestWorkItem = DispatchWorkItem { [weak self] in
            
            searchFriendsModel.searchString = ""
            
            self?.callSearchNotification()
        }
        
        // Save the new work item and execute it after 250 ms
        pendingRequestWorkItem = requestWorkItem
        DispatchQueue.main.asyncAfter(deadline: .now() + .milliseconds(250), execute: requestWorkItem)
        
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool
    {
        let  char = string.cString(using: String.Encoding.utf8)!
        let isBackSpace = strcmp(char, "\\b")
        
        if (isBackSpace == -92)
        {
            if textField.text?.count == 1
            {
                // Cancel the currently pending item
                pendingRequestWorkItem?.cancel()
                
                // Wrap our request in a work item
                let requestWorkItem = DispatchWorkItem { [weak self] in
                    
                    searchFriendsModel.searchString = ""
                    
                    self?.callSearchNotification()
                }
                
                // Save the new work item and execute it after 250 ms
                pendingRequestWorkItem = requestWorkItem
                DispatchQueue.main.asyncAfter(deadline: .now() + .milliseconds(250), execute: requestWorkItem)
            }
        }
        
        var newString : String?
        
        if string.count == 0
        {
            if textField.text?.count == 0
            {
                newString = textField.text
            }
            else
            {
                var newStr = textField.text! as NSString
                newStr = newStr.replacingCharacters(in: range, with: string) as NSString
                newString = newStr as String
            }
        }
        else
        {
            var newStr = textField.text! as NSString
            
            newStr = newStr.replacingCharacters(in: range, with: string) as NSString
            
            newString = newStr as String
        }
        
        if (newString?.count)! >= 3
        {
            let name = newString! as NSString
            
            print("name = \(name)")
            
            // Cancel the currently pending item
            pendingRequestWorkItem?.cancel()
            
            // Wrap our request in a work item
            let requestWorkItem = DispatchWorkItem { [weak self] in
                
                searchFriendsModel.searchString = name as String
                
                self?.callSearchNotification()
            }
            
            // Save the new work item and execute it after 250 ms
            pendingRequestWorkItem = requestWorkItem
            DispatchQueue.main.asyncAfter(deadline: .now() + .milliseconds(250), execute: requestWorkItem)
        }
        
        return true
    }
}
