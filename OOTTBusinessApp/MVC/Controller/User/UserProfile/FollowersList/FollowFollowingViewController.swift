//
//  FollowFollowingViewController.swift
//  OOTTBusinessApp
//
//  Created by Santosh on 15/11/17.
//  Copyright © 2017 KonstantInfoSolutions. All rights reserved.
//

import UIKit
import PKHUD
import DZNEmptyDataSet

class FollowFollowingViewController: UIViewController {

    
    @IBOutlet weak var tableFollowFollowingList: UITableView!
    
    var arrayUsers = [ModelFriendsListing]()
    
    var showFollowingList = false
    
    var userID = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        setupNavigationView()
        setupTableview()
        getUserListApi()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    //MARK:-
    func setupNavigationView() {
        
        self.title = showFollowingList ? "Following".uppercased() :"Followers".uppercased()
        
        let leftBarButton = self.setLeftBarButtonItem(withImage: #imageLiteral(resourceName: "backButton"))
        leftBarButton.action = #selector(onBackButtonAction)
        leftBarButton.target = self
    }
    
    //MARK:-
    func setupTableview()
    {
        tableFollowFollowingList.delegate = self
        tableFollowFollowingList.dataSource = self
        
        tableFollowFollowingList.emptyDataSetDelegate = self
        tableFollowFollowingList.emptyDataSetSource = self
    }
    
    //MARK:-
    func onBackButtonAction() {
        self.navigationController?.popViewController(animated: true)
    }
    
    //MARK:- UITableView Delegate and DataSource
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrayUsers.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 77 * scaleFactorX
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: FollowingCell = tableView.dequeueReusableCell(withIdentifier: kCellIdentifier_following_cell) as! FollowingCell
        
        cell.tag = indexPath.row
        
        
        cell.updateData(usingModel: arrayUsers[indexPath.row])
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    //MARK:- DZNEmptyDataSetSource -> Title
    func title(forEmptyDataSet scrollView: UIScrollView!) -> NSAttributedString! {
        
        let text = "No Users found".capitalized
        
        let attributes = [NSFontAttributeName:UIFont .boldSystemFont(ofSize: 16*scaleFactorX), NSForegroundColorAttributeName:UIColor.white]
        
        return NSAttributedString (string: text, attributes: attributes)
    }
    
    func emptyDataSetShouldAllowScroll(_ scrollView: UIScrollView!) -> Bool {
        return true
    }
    
    //MARK:- Get Blocked User List Api
    func getUserListApi()
    {
        HUD.show(.systemActivity, onView: self.view.window)
        
        var urlToHit = EndPoints.getFollowersList(UtilityClass.getUserSidData()!, userID).path
        
        if showFollowingList
        {
            urlToHit = EndPoints.getFollowingList(UtilityClass.getUserSidData()!, userID).path
        }
        
        AppWebHandler.sharedInstance().fetchData(fromURL: urlToHit, httpMethod: .get, parameters: nil)
        {[weak self] (data, dictionary, statusCode, error) in
            
            HUD.hide()
            
            guard let `self` = self else { return }
            
            if error != nil
            {
                UtilityClass.showAlertWithTitle(title: App_Name, message: error?.localizedDescription, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                return
            }
            
            
            let responseDictionary = dictionary!
            let message = responseDictionary["message"] as! String
            let type = responseDictionary["type"] as! Bool
            
            if statusCode == 203
            {
                return
            }
            
            if statusCode == 200
            {
                if type
                {
                    // success
                    let dataArray = responseDictionary["data"] as! [[String:Any]] // As array
                    print(dataArray)
                    self.updateModelArray(dataArray)
                    return
                }
                else
                {
                    // alert
                    UtilityClass.showAlertWithTitle(title: App_Name, message: message, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                    return
                }
            }
        }
    }
    
    //MARK:-
    func updateModelArray(_ usingArray : [[String:Any]])
    {
        arrayUsers.removeAll()
        let dataArray = usingArray
        
        for dict in dataArray // Iterating dictionaries
        {
            let model =  ModelFriendsListing() // Model creation
            model.updateModel(usingDictionary: dict) // Updating model
            arrayUsers.append(model) // Adding model to array
        }
        tableFollowFollowingList.reloadData()
    }
}

//MARK:-
//MARK:- extension
extension FollowFollowingViewController : UITableViewDelegate, UITableViewDataSource, DZNEmptyDataSetDelegate, DZNEmptyDataSetSource
{
    
}
