//
//  UserSettingsViewController.swift
//  OOTTBusinessApp
//
//  Created by Bharat Kumar Pathak on 11/07/17.
//  Copyright © 2017 KonstantInfoSolutions. All rights reserved.
//

import UIKit
import PKHUD

//enum LogoutCaseEnum : Int {
//    case singleLogout
//    case groupLogout
//    case addAccountLogout
//    case switchAccount
//}

struct UserSettings {
    var settingName = ""
    var settingsImage : UIImage!
}

class UserSettingsViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    // MARK:-
    var isNotificationEnabled = true
    var isPrivateAccount = false
    
    var isGhostMode = false
    
    var arraySettings = [UserSettings]()
    
//    var logoutCase = LogoutCaseEnum.singleLogout
    
    var isSwitchModeEnabled = false
    
    var selectedIndex = 0
    
    
    @IBOutlet weak var tableSettings: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let contains = UtilityClass.isCurrentUserInStackedUserInfoData()
        if !contains
        {
            UtilityClass.deleteStackedUserInfoData()
        }
        else
        {
            if let stackedUsers = UtilityClass.getStackedUserInfoData()
            {
                if stackedUsers.count == 1
                {
                    UtilityClass.deleteStackedUserInfoData()
                }
//                else // Check for updated name and replace
//                {
//                    let contains = stackedUsers.contains(where: { (userDict) -> Bool in
//                        return userDict["sid"] as! String == UtilityClass.getUserSidData()!
//                    })
//
//                    if contains
//                    {
//                        let index = stackedUsers.index(where: { (userDict) -> Bool in
//                            return userDict["sid"] as! String == UtilityClass.getUserSidData()!
//                        })
//
//                        var userInfo = stackedUsers[index!]
//                        userInfo["name"] = UtilityClass.getUserNameData()!
//                        stackedUsers[index!] = userInfo
//                        UtilityClass.saveStackedUserInfoArray(userArray: stackedUsers)
//                    }
//                }
            }
        }
        
        setupView()
        setupArray()
        
        isNotificationEnabled = UtilityClass.getNotificationSetting()
        isPrivateAccount = UtilityClass.getIsPrivateAccountData()
        isGhostMode = UtilityClass.getIsGhostAccountData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        showNavigationBar()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    //MARK:- Initial Setup View
    func setupView() -> Void
    {
        self.navigationItem.title = "settings".uppercased()
        
        let leftBarButton = self.setLeftBarButtonItem(withImage: #imageLiteral(resourceName: "backButton"))
        leftBarButton.action = #selector(onBackButtonAction)
        leftBarButton.target = self
    }
    
    //MARK:- Setup Array
    func setupArray() -> Void
    {
        arraySettings.append(UserSettings (settingName: "Edit Profile", settingsImage: #imageLiteral(resourceName: "setting_profile")))
        arraySettings.append(UserSettings (settingName: "Blocked Users", settingsImage: #imageLiteral(resourceName: "setting_deleteAccount")))
        arraySettings.append(UserSettings (settingName: "Notification", settingsImage: #imageLiteral(resourceName: "setting_notification")))
        arraySettings.append(UserSettings (settingName: "Private Account", settingsImage: #imageLiteral(resourceName: "setting_notification")))
        arraySettings.append(UserSettings (settingName: "Ghost Mode", settingsImage: #imageLiteral(resourceName: "setting_notification")))
        arraySettings.append(UserSettings (settingName: "Privacy Policy", settingsImage: #imageLiteral(resourceName: "setting_info")))
        arraySettings.append(UserSettings (settingName: "Terms & Conditions", settingsImage: #imageLiteral(resourceName: "setting_info")))
        arraySettings.append(UserSettings (settingName: "Delete Account", settingsImage: #imageLiteral(resourceName: "setting_deleteAccount")))
        arraySettings.append(UserSettings (settingName: "Add Account", settingsImage: #imageLiteral(resourceName: "setting_addAcount")))
        arraySettings.append(UserSettings (settingName: "Logout", settingsImage: #imageLiteral(resourceName: "setting_logout")))
        
        if (UtilityClass.getStackedUserInfoData() != nil)
        {
            arraySettings.append(UserSettings (settingName: "Logout All Accounts", settingsImage: #imageLiteral(resourceName: "setting_logout")))
            arraySettings.append(UserSettings (settingName: "Switch Account", settingsImage: #imageLiteral(resourceName: "setting_addAcount")))
            
            //            for userInfo in stackedUsers
            //            {
            //                arraySettings.append(UserSettings (settingName: userInfo["name"] as! String, settingsImage: #imageLiteral(resourceName: "setting_addAcount")))
            //            }
        }
        
        tableSettings.reloadData()
    }
    
    func onBackButtonAction() {
        self.navigationController?.popViewController(animated: true)
    }
    
    // MARK:- UITableView Delegate and Datasource Methods
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 1
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let sectionHeader = UIView()
        sectionHeader.backgroundColor = UIColor(RED: 56, GREEN: 71, BLUE: 85, ALPHA: 0.2)
        return sectionHeader
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arraySettings.count
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 55
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 55 * scaleFactorX;
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell =  tableView.dequeueReusableCell(withIdentifier: kCellIdentifier_setting_arrowCell) as! SettingsCell
        //        cell.updateUserSettingsCellContents(withIndexpath: indexPath)
        cell.updateUserSettingsCellContents(usingModel: arraySettings[indexPath.row])
        
        if indexPath.row == 2 { // Notification
            cell.imageRightArrow.isHidden=true
            cell.buttonNotificationToggle.isHidden=false
            cell.buttonNotificationToggle.tag = indexPath.row
            
            if isNotificationEnabled
            {
                cell.buttonNotificationToggle.setImage(#imageLiteral(resourceName: "toggle_on").withRenderingMode(.alwaysOriginal), for: .normal)
            }
            else
            {
                cell.buttonNotificationToggle.setImage(#imageLiteral(resourceName: "toggle_off").withRenderingMode(.alwaysOriginal), for: .normal)
            }
            cell.buttonNotificationToggle.addTarget(self, action:#selector(toggleButtonClickedAction(_sender:)), for: .touchUpInside)
        }
        if indexPath.row == 3 { // Private account
            cell.imageRightArrow.isHidden=true
            cell.buttonNotificationToggle.isHidden=false
            cell.buttonNotificationToggle.tag = indexPath.row
            
            if isPrivateAccount
            {
                cell.buttonNotificationToggle.setImage(#imageLiteral(resourceName: "toggle_on").withRenderingMode(.alwaysOriginal), for: .normal)
            }
            else
            {
                cell.buttonNotificationToggle.setImage(#imageLiteral(resourceName: "toggle_off").withRenderingMode(.alwaysOriginal), for: .normal)
            }
            cell.buttonNotificationToggle.addTarget(self, action:#selector(performPrivateAccountApi(_sender:)), for: .touchUpInside)
        }
        if indexPath.row == 4 { // Ghost Mode
            cell.imageRightArrow.isHidden=true
            cell.buttonNotificationToggle.isHidden=false
            cell.buttonNotificationToggle.tag = indexPath.row
            
            if isGhostMode
            {
                cell.buttonNotificationToggle.setImage(#imageLiteral(resourceName: "toggle_on").withRenderingMode(.alwaysOriginal), for: .normal)
            }
            else
            {
                cell.buttonNotificationToggle.setImage(#imageLiteral(resourceName: "toggle_off").withRenderingMode(.alwaysOriginal), for: .normal)
            }
            cell.buttonNotificationToggle.addTarget(self, action:#selector(performGhostAccountApi(_sender:)), for: .touchUpInside)
        }
        if indexPath.row > 11
        {
            cell.imageRightArrow.isHidden=true
            //            cell.imageIcon.isHidden=true
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        selectedIndex = indexPath.row
        
        var controllerToDisplay : UIViewController? = nil
        
        
        switch indexPath.row
        {
            
        case 0: //Edit Profile
            
            controllerToDisplay = UIStoryboard.getUserProfileStoryboard().instantiateViewController(withIdentifier: "editProfileVCC")
            
            break
            
        case 1: //Blocked Users
            
            controllerToDisplay = UIStoryboard.getUserProfileStoryboard().instantiateViewController(withIdentifier: "blockedUserVC")
            
            break
            
            
        case 2: //Notification
            
            break
            
        case 3: //Private Account
            
            break
            
        case 4: //Ghost Mode
            
            break
        case 5: //Privacy Policies
            UtilityClass.openSafariController(usingLink: LinksEnum.privacyPolicy, onViewController: self.navigationController)
            break
            
        case 6: //Terms And Condition
            UtilityClass.openSafariController(usingLink: LinksEnum.termsAndCond, onViewController: self.navigationController)
            break
            
        case 7: //Delete Account
            UtilityClass.showAlertWithTitle(title: "Confirm Delete", message: "Your account will be deactivated. Please visit oottnightlife.com to reactivate your account.", onViewController: self, withButtonArray: ["Delete"], cancelButtonTitle: "Cancel", dismissHandler:
                {[weak self] (butotnIndex) in
                    guard let `self` = self else {return}
                    if butotnIndex == 0
                    {
                        self.deleteAccount()
                    }
            })
            break
            
        case 8: //Add Account
            logoutCase = .addAccountLogout
            logoutAPI()
            return
            
        case 9: //Logout
            logoutCase = .singleLogout
            logoutAPI()
            return
            
        case 10: //Logout from All
            logoutCase = .groupLogout
            logoutAPI()
            return
            
        case 11: //Switch Account
            isSwitchModeEnabled = !isSwitchModeEnabled
            
            let cell = tableView.cellForRow(at: indexPath) as! SettingsCell
            
            cell.imageRightArrow.transform = isSwitchModeEnabled ? CGAffineTransform (rotationAngle: CGFloat(Double.pi)/2) : CGAffineTransform.identity
            
            if let stackedUsers = UtilityClass.getStackedUserInfoData()
            {
                if isSwitchModeEnabled
                {
                    for userInfo in stackedUsers
                    {
                        if userInfo["sid"] as! String == UtilityClass.getUserSidData()!
                        {
                            continue
                        }
                        arraySettings.append(UserSettings (settingName: userInfo["username"] as! String, settingsImage: #imageLiteral(resourceName: "setting_employees")))
                    }
                }
                else
                {
                    var count = 0
                    for userInfo in stackedUsers
                    {
                        if userInfo["sid"] as! String == UtilityClass.getUserSidData()!
                        {
                            continue
                        }
                        count = count + 1
                    }
                    let range = arraySettings.endIndex.advanced(by: -count)..<arraySettings.endIndex
                    arraySettings.removeSubrange(range)
                }
                tableView.reloadData()
//                tableView.reloadRows(at: [IndexPath (row: arraySettings.count-1, section: 0)], with: .bottom)
            }
            return
            
        default: // For switching users
            logoutCase = .switchAccount
            logoutAPI()
            break
        }
        
        guard controllerToDisplay != nil else
        {
            return
        }
        self.navigationController?.pushViewController(controllerToDisplay!, animated: true)
    }
    
    //MARK: - User notification ON/OFF Api
    @IBAction func toggleButtonClickedAction(_sender : UIButton){
        
        _sender.isUserInteractionEnabled = false
        
        let params = ["isNotify" : !isNotificationEnabled]
        
        let urlToHit = EndPoints.editProfile(UtilityClass.getUserSidData()!).path
        
        AppWebHandler.sharedInstance().fetchData(fromURL: urlToHit, httpMethod: .post, parameters: params)
        {[weak self] (data, dictionary, statusCode, error) in
            
            _sender.isUserInteractionEnabled = true
            
            HUD.hide()
            
            guard let `self` = self else { return }
            
            if error != nil
            {
                UtilityClass.showAlertWithTitle(title: App_Name, message: error?.localizedDescription, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                return
            }
            
            let responseDictionary = dictionary!
            let message = responseDictionary["message"] as! String
            let type = responseDictionary["type"] as! Bool
            
            if statusCode == 203
            {
                // show alert
                UtilityClass.showAlertWithTitle(title: App_Name, message: message, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                return
            }
            
            if statusCode == 200
            {
                if type
                {
                    // success
                    
                    self.isNotificationEnabled = !self.isNotificationEnabled
                    
                    var userDict = UtilityClass.getUserInfoData()
                    userDict["isNotify"] = self.isNotificationEnabled
                    UtilityClass.saveUserInfoData(userDict: userDict)
                    self.tableSettings.reloadData()
                    return
                }
                else
                {
                    // alert
                    UtilityClass.showAlertWithTitle(title: App_Name, message: message, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                    return
                }
            }
        }
    }
    
    
    //MARK: - User Ghost Account ON/OFF Api
    @IBAction func performGhostAccountApi(_sender : UIButton){
        
        _sender.isUserInteractionEnabled = false
        
        let params = ["isGhostAccount" : !isGhostMode]
        
        let urlToHit = EndPoints.editProfile(UtilityClass.getUserSidData()!).path
        
        AppWebHandler.sharedInstance().fetchData(fromURL: urlToHit, httpMethod: .post, parameters: params)
        {[weak self] (data, dictionary, statusCode, error) in
            
            _sender.isUserInteractionEnabled = true
            
            HUD.hide()
            
            guard let `self` = self else { return }
            
            if error != nil
            {
                UtilityClass.showAlertWithTitle(title: App_Name, message: error?.localizedDescription, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                return
            }
            
            let responseDictionary = dictionary!
            let message = responseDictionary["message"] as! String
            let type = responseDictionary["type"] as! Bool
            
            if statusCode == 203
            {
                // show alert
                UtilityClass.showAlertWithTitle(title: App_Name, message: message, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                return
            }
            
            if statusCode == 200
            {
                if type
                {
                    // success
                    
                    self.isGhostMode = !self.isGhostMode
                    
                    UtilityClass.setGhostModeAccount(self.isGhostMode)
                    self.tableSettings.reloadData()
                    return
                }
                else
                {
                    // alert
                    UtilityClass.showAlertWithTitle(title: App_Name, message: message, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                    return
                }
            }
        }
    }
    
    //MARK: - User Private Account ON/OFF Api
    @IBAction func performPrivateAccountApi(_sender : UIButton){
        
        _sender.isUserInteractionEnabled = false
        
        let params = ["isPrivateAccount" : !isPrivateAccount]
        
        let urlToHit = EndPoints.editProfile(UtilityClass.getUserSidData()!).path
        
        AppWebHandler.sharedInstance().fetchData(fromURL: urlToHit, httpMethod: .post, parameters: params)
        {[weak self] (data, dictionary, statusCode, error) in
            
            _sender.isUserInteractionEnabled = true
            
            HUD.hide()
            
            guard let `self` = self else { return }
            
            if error != nil
            {
                UtilityClass.showAlertWithTitle(title: App_Name, message: error?.localizedDescription, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                return
            }
            
            let responseDictionary = dictionary!
            let message = responseDictionary["message"] as! String
            let type = responseDictionary["type"] as! Bool
            
            if statusCode == 203
            {
                // show alert
                UtilityClass.showAlertWithTitle(title: App_Name, message: message, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                return
            }
            
            if statusCode == 200
            {
                if type
                {
                    // success
                    
                    self.isPrivateAccount = !self.isPrivateAccount
                    
                    UtilityClass.setIsPrivateAccount(self.isPrivateAccount)
                    self.tableSettings.reloadData()
                    return
                }
                else
                {
                    // alert
                    UtilityClass.showAlertWithTitle(title: App_Name, message: message, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                    return
                }
            }
        }
    }
    
    
    //MARK:- Logout API Call
    func logoutAPI() -> Void {
        
        HUD.show(.systemActivity, onView: self.view.window)
        
        self.view.endEditing(true)
        
        let urlToHit = EndPoints.logout(UtilityClass.getUserSidData()!).path
        
        AppWebHandler.sharedInstance().fetchData(fromURL: urlToHit, httpMethod: .get, parameters: nil, shouldDeserialize: true)
        {[weak self] (data, dictionary, statusCode, error) in
            
            HUD.hide()
            
            guard let `self` = self else { return }
            
            if error != nil
            {
                UtilityClass.showAlertWithTitle(title: App_Name, message: error!.localizedDescription, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                
                return
            }
            
            
            
            if dictionary == nil
            {
                UtilityClass.showAlertWithTitle(title: App_Name, message: App_Global_Error_Msg, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                
                return
            }
            
            if statusCode == 203
            {
                let responseDictionary = dictionary!
                guard (responseDictionary["message"] != nil) else
                {
                    UtilityClass.showAlertWithTitle(title: App_Name, message: App_Global_Error_Msg, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                    return
                }
                let messageStr = responseDictionary["message"] as! String
                
                UtilityClass.showAlertWithTitle(title: App_Name, message: messageStr, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                return
            }
            
            if statusCode == 200
            {
                let responseDictionary = dictionary!
                let type = responseDictionary["type"] as! Bool
                let msgStr = responseDictionary["message"] as! String
                
                if type
                {
                    if logoutCase == .addAccountLogout // Adding current user to stack list...
                    {
                        UtilityClass.addCurrentUserToStack()
                    }
                    if logoutCase == .singleLogout // Removing current user from stack list...
                    {
                        _ = UtilityClass.removeCurrentUserFromStack()
                    }
                    if logoutCase == .groupLogout // Removing all user from stack list...
                    {
                        UtilityClass.deleteStackedUserInfoData()
                    }
                    if logoutCase == .switchAccount // updating current user in stack list...
                    {
                        UtilityClass.updateCurrentUserInStack()
                    }
                    
                    UtilityClass.deleteDataOnLogout()
                    
                    currentUserID = nil
                    SocketManager.sharedInstance().removeIncomingMessagesListener()
                    SocketManager.sharedInstance().disconnectSocket()
                    
                    if UtilityClass.getFirstUserFromStackedList() != nil
                    {
                        self.updateRootViewController()
                        return
                    }
                    
                    let loginVC = UIStoryboard.getLoginFlowStoryboard().instantiateInitialViewController()
                    
                    UtilityClass.changeRootViewController(with: loginVC!)
                }
                else
                {
                    UtilityClass.showAlertWithTitle(title: App_Name, message: msgStr, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                }
            }
        }
    }
    
    func deleteAccount()
    {
        HUD.show(.systemActivity, onView: self.view.window)
        
        self.view.endEditing(true)
        
        let urlToHit = EndPoints.deleteAccount(UtilityClass.getUserSidData()!).path
        
        AppWebHandler.sharedInstance().fetchData(fromURL: urlToHit, httpMethod: .get, parameters: nil, shouldDeserialize: true)
        {[weak self] (data, dictionary, statusCode, error) in
            
            HUD.hide()
            
            guard let `self` = self else { return }
            
            if error != nil
            {
                UtilityClass.showAlertWithTitle(title: App_Name, message: error!.localizedDescription, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                
                return
            }
            
            if dictionary == nil
            {
                UtilityClass.showAlertWithTitle(title: App_Name, message: App_Global_Error_Msg, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                
                return
            }
            
            if statusCode == 203
            {
                let responseDictionary = dictionary!
                guard (responseDictionary["message"] != nil) else
                {
                    UtilityClass.showAlertWithTitle(title: App_Name, message: App_Global_Error_Msg, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                    return
                }
                let messageStr = responseDictionary["message"] as! String
                
                UtilityClass.showAlertWithTitle(title: App_Name, message: messageStr, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                return
            }
            
            if statusCode == 200
            {
                let responseDictionary = dictionary!
                let type = responseDictionary["type"] as! Bool
                let msgStr = responseDictionary["message"] as! String
                
                if type
                {
                    UtilityClass.removeCurrentUserFromStack()
                    UtilityClass.deleteDataOnLogout()
                    
                    currentUserID = nil
                    SocketManager.sharedInstance().removeIncomingMessagesListener()
                    SocketManager.sharedInstance().disconnectSocket()
                    
                    if UtilityClass.getFirstUserFromStackedList() != nil
                    {
                        self.updateRootViewController()
                        return
                    }
                    
                    let loginVC = UIStoryboard.getLoginFlowStoryboard().instantiateInitialViewController()
                    
                    UtilityClass.changeRootViewController(with: loginVC!)
                }
                else
                {
                    UtilityClass.showAlertWithTitle(title: App_Name, message: msgStr, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                }
            }
        }
    }
    
    func updateRootViewController()
    {
        if logoutCase == .switchAccount || logoutCase == .singleLogout
        {
            if logoutCase == .switchAccount
            {
                UtilityClass.switchCurrentUserWithUser(self.arraySettings[self.selectedIndex].settingName)
            }
            else
            {
                UtilityClass.saveUserInfoData(userDict: UtilityClass.getFirstUserFromStackedList()!)
            }
            
            if UtilityClass.getUserTypeData() == AppUserTypeEnum.employee.rawValue
            {
                let employeeVC = UIStoryboard.getBusinessHomeFlowStoryboard().instantiateInitialViewController()
                
                UtilityClass.changeRootViewController(with: employeeVC!)
                
                return
            }
            
            if UtilityClass.getUserTypeData() == AppUserTypeEnum.provider.rawValue
            {
                let businessVC = UIStoryboard.getBusinessTabBarFlowStoryboard().instantiateInitialViewController()
                
                UtilityClass.changeRootViewController(with: businessVC!)
                return
            }
            
            let userHomeVC = UIStoryboard.getUserTabsStoryboard().instantiateInitialViewController()
            
            UtilityClass.changeRootViewController(with: userHomeVC!)
            
            return
        }
        let loginVC = UIStoryboard.getLoginFlowStoryboard().instantiateInitialViewController()
        
        UtilityClass.changeRootViewController(with: loginVC!)
    }
}
