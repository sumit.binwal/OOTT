//
//  BusinessGroupListViewController.swift
//  OOTTBusinessApp
//
//  Created by Bharat Kumar Pathak on 06/07/17.
//  Copyright © 2017 KonstantInfoSolutions. All rights reserved.
//

import UIKit
import PKHUD
import DZNEmptyDataSet

import CoreLocation
import UberCore
import UberRides
import LyftSDK

var arrayGroupListing = [ModelGroupListing]()
var arrayGroupFavListing = [ModelGroupListing]()

var arrayGroupTempListing = [ModelGroupListing]()

//MARK:-
//MARK:-
class BusinessGroupListViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, BusinessGroupCellDelegate, DZNEmptyDataSetSource, DZNEmptyDataSetDelegate,MyGroupDelegate {

    

    
    
    
    @IBOutlet weak var tableGroupListing: UITableView!
    
    var modelFilter = ModelGroupFilter ()
    
    var arrayListing = [ModelGroupListing]()
    
    var isFavouriteScreen = false
    
    var isViewAppearedFirstTime = true
    
    var refreshControl: UIRefreshControl!
    
    var isSearchModeOn = false
    
    var searchedString = ""
    
    var isViewLoadedd: Bool = false
    
    var didDisappear = false
    
    var dropLatitude = Double()
    var dropLongitude = Double()
    var selectedIndex: Int = -1
    
    
    var pickupAddressStr = NSString()
    var dropoffAddressStr = NSString()
    
    
    //MARK:- viewDidLoad
    override func viewDidLoad() {
        super.viewDidLoad()
        setupTableview()
        applyRefreshControl()
        NotificationCenter.default.addObserver(self, selector: #selector(locationManagerDidCalledForFirstTime), name: NSNotification.Name(rawValue: "LocationUpdate"), object: nil)
        
        //        if LocationManager.sharedInstance().locationUpdateCount>1
        //        {
        //            getGroupsApi(onRefresh: true, shouldShowHUD: true)
        //        }
    }
    
    //MARK:- didReceiveMemoryWarning
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    //MARK:- deinit
    deinit {
        arrayGroupListing.removeAll()
        arrayGroupFavListing.removeAll()
        arrayGroupTempListing.removeAll()
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "LocationUpdate"), object: nil)
        print("BusinessGroupListViewController ")
    }
    
    func setupTableview()
    {
        tableGroupListing.delegate = self
        tableGroupListing.dataSource = self
        tableGroupListing.emptyDataSetSource = self
        tableGroupListing.emptyDataSetDelegate = self
    }
    
    func viewAboutToBeSelected()
    {
        if LocationManager.sharedInstance().locationUpdateCount>1
        {
            getGroupsApi(onRefresh: true, shouldShowHUD: false)
        }
        
        tableGroupListing.reloadData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        
        
        if !isViewLoadedd {
            isViewLoadedd = true
            if LocationManager.sharedInstance().locationUpdateCount>1
            {
                getGroupsApi(onRefresh: true, shouldShowHUD: true)
            }
        }
        else {
            if didDisappear {
                didDisappear = false
                if LocationManager.sharedInstance().locationUpdateCount>1
                {
                    getGroupsApi(onRefresh: true, shouldShowHUD: false)
                }
            }
        }
        
        
    }
    
    // MARK:- Refresh Control
    func applyRefreshControl() {
        
        //        LocationManager.sharedInstance().askForLocation()
        refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(self.refreshPage), for: UIControlEvents.valueChanged)
        self.tableGroupListing.addSubview(refreshControl)
    }
    
    func refreshPage(sender:AnyObject) {
        if LocationManager.sharedInstance().locationUpdateCount>1
        {
            getGroupsApi(onRefresh: true, shouldShowHUD: true)
        }
        else
        {
            refreshControl.endRefreshing()
        }
    }
    
    // MARK:- UITableView Delegate and Datasource Methods
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if isSearchModeOn
        {
            return arrayGroupTempListing.count
        }
        return isFavouriteScreen ? arrayGroupFavListing.count : arrayGroupListing.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 162 * scaleFactorX
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell: BusinessGroupCell = tableView.dequeueReusableCell(withIdentifier: kCellIdentifier_business_group_cell) as! BusinessGroupCell
        cell.tag = indexPath.row
        cell.cellDelegate = self
        if isSearchModeOn
        {
            cell.updateData(usingModel: arrayGroupTempListing[indexPath.row])
        }
        else if isFavouriteScreen
        {
            cell.updateData(usingModel: arrayGroupFavListing[indexPath.row])
        }
        else
        {
            cell.updateData(usingModel: arrayGroupListing[indexPath.row])
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if isSearchModeOn
        {
            let businessGroupModel = arrayGroupTempListing[indexPath.row]
            navigateToBusinessDetails(forBusinessId: businessGroupModel.groupID!)
        }
        else if isFavouriteScreen
        {
            let businessGroupModel = arrayGroupFavListing[indexPath.row]
            navigateToBusinessDetails(forBusinessId: businessGroupModel.groupID!)
        }
        else
        {
            let businessGroupModel = arrayGroupListing[indexPath.row]
            navigateToBusinessDetails(forBusinessId: businessGroupModel.groupID!)
        }
    }
    
    //MARK:- DZNEmptyDataSetSource -> Title
    func title(forEmptyDataSet scrollView: UIScrollView!) -> NSAttributedString! {
        
        var text = isFavouriteScreen ? "No Favorites" : "No Locations Found"
        if isSearchModeOn
        {
            text = "No Search Result Found"
        }
        
        //        if !LocationManager.sharedInstance().isLocationAccessAllowed()
        //        {
        //            text = "Location is turned off, please turn on by going to settings"
        //        }
        
        let attributes = [NSFontAttributeName:UIFont .boldSystemFont(ofSize: 16*scaleFactorX), NSForegroundColorAttributeName:UIColor.white]
        
        return NSAttributedString (string: text, attributes: attributes)
    }
    
    //    func buttonTitle(forEmptyDataSet scrollView: UIScrollView!, for state: UIControlState) -> NSAttributedString! {
    //        let text = !LocationManager.sharedInstance().isLocationAccessAllowed() ? "Go To Settings" : ""
    //
    //        let attributes = [NSFontAttributeName:UIFont .boldSystemFont(ofSize: 18*scaleFactorX), NSForegroundColorAttributeName:UIColor.cyan]
    //
    //        return NSAttributedString (string: text, attributes: attributes)
    //    }
    //
    //    func emptyDataSet(_ scrollView: UIScrollView!, didTap button: UIButton!) {
    ////        if LocationManager.sharedInstance().isLocationAccessAllowed() {
    //            UIApplication.shared.open(URL (string: UIApplicationOpenSettingsURLString)!, options: [:], completionHandler: nil)
    ////        }
    //    }
    
    func emptyDataSetShouldAllowScroll(_ scrollView: UIScrollView!) -> Bool {
        return true
    }
    
    //MARK:- BusinessGroupCell Delegate -> Like Button
    func groupCell(cell: BusinessGroupCell, didTapOnLikeButton likeButton: UIButton) {
        
        likeButton.isUserInteractionEnabled = false
        
        let businessGroupModel = isSearchModeOn ? arrayGroupTempListing[cell.tag] : isFavouriteScreen ? arrayGroupFavListing[cell.tag] : arrayGroupListing[cell.tag]
        let groupID = businessGroupModel.groupID
        
        if isFavouriteScreen {
            performUnfavouriteGroupApi(forGroupID: groupID!, groupCell: cell)
        } else {
            if businessGroupModel.isFavourite!
            {
                performUnfavouriteGroupApi(forGroupID: groupID!, groupCell: cell)
            }
            else
            {
                performFavouriteGroupApi(forGroupID: groupID!, groupCell: cell)
            }
        }
        
    }
    
    //MARK:- BusinessGroupCell Delegate -> Heart List Button
    func groupCell(cell: BusinessGroupCell, didTapOnHeartButton heartButton: UIButton) {
        navigateToLikesListing(forBusinessGroup: isSearchModeOn ? arrayGroupTempListing[cell.tag] :  isFavouriteScreen ? arrayGroupFavListing[cell.tag] : arrayGroupListing[cell.tag])
    }
    
    //MARK:- BusinessGroupCell Delegate -> Friends List Button
    func groupCell(cell: BusinessGroupCell, didTapOnFriendsButton friendsButton: UIButton) {
        navigateToFriendsListing(forBusinessGroup: isSearchModeOn ? arrayGroupTempListing[cell.tag] : isFavouriteScreen ? arrayGroupFavListing[cell.tag] : arrayGroupListing[cell.tag])
    }
    
    //MARK:- Navigation Methods
    func navigateToFriendsListing(forBusinessGroup groupModel:ModelGroupListing) {
        
        didDisappear = true
        let friendsViewController = self.storyboard?.instantiateViewController(withIdentifier: "groupLikesViewController") as! GroupLikesViewController
        friendsViewController.viewType = .friendsHere
        friendsViewController.businessID = groupModel.groupID!
        friendsViewController.businessFriendCount = groupModel.friendsCount!
        //        friendsViewController.businessModel = groupModel
        friendsViewController.hidesBottomBarWhenPushed = true
        self.parent?.navigationController?.pushViewController(friendsViewController, animated: true)
    }
    
    func navigateToLikesListing(forBusinessGroup groupModel:ModelGroupListing) {
        didDisappear = true
        let likeViewController = self.storyboard?.instantiateViewController(withIdentifier: "groupLikesViewController") as! GroupLikesViewController
        likeViewController.viewType = .peopleLiked
        likeViewController.businessID = groupModel.groupID!
        likeViewController.businessFriendCount = groupModel.peopleLiked!
        //        likeViewController.businessModel = groupModel
        likeViewController.hidesBottomBarWhenPushed = true
        self.parent?.navigationController?.pushViewController(likeViewController, animated: true)
    }
    
    func navigateToBusinessDetails(forBusinessId businessID:String) {
        didDisappear = true
        let detailsViewController = UIStoryboard.getBusinessDetailsStoryboard().instantiateViewController(withIdentifier: "businessDetailsViewController") as! BusinessDetailsViewController
        detailsViewController.businessId = businessID
        detailsViewController.isFromDetail = true
        detailsViewController.hidesBottomBarWhenPushed = true
        self.parent?.navigationController?.pushViewController(detailsViewController, animated: true)
    }
    
    //MARK:- BusinessGroupCell Delegate -> Make Move Button
    func groupCell(cell: BusinessGroupCell, didTapOnMakeMovesButton makeMoveButton: UIButton) {
        let businessGroupModel = isSearchModeOn ? arrayGroupTempListing[cell.tag] : isFavouriteScreen ? arrayGroupFavListing[cell.tag] : arrayGroupListing[cell.tag]
        
        let groupID = businessGroupModel.groupID
        let groupName = businessGroupModel.groupName
        
        
        
        if businessGroupModel.isMakeMove!
        {
            
            cancelMakeMove(isGroupMove: true, forGroupID: groupID!, groupCell: cell)
        }
        else
        {
            selectedIndex = cell.tag
            dropLatitude = (businessGroupModel.latitude! as NSString).doubleValue
            dropLongitude = (businessGroupModel.longitude! as NSString).doubleValue
            performMakeMoveApi(forGroupID: groupID!, groupCell: cell, groupName: groupName ?? "")
            
        }
        //        makeMoveButton.isUserInteractionEnabled = false
        
        
    }
    
    //MARK:- Get Group Listing Api
    func getGroupsApi(onRefresh: Bool, shouldShowHUD showHUD:Bool) -> Void
    {
        if showHUD
        {
            HUD.show(.systemActivity, onView: self.view.window)
        }
        
        var params = modelFilter.getDictionary()
        params["isFavourite"] = isFavouriteScreen
        
        print("**Dict :",params)
        
        let urlToHit = EndPoints.getGroupsHome(UtilityClass.getUserSidData()!).path
        
        AppWebHandler.sharedInstance().fetchData(fromURL: urlToHit, httpMethod: .post, parameters: params)
        {[weak self] (data, dictionary, statusCode, error) in
            
            HUD.hide()
            
            guard let `self` = self else { return }
            
            self.refreshControl.endRefreshing()
            
            if error != nil
            {
                UtilityClass.showAlertWithTitle(title: App_Name, message: error?.localizedDescription, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                return
            }
            
            let responseDictionary = dictionary!
            print(responseDictionary)
            let message = responseDictionary["message"] as! String
            let type = responseDictionary["type"] as! Bool
            
            if statusCode == 203
            {
                // show alert
                //                UtilityClass.showAlertWithTitle(title: App_Name, message: message, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                return
            }
            
            if statusCode == 200
            {
                if type
                {
                    // success
                    let dataArray = responseDictionary["data"] as! [[String:Any]] // As array
                    if onRefresh {
                        //                        self.isSearchModeOn = false
                        //                        arrayGroupTempListing.removeAll()
                        self.isFavouriteScreen ? arrayGroupFavListing.removeAll() : arrayGroupListing.removeAll()
                    }
                    self.updateModelArray(usingArray: dataArray)
                    return
                }
                else
                {
                    // alert
                    UtilityClass.showAlertWithTitle(title: App_Name, message: message, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                    return
                }
            }
        }
    }
    
    //MARK:- Updating Model Array
    func updateModelArray(usingArray array : [[String:Any]]) -> Void
    {
        let dataArray = array
        
        for dict in dataArray // Iterating dictionaries
        {
            let model = ModelGroupListing () // Model creation
            model.updateModel(usingDictionary: dict) // Updating model
            isFavouriteScreen ? arrayGroupFavListing.append(model) : arrayGroupListing.append(model) // Adding model to array
            //            arrayListing.append(model) // Adding model to array
        }
        
        if isSearchModeOn
        {
            arrayGroupTempListing.removeAll()
            arrayGroupTempListing = arrayGroupListing.filter { (model) -> Bool in
                
                let isAvailable = model.groupName!.localizedCaseInsensitiveContains(searchedString)
                
                if !isAvailable
                {
                    if let tagArray = model.groupTags
                    {
                        let contains = tagArray.contains(where: { (tag) -> Bool in
                            return tag.localizedCaseInsensitiveContains(searchedString)
                        })
                        if contains
                        {
                            return true
                        }
                        else
                        {
                            return false
                        }
                    }
                    else
                    {
                        return false
                    }
                }
                return true
            }
        }
        
        tableGroupListing.reloadData()
    }
    
    
    //MARK:- BusinessGroupCellDelegate -> Favourite Api
    func performFavouriteGroupApi(forGroupID groupID:String, groupCell:BusinessGroupCell) -> Void
    {
        UtilityClass.showNetworkActivityLoader(show: true)
        
        let urlToHit = EndPoints.favouriteGroup(UtilityClass.getUserSidData()!, groupID).path
        
        AppWebHandler.sharedInstance().fetchData(fromURL: urlToHit, httpMethod: .get, parameters: nil)
        { [weak self] (data, dictionary, statusCode, error) in
            
            UtilityClass.showNetworkActivityLoader(show: false)
            
            guard let `self` = self else { return }
            
            groupCell.buttonLikeUnlike.isUserInteractionEnabled = false
            
            if error != nil
            {
                UtilityClass.showAlertWithTitle(title: App_Name, message: error?.localizedDescription, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                return
            }
            
            
            let responseDictionary = dictionary!
            let message = responseDictionary["message"] as! String
            let type = responseDictionary["type"] as! Bool
            
            if statusCode == 203
            {
                // show alert
                UtilityClass.showAlertWithTitle(title: App_Name, message: message, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                return
            }
            
            if statusCode == 200
            {
                if type
                {
                    // success
                    groupCell.buttonLikeUnlike.isUserInteractionEnabled = true
                    let index = groupCell.tag
                    
                    if self.isSearchModeOn
                    {
                        let businessGroupModel = arrayGroupTempListing[index]
                        businessGroupModel.isFavourite = true
                        businessGroupModel.peopleLiked = String (Int (businessGroupModel.peopleLiked!)!+1)
                        arrayGroupTempListing[index] = businessGroupModel
                        
                        if let newIndex = arrayGroupListing.index(where: { (model) -> Bool in
                            return model.groupID == businessGroupModel.groupID
                        })
                        {
                            arrayGroupListing[newIndex] = businessGroupModel
                            arrayGroupFavListing.append(businessGroupModel)
                        }
                    }
                    else
                    {
                        let businessGroupModel = arrayGroupListing[index]
                        businessGroupModel.isFavourite = true
                        businessGroupModel.peopleLiked = String (Int (businessGroupModel.peopleLiked!)!+1)
                        arrayGroupListing[index] = businessGroupModel
                        
                        arrayGroupFavListing.append(businessGroupModel)
                    }
                    
                    self.tableGroupListing.reloadRows(at: [IndexPath (row: index, section: 0)], with: .none)
                    return
                }
                else
                {
                    // alert
                    UtilityClass.showAlertWithTitle(title: App_Name, message: message, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                    return
                }
            }
            
        }
    }
    
    //MARK:- BusinessGroupCellDelegate -> UnFavourite Api
    func performUnfavouriteGroupApi(forGroupID groupID:String, groupCell:BusinessGroupCell) -> Void
    {
        UtilityClass.showNetworkActivityLoader(show: true)
        
        let urlToHit = EndPoints.unFavouriteGroup(UtilityClass.getUserSidData()!, groupID).path
        
        AppWebHandler.sharedInstance().fetchData(fromURL: urlToHit, httpMethod: .get, parameters: nil)
        { [weak self] (data, dictionary, statusCode, error) in
            
            UtilityClass.showNetworkActivityLoader(show: false)
            
            guard let `self` = self else { return }
            
            groupCell.buttonLikeUnlike.isUserInteractionEnabled = false
            
            if error != nil
            {
                UtilityClass.showAlertWithTitle(title: App_Name, message: error?.localizedDescription, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                return
            }
            
            
            let responseDictionary = dictionary!
            let message = responseDictionary["message"] as! String
            let type = responseDictionary["type"] as! Bool
            
            if statusCode == 203
            {
                // show alert
                UtilityClass.showAlertWithTitle(title: App_Name, message: message, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                return
            }
            
            if statusCode == 200
            {
                if type
                {
                    // success
                    groupCell.buttonLikeUnlike.isUserInteractionEnabled = true
                    let index = groupCell.tag
                    let businessGroupModel = self.isSearchModeOn ? arrayGroupTempListing[index] : self.isFavouriteScreen ? arrayGroupFavListing[index] : arrayGroupListing[index]
                    businessGroupModel.isFavourite = false
                    businessGroupModel.peopleLiked = String (Int (businessGroupModel.peopleLiked!)!-1)
                    if self.isFavouriteScreen
                    {
                        arrayGroupFavListing.remove(at: index)
                        if let newIndex = arrayGroupListing.index(where: { (model) -> Bool in
                            model.groupID == businessGroupModel.groupID
                        })
                        {
                            arrayGroupListing[newIndex] = businessGroupModel
                        }
                        
                        // Incase search mode was on
                        if arrayGroupTempListing.count > 0
                        {
                            let contains = arrayGroupTempListing.contains(where: { (model) -> Bool in
                                return model.groupID == businessGroupModel.groupID
                            })
                            if contains
                            {
                                if let newIndex = arrayGroupTempListing.index(where: { (model) -> Bool in
                                    model.groupID == businessGroupModel.groupID
                                })
                                {
                                    arrayGroupTempListing[newIndex] = businessGroupModel
                                }
                            }
                        }
                    }
                    else
                    {
                        
                        if self.isSearchModeOn
                        {
                            arrayGroupTempListing[index] = businessGroupModel
                            
                            if let newIndex = arrayGroupListing.index(where: { (model) -> Bool in
                                return model.groupID == businessGroupModel.groupID
                            })
                            {
                                arrayGroupListing[newIndex] = businessGroupModel
                                let contains = arrayGroupFavListing.contains(where: { (model) -> Bool in
                                    model.groupID == businessGroupModel.groupID
                                })
                                if contains
                                {
                                    if let newIndex = arrayGroupFavListing.index(where: { (model) -> Bool in
                                        model.groupID == businessGroupModel.groupID
                                    })
                                    {
                                        arrayGroupFavListing.remove(at: newIndex)
                                    }
                                }
                            }
                        }
                        else
                        {
                            arrayGroupListing[index] = businessGroupModel
                            let contains = arrayGroupFavListing.contains(where: { (model) -> Bool in
                                model.groupID == businessGroupModel.groupID
                            })
                            if contains
                            {
                                if let newIndex = arrayGroupFavListing.index(where: { (model) -> Bool in
                                    model.groupID == businessGroupModel.groupID
                                })
                                {
                                    arrayGroupFavListing.remove(at: newIndex)
                                }
                            }
                        }
                    }
                    
                    self.tableGroupListing.reloadData()
                    return
                }
                else
                {
                    // alert
                    UtilityClass.showAlertWithTitle(title: App_Name, message: message, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                    return
                }
            }
            
        }
    }
    
    //MARK:- BusinessGroupCellDelegate -> MakeMove Api
    func performMakeMoveApi(forGroupID groupID:String, groupCell:BusinessGroupCell, groupName: String) -> Void
    {
        AppDelegate.shared.showCheckinPopup(withDetails: [:], type: .makeMove, delegate: self)
        return;
        UtilityClass.showActionSheetWithTitle(title: "Make Moves", message: "How are you Making Moves?", onViewController: self.navigationController, withButtonArray: ["Solo", "Group"])
        {[weak self] (buttonIndex) in
            guard let `self` = self else {return}
            switch (buttonIndex)
            {
            case 0: // Personal make move
                self.makemoveOnGroup(isGroupMove: false, forGroupID: groupID, groupCell: groupCell)
                break
            case 1: // Group make move
                //                return
                //                self.makemoveOnGroup(isGroupMove: true, forGroupID: groupID, groupCell: groupCell)
                let myGroupListNav : UINavigationController = self.storyboard?.instantiateViewController(withIdentifier: "myGroupListNav") as! UINavigationController
                
                let mygroupListVC = myGroupListNav.viewControllers.first as! MyGroupListViewController
                
                mygroupListVC.groupDelegate = self
                mygroupListVC.isMakeMoveInvitation = true
                mygroupListVC.businessID = groupID
                mygroupListVC.businessName = groupID
                mygroupListVC.selectedIndex = groupCell.tag
                self.navigationController?.present(myGroupListNav, animated: true, completion: nil)
                
                
                break
            default:
                break
            }
        }
    }
    
    func makemoveOnGroup(isGroupMove : Bool,forGroupID groupID:String, groupCell:BusinessGroupCell)
    {
        groupCell.buttonMakeMove.isUserInteractionEnabled = false
        
        UtilityClass.showNetworkActivityLoader(show: true)
        
        let urlToHit = EndPoints.makeMoveToGroup(UtilityClass.getUserSidData()!, groupID).path
        
        AppWebHandler.sharedInstance().fetchData(fromURL: urlToHit, httpMethod: .get, parameters: nil)
        { [weak self] (data, dictionary, statusCode, error) in
            
            UtilityClass.showNetworkActivityLoader(show: false)
            
            guard let `self` = self else { return }
            
            groupCell.buttonMakeMove.isUserInteractionEnabled = true
            
            if error != nil
            {
                UtilityClass.showAlertWithTitle(title: App_Name, message: error?.localizedDescription, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                return
            }
            
            
            let responseDictionary = dictionary!
            let message = responseDictionary["message"] as! String
            let type = responseDictionary["type"] as! Bool
            
            if statusCode == 203
            {
                // show alert
                UtilityClass.showAlertWithTitle(title: App_Name, message: message, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                return
            }
            
            if statusCode == 200
            {
                if type
                {
                    // success
                    let index = groupCell.tag
                    let businessGroupModel = self.isSearchModeOn ? arrayGroupTempListing[index] : self.isFavouriteScreen ? arrayGroupFavListing[index] : arrayGroupListing[index]
                    businessGroupModel.isMakeMove = true
                    if self.isFavouriteScreen
                    {
                        arrayGroupFavListing[index] = businessGroupModel
                    }
                    else
                    {
                        arrayGroupListing[index] = businessGroupModel
                    }
                    self.tableGroupListing.reloadRows(at: [IndexPath (row: index, section: 0)], with: .none)
                    
                    //MakeMove Action
                    self.makeMoovActionSheet()
                    
                    return
                }
                else
                {
                    // alert
                    UtilityClass.showAlertWithTitle(title: App_Name, message: message, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                    return
                }
            }
            
        }
    }
    
    func cancelMakeMove(isGroupMove : Bool,forGroupID groupID:String, groupCell:BusinessGroupCell)
    {
        groupCell.buttonMakeMove.isUserInteractionEnabled = false
        
        UtilityClass.showNetworkActivityLoader(show: true)
        
        let urlToHit = EndPoints.cancelMakeMove(UtilityClass.getUserSidData()!, groupID).path
        
        AppWebHandler.sharedInstance().fetchData(fromURL: urlToHit, httpMethod: .get, parameters: nil)
        { [weak self] (data, dictionary, statusCode, error) in
            
            UtilityClass.showNetworkActivityLoader(show: false)
            
            guard let `self` = self else { return }
            
            groupCell.buttonMakeMove.isUserInteractionEnabled = true
            
            if error != nil
            {
                UtilityClass.showAlertWithTitle(title: App_Name, message: error?.localizedDescription, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                return
            }
            
            
            let responseDictionary = dictionary!
            let message = responseDictionary["message"] as! String
            let type = responseDictionary["type"] as! Bool
            
            if statusCode == 203
            {
                // show alert
                UtilityClass.showAlertWithTitle(title: App_Name, message: message, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                return
            }
            
            if statusCode == 200
            {
                if type
                {
                    // success
                    let index = groupCell.tag
                    let businessGroupModel = self.isSearchModeOn ? arrayGroupTempListing[index] : self.isFavouriteScreen ? arrayGroupFavListing[index] : arrayGroupListing[index]
                    businessGroupModel.isMakeMove = false
                    if self.isFavouriteScreen
                    {
                        arrayGroupFavListing[index] = businessGroupModel
                    }
                    else
                    {
                        arrayGroupListing[index] = businessGroupModel
                    }
                    self.tableGroupListing.reloadRows(at: [IndexPath (row: index, section: 0)], with: .none)
                    
                    
                    return
                }
                else
                {
                    // alert
                    UtilityClass.showAlertWithTitle(title: App_Name, message: message, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                    return
                }
            }
            
        }
    }
    
    //MARK:-  GroupDelate Action

    func makeMoveWithGroup(cellIndex: Int) {
        makeMoovActionSheet()
        self.tableGroupListing.reloadData()
        let businessGroupModel = self.isSearchModeOn ? arrayGroupTempListing[cellIndex] : self.isFavouriteScreen ? arrayGroupFavListing[cellIndex] : arrayGroupListing[cellIndex]

        businessGroupModel.isMakeMove = true

        self.tableGroupListing.reloadRows(at: [IndexPath (row: cellIndex, section: 0)], with: .none)
        
    }
    
    //MARK:-  MakeMove Action
    func makeMoovActionSheet() {
        let pickupLocation = CLLocation(latitude: LocationManager.sharedInstance().newLatitude, longitude: LocationManager.sharedInstance().newLongitude)
        let dropoffLocation = CLLocation(latitude: dropLatitude, longitude: dropLongitude)
        
        AppDelegate.shared.showCheckinPopup(withDetails: [:], type: .uberLyftPopup, delegate: self, pickupLocation: pickupLocation, dropoffLocation: dropoffLocation)

        return;
        
        let viewHasMovedToUber=UIApplication.shared.canOpenURL(URL(string: "Uber://test")!)
        let viewHasMovedToLyft=UIApplication.shared.canOpenURL(URL(string: "Lyft://test")!)
        
        let makeMoveArr: NSMutableArray = []
        
        
        if viewHasMovedToUber && viewHasMovedToLyft {
            makeMoveArr.addObjects(from: ["Uber" ,"Lyft"])
        }
        else if viewHasMovedToUber{
            makeMoveArr.addObjects(from: ["Uber"])
        }
        else if viewHasMovedToLyft{
            makeMoveArr.addObjects(from: ["Lyft"])
        }
        else{
            makeMoveArr.addObjects(from: ["Install Uber Or Lyft App"])
        }
        
        
        UtilityClass.showActionSheetWithTitle(title: "Choose Taxi", message: "Please select your move with", onViewController: self.navigationController, withButtonArray: makeMoveArr as? [String])
        {[weak self] (buttonIndex) in
            guard self != nil else {return}
            switch (buttonIndex)
            {
            case 0: // Uber make move
                let group = DispatchGroup()
                group.enter()
                let pickupLat = LocationManager.sharedInstance().newLatitude
                let pickupLong = LocationManager.sharedInstance().newLongitude
                self?.pickupAddressStr = (self?.getAddressFromLatlong(Latitude: pickupLat, Longitude: pickupLong))!
                 group.leave() // When your task completes
                self?.dropoffAddressStr = (self?.getAddressFromLatlong(Latitude: (self?.dropLatitude)!, Longitude: (self?.dropLongitude)!))!
                group.wait()
                group.notify(queue: DispatchQueue.main) {
                    self?.redirectOnUberApp()
                }
                break
            case 1: // Lyft make move
                self?.redirectOnLyftApp()
                break
            default:
                break
            }
        }
    }
    
    // Mark: UberSetup
    // Mark: Private Interface
    fileprivate func redirectOnUberApp() {
        
        let pickupLocation = CLLocation(latitude: LocationManager.sharedInstance().newLatitude, longitude: LocationManager.sharedInstance().newLongitude)
        let dropoffLocation = CLLocation(latitude: dropLatitude, longitude: dropLongitude)
        
        let builder = RideParametersBuilder()
        
        builder.pickupLocation = pickupLocation
        builder.dropoffLocation = dropoffLocation
        builder.pickupAddress = pickupAddressStr as String
        builder.dropoffAddress = dropoffAddressStr as String
        
        builder.productID = "cBDl51HAHDoiXKydFvkZq7C9EhXCUNGU"
        
        let rideParameters = builder.build()
        
        let deeplink = RequestDeeplink(rideParameters: rideParameters)
        deeplink.execute()

    }
    
    // Mark: LyftSetup
    fileprivate func redirectOnLyftApp() {
        
        let pickupLocation = CLLocationCoordinate2D(latitude: LocationManager.sharedInstance().newLatitude, longitude: LocationManager.sharedInstance().newLongitude)
        let dropoffLocation = CLLocationCoordinate2D(latitude: dropLatitude, longitude: dropLongitude)
        
        LyftDeepLink.requestRide(kind: .Standard, from: pickupLocation, to: dropoffLocation)
    }
    
    func getAddressFromLatlong(Latitude: Double, Longitude: Double) -> NSString {
        var addressString : String = ""
        
        let geoCoder = CLGeocoder()
        let location = CLLocation(latitude: CLLocationDegrees(Latitude), longitude:CLLocationDegrees(Longitude))
        
        geoCoder.reverseGeocodeLocation(location, completionHandler: { (placemarks, error) -> Void in
            
            // Place details
            var pm: CLPlacemark!
            pm = placemarks?[0]
            
            if pm.subLocality != nil {
                addressString = addressString + pm.subLocality! + ", "
            }
            if pm.thoroughfare != nil {
                addressString = addressString + pm.thoroughfare! + ", "
            }
            if pm.locality != nil {
                addressString = addressString + pm.locality! + ", "
            }
            if pm.country != nil {
                addressString = addressString + pm.country! + ", "
            }
        })
        return addressString as NSString
    }
}


//MARK:-
//MARK:- BusinessGroupListViewController - Extension
extension BusinessGroupListViewController : UITextFieldDelegate
{
    //MARK:- UITextFieldDelegate
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func textFieldShouldClear(_ textField: UITextField) -> Bool {
        isSearchModeOn = false
        arrayGroupTempListing.removeAll()
        tableGroupListing.reloadData()
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        var newString : String?
        var finalCount = 0
        
        if string.count == 0
        {
            if textField.text?.count==0
            {
                finalCount = 0
                newString = textField.text
            }
            else
            {
                finalCount = textField.text!.count - 1
                var newStr = textField.text! as NSString
                newStr = newStr.replacingCharacters(in: range, with: string) as NSString
                newString = newStr as String
            }
        }
        else
        {
            finalCount = textField.text!.count + 1
            var newStr = textField.text! as NSString
            
            newStr = newStr.replacingCharacters(in: range, with: string) as NSString
            
            newString = newStr as String
        }
        
        searchedString = newString!
        if finalCount == 0
        {
            isSearchModeOn = false
        }
        else
        {
            isSearchModeOn = true
            
            arrayGroupTempListing = arrayGroupListing.filter { (model) -> Bool in
                
                let isAvailable = model.groupName!.localizedCaseInsensitiveContains(newString!)
                
                if !isAvailable
                {
                    if let tagArray = model.groupTags
                    {
                        let contains = tagArray.contains(where: { (tag) -> Bool in
                            return tag.localizedCaseInsensitiveContains(newString!)
                        })
                        if contains
                        {
                            return true
                        }
                        else
                        {
                            return false
                        }
                    }
                    else
                    {
                        return false
                    }
                }
                return true
            }
        }
        
        self.tableGroupListing.reloadData()
        
        return true
    }
    
    //MARK:- LocationManager Location Update Notification Selector
    func locationManagerDidCalledForFirstTime() {
        modelFilter.latitude = String (LocationManager.sharedInstance().newLatitude)
        modelFilter.longitude = String (LocationManager.sharedInstance().newLongitude)
        getGroupsApi(onRefresh: true, shouldShowHUD: true)
    }
}


//MARK:-
//MARK:- Extension -> CheckinPopupViewControllerDelegate
extension BusinessGroupListViewController : CheckinPopupViewControllerDelegate
{
    func checkinPopUp(_ controller: CheckinPopupViewController, didTapOnSoloButton soloButton: UIButton) {
        controller.dismiss(animated: true, completion: nil)
        let businessGroupModel = isSearchModeOn ? arrayGroupTempListing[selectedIndex] : isFavouriteScreen ? arrayGroupFavListing[selectedIndex] : arrayGroupListing[selectedIndex]
        
        let cell = tableGroupListing.cellForRow(at: IndexPath (row: selectedIndex, section: 0)) as! BusinessGroupCell
        
        makemoveOnGroup(isGroupMove: false, forGroupID: businessGroupModel.groupID!, groupCell: cell)
    }
    
    func checkinPopUp(_ controller: CheckinPopupViewController, didTapOnGroupButton groupButton: UIButton) {
        controller.dismiss(animated: true, completion: nil)
        let businessGroupModel = isSearchModeOn ? arrayGroupTempListing[selectedIndex] : isFavouriteScreen ? arrayGroupFavListing[selectedIndex] : arrayGroupListing[selectedIndex]
        
        let myGroupListNav : UINavigationController = self.storyboard?.instantiateViewController(withIdentifier: "myGroupListNav") as! UINavigationController
        
        let mygroupListVC = myGroupListNav.viewControllers.first as! MyGroupListViewController
        
        mygroupListVC.groupDelegate = self
        mygroupListVC.isMakeMoveInvitation = true
        mygroupListVC.businessID = businessGroupModel.groupID!
        mygroupListVC.businessName = businessGroupModel.groupName ?? ""
        mygroupListVC.selectedIndex = selectedIndex
        self.navigationController?.present(myGroupListNav, animated: true, completion: nil)
    }
    
    func checkinPopUp(_ controller: CheckinPopupViewController, didTapOnLyftButton button: UIButton) {
        self.redirectOnLyftApp()
    }
    
    
    func checkinPopUp(_ controller: CheckinPopupViewController, didTapOnUberButton button: UIButton) {
        let group = DispatchGroup()
        group.enter()
        let pickupLat = LocationManager.sharedInstance().newLatitude
        let pickupLong = LocationManager.sharedInstance().newLongitude
        self.pickupAddressStr = (self.getAddressFromLatlong(Latitude: pickupLat, Longitude: pickupLong))
        group.leave() // When your task completes
        self.dropoffAddressStr = (self.getAddressFromLatlong(Latitude: (self.dropLatitude), Longitude: (self.dropLongitude)))
        group.wait()
        group.notify(queue: DispatchQueue.main) {[weak self] in
            self?.redirectOnUberApp()
        }
    }
}
