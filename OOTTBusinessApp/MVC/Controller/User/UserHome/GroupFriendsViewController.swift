//
//  GroupFriendsViewController.swift
//  OOTTBusinessApp
//
//  Created by Bharat Kumar Pathak on 12/07/17.
//  Copyright © 2017 KonstantInfoSolutions. All rights reserved.
//

import UIKit
import PKHUD
import DZNEmptyDataSet

class GroupFriendsViewController: UIViewController, UITableViewDelegate, UITableViewDataSource,DZNEmptyDataSetDelegate,DZNEmptyDataSetSource {

    @IBOutlet weak var tableGroupFriendsList: UITableView!
    
    var arrayListing = [ModelFriendsListing]()
    var businessModel: ModelGroupListing!
    var businessDetailModel : ModelGroupDetail!
    
    var isFromBusinessDetail = false
    
    var businessID : String!
    var businessFriendCount : String!
    
    
    var refreshControl: UIRefreshControl!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
//        if isFromBusinessDetail
//        {
//            businessID = businessDetailModel.groupID
//            businessFriendCount = businessDetailModel.friendsCount
//        }
//        else
//        {
//            businessID = businessModel.groupID
//            businessFriendCount = businessModel.friendsCount
//        }
        
        setupView()
        setupTableview()
//        getGroupFriendsApi(forGroupId: businessID, onRefresh: false)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        getGroupFriendsApi(forGroupId: businessID, onRefresh: true)
    }
    //MARK:- deinit
    deinit {
        print("GroupFriendsViewController deinit")
    }
    
    //MARK:- Initial Setup View
    
    func setupView() -> Void
    {
        let friendsCount = Int(UtilityClass.extractNumber(fromString: businessFriendCount))
        updateScreenTitle(usingFriendsCount: friendsCount!)
        
        let leftBarButton = self.setLeftBarButtonItem(withImage: #imageLiteral(resourceName: "backButton"))
        leftBarButton.action = #selector(onBackButtonAction)
        leftBarButton.target = self
        applyRefreshControl()
    }
    
    func setupTableview()
    {
        tableGroupFriendsList.delegate = self
        tableGroupFriendsList.dataSource = self
        tableGroupFriendsList.emptyDataSetSource = self
        tableGroupFriendsList.emptyDataSetDelegate = self
    }
    
    func updateScreenTitle(usingFriendsCount friendsCount:Int)
    {
//        let titleSuffix = (friendsCount == 1) ? "friend here" : "friends here"
        self.navigationItem.title = "friends here".uppercased()//(String(friendsCount)+" "+titleSuffix).uppercased()
    }
    
    func onBackButtonAction() {
        self.navigationController?.popViewController(animated: true)
    }
    
    // MARK:- Refresh Control
    func applyRefreshControl() {
        
        refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(self.refreshPage), for: UIControlEvents.valueChanged)
        self.tableGroupFriendsList.addSubview(refreshControl)
    }
    
    func refreshPage(sender:AnyObject) {
        getGroupFriendsApi(forGroupId: businessID, onRefresh: true)
    }
    
    // MARK:- UITableView Delegate and Datasource Methods
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrayListing.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 162 * scaleFactorX
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell: FollowingCell = tableView.dequeueReusableCell(withIdentifier: kCellIdentifier_following_cell) as! FollowingCell
        cell.tag = indexPath.row
        cell.updateData(usingModel: arrayListing[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        print("selected from cell at index \(indexPath.row)")
        
        navigateToProfile(forUser: arrayListing[indexPath.row])
    }
    
    //MARK:- DZNEmptyDataSetSource
    func title(forEmptyDataSet scrollView: UIScrollView!) -> NSAttributedString! {
        
        let text = "No Friends Here"
        
        let attributes = [NSFontAttributeName:UIFont .boldSystemFont(ofSize: 16*scaleFactorX), NSForegroundColorAttributeName:UIColor.white]
        
        return NSAttributedString (string: text, attributes: attributes)
    }
    
    func emptyDataSetShouldAllowScroll(_ scrollView: UIScrollView!) -> Bool {
        return true
    }
    
    // MARK:- Navigation Methods
    
    func navigateToProfile(forUser userModel:ModelFriendsListing) {
        if userModel.isSameUser
        {
            return
        }
        if userModel.isAccountDeleted {
            UtilityClass.showAlertWithTitle(title: "Profile Deactivated", message: "User has deactivated the profile.", onViewController: self, withButtonArray: nil, dismissHandler: nil)
            return
        }
        let profileViewController = UIStoryboard.getUserProfileStoryboard().instantiateViewController(withIdentifier: "userProfileViewController") as! UserProfileViewController
        profileViewController.targetUserID = userModel.friendID
        profileViewController.isLoggedInUser = false
        profileViewController.hidesBottomBarWhenPushed = true
        self.navigationController?.pushViewController(profileViewController, animated: true)
    }

    
    //MARK:- Get following(friends) Listing Api

    func getGroupFriendsApi(forGroupId groupID:String, onRefresh: Bool) -> Void
    {
        HUD.show(.systemActivity, onView: self.view.window)
        
        let urlToHit = EndPoints.getGroupFriendsList(UtilityClass.getUserSidData()!, groupID).path
        
        AppWebHandler.sharedInstance().fetchData(fromURL: urlToHit, httpMethod: .get, parameters: nil)
        {[weak self] (data, dictionary, statusCode, error) in
            
            HUD.hide()
            
            guard let `self` = self else { return }
            
            self.refreshControl.endRefreshing()
            
            if error != nil
            {
                UtilityClass.showAlertWithTitle(title: App_Name, message: error?.localizedDescription, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                return
            }
            
            
            let responseDictionary = dictionary!
            let message = responseDictionary["message"] as! String
            let type = responseDictionary["type"] as! Bool
            
            if statusCode == 203
            {
                // show alert
//                UtilityClass.showAlertWithTitle(title: App_Name, message: message, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                return
            }
            
            if statusCode == 200
            {
                if type
                {
                    // success
                    let dataDict = responseDictionary["data"] as! [String:Any]  // As dict
                    let modeArray = dataDict["moves"] as! [[String:Any]]
                    if onRefresh {
                        self.arrayListing.removeAll()
                    }
                    self.updateModelArray(usingArray: modeArray)
                    return
                }
                else
                {
                    // alert
                    UtilityClass.showAlertWithTitle(title: App_Name, message: message, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                    self.arrayListing.removeAll()
                    self.tableGroupFriendsList.reloadData()
                    return
                }
            }
        }
    }
    
    //MARK:- Updating Model Array
    func updateModelArray(usingArray array : [[String:Any]]) -> Void
    {
        let dataArray = array
        
        for dict in dataArray // Iterating dictionaries
        {
            let model = ModelFriendsListing () // Model creation
            model.updateModel(usingDictionary: dict) // Updating model
            arrayListing.append(model) // Adding model to array
        }
        updateScreenTitle(usingFriendsCount: arrayListing.count)
        tableGroupFriendsList.reloadData()
    }
}
