//
//  UserHomeViewController.swift
//  OOTTBusinessApp
//
//  Created by Bharat Kumar Pathak on 06/07/17.
//  Copyright © 2017 KonstantInfoSolutions. All rights reserved.
//

import UIKit
import MXSegmentedPager

class SearchBarView: UIView
{
    override var intrinsicContentSize: CGSize
        {
        return UILayoutFittingExpandedSize
    }
}

class UserHomeViewController: MXSegmentedPagerController, FilterValueDelegate {

    var segmentedViewControllers = [BusinessGroupListViewController]()
    var segmentTitles = ["Local", "Favorites"]
    var selectedPage: Int = 0
    
    @IBOutlet var searchFieldView: SearchBarView!
    @IBOutlet weak var searchTextField: UITextField!
    
    // MARK:-
    override func viewDidLoad() {
        
        super.viewDidLoad()
        setupNavigationView()
        initializeChildViewControllers()
        segmentedPager.initializeSegments()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    deinit {
        print("UserHomeViewController deinit")
    }
    
    // MARK:- Action Events
    
    @IBAction func onFilterAction(_ sender: UIBarButtonItem) {
        
        let filterViewController = self.storyboard?.instantiateViewController(withIdentifier: "businessGroupFilterViewController") as! BusinessGroupFilterViewController
        filterViewController.filterDelegate = self
        filterViewController.modelGroupFilter = (segmentedViewControllers.first?.modelFilter)!
        self.present(filterViewController, animated: true, completion: nil)
    }
    
    // MARK:- Custom Methods
    
    func setupNavigationView() {
    
        searchTextField.setPlaceholderColor(UIColor.init(RED: 184.0, GREEN: 187.0, BLUE: 190.0, ALPHA: 1.0))
        searchTextField.customize(withLeftIcon: #imageLiteral(resourceName: "searchIcon"))
        
        self.navigationItem.titleView = searchFieldView
    }
    
    func initializeChildViewControllers() {
        
        let localListingController : BusinessGroupListViewController = self.storyboard!.instantiateViewController(withIdentifier: "businessGroupListViewController") as! BusinessGroupListViewController
        
        localListingController.isFavouriteScreen = false
        searchTextField.delegate = localListingController as? UITextFieldDelegate
        
        segmentedViewControllers.append(localListingController)
        
        let favListingController : BusinessGroupListViewController = self.storyboard!.instantiateViewController(withIdentifier: "businessGroupListViewController") as! BusinessGroupListViewController
        
        favListingController.isFavouriteScreen = true
        
        segmentedViewControllers.append(favListingController)
    }
    
    // MARK:- MXSegmentedPager Delegate and Datasource Methods
    
    override func numberOfPages(in segmentedPager: MXSegmentedPager) -> Int {
        return segmentedViewControllers.count
    }
    
    override func segmentedPager(_ segmentedPager: MXSegmentedPager, titleForSectionAt index: Int) -> String {
        return segmentTitles[index]
    }
    
    override func segmentedPager(_ segmentedPager: MXSegmentedPager, viewControllerForPageAt index: Int) -> UIViewController {
        let viewController = segmentedViewControllers[index]
        self.addChildViewController(viewController)
        return viewController
    }
    
    override func segmentedPager(_ segmentedPager: MXSegmentedPager, didSelectViewWith index: Int) {
        
        if index == selectedPage {
            return
        }
        selectedPage = index
        segmentedViewControllers[index].viewAboutToBeSelected()
    }
    
    //MARK:-
    func filterUpdatedValue(withModel model: ModelGroupFilter) {
        for viewcontroller in segmentedViewControllers
        {
            viewcontroller.modelFilter = model
            viewcontroller.refreshPage(sender: UIButton ())
        }
    }
}

extension UIView
{
    
}

