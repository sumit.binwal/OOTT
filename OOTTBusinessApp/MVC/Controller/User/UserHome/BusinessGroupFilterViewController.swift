//
//  BusinessGroupFilterViewController.swift
//  OOTTBusinessApp
//
//  Created by Bharat Kumar Pathak on 07/07/17.
//  Copyright © 2017 KonstantInfoSolutions. All rights reserved.
//

import UIKit
import ActionSheetPicker_3_0
import GoogleMapsBase
import PKHUD

let kCellIdentifier_filter_location_cell = "filterLocationCell"
let kCellIdentifier_filter_picker_cell = "filterPickerCell"
let kCellIdentifier_filter_slider_cell = "filterSliderCell"
let kCellIdentifier_filter_radio_cell = "filterRadioCell"

//MARK:- FilterValueDelegate Method Declaration
protocol FilterValueDelegate: class
{
    func filterUpdatedValue(withModel model:ModelGroupFilter)
}


class BusinessGroupFilterViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, FilterGroupCellDelegate, UITextFieldDelegate {

    
    
    @IBOutlet weak var buttonCross: UIButton!
    
    @IBOutlet weak var tableFilterList: UITableView!
    
    @IBOutlet var footerTableview: UIView!
    
    weak var filterDelegate : FilterValueDelegate?
    
    //MARK: Array Holding Values
    var modelGroupFilter = ModelGroupFilter ()
    
    var isViewLoadedFirstTime = false
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //self.setupView()
        self.setupTableView()
        
        if modelGroupFilter.locationName != nil, modelGroupFilter.locationName!.isEmptyString() {
            HUD.show(.systemActivity, onView: self.view)
            UtilityClass.getCurrentAddress(currentAdd:
                {[weak self] (address, error) in
                    HUD.hide()
                    guard let `self` = self else {return}
                    self.modelGroupFilter.locationName = address
                    self.modelGroupFilter.latitude = String(LocationManager.sharedInstance().newLatitude)
                    self.modelGroupFilter.longitude = String(LocationManager.sharedInstance().newLongitude)
                    self.tableFilterList.reloadData()
            })
        }
        // Do any additional setup after loading the view.
    }
    
    deinit {
        print("BusinessGroupFilterViewController deinit")
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        isViewLoadedFirstTime = true
    }
    

    func setupTableView()
    {
        tableFilterList.delegate = self
        tableFilterList.dataSource = self
        
        footerTableview.frame = CGRect (x: 0, y: 0, width: self.view.frame.size.width, height: footerTableview.frame.size.height * scaleFactorX)
        tableFilterList.tableFooterView = footerTableview
    }
    
    @IBAction func onBackAction(_ sender: UIButton) {
        self.view.endEditing(true)
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func onApplyAction(_ sender: UIButton)
    {
        self.view.endEditing(true)
        
//        if modelGroupFilter.sortType == .distance || modelGroupFilter.sortType == .lineWaitTime
//        {
//            if modelGroupFilter.sliderValue == "0"
//            {
//                UtilityClass.showAlertWithTitle(title: App_Name, message: "Please select filter first", onViewController: self, withButtonArray: nil, dismissHandler: nil)
//                return
//            }
//        }
        
        guard let delegate = filterDelegate else {
            return
        }
        delegate.filterUpdatedValue(withModel: modelGroupFilter)
        self.dismiss(animated: true, completion: nil)
    }
    

    
    //MARK:- TextView Delegate Method
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        modelGroupFilter.scene = textField.text!
    }
    
//    func textViewDidEndEditing(_ textView: UITextView) {
//        
//        modelGroupFilter.scene = textView.text!
//    }

    //MARK:- TableView Delegate and Datasource Methods

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 7
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row==0 || indexPath.row==1 {
            return 87*scaleFactorX
        }
        else
        {
            return 70*scaleFactorX
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        var cell : FilterGroupCell
        
        var cellIdentifier = kCellIdentifier_filter_picker_cell
        
        var newCellType = FilterCellType.none
        
        switch indexPath.row
        {
        case 0:
            cellIdentifier = kCellIdentifier_filter_location_cell
            newCellType = .location;
            
        case 1:
            cellIdentifier = kCellIdentifier_filter_location_cell
            newCellType = .scene;
            
            
        case 2:
            cellIdentifier = kCellIdentifier_filter_slider_cell
            newCellType = .distance;
            
        case 3:
            cellIdentifier = kCellIdentifier_filter_picker_cell
            newCellType = .lineWaitTime;
            
        case 4:
            cellIdentifier = kCellIdentifier_filter_picker_cell
            newCellType = .numberOfFriends;
            
        case 5:
            cellIdentifier = kCellIdentifier_filter_picker_cell
            newCellType = .numberOfPeople;
        case 6:
            cellIdentifier = kCellIdentifier_filter_radio_cell
            newCellType = .closedBar;
            
        default:
            break
        }
        
        cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier) as! FilterGroupCell
        cell.cellType = newCellType
       
        if newCellType == FilterCellType.scene {
        cell.inputField.delegate = self
        }
        
        cell.filterDelegate = self
        
        cell.updateCell(usingModel: modelGroupFilter, forIndex: indexPath.row)
        
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
        var isSameFilter = false
        
        var sortType = GroupFilterName.distance
        
        
        switch indexPath.row {
//        case 2:
//            if modelGroupFilter.sortType == .distance
//            {
//                isSameFilter = true
//            }
//            sortType = .distance
        case 3:
            if modelGroupFilter.sortType == .lineWaitTime
            {
                isSameFilter = true
            }
            sortType = .lineWaitTime
        case 4:
            if modelGroupFilter.sortType == .numberOfFriends
            {
                isSameFilter = true
            }
            sortType = .numberOfFriends
        case 5:
            if modelGroupFilter.sortType == .numberOfPeople
            {
                isSameFilter = true
            }
            sortType = .numberOfPeople
        default:
            return
        }

        var initialSelection = 0
        
        if modelGroupFilter.sort == .highToLow, isSameFilter
        {
            initialSelection = 1
        }
        
        
        ActionSheetStringPicker.show(withTitle: "", rows: ["Low to High","High to Low"], initialSelection: initialSelection, doneBlock:
            {[weak self] (picker, index, value) in
                
                guard let `self` = self else {return}
                
                self.modelGroupFilter.sortType = sortType
                
            switch index {
            case 0:
                self.modelGroupFilter.sort = .lowToHigh
                self.modelGroupFilter.priorityTypeValue = value as? String
            case 1:
                self.modelGroupFilter.sort = .highToLow
                self.modelGroupFilter.priorityTypeValue = value as? String
            case 2:
                self.modelGroupFilter.sort = .random
                self.modelGroupFilter.priorityTypeValue = value as? String
            
            default:
                break
                
            }
                tableView.reloadData()
        }, cancel: { (picker) in
            
        }, origin: tableView)
    }
    
    //MARK:- FilterGroupCell Delegate -> Textfield updated Action
    
    func filterGroupCell(_ cell: FilterGroupCell, didTapOnRadioButton value: Bool)
    {
        if self.modelGroupFilter.closedBar
        {
            cell.buttonRadio.setImage( #imageLiteral(resourceName: "radioDeselected"), for: .normal)
            self.modelGroupFilter.closedBar = false
        }
        else
        {
            cell.buttonRadio.setImage(#imageLiteral(resourceName: "radioSelect"), for: .normal)
            self.modelGroupFilter.closedBar = true
        }
    }
    
    func filterGroupCell(cell: FilterGroupCell, updatedInputfieldText: Any, latitude: String, longitude: String) {
        self.modelGroupFilter.locationName = updatedInputfieldText as? String
        self.modelGroupFilter.latitude = latitude
        self.modelGroupFilter.longitude = longitude
    }
    
    func filterGroupCell(_ cell: FilterGroupCell, didUpdatedSliderValue sliderValue: String) {
        if isViewLoadedFirstTime
        {
            switch cell.cellType
            {
            case .distance:
                modelGroupFilter.sortType = .distance
                modelGroupFilter.sliderValue =  sliderValue
            default:
                modelGroupFilter.sortType = .lineWaitTime
                modelGroupFilter.sliderValue = sliderValue
            }
            tableFilterList.reloadData()
        }
    }
}
