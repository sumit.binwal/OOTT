//
//  BusinessDetailsViewController.swift
//  OOTTBusinessApp
//
//  Created by Bharat Kumar Pathak on 14/07/17.
//  Copyright © 2017 KonstantInfoSolutions. All rights reserved.
//

import UIKit
import PKHUD

import CoreLocation
import UberCore
import UberRides
import LyftSDK



//MARK:-
//MARK:- BusinessDetailsViewController
class BusinessDetailsViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, BusinessNameStatusTagsCellDelegate, BusinessFriendsLikesCellDelegate, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout,MyGroupDelegate {
    

    
    

    @IBOutlet weak var tableBusinessDetails: UITableView!
    @IBOutlet var footerView: UIView!
    @IBOutlet weak var makeMoveButton: UIButton!
    
    var isDataReceivedFromServer = false
    
    
    var businessId: String!
    let arrayMenuItems = [[String:Any]]()
    var dictionaryBusinessGroup = ModelGroupDetail()
    
    var shouldReloadMap = true
    var isFromDetail = false
    
    var dropLatitude = Double()
    var dropLongitude = Double()
    
    var pickupAddressStr = NSString()
    var dropoffAddressStr = NSString()
    
    
    
    //MARK:- viewDidLoad
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
//        getBusinessGroupDetailApi(forBusinessId: businessId)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if UtilityClass.getUserTypeData() == AppUserTypeEnum.provider.rawValue, !isFromDetail {
            getBusinessGroupDetailApi(forBusinessId: UtilityClass.getUserIDData()!)
        }
        else
        {
            getBusinessGroupDetailApi(forBusinessId: businessId)
        }
    }

    //MARK:- didReceiveMemoryWarning
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    //MARK:- deinit
    deinit {
        print("BusinessDetailsViewController deinit")
    }
    
    //MARK:- setupView
    func setupView() {
        
        tableBusinessDetails.isHidden = true
        
        self.tableBusinessDetails.tableFooterView = self.footerView
        self.tableBusinessDetails.delegate = self
        self.tableBusinessDetails.dataSource = self
        
        if UtilityClass.getUserTypeData() == AppUserTypeEnum.provider.rawValue, !isFromDetail {
                    let rightBarButton = self.setRightBarButtonItem(withImage: #imageLiteral(resourceName: "settingIcon"))
                    rightBarButton.action = #selector(onSettingsButtonAction)
                    rightBarButton.target = self
                    rightBarButton.tintColor = UIColor.white
            
            self.navigationItem.title = "My Profile"
            
            self.showNavigationBar()
            
            self.tableBusinessDetails.tableFooterView = nil
        }
        else
        {
            let backBarButton = self.setLeftBarButtonItem(withImage: #imageLiteral(resourceName: "backButtonWhite"))
            backBarButton.action = #selector(onBackButtonAction)
            backBarButton.target = self
            self.navigationItem.title = "Business Profile"
            
            self.showNavigationBar()
            
            if UtilityClass.getUserTypeData() == AppUserTypeEnum.employee.rawValue {
                
                
                self.tableBusinessDetails.tableFooterView = nil
            }
        }
    }
    
    //MARK:- onBackButtonAction
    func onBackButtonAction() {
        self.navigationController?.popViewController(animated: true)
    }
    
    //MARK:- Settings button action
    func onSettingsButtonAction() -> Void {
        //        businessSettingsVC
        let settingsVC = UIStoryboard.getBusinessHomeFlowStoryboard().instantiateViewController(withIdentifier: "businessSettingsVC") as! BusinessSettingViewController
        settingsVC.hidesBottomBarWhenPushed = true
        self.navigationController?.pushViewController(settingsVC, animated: true)
    }
    
    // MARK:- UITableView Delegate and Datasoure Methods
    func numberOfSections(in tableView: UITableView) -> Int {
        return isDataReceivedFromServer ? 4 : 0
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if section == 0 {
            return 4
        } else if section == 1 {
            return 7
        } else if section == 2 {
            return dictionaryBusinessGroup.groupMenuImages.count
        } else {
            return 2
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
//        if indexPath.section == 0, indexPath.row == 1 {
//            return 150
//        }
        if indexPath.section == 3
        {
            if dictionaryBusinessGroup.groupBusinessImages.count==0 , indexPath.row == 0
            {
                return 0 
            }
        }
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100 * scaleFactorY
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        var cellIdentifier = ""
        
        if indexPath.section == 0 {
            
            if indexPath.row == 0 {
                cellIdentifier = kCellIdentifier_business_cover_image_cell
                let cell:BusinessCoverImageCell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier) as! BusinessCoverImageCell
                cell.updateData(usingModel: dictionaryBusinessGroup)
                return cell
                
            } else if indexPath.row == 1 {
                cellIdentifier = kCellIdentifier_business_name_status_tags_cell
                let cell: BusinessNameStatusTagsCell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier) as! BusinessNameStatusTagsCell
                cell.delegate = self
                cell.updateData(usingModel: dictionaryBusinessGroup)
                return cell
            } else if indexPath.row == 2 {
                cellIdentifier = UtilityClass.getUserTypeData() == AppUserTypeEnum.user.rawValue ?kCellIdentifier_business_friend_likes_cell : kCellIdentifier_business_likes_cell
                let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier) as! BusinessFriendsLikesCell
                cell.delegate = self
                cell.updateCell(usingModel: dictionaryBusinessGroup)
                return cell
            } else {
                cellIdentifier = kCellIdentifier_BusinessDetail_InfoCell
                let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier) as! BusinessDetailInfoCell
                cell.updateCell(usingModel: dictionaryBusinessGroup)
                return cell
            }
        
        } else if indexPath.section == 1 {
            
            cellIdentifier = kCellIdentifier_business_option_detail_cell
            let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier) as! BusinessOptionDetailCell
            cell.updateCell(usingModel: dictionaryBusinessGroup, forIndex: indexPath.row)
            return cell
        
        } else if indexPath.section == 2 {
        
            cellIdentifier = kCellIdentifier_business_menu_cell
            let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier) as! BusinessMenuCell
            cell.updateCell(usingModel: dictionaryBusinessGroup.groupMenuImages[indexPath.row])
            return cell
        
        } else {
        
            if indexPath.row == 0 {
                cellIdentifier = kCellIdentifier_frequent_visited_cell
                let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier) as! FrequentVisitedCell
                cell.updateCell(usingModel: dictionaryBusinessGroup)
                cell.frequentVisitsCollectionView.delegate = self
                cell.frequentVisitsCollectionView.dataSource = self
                return cell
            } else {
                cellIdentifier = kCellIdentifier_business_map_cell
                let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier) as! BusinessMapCell
                cell.updateCell(usingModel: dictionaryBusinessGroup, shouldReloadMap: shouldReloadMap)
                shouldReloadMap = false
                return cell
            }
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        switch indexPath.section
        {
        case 0:
            break
            
        case 1:
            if indexPath.row == 0
            {
                UtilityClass.openSafariController(usingLink: .other(dictionaryBusinessGroup.groupWebsite), onViewController: self.navigationController)
                return
            }
            if indexPath.row == 1
            {
                if dictionaryBusinessGroup.groupOpenTill != nil
                {
                    let hoursView : HoursViewController = UIStoryboard.getHourSelectionViewStoryboard().instantiateInitialViewController() as! HoursViewController
                    hoursView.hidesBottomBarWhenPushed = true
                    
                    hoursView.dictHours = dictionaryBusinessGroup.groupOpenTill!
                    
                    hoursView.isFromDetailPage = true
                    
                    self.navigationController?.pushViewController(hoursView, animated: true)
                }
                return
            }
            if indexPath.row == 2
            {
                if dictionaryBusinessGroup.groupCharges != nil
                {
                    let coverChargeView : CoverChargeViewController = UIStoryboard.getHourSelectionViewStoryboard().instantiateViewController(withIdentifier: "coverChargeVC") as! CoverChargeViewController
                    coverChargeView.hidesBottomBarWhenPushed = true
                    
                    coverChargeView.dictCoverCharge = dictionaryBusinessGroup.groupCharges!
                    
                    coverChargeView.isFromDetailPage = true
                    
                    self.navigationController?.pushViewController(coverChargeView, animated: true)
                }
                return
            }
            if indexPath.row == 6
            {
                let number = dictionaryBusinessGroup.groupCallingNumber
                UtilityClass.openCallScreen(withNumber: number)
            }
            break
            
        case 2: // Menu images
            
            let menuVC : FoodMenuViewController = self.storyboard?.instantiateViewController(withIdentifier: "foodMenuVC") as! FoodMenuViewController
            
            menuVC.foodMenuUrlString = dictionaryBusinessGroup.groupMenuImages[indexPath.row].imageURLString!
            menuVC.titleName = dictionaryBusinessGroup.groupMenuImages[indexPath.row].imageCaption
            
            self.navigationController?.pushViewController(menuVC, animated: true)
            
            break
            
        case 3: // Business and map
            
            if indexPath.row == 0
            {
                break
            }
            UtilityClass.showAlertWithTitle(title: App_Name, message: "Open in maps application?", onViewController: self, withButtonArray: ["Open"], dismissHandler:
                {[weak self] (buttonIndex) in
                    guard let `self` = self else {return}
                    if buttonIndex == 0
                    {
                        // open maps
                        UtilityClass.openGMapsApplication(Double (self.dictionaryBusinessGroup.groupLatitude)!, longitude: Double (self.dictionaryBusinessGroup.groupLongitude)!, displayName: self.dictionaryBusinessGroup.groupName, phoneNumber: self.dictionaryBusinessGroup.groupCallingNumber)
                    }
            })
            
            break
            
        default:
            break
        }
    }
    
    //MARK:- UICollectionView Delegate and Datasource
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        let businessImagesCount = dictionaryBusinessGroup.groupBusinessImages.count
        return businessImagesCount >= 5 ? 4 : businessImagesCount
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 10 * scaleFactorX
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 90*scaleFactorX, height: 90*scaleFactorX)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView .dequeueReusableCell(withReuseIdentifier: kCellIdentifier_frequent_collection_cell, for: indexPath) as! FrequentCollectionCell
        
        cell.contentView.frame = cell.bounds
        cell.contentView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        
        cell.tag = indexPath.item
        cell.updateLayouts(withIncrementCount: 1)
        
        cell.updateCell(usingModel: dictionaryBusinessGroup)
        
        return cell
    }
    
//    func collectionView(_ collectionView: UICollectionView, prefetchItemsAt indexPaths: [IndexPath]) {
//        for indexpath in indexPaths
//        {
//            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: kCellIdentifier_frequent_collection_cell, for: indexpath) as! FrequentCollectionCell
//
//            cell.tag = indexpath.item
//            cell.updateLayouts()
//
//            cell.updateCell(usingModel: dictionaryBusinessGroup)
//        }
//    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        collectionView.deselectItem(at: indexPath, animated: true)
        
        let galleryVC : GalleryViewController = UIStoryboard.getGalleryViewStoryboard().instantiateInitialViewController() as! GalleryViewController
        
        galleryVC.selectedBarName = dictionaryBusinessGroup.groupName
        galleryVC.selectedIndex = indexPath.item < 3 ? indexPath.item : 0
        galleryVC.updateModelArray(usingArray: dictionaryBusinessGroup.groupBusinessImages)
        
        self.navigationController?.pushViewController(galleryVC, animated: true)
    }
    
    //MARK:- Make Move Action
    @IBAction func onMakeMoveButtonAction(_ sender: UIButton) {
        
        if dictionaryBusinessGroup.isMakeMove
        {
            
            cancelMakeMove(forGroupID: dictionaryBusinessGroup.groupID)
        }
        else
        
        {
            
            dropLatitude = (self.dictionaryBusinessGroup.groupLatitude as NSString).doubleValue
            dropLongitude = (self.dictionaryBusinessGroup.groupLongitude as NSString).doubleValue
            
            AppDelegate.shared.showCheckinPopup(withDetails: [:], type: .makeMove, delegate: self)
            
            return;
            
            UtilityClass.showActionSheetWithTitle(title: "Make Moves", message: "How are you Making Moves?", onViewController: self.navigationController, withButtonArray: ["Solo", "Group"])
            {[weak self] (buttonIndex) in
                guard let `self` = self else {return}
                switch (buttonIndex)
                {
                case 0: // Personal make move
                    self.performMakeMove(sender : sender)
                    break
                case 1: // Group make move
                    
                    let myGroupListNav : UINavigationController = UIStoryboard.getUserHomeStoryboard().instantiateViewController(withIdentifier: "myGroupListNav") as! UINavigationController
                    
                    let mygroupListVC = myGroupListNav.viewControllers.first as! MyGroupListViewController
                    
                    mygroupListVC.groupDelegate = self
                    mygroupListVC.isMakeMoveInvitation = true
                    mygroupListVC.businessID = self.dictionaryBusinessGroup.groupID
                    
                    self.navigationController?.present(myGroupListNav, animated: true, completion: nil)
                    
                    break
                default:
                    break
                }
            }
        }
        
        
    }
    
    
    func cancelMakeMove(forGroupID groupID:String)
    {
        self.makeMoveButton.isUserInteractionEnabled = false
        
        UtilityClass.showNetworkActivityLoader(show: true)
        
        let urlToHit = EndPoints.cancelMakeMove(UtilityClass.getUserSidData()!, groupID).path
        
        AppWebHandler.sharedInstance().fetchData(fromURL: urlToHit, httpMethod: .get, parameters: nil)
        { [weak self] (data, dictionary, statusCode, error) in
            
            UtilityClass.showNetworkActivityLoader(show: false)
            
            guard let `self` = self else { return }
            
            self.makeMoveButton.isUserInteractionEnabled = true
            
            if error != nil
            {
                UtilityClass.showAlertWithTitle(title: App_Name, message: error?.localizedDescription, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                return
            }
            
            
            let responseDictionary = dictionary!
            let message = responseDictionary["message"] as! String
            let type = responseDictionary["type"] as! Bool
            
            if statusCode == 203
            {
                // show alert
                UtilityClass.showAlertWithTitle(title: App_Name, message: message, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                return
            }
            
            if statusCode == 200
            {
                if type
                {
                    // success
                    self.dictionaryBusinessGroup.isMakeMove = false
                    self.makeMoveButton.alpha = self.dictionaryBusinessGroup.isMakeMove ? 0.7 : 1

                    if self.dictionaryBusinessGroup.isMakeMove
                    {
                        self.makeMoveButton.setTitle("CANCEL", for: .normal)
                        
                    }
                    else
                        
                    {
                        self.makeMoveButton.setTitle("MAKE MOVES", for: .normal)
                    }
                    
                    return
                }
                else
                {
                    // alert
                    UtilityClass.showAlertWithTitle(title: App_Name, message: message, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                    return
                }
            }
            
        }
    }
    
    func performMakeMove(sender : UIButton)
    {
        sender.isUserInteractionEnabled = false
        
        UtilityClass.showNetworkActivityLoader(show: true)
        
        let urlToHit = EndPoints.makeMoveToGroup(UtilityClass.getUserSidData()!, dictionaryBusinessGroup.groupID).path
        
        AppWebHandler.sharedInstance().fetchData(fromURL: urlToHit, httpMethod: .get, parameters: nil)
        { [weak self] (data, dictionary, statusCode, error) in
            
            UtilityClass.showNetworkActivityLoader(show: false)
            
            guard let `self` = self else { return }
            
            sender.isUserInteractionEnabled = true
            
            if error != nil
            {
                UtilityClass.showAlertWithTitle(title: App_Name, message: error?.localizedDescription, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                return
            }
            
            sender.isUserInteractionEnabled = true
            let responseDictionary = dictionary!
            let message = responseDictionary["message"] as! String
            let type = responseDictionary["type"] as! Bool
            
            if statusCode == 203
            {
                // show alert
                UtilityClass.showAlertWithTitle(title: App_Name, message: message, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                return
            }
            
            if statusCode == 200
            {
                if type
                {
                    // success
                    
                    self.dictionaryBusinessGroup.isMakeMove = true
//                    self.makeMoveButton.isEnabled = false
//                    self.makeMoveButton.alpha = 0.5
                    if let newIndex = arrayGroupListing.index(where: { (model) -> Bool in
                        return model.groupID == self.dictionaryBusinessGroup.groupID
                    })
                    {
                        arrayGroupListing[newIndex].isMakeMove = true
                    }
                    
                    let contains = arrayGroupFavListing.contains(where: { (model) -> Bool in
                        return model.groupID == self.dictionaryBusinessGroup.groupID
                    })
                    
                    if contains
                    {
                        if let newIndex = arrayGroupFavListing.index(where: { (model) -> Bool in
                            return model.groupID == self.dictionaryBusinessGroup.groupID
                        })
                        {
                            arrayGroupFavListing[newIndex].isMakeMove = true
                        }
                    }
                    self.dictionaryBusinessGroup.isMakeMove = true
                    self.makeMoveButton.alpha = self.dictionaryBusinessGroup.isMakeMove ? 0.7 : 1
                    
                    if self.dictionaryBusinessGroup.isMakeMove
                    {
                        self.makeMoveButton.setTitle("CANCEL", for: .normal)
                        
                    }
                    else
                    {
                        self.makeMoveButton.setTitle("MAKE MOVES", for: .normal)
                    }
                    //MakeMove Action
                    self.makeMoovActionSheet()
                    return
                }
                else
                {
                    // alert
                    UtilityClass.showAlertWithTitle(title: App_Name, message: message, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                    return
                }
            }
        }
    }
    
    //MARK:-  GroupDelate Action
    
    func makeMoveWithGroup(cellIndex: Int) {
        self.dictionaryBusinessGroup.isMakeMove = true
        self.makeMoveButton.alpha = self.dictionaryBusinessGroup.isMakeMove ? 0.7 : 1
        
        if self.dictionaryBusinessGroup.isMakeMove
        {
            self.makeMoveButton.setTitle("CANCEL", for: .normal)
            
        }
        else
            
        {
            self.makeMoveButton.setTitle("MAKE MOVES", for: .normal)
        }
        makeMoovActionSheet()
    }
    
    //MARK:-  MakeMove Action
    func makeMoovActionSheet() {
        
        AppDelegate.shared.showCheckinPopup(withDetails: [:], type: .uberLyftPopup, delegate: self)
        return;
        
        let viewHasMovedToUber=UIApplication.shared.canOpenURL(URL(string: "Uber://test")!)
        let viewHasMovedToLyft=UIApplication.shared.canOpenURL(URL(string: "Lyft://test")!)
        
        let makeMoveArr: NSMutableArray = []
        
        if viewHasMovedToUber && viewHasMovedToLyft {
            makeMoveArr.addObjects(from: ["Uber" ,"Lyft"])
        }
        else if viewHasMovedToUber{
            makeMoveArr.addObjects(from: ["Uber"])
        }
        else if viewHasMovedToLyft{
            makeMoveArr.addObjects(from: ["Lyft"])
        }
        else{
            makeMoveArr.addObjects(from: ["Install Uber Or Lyft App"])
        }
        
        
        UtilityClass.showActionSheetWithTitle(title: "Choose Taxi", message: "Please select your move with", onViewController: self.navigationController, withButtonArray: makeMoveArr as? [String])
        {[weak self] (buttonIndex) in
            guard self != nil else {return}
            switch (buttonIndex)
            {
            case 0: // Uber make move
                let group = DispatchGroup()
                group.enter()
                let pickupLat = LocationManager.sharedInstance().newLatitude
                let pickupLong = LocationManager.sharedInstance().newLongitude
                self?.pickupAddressStr = (self?.getAddressFromLatlong(Latitude: pickupLat, Longitude: pickupLong))!
                group.leave() // When your task completes
                self?.dropoffAddressStr = (self?.getAddressFromLatlong(Latitude: (self?.dropLatitude)!, Longitude: (self?.dropLongitude)!))!
                group.wait()
                group.notify(queue: DispatchQueue.main) {
                    self?.redirectOnUberApp()
                }
                break
            case 1: // Lyft make move
                self?.redirectOnLyftApp()
                break
            default:
                break
            }
        }
    }
    
    // Mark: UberSetup
    // Mark: Private Interface
    fileprivate func redirectOnUberApp() {
        
        let pickupLocation = CLLocation(latitude: LocationManager.sharedInstance().newLatitude, longitude: LocationManager.sharedInstance().newLongitude)
        let dropoffLocation = CLLocation(latitude: dropLatitude, longitude: dropLongitude)
        
        let builder = RideParametersBuilder()
        
        builder.pickupLocation = pickupLocation
        builder.dropoffLocation = dropoffLocation
        builder.pickupAddress = pickupAddressStr as String
        builder.dropoffAddress = dropoffAddressStr as String
        
        builder.productID = "a1111c8c-c720-46c3-8534-2fcdd730040d"
        
        let rideParameters = builder.build()
        
        let deeplink = RequestDeeplink(rideParameters: rideParameters)
        deeplink.execute()
        
    }
    
    // Mark: LyftSetup
    fileprivate func redirectOnLyftApp() {
        
        let pickupLocation = CLLocationCoordinate2D(latitude: LocationManager.sharedInstance().newLatitude, longitude: LocationManager.sharedInstance().newLongitude)
        let dropoffLocation = CLLocationCoordinate2D(latitude: dropLatitude, longitude: dropLongitude)
        
        LyftDeepLink.requestRide(kind: .Standard, from: pickupLocation, to: dropoffLocation)
    }
    
    func getAddressFromLatlong(Latitude: Double, Longitude: Double) -> NSString {
        var addressString : String = ""
        
        let geoCoder = CLGeocoder()
        let location = CLLocation(latitude: CLLocationDegrees(Latitude), longitude:CLLocationDegrees(Longitude))
        
        geoCoder.reverseGeocodeLocation(location, completionHandler: { (placemarks, error) -> Void in
            
            // Place details
            var pm: CLPlacemark!
            pm = placemarks?[0]
            
            if pm.subLocality != nil {
                addressString = addressString + pm.subLocality! + ", "
            }
            if pm.thoroughfare != nil {
                addressString = addressString + pm.thoroughfare! + ", "
            }
            if pm.locality != nil {
                addressString = addressString + pm.locality! + ", "
            }
            if pm.country != nil {
                addressString = addressString + pm.country! + ", "
            }
        })
        return addressString as NSString
    }
    
    // MARK:- Get Business Group Details Api
    func getBusinessGroupDetailApi(forBusinessId businessID:String) -> Void
    {
        HUD.show(.systemActivity, onView: self.view.window)
        
        let urlToHit = EndPoints.viewBusiness(UtilityClass.getUserSidData()!, businessID, String(LocationManager.sharedInstance().newLatitude), String(LocationManager.sharedInstance().newLongitude)).path
        
        AppWebHandler.sharedInstance().fetchData(fromURL: urlToHit, httpMethod: .get, parameters: nil)
        {[weak self] (data, dictionary, statusCode, error) in
            
            HUD.hide()
            //            self.refreshControl.endRefreshing()
            
            guard let `self` = self else {return}
            
            self.isDataReceivedFromServer = false
            
            if error != nil
            {
                UtilityClass.showAlertWithTitle(title: App_Name, message: error?.localizedDescription, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                return
            }
            
            
            let responseDictionary = dictionary!
            let message = responseDictionary["message"] as! String
            let type = responseDictionary["type"] as! Bool
            
            print(responseDictionary)
            if statusCode == 203
            {
                // show alert
                UtilityClass.showAlertWithTitle(title: App_Name, message: message, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                return
            }
            
            if statusCode == 200
            {
                if type
                {
                    // success
                    
                    self.isDataReceivedFromServer = true
                    
                    let dataDictionary = responseDictionary["data"] as! [String:Any] // As dictionary
                    //                    if onRefresh {
                    //                        self.arrayListing.removeAll()
                    //                    }
                    
                    let verified = dataDictionary["verified"] as! Bool
//                    if (verified)
//                    {
//                        
//                        
//                        if UtilityClass.getUserTypeData() == AppUserTypeEnum.provider.rawValue, !self.isFromDetail {
//                            let rightBarButton = self.setRightBarButtonItem(withImage: #imageLiteral(resourceName: "settingIcon"))
//                            rightBarButton.action = #selector(self.onSettingsButtonAction)
//                            rightBarButton.target = self
//                            rightBarButton.tintColor = UIColor.white
//                            
//                            self.navigationItem.title = "My Profile"
//                            
//                            self.showNavigationBar()
//                            
//                            self.tableBusinessDetails.tableFooterView = nil
//                            
//                            let logo = UIBarButtonItem(image: UIImage (named: "verifiedBusiness"), style: UIBarButtonItemStyle.plain, target: self, action: nil)
//                            logo.isEnabled = false
//                            self.navigationItem.rightBarButtonItems = [rightBarButton,logo]
//                        }
//                        else
//                        {
//                            let logo = UIBarButtonItem(image: UIImage (named: "verifiedBusiness"), style: UIBarButtonItemStyle.plain, target: self, action: nil)
//                            logo.isEnabled = false
//                            self.navigationItem.rightBarButtonItem = logo
//                        }
//                        
//                        
//                    }
                    
                    self.updateModelDictionary(usingDictionary: dataDictionary)
                   
                    return
                }
                else
                {
                    // alert
                    UtilityClass.showAlertWithTitle(title: App_Name, message: message, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                    return
                }
            }
        }
    }
    
    //MARK:- Updating Model Dictionary
    func updateModelDictionary(usingDictionary dictionary : [String:Any]) -> Void
    {
        let dataDictionary = dictionary
        let model = ModelGroupDetail() // Model creation
        model.updateModel(usingDictionary: dataDictionary) // Updating model
        model.groupCallingNumber.append(model.groupCountryCode+"-"+model.groupContact)
        dictionaryBusinessGroup = model // Adding model to dictionary
        
        
//        self.makeMoveButton.isEnabled = !dictionaryBusinessGroup.isMakeMove
        self.makeMoveButton.alpha = dictionaryBusinessGroup.isMakeMove ? 0.7 : 1
        
        if dictionaryBusinessGroup.isMakeMove
        {
            self.makeMoveButton.setTitle("CANCEL", for: .normal)
            
        }
        else
            
        {
            self.makeMoveButton.setTitle("MAKE MOVES", for: .normal)
        }
        
        shouldReloadMap = true
        
        tableBusinessDetails.reloadData()
        tableBusinessDetails.isHidden = false
    }
    
    //MARK:- BusinessNameStatusTagsCellDelegate -> Open Timing
    func businessStatusTagCell(_ cell: BusinessNameStatusTagsCell, didTapOnOpenTimingButton openTimeButton: UIButton) {
        
//        if dictionaryBusinessGroup.groupOpenTill != nil
//        {
//            let hoursView : HoursViewController = UIStoryboard.getHourSelectionViewStoryboard().instantiateInitialViewController() as! HoursViewController
//            hoursView.dictHours = dictionaryBusinessGroup.groupOpenTill!
//
//            hoursView.isFromDetailPage = true
//
//            self.navigationController?.pushViewController(hoursView, animated: true)
//        }
    }
    
    //MARK:- BusinessNameStatusTagsCellDelegate -> Favorite/UnFavorite
    func businessStatusTagCell(_ cell: BusinessNameStatusTagsCell, didTapOnFavoriteButton favoriteButton: UIButton) {
        
//        favoriteButton.isUserInteractionEnabled = false
//
//        let groupID = dictionaryBusinessGroup.groupID
//
//            if dictionaryBusinessGroup.isFavourite
//            {
//                performUnfavouriteGroupApi(forGroupID: groupID, groupCell: cell)
//            }
//            else
//            {
//                performFavouriteGroupApi(forGroupID: groupID, groupCell: cell)
//            }
    }
    
    //MARK:- BusinessFriendsLikesCellDelegate - > Vip Button
    func businessLikeCell(_ cell: BusinessFriendsLikesCell, didTapOnVIPButton vipButton: UIButton) {
        let eventsVC : EventListingViewController = UIStoryboard.getEventListStoryboard().instantiateViewController(withIdentifier: "eventListVC") as! EventListingViewController
        eventsVC.businessID = dictionaryBusinessGroup.groupID
        eventsVC.isViewOnlyMode = true
        eventsVC.shouldShowUpcomingList = true
        self.navigationController?.pushViewController(eventsVC, animated: true)
    }
    
    //MARK:- BusinessFriendsLikesCellDelegate - > Like Button
    func businessLikeCell(_ cell: BusinessFriendsLikesCell, didTapOnLikesButton likesButton: UIButton) {
        let likeViewController = UIStoryboard.getUserHomeStoryboard().instantiateViewController(withIdentifier: "groupLikesViewController") as! GroupLikesViewController
        likeViewController.businessID = dictionaryBusinessGroup.groupID
        likeViewController.businessFriendCount = dictionaryBusinessGroup.peopleLiked
        //        likeViewController.businessModel = groupModel
        likeViewController.hidesBottomBarWhenPushed = true
        self.navigationController?.pushViewController(likeViewController, animated: true)
    }
    
    //MARK:- BusinessFriendsLikesCellDelegate - > Friends Here Button
    func businessLikeCell(_ cell: BusinessFriendsLikesCell, didTapOnFriendsHereButton friendsHereButton: UIButton) {
        let friendsViewController = UIStoryboard.getUserHomeStoryboard().instantiateViewController(withIdentifier: "groupFriendsViewController") as! GroupFriendsViewController
        friendsViewController.businessID = dictionaryBusinessGroup.groupID
        friendsViewController.businessFriendCount = dictionaryBusinessGroup.friendsCount
        friendsViewController.hidesBottomBarWhenPushed = true
        self.navigationController?.pushViewController(friendsViewController, animated: true)
    }
    
    //MARK:- BusinessFriendsLikesCellDelegate - > Fav/UnFav Button
    func businessLikeCell(_ cell: BusinessFriendsLikesCell, didTapOnLikeUnlikeButton likeUnlikeButton: UIButton) {
        likeUnlikeButton.isUserInteractionEnabled = false
        
        let groupID = dictionaryBusinessGroup.groupID
        
        if dictionaryBusinessGroup.isFavourite
        {
            performUnfavouriteGroupApi(forGroupID: groupID, groupCell: cell)
        }
        else
        {
            performFavouriteGroupApi(forGroupID: groupID, groupCell: cell)
        }
    }
    
    
    //MARK:- Favorite Group
    func performFavouriteGroupApi(forGroupID groupID:String, groupCell:BusinessFriendsLikesCell) -> Void
    {
        UtilityClass.showNetworkActivityLoader(show: true)
        
        let urlToHit = EndPoints.favouriteGroup(UtilityClass.getUserSidData()!, groupID).path
        
        AppWebHandler.sharedInstance().fetchData(fromURL: urlToHit, httpMethod: .get, parameters: nil)
        { [weak self] (data, dictionary, statusCode, error) in
            
            UtilityClass.showNetworkActivityLoader(show: false)
            
            guard let `self` = self else {return}
            
            groupCell.buttonLikeUnlike.isUserInteractionEnabled = true
            
            if error != nil
            {
                UtilityClass.showAlertWithTitle(title: App_Name, message: error?.localizedDescription, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                return
            }
            
            
            let responseDictionary = dictionary!
            let message = responseDictionary["message"] as! String
            let type = responseDictionary["type"] as! Bool
            
            if statusCode == 203
            {
                // show alert
                UtilityClass.showAlertWithTitle(title: App_Name, message: message, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                return
            }
            
            if statusCode == 200
            {
                if type
                {
                    // success
                    self.dictionaryBusinessGroup.isFavourite = true
                    self.dictionaryBusinessGroup.peopleLiked = String (Int (self.dictionaryBusinessGroup.peopleLiked)!+1)
                    
                    var originalIndex = -1
                    
                    if let newIndex = arrayGroupListing.index(where: { (model) -> Bool in
                        return model.groupID == self.dictionaryBusinessGroup.groupID
                    })
                    {
                        originalIndex = newIndex
                        arrayGroupListing[newIndex].isFavourite = true
                        arrayGroupListing[newIndex].peopleLiked = self.dictionaryBusinessGroup.peopleLiked
                    }
                    
                    
                    let contains = arrayGroupFavListing.contains(where: { (model) -> Bool in
                        return model.groupID == self.dictionaryBusinessGroup.groupID
                    })
                    
                    if originalIndex >= 0
                    {
                        arrayGroupFavListing.append(arrayGroupListing[originalIndex])
                    }
                    
//                    if contains
//                    {
//                        if let newIndex = arrayGroupFavListing.index(where: { (model) -> Bool in
//                            return model.groupID == self.dictionaryBusinessGroup.groupID
//                        })
//                        {
//                            arrayGroupFavListing[newIndex].isFavourite = true
//                            arrayGroupFavListing[newIndex].peopleLiked = self.dictionaryBusinessGroup.peopleLiked
//                        }
//                    }
//                    else
//                    {
//                        if originalIndex >= 0
//                        {
//                            arrayGroupFavListing.append(arrayGroupListing[originalIndex])
//                        }
//                    }
                    
                    if let newIndex = arrayGroupTempListing.index(where: { (model) -> Bool in
                        return model.groupID == self.dictionaryBusinessGroup.groupID
                    })
                    {
                        arrayGroupTempListing[newIndex].isFavourite = true
                        arrayGroupTempListing[newIndex].peopleLiked = self.dictionaryBusinessGroup.peopleLiked
                    }
                    
                    self.tableBusinessDetails.reloadRows(at: [IndexPath (row: 2, section: 0)], with: .none)
                    return
                }
                else
                {
                    // alert
                    UtilityClass.showAlertWithTitle(title: App_Name, message: message, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                    return
                }
            }
            
        }
    }
    
    //MARK:- BusinessGroupCellDelegate -> UnFavourite Api
    func performUnfavouriteGroupApi(forGroupID groupID:String, groupCell:BusinessFriendsLikesCell) -> Void
    {
        UtilityClass.showNetworkActivityLoader(show: true)
        
        let urlToHit = EndPoints.unFavouriteGroup(UtilityClass.getUserSidData()!, groupID).path
        
        AppWebHandler.sharedInstance().fetchData(fromURL: urlToHit, httpMethod: .get, parameters: nil)
        { [weak self] (data, dictionary, statusCode, error) in
            
            UtilityClass.showNetworkActivityLoader(show: false)
           
            guard let `self` = self else {return}
            
            groupCell.buttonLikeUnlike.isUserInteractionEnabled = true
            
            if error != nil
            {
                UtilityClass.showAlertWithTitle(title: App_Name, message: error?.localizedDescription, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                return
            }
            
            
            let responseDictionary = dictionary!
            let message = responseDictionary["message"] as! String
            let type = responseDictionary["type"] as! Bool
            
            if statusCode == 203
            {
                // show alert
                UtilityClass.showAlertWithTitle(title: App_Name, message: message, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                return
            }
            
            if statusCode == 200
            {
                if type
                {
                    // success
                    self.dictionaryBusinessGroup.isFavourite = false
                    self.dictionaryBusinessGroup.peopleLiked = String (Int (self.dictionaryBusinessGroup.peopleLiked)!-1)
                    
                    var originalIndex = -1
                    
                    if let newIndex = arrayGroupListing.index(where: { (model) -> Bool in
                        return model.groupID == self.dictionaryBusinessGroup.groupID
                    })
                    {
                        originalIndex = newIndex
                        arrayGroupListing[newIndex].isFavourite = false
                        arrayGroupListing[newIndex].peopleLiked = self.dictionaryBusinessGroup.peopleLiked
                    }
                    
                    
                    if let newIndex = arrayGroupFavListing.index(where: { (model) -> Bool in
                        return model.groupID == self.dictionaryBusinessGroup.groupID
                    })
                    {
                        arrayGroupFavListing.remove(at: newIndex)
                    }
                    
                    if let newIndex = arrayGroupTempListing.index(where: { (model) -> Bool in
                        return model.groupID == self.dictionaryBusinessGroup.groupID
                    })
                    {
                        arrayGroupTempListing[newIndex].isFavourite = false
                        arrayGroupTempListing[newIndex].peopleLiked = self.dictionaryBusinessGroup.peopleLiked
                    }
                    
                    self.tableBusinessDetails.reloadRows(at: [IndexPath (row: 2, section: 0)], with: .none)
                    
                    return
                }
                else
                {
                    // alert
                    UtilityClass.showAlertWithTitle(title: App_Name, message: message, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                    return
                }
            }
            
        }
    }
}



//MARK:-
//MARK:- Extension -> CheckinPopupViewControllerDelegate
extension BusinessDetailsViewController : CheckinPopupViewControllerDelegate
{
    func checkinPopUp(_ controller: CheckinPopupViewController, didTapOnSoloButton soloButton: UIButton) {
        controller.dismiss(animated: true, completion: nil)
        self.performMakeMove(sender : soloButton)
    }
    
    func checkinPopUp(_ controller: CheckinPopupViewController, didTapOnGroupButton groupButton: UIButton) {
        controller.dismiss(animated: true, completion: nil)
        
        let myGroupListNav : UINavigationController = UIStoryboard.getUserHomeStoryboard().instantiateViewController(withIdentifier: "myGroupListNav") as! UINavigationController
        
        let mygroupListVC = myGroupListNav.viewControllers.first as! MyGroupListViewController
        
        mygroupListVC.groupDelegate = self
        mygroupListVC.isMakeMoveInvitation = true
        mygroupListVC.businessID = self.dictionaryBusinessGroup.groupID
        mygroupListVC.businessName = self.dictionaryBusinessGroup.groupName
        self.navigationController?.present(myGroupListNav, animated: true, completion: nil)
    }
    
    func checkinPopUp(_ controller: CheckinPopupViewController, didTapOnLyftButton button: UIButton) {
        self.redirectOnLyftApp()
    }
    
    
    func checkinPopUp(_ controller: CheckinPopupViewController, didTapOnUberButton button: UIButton) {
        
        let group = DispatchGroup()
        group.enter()
        let pickupLat = LocationManager.sharedInstance().newLatitude
        let pickupLong = LocationManager.sharedInstance().newLongitude
        self.pickupAddressStr = (self.getAddressFromLatlong(Latitude: pickupLat, Longitude: pickupLong))
        group.leave() // When your task completes
        self.dropoffAddressStr = (self.getAddressFromLatlong(Latitude: (self.dropLatitude), Longitude: (self.dropLongitude)))
        group.wait()
        group.notify(queue: DispatchQueue.main) {[weak self] in
            self?.redirectOnUberApp()
        }
        
    }
}
