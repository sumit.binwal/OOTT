//
//  Date-Helper.swift
//  OOTTBusinessApp
//
//  Created by Santosh on 14/06/17.
//  Copyright © 2017 KonstantInfoSolutions. All rights reserved.
//

import Foundation

extension Date
{
    static func getDateBeforeYears(_ years:Int) -> Date
    {
        let previousDate = Calendar.current.date(byAdding: .year, value: -years, to: Date())
        return previousDate!
    }
    
    static func getDateAfterDays(_ days:Int) -> Date
    {
        let previousDate = Calendar.current.date(byAdding: .day, value: days, to: Date())
        return previousDate!
    }
    
    func getStringForFormat(format : String? = "MM-dd-yyyy") -> String
    {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = format!
        let dateString : String = dateFormatter.string(from: self)
        return dateString
    }
    
    func get24HourFormat(format : String? = "H : mm") -> String
    {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = format!
        let dateString : String = dateFormatter.string(from: self)
        return dateString
    }
    
    static func getDate(fromString:String, usingFormat:String? = "dd-MM-yyyy") -> Date
    {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = usingFormat!
        
        guard let date = dateFormatter.date(from: fromString) else
        {
            return Date()
        }
        
        return date
    }
    static func getDateTime(fromString:String, usingFormat:String? = "d MMM yyyy h:mm a") -> Date
    {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = usingFormat!
        
        guard let date = dateFormatter.date(from: fromString) else
        {
            return Date()
        }
        
        return date
    }
    

    
    static func getTime(fromString:String, usingFormat:String? = "H : mm") -> Date
    {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = usingFormat!
        
        guard let date = dateFormatter.date(from: fromString) else
        {
            return Date()
        }
        
        return date
    }
    
    static func getString() -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd-HH-mm-ss"
        return dateFormatter.string(from: Date ())
    }
}
