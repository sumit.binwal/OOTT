//
//  UIImageView-Extension.swift
//  OOTTBusinessApp
//
//  Created by Bharat Kumar Pathak on 06/07/17.
//  Copyright © 2017 KonstantInfoSolutions. All rights reserved.
//

import Foundation
import UIKit

extension UIImageView {

    func makeRound() -> Void {
        self.layer.cornerRadius = self.frame.size.width/2 * scaleFactorX
        self.clipsToBounds = true
    }
    
    func makeRoundWithoutScale() -> Void {
        self.layer.cornerRadius = self.frame.size.width/2
        self.clipsToBounds = true
    }
    
    func createBorder(withColor borderColor:UIColor, andBorderWidth borderWidth:CGFloat) -> Void {
        self.layer.borderColor = borderColor.cgColor
        self.layer.borderWidth = CGFloat(borderWidth)
    }
}
