//
//  UtilityClass.swift
//  OOTTUserApp
//
//  Created by Santosh on 06/06/17.
//  Copyright © 2017 KonstantInfoSolutions. All rights reserved.
//

import UIKit
import SafariServices
import GoogleMaps
import MapKit
import AVFoundation
let userDefault = UserDefaults.standard

private enum UserDefaultEnum : String {
    case userInfoData = "userInfoData"
    case stackedUserInfoData = "stackedUserInfoData"
    case shouldRemember = "shouldRemember"
    case username = "username"
    case password = "password"
}

class UtilityClass: NSObject {
    
    static let fontNavigationBar = 18 * scaleFactorX
    

    //MARK:- Navigation Bar Update
    static func updateNavigationBarForApp() -> Void
    {
        let appearance = UINavigationBar.appearance()
        appearance.barTintColor = UIColor.clear
        appearance.backgroundColor = UIColor.clear
        appearance.shadowImage = UIImage()
        appearance.setBackgroundImage(UIImage(), for: .default)
        
        appearance.titleTextAttributes = [
            NSForegroundColorAttributeName : UIColor.white,
            NSFontAttributeName : UIFont(name: FONT_PROXIMA_REGULAR, size: fontNavigationBar)!
        ]
        
    }
    
    
    
    //MARK:- Alert View
    static func showAlertWithTitle(title:String? = App_Name, message:String? = App_Global_Msg, onViewController:UIViewController?, withButtonArray buttonArray:[String]? = [], dismissHandler:((_ buttonIndex:Int)->())?) -> Void {
        
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        var ignoreButtonArray = false
        
        if buttonArray == nil
        {
            ignoreButtonArray = true
        }
        
        if !ignoreButtonArray
        {
            for item in buttonArray!
            {
                let action = UIAlertAction(title: item, style: .default, handler: { (action) in
                    
                    alertController.dismiss(animated: true, completion: nil)

                    guard (dismissHandler != nil) else
                    {
                        return
                    }
                    dismissHandler!(buttonArray!.index(of: item)!)
                })
                alertController.addAction(action)
            }
        }
        
        let action = UIAlertAction(title: "Dismiss", style: .cancel, handler: { (action) in
            
            guard (dismissHandler != nil) else
            {
                return
            }
            dismissHandler!(LONG_MAX)
        })
        alertController.addAction(action)
    
        onViewController?.present(alertController, animated: true, completion: nil)
        
    }
    
    static func showAlertWithTitle(title:String? = App_Name, message:String? = App_Global_Msg, onViewController:UIViewController?, withButtonArray buttonArray:[String]? = [], cancelButtonTitle: String, dismissHandler:((_ buttonIndex:Int)->())?) -> Void {
        
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        var ignoreButtonArray = false
        
        if buttonArray == nil
        {
            ignoreButtonArray = true
        }
        
        if !ignoreButtonArray
        {
            for item in buttonArray!
            {
                let action = UIAlertAction(title: item, style: .default, handler: { (action) in
                    
                    alertController.dismiss(animated: true, completion: nil)
                    
                    guard (dismissHandler != nil) else
                    {
                        return
                    }
                    dismissHandler!(buttonArray!.index(of: item)!)
                })
                alertController.addAction(action)
            }
        }
        
        let action = UIAlertAction(title: cancelButtonTitle, style: .cancel, handler: { (action) in
            
            guard (dismissHandler != nil) else
            {
                return
            }
            dismissHandler!(LONG_MAX)
        })
        alertController.addAction(action)
        
        onViewController?.present(alertController, animated: true, completion: nil)
        
    }
    
    
    //MARK:- Action Sheet
    static func showActionSheetWithTitle(title:String? = App_Name, message:String? = App_Global_Msg, onViewController:UIViewController?, withButtonArray buttonArray:[String]? = [], dismissHandler:((_ buttonIndex:Int)->())?) -> Void {
        
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .actionSheet)
        
        var ignoreButtonArray = false
        
        if buttonArray == nil
        {
            ignoreButtonArray = true
        }
        
        if !ignoreButtonArray
        {
            for item in buttonArray!
            {
                let action = UIAlertAction(title: item, style: .default, handler: { (action) in
                    
                    alertController.dismiss(animated: true, completion: nil)
                    
                    guard (dismissHandler != nil) else
                    {
                        return
                    }
                    dismissHandler!(buttonArray!.index(of: item)!)
                })
                alertController.addAction(action)
            }
        }
        
        let action = UIAlertAction(title: "Cancel", style: .cancel, handler: { (action) in
            
            guard (dismissHandler != nil) else
            {
                return
            }
            dismissHandler!(LONG_MAX)
        })
        alertController.addAction(action)
        
        onViewController?.present(alertController, animated: true, completion: nil)
    }
    
    //MARK:- Change RootViewController
    class func changeRootViewController(with newRootViewController : UIViewController) -> Void
    {
        if let navigationCont = appDelegate?.window??.rootViewController as? UINavigationController
        {
            navigationCont.popToRootViewController(animated: false)
        }
        else if let tabbarCont = appDelegate?.window??.rootViewController as? UITabBarController
        {
            print("tabbar")
            for viewcontroller in tabbarCont.viewControllers!
            {
                if let navigationVC = viewcontroller as? UINavigationController
                {
                    navigationVC.popToRootViewController(animated: false)
                }
            }
            tabbarCont.setViewControllers(nil, animated: false)
        }
        appDelegate?.window??.rootViewController = nil
        appDelegate?.window??.rootViewController = newRootViewController
    }
    
    //MARK:- Normal Picker
    class func getNormalPickerView() -> UIPickerView
    {
        let pickerview = UIPickerView()
        return pickerview
    }
    
    //MARK:- DOB Picker
    class func getDOBPickerView() -> UIDatePicker
    {
        let pickerview = UIDatePicker()
        pickerview.datePickerMode = .date
        pickerview.maximumDate = Date.getDateBeforeYears(21)
        pickerview.date = pickerview.maximumDate!
        return pickerview
    }
    
    
    //MARK:- Date Picker
    class func getDatePickerView() -> UIDatePicker
    {
        let pickerview = UIDatePicker()
        pickerview.datePickerMode = .date
        pickerview.minimumDate=Date()
//        pickerview.maximumDate = Date.getDateAfterDays(7)
        pickerview.date = Date()
        return pickerview
    }
    
    //MARK:- DOB Picker
    class func getTimePicker24Format() -> UIDatePicker
    {
        let pickerview = UIDatePicker()
        pickerview.datePickerMode = .time
        pickerview.date = Date()
        pickerview.locale = Locale(identifier: "en_GB")
        return pickerview
    }
    
    //MARK:- DOB Picker
    class func getTimePicker12Format() -> UIDatePicker
    {
        let pickerview = UIDatePicker()
        pickerview.datePickerMode = .time
        pickerview.date = Date()
        pickerview.locale = Locale(identifier: "en_US")
        return pickerview
    }
    
    //MARK:- Show/Hide Network Activity Indicator
    class func showNetworkActivityLoader(show : Bool) -> Void
    {
        UIApplication.shared.isNetworkActivityIndicatorVisible = show
    }
    
    //MARK:-
    //MARK:- User Info ( SAVE )
    class func saveUserInfoData(userDict : [String:Any]) -> Void
    {
        let data = NSKeyedArchiver.archivedData(withRootObject: userDict)
        userDefault.set(data, forKey: UserDefaultEnum.userInfoData.rawValue)
    }
    
    //MARK: User Name ( SAVE )
    class func saveUserUpdatedName(updatedName : String) -> Void
    {
        var userinfo = getUserInfoData()
        userinfo.updateValue(updatedName, forKey: "name")
        saveUserInfoData(userDict: userinfo)
    }
    
    //MARK: User Image String ( SAVE )
    class func saveUserImageString(updatedString : String) -> Void
    {
        var userinfo = getUserInfoData()
        userinfo.updateValue(updatedString, forKey: "picture")
        saveUserInfoData(userDict: userinfo)
    }
    
    //MARK:- User Info ( GET )
    class func getUserInfoData() -> [String:Any]
    {
        let data : Data = userDefault.object(forKey: UserDefaultEnum.userInfoData.rawValue) as! Data
        let userDict : [String:Any] = NSKeyedUnarchiver.unarchiveObject(with: data) as! [String : Any]
        return userDict
    }
    
    //MARK: User Full Name ( GET )
    class func getUserNameData() -> String?
    {
        guard let data = userDefault.object(forKey: UserDefaultEnum.userInfoData.rawValue) as? Data else
        {
            return nil
        }
        let userDict : [String:Any] = NSKeyedUnarchiver.unarchiveObject(with: data) as! [String : Any]
        let strSid = userDict["name"] as! String
        return strSid
    }
    
    //MARK: User username ( GET )
    class func getUserUsernameData() -> String?
    {
        guard let data = userDefault.object(forKey: UserDefaultEnum.userInfoData.rawValue) as? Data else
        {
            return nil
        }
        let userDict : [String:Any] = NSKeyedUnarchiver.unarchiveObject(with: data) as! [String : Any]
        let strSid = userDict["username"] as! String
        return strSid
    }
    
    //MARK: User Image ( GET )
    class func getUserImageString() -> String?
    {
        guard let data = userDefault.object(forKey: UserDefaultEnum.userInfoData.rawValue) as? Data else
        {
            return nil
        }
        let userDict : [String:Any] = NSKeyedUnarchiver.unarchiveObject(with: data) as! [String : Any]
        let strSid = userDict["picture"] as! String
        return strSid
    }
    
    //MARK: User Mobile no ( GET )
    class func getUserMobileNo() -> String?
    {
        guard let data = userDefault.object(forKey: UserDefaultEnum.userInfoData.rawValue) as? Data else
        {
            return nil
        }
        let userDict : [String:Any] = NSKeyedUnarchiver.unarchiveObject(with: data) as! [String : Any]
        let strSid = userDict["mobileNo"] as! String
        return strSid
    }
    
    //MARK: User Sid ( GET )
    class func getUserSidData() -> String?
    {
//        return "4e01f9b4a591e847c101375c06a3169a0166c8b88e88900b3d6f8f4edeb16a4e";
        
        guard let data = userDefault.object(forKey: UserDefaultEnum.userInfoData.rawValue) as? Data else
        {
            return nil
        }
        let userDict : [String:Any] = NSKeyedUnarchiver.unarchiveObject(with: data) as! [String : Any]
        let strSid = userDict["sid"] as! String
        return strSid
    }
    //MARK: User ID ( GET )
    class func getUserIDData() -> String?
    {
        guard let data = userDefault.object(forKey: UserDefaultEnum.userInfoData.rawValue) as? Data else
        {
            return nil
        }
        let userDict : [String:Any] = NSKeyedUnarchiver.unarchiveObject(with: data) as! [String : Any]
        let strSid = userDict["user_id"] as! String
        return strSid
    }
    
    //MARK: User Type ( GET )
    class func getUserTypeData() -> String?
    {
        guard let data = userDefault.object(forKey: UserDefaultEnum.userInfoData.rawValue) as? Data else
        {
            return nil
        }
        let userDict : [String:Any] = NSKeyedUnarchiver.unarchiveObject(with: data) as! [String : Any]
        let strSid = userDict["userType"] as! String
        return strSid
    }
    
    //MARK: User Picture ( GET )
    class func getUserPictureData() -> String?
    {
        guard let data = userDefault.object(forKey: UserDefaultEnum.userInfoData.rawValue) as? Data else
        {
            return nil
        }
        let userDict : [String:Any] = NSKeyedUnarchiver.unarchiveObject(with: data) as! [String : Any]
        let strSid = userDict["picture"] as! String
        return strSid
    }
    
    //MARK: User Private Account Status ( GET )
    class func getIsPrivateAccountData() -> Bool
    {
        guard let data = userDefault.object(forKey: UserDefaultEnum.userInfoData.rawValue) as? Data else
        {
            return false
        }
        let userDict : [String:Any] = NSKeyedUnarchiver.unarchiveObject(with: data) as! [String : Any]
        let strSid = userDict["isPrivateAccount"] as! Bool
        return strSid
    }
    
    //MARK: User Ghost Account Status ( GET )
    class func getIsGhostAccountData() -> Bool
    {
        guard let data = userDefault.object(forKey: UserDefaultEnum.userInfoData.rawValue) as? Data else
        {
            return false
        }
        let userDict : [String:Any] = NSKeyedUnarchiver.unarchiveObject(with: data) as! [String : Any]
        let strSid = userDict["isGhostAccount"] as! Bool
        return strSid
    }
    
    //MARK: Profile Info ( GET )
    class func isProfileDone() -> Bool
    {
        guard let data = userDefault.object(forKey: UserDefaultEnum.userInfoData.rawValue) as? Data else
        {
            return false
        }
        let userDict : [String:Any] = NSKeyedUnarchiver.unarchiveObject(with: data) as! [String : Any]
        let strSid = userDict["isProfileDone"] as! Bool
        return strSid
    }
    
    //MARK: Full Name ( SET )
    class func updateFullName(_ fullname : String)
    {
        guard let data = userDefault.object(forKey: UserDefaultEnum.userInfoData.rawValue) as? Data else
        {
            return
        }
        var userDict : [String:Any] = NSKeyedUnarchiver.unarchiveObject(with: data) as! [String : Any]
        userDict["name"] = fullname
        saveUserInfoData(userDict: userDict)
    }
    
    //MARK: Profile Info ( SET )
    class func updateProfileDone(isDone:Bool)
    {
        guard let data = userDefault.object(forKey: UserDefaultEnum.userInfoData.rawValue) as? Data else
        {
            return
        }
        var userDict : [String:Any] = NSKeyedUnarchiver.unarchiveObject(with: data) as! [String : Any]
        userDict["isProfileDone"] = isDone
        saveUserInfoData(userDict: userDict)
    }
    
    //MARK: User Notification ( SET )
    class func updateNotificationSetting(_ notificationEnabled:Bool)
    {
        guard let data = userDefault.object(forKey: UserDefaultEnum.userInfoData.rawValue) as? Data else
        {
            return
        }
        var userDict : [String:Any] = NSKeyedUnarchiver.unarchiveObject(with: data) as! [String : Any]
        userDict["isNotify"] = notificationEnabled
        saveUserInfoData(userDict: userDict)
    }
    
    //MARK: User Notification ( SET )
    class func setIsPrivateAccount(_ isPrivateAccount:Bool)
    {
        guard let data = userDefault.object(forKey: UserDefaultEnum.userInfoData.rawValue) as? Data else
        {
            return
        }
        var userDict : [String:Any] = NSKeyedUnarchiver.unarchiveObject(with: data) as! [String : Any]
        userDict["isPrivateAccount"] = isPrivateAccount
        saveUserInfoData(userDict: userDict)
    }
    
    //MARK: User Ghost Mode ( SET )
    class func setGhostModeAccount(_ isPrivateAccount:Bool)
    {
        guard let data = userDefault.object(forKey: UserDefaultEnum.userInfoData.rawValue) as? Data else
        {
            return
        }
        var userDict : [String:Any] = NSKeyedUnarchiver.unarchiveObject(with: data) as! [String : Any]
        userDict["isGhostAccount"] = isPrivateAccount
        saveUserInfoData(userDict: userDict)
    }
    
    //MARK: User Notification ( GET )
    class func getNotificationSetting() -> Bool
    {
        guard let data = userDefault.object(forKey: UserDefaultEnum.userInfoData.rawValue) as? Data else
        {
            return false
        }
        var userDict : [String:Any] = NSKeyedUnarchiver.unarchiveObject(with: data) as! [String : Any]
        return userDict["isNotify"] as! Bool
    }
    
    //MARK: User Info ( DELETE )
    class func deleteUserData() -> Void
    {
        guard userDefault.object(forKey: UserDefaultEnum.userInfoData.rawValue) != nil else
        {
            return
        }
        userDefault.removeObject(forKey: UserDefaultEnum.userInfoData.rawValue)
    }
    
    //MARK: Buisness Info ( SAVE )
    class func saveBusinessInfoData(businessDict : [String:Any]) -> Void
    {
        let data = NSKeyedArchiver.archivedData(withRootObject: businessDict)
        userDefault.set(data, forKey: "businessInfoData")
    }
    
    //MARK: Buisness Info ( GET )
    class func getBusinessInfoData() -> [String:Any]?
    {
        guard let data = userDefault.object(forKey: "businessInfoData") as? Data else
        {
            return nil
        }
        let businessDict : [String:Any] = NSKeyedUnarchiver.unarchiveObject(with: data) as! [String : Any]
        return businessDict
    }
    
    //MARK: User Info ( DELETE )
    class func deleteBusinessData() -> Void
    {
        guard userDefault.object(forKey: "businessInfoData") != nil else
        {
            return
        }
        userDefault.removeObject(forKey: "businessInfoData")
    }
    
    //MARK: Logout ( DELETE )
    class func deleteDataOnLogout() -> Void
    {
        isFromFacebook = false
        userDefault.removeObject(forKey: UserDefaults.MyApp.checkinLatitude)
        userDefault.removeObject(forKey: UserDefaults.MyApp.checkinLongitude)
        checkInTimer.invalidate()
        deleteUserData()
        deleteBusinessData()
    }
    
    class var shouldRemember: Bool {
        get{
            return userDefault.bool(forKey: UserDefaultEnum.shouldRemember.rawValue)
        }
        set{
            userDefault.set(newValue, forKey: UserDefaultEnum.shouldRemember.rawValue)
        }
    }
    
    class var savedUsername: String {
        get{
            return userDefault.string(forKey: UserDefaultEnum.username.rawValue) ?? ""
        }
        set{
            userDefault.set(newValue, forKey: UserDefaultEnum.username.rawValue)
        }
    }
    
    class var savedPassword: String {
        get{
            return userDefault.string(forKey: UserDefaultEnum.password.rawValue) ?? ""
        }
        set{
            userDefault.set(newValue, forKey: UserDefaultEnum.password.rawValue)
        }
    }
    
    
    //MARK:-
    //MARK:- Accounts Section
    class func saveStackedUserInfoArray(userArray : [[String:Any]])
    {
        let data = NSKeyedArchiver.archivedData(withRootObject: userArray)
        userDefault.set(data, forKey: UserDefaultEnum.stackedUserInfoData.rawValue)
    }
    
    class func getStackedUserInfoData() -> [[String:Any]]?
    {
        if let data : Data = userDefault.object(forKey: UserDefaultEnum.stackedUserInfoData.rawValue) as? Data
        {
            let userArray : [[String:Any]] = NSKeyedUnarchiver.unarchiveObject(with: data) as! [[String : Any]]
            return userArray
        }
        return nil
    }
    
    class func isCurrentUserInStackedUserInfoData() -> Bool
    {
        if let data : Data = userDefault.object(forKey: UserDefaultEnum.stackedUserInfoData.rawValue) as? Data
        {
            if let userArray : [[String:Any]] = NSKeyedUnarchiver.unarchiveObject(with: data) as? [[String : Any]]
            {
                let contains = userArray.contains(where: { (userInfo) -> Bool in
                    return userInfo["sid"] as! String == UtilityClass.getUserSidData()!
                })
                if contains
                {
                    return true
                }
            }
        }
        return false
    }
    
    class func deleteStackedUserInfoData()
    {
        userDefault.removeObject(forKey: UserDefaultEnum.stackedUserInfoData.rawValue)
    }
    
    class func updateCurrentUserInStack() {
        
        if let data : Data = userDefault.object(forKey: UserDefaultEnum.stackedUserInfoData.rawValue) as? Data
        {
            if var userArray : [[String:Any]] = NSKeyedUnarchiver.unarchiveObject(with: data) as? [[String : Any]]
            {
                if let index = userArray.index(where: { (userInfo) -> Bool in
                    return userInfo["sid"] as! String == UtilityClass.getUserSidData()!
                })
                {
                    userArray[index] = UtilityClass.getUserInfoData()
                    UtilityClass.saveStackedUserInfoArray(userArray: userArray)
                }
            }
        }
    }
    
    //MARK:- First User From Stack
    class func getFirstUserFromStackedList() -> [String:Any]?
    {
        var arrayAddedusers : [[String:Any]]?
        if let arrayUsersStack = getStackedUserInfoData()
        {
            arrayAddedusers = arrayUsersStack
        }
        else
        {
            return nil
        }
        
        if arrayAddedusers!.count > 0
        {
            let userInfo = arrayAddedusers?.first
            return userInfo
        }
        
        return nil
    }
    
    //MARK:- Add User Stack
    class func addCurrentUserToStack()
    {
//        UtilityClass.removeCurrentUserFromStack()
        
        var arrayAddedusers : [[String:Any]]?
        if let arrayUsersStack = getStackedUserInfoData()
        {
            arrayAddedusers = arrayUsersStack
        }
        else
        {
            arrayAddedusers = [[String:Any]]()
        }
        
        if let contains = arrayAddedusers?.contains(where: { (userInfo) -> Bool in
            return userInfo["sid"] as! String == UtilityClass.getUserSidData()!
        })
        {
            if contains
            {
                return
            }
        }
        
        arrayAddedusers?.append(UtilityClass.getUserInfoData())
        
        UtilityClass.saveStackedUserInfoArray(userArray: arrayAddedusers!)
    }
    
    //MARK:- Remove User Stack
    class func removeCurrentUserFromStack()
    {
        var arrayAddedusers : [[String:Any]]?
        if let arrayUsersStack = getStackedUserInfoData()
        {
            arrayAddedusers = arrayUsersStack
        }
        else
        {
            return
        }
        
        if let contains = arrayAddedusers?.contains(where: { (userInfo) -> Bool in
            return userInfo["sid"] as! String == UtilityClass.getUserSidData()!
        })
        {
            if contains
            {
                let index = arrayAddedusers?.index(where: { (userInfo) -> Bool in
                    return userInfo["sid"] as! String == UtilityClass.getUserSidData()!
                })
                
                arrayAddedusers?.remove(at: index!)
            }
            else
            {
                return
            }
        }
        else
        {
            return
        }
        
        
        UtilityClass.saveStackedUserInfoArray(userArray: arrayAddedusers!)
        
        if arrayAddedusers!.count == 0
        {
            UtilityClass.deleteStackedUserInfoData()
        }
    }
    
    //MARK:- Remove User Stack
    class func removeUserFromStack(_ username : String)
    {
        var arrayAddedusers : [[String:Any]]?
        if let arrayUsersStack = getStackedUserInfoData()
        {
            arrayAddedusers = arrayUsersStack
        }
        else
        {
            return
        }
        
        if let contains = arrayAddedusers?.contains(where: { (userInfo) -> Bool in
            return userInfo["username"] as? String == username
        })
        {
            if contains
            {
                let index = arrayAddedusers?.index(where: { (userInfo) -> Bool in
                    return userInfo["username"] as! String == username
                })
                
                arrayAddedusers?.remove(at: index!)
            }
            else
            {
                return
            }
        }
        else
        {
            return
        }
        
        
        UtilityClass.saveStackedUserInfoArray(userArray: arrayAddedusers!)
        
        if arrayAddedusers!.count == 0
        {
            UtilityClass.deleteStackedUserInfoData()
        }
    }
    
    //MARK:- Switch Current User With Other User
    class func switchCurrentUserWithUser(_ username : String)
    {
        var arrayAddedusers : [[String:Any]]?
        if let arrayUsersStack = getStackedUserInfoData()
        {
            arrayAddedusers = arrayUsersStack
        }
        else
        {
            return
        }
        
        let index = arrayAddedusers?.index(where: { (userInfo) -> Bool in
            return userInfo["username"] as! String == username
        })
        
        let userInfo = arrayAddedusers![index!]
        
        UtilityClass.saveUserInfoData(userDict: userInfo)
    }
    
    //MARK:-
    //MARK:-
    class func openSafariController(usingLink openLink:LinksEnum, onViewController: UIViewController?) -> Void
    {
        guard let visibleController = onViewController else {
            return
        }
        
        if var url = URL (string: openLink.path) {
            if url.scheme == nil {
                url = URL (string: "http://"+url.absoluteString)!
            }
            let safariController : SFSafariViewController = SFSafariViewController (url: url)
            safariController.delegate = visibleController as? SFSafariViewControllerDelegate
            visibleController.present(safariController, animated: true, completion: nil)
        }
        else {
            return
        }
    }
    
    
    //MARK:-
    class func openSafariControllerWithString(usingLink openLink:String, onViewController: UIViewController?) -> Void
    {
        guard let visibleController = onViewController else {
            return
        }
        
        if var url = URL (string: openLink) {
            if url.scheme == nil {
                url = URL (string: "http://"+url.absoluteString)!
            }
            let safariController : SFSafariViewController = SFSafariViewController (url: url)
            safariController.delegate = visibleController as? SFSafariViewControllerDelegate
            visibleController.present(safariController, animated: true, completion: nil)
        }
        else {
            return
        }
    }
    //MARK:-
    class func createPickerView() -> UIPickerView
    {
        let pickerView=UIPickerView()
        return pickerView
        
    }
    
    //MARK:-
    class func checkAndConvertMinutesToHour(usingString minuteString:String) -> String
    {
        var updatedString = extractNumber(fromString: minuteString)
        
        if Int(updatedString)! < 60
        {
            return String(updatedString+" min")
        }
        
        if Int(updatedString)! % 60 == 0
        {
            updatedString = String (Int(updatedString)! / 60)
            return String(updatedString+" h")
        }
        
        let dividend : Int = Int(updatedString)! / 60
        
        let remainingTime = Int(updatedString)! - 60*dividend
        
        updatedString = String(format: "%dh %dmin", dividend,remainingTime)
        
        return updatedString
    }
    
    //MARK:-
    class func extractNumber(fromString string:String) -> String
    {
        let characterSet = CharacterSet.decimalDigits.inverted
        let stringArray = string.components(separatedBy: characterSet)
        let newString = stringArray.joined(separator: "")
        return newString
    }
    
    class func compareDate(_ fromDate:Date,to toDate:Date, toGranularity:Calendar.Component) -> ComparisonResult {
        
        let order = Calendar.current.compare(fromDate, to: toDate, toGranularity: toGranularity)
        return order
    }
    
    class func getFormattedNumberString(usingString string:String) -> String {
        
        var updatedString = extractNumber(fromString: string)
        
        if Int(updatedString)! < 1000
        {
            return updatedString
        }
        
        if Int(updatedString)! % 1000 == 0
        {
            updatedString = String (Int(updatedString)! / 1000)
            return updatedString.appending("K")
        }
        
        let dividend : Int = Int(updatedString)! / 1000
        
        let remainingTime = (Int(updatedString)! % 1000)/100
        
        updatedString = String(format: "%d.%dK", dividend,remainingTime)
        
        return updatedString
    }
    
    //MARK:-
    class func getCurrentAddress(currentAdd : @escaping (_ returnAddress :String?, _ error:Error?)->Void){
        
       // let infoWindow = Bundle.mainBundle().loadNibNamed("InfoWindowCurrent", owner: self, options: nil)[0] as! InfoWindowCurrent

        let geocoder = GMSGeocoder()

        let coordinate = CLLocationCoordinate2DMake(LocationManager.sharedInstance().newLatitude,LocationManager.sharedInstance().newLongitude)
        
    
        var currentAddress = String()
        
        geocoder.reverseGeocodeCoordinate(coordinate) { response , error in
            
            if error != nil
            {
                currentAdd(nil, error)
                return
            }
            
            if let address = response?.firstResult() {
                if let lines = address.lines
                {
                    currentAddress = lines.joined(separator: ", ")
                    
                    currentAdd(currentAddress, nil)
                }
                else
                {
                    currentAdd("Unable to find Address",nil)
                }
            }
            else
            {
                currentAdd("Unable to find Address",nil)
            }
        }
    }
    
    class func getAddressFromLatLon(pdblLatitude: String, withLongitude pdblLongitude: String) -> String {
        var center : CLLocationCoordinate2D = CLLocationCoordinate2D()
        let lat: Double = Double("\(pdblLatitude)")!
        let lon: Double = Double("\(pdblLongitude)")!

        let ceo: CLGeocoder = CLGeocoder()
        center.latitude = lat
        center.longitude = lon
        
        let loc: CLLocation = CLLocation(latitude:center.latitude, longitude: center.longitude)
        var addressString : String = ""
        
        ceo.reverseGeocodeLocation(loc, completionHandler:
            {(placemarks, error) in
                if (error != nil)
                {
                    print("reverse geodcode fail: \(error!.localizedDescription)")
                }
                let pm = placemarks! as [CLPlacemark]
                
                if pm.count > 0 {
                    let pm = placemarks![0]
                    print(pm.country as Any)
                    print(pm.locality as Any)
                    print(pm.subLocality as Any)
                    print(pm.thoroughfare as Any)
         
                    if pm.subLocality != nil {
                        addressString = addressString + pm.subLocality! + ", "
                    }
                    if pm.thoroughfare != nil {
                        addressString = addressString + pm.thoroughfare! + ", "
                    }
                    if pm.locality != nil {
                        addressString = addressString + pm.locality! + ", "
                    }
                    if pm.country != nil {
                        addressString = addressString + pm.country! + ", "
                    }

                    print(addressString)
                }
        })
        return addressString
    }
    

    
    class func openCallScreen(withNumber number:String)
    {
        if let dialNumber = URL (string: "tel://"+number)
        {
            if UIApplication.shared.canOpenURL(dialNumber)
            {
                UIApplication.shared.open(dialNumber, options: [:], completionHandler: nil)
            }
        }
    }
    
    //MARK:- Global Favourite Methods
    class func addToFavouriteArray(theID businessID : String)
    {
        if global_FavouriteBusinessArray.count > 0
        {
            let contains = global_FavouriteBusinessArray.contains(where: { (businessUser) -> Bool in
                return businessUser.userID == businessID
            })
            if contains
            {
                return
            }
            global_FavouriteBusinessArray.append(GlobalBusinessUser (id: businessID))
        }
        else
        {
            global_FavouriteBusinessArray.append(GlobalBusinessUser (id: businessID))
        }
    }
    
    class func removeFromFavouriteArray(theID businessID : String)
    {
        if global_FavouriteBusinessArray.count > 0
        {
            let contains = global_FavouriteBusinessArray.contains(where: { (businessUser) -> Bool in
                return businessUser.userID == businessID
            })
            if contains
            {
                if let index = global_FavouriteBusinessArray.index(where: { (businessUser) -> Bool in
                    return businessUser.userID == businessID
                })
                {
                    global_FavouriteBusinessArray.remove(at: index)
                }
            }
        }
    }
    
    class func isBusinessExistInFavouriteArray(id businessID:String,handler:(_ isExist:Bool, _ index:Int)->())
    {
        if global_FavouriteBusinessArray.count > 0
        {
            let contains = global_FavouriteBusinessArray.contains(where: { (businessUser) -> Bool in
                return businessUser.userID == businessID
            })
            if contains
            {
                if let index = global_FavouriteBusinessArray.index(where: { (businessUser) -> Bool in
                    return businessUser.userID == businessID
                })
                {
                    handler (true, index)
                }
            }
            else
            {
                handler (false, 0)
            }
        }
        else
        {
            handler (false, 0)
        }
    }
    
    class func getFormattedFloatString(_ floatValue:Double, uptoDecimalLimit decimalLimit:Int) -> String
    {
        var formattedFloat = String (floatValue)
        let arrOfValue = String (floatValue).components(separatedBy: ".") as [String]
        if arrOfValue.count>1
        {
            let valueBeforeDecimal = arrOfValue.first
            let valueAfterDecimal = arrOfValue.last
            
            var decimalCount = valueAfterDecimal?.count
            
            if decimalCount! > decimalLimit
            {
                decimalCount = decimalLimit
            }
            
            let index = valueAfterDecimal?.index((valueAfterDecimal?.startIndex)!, offsetBy: decimalCount!)
            let str = String (format: "%@.%@", valueBeforeDecimal!,(valueAfterDecimal?.substring(to: index!))!)
            
            formattedFloat = (valueAfterDecimal?.hasPrefix("0"))! ? valueBeforeDecimal! : str
        }
        
        return formattedFloat
    }
    
    class func timeAgoSinceDate(_ date:Date,currentDate:Date, numericDates:Bool) -> String {
        let calendar = Calendar.current
        let now = currentDate
        let earliest = (now as NSDate).earlierDate(date)
        let latest = (earliest == now) ? date : now
        let components:DateComponents = (calendar as NSCalendar).components([NSCalendar.Unit.minute , NSCalendar.Unit.hour , NSCalendar.Unit.day , NSCalendar.Unit.weekOfYear , NSCalendar.Unit.month , NSCalendar.Unit.year , NSCalendar.Unit.second], from: earliest, to: latest, options: NSCalendar.Options())
        
        if (components.year! >= 2) {
            return "\(components.year!) years ago"
        } else if (components.year! >= 1){
            if (numericDates){
                return "1 year ago"
            } else {
                return "Last year"
            }
        } else if (components.month! >= 2) {
            return "\(components.month!) months ago"
        } else if (components.month! >= 1){
            if (numericDates){
                return "1 month ago"
            } else {
                return "Last month"
            }
        } else if (components.weekOfYear! >= 2) {
            return "\(components.weekOfYear!) weeks ago"
        } else if (components.weekOfYear! >= 1){
            if (numericDates){
                return "1 week ago"
            } else {
                return "Last week"
            }
        } else if (components.day! >= 2) {
            return "\(components.day!) days ago"
        } else if (components.day! >= 1){
            if (numericDates){
                return "1 day ago"
            } else {
                return "Yesterday"
            }
        } else if (components.hour! >= 2) {
            return "\(components.hour!) hours ago"
        } else if (components.hour! >= 1){
            if (numericDates){
                return "1 hour ago"
            } else {
                return "An hour ago"
            }
        } else if (components.minute! >= 2) {
            return "\(components.minute!) minutes ago"
        } else if (components.minute! >= 1){
            if (numericDates){
                return "1 minute ago"
            } else {
                return "A minute ago"
            }
        } else if (components.second! >= 3) {
            return "\(components.second!) seconds ago"
        } else {
            return "Just now"
        }
    }
    
    class func openMapsApplication(_ latitude : CLLocationDegrees, longitude : CLLocationDegrees, displayName : String? = "Venue", phoneNumber : String? = "")
    {
        let coordinate = CLLocationCoordinate2DMake(latitude, longitude)
        let mapItem = MKMapItem(placemark: MKPlacemark(coordinate: coordinate, addressDictionary:nil))
        mapItem.name = displayName
        mapItem.phoneNumber = phoneNumber
        mapItem.openInMaps(launchOptions: nil)
    }
    
    class func openGMapsApplication(_ latitude : CLLocationDegrees, longitude : CLLocationDegrees, displayName : String? = "Venue", phoneNumber : String? = "")
    {
        let coordinate = CLLocationCoordinate2DMake(latitude, longitude)
//        let mapItem = MKMapItem(placemark: MKPlacemark(coordinate: coordinate, addressDictionary:nil))
//        mapItem.name = displayName
//        mapItem.phoneNumber = phoneNumber
//        mapItem.openInMaps(launchOptions: nil)
        
        let lat = coordinate.latitude
        let long = coordinate.longitude
        
        if UIApplication.shared.canOpenURL(URL(string: "comgooglemaps://")!)
        {
            let urlString = "comgooglemaps://?daddr=\(lat),\(long)&directionsmode=driving"
            
            // use bellow line for specific source location
            
            //let urlString = "http://maps.google.com/?saddr=\(sourceLocation.latitude),\(sourceLocation.longitude)&daddr=\(destinationLocation.latitude),\(destinationLocation.longitude)&directionsmode=driving"
            
            UIApplication.shared.openURL(URL(string: urlString)!)
        }
        else
        {

            let urlString = "http://maps.google.com/maps?daddr=\(lat),\(long)&dirflg=d"
            
            UIApplication.shared.openURL(URL(string: urlString)!)
        }
        
    
    }

    
    //MARK: - Present Image PickerController
    class func showImagePickerController(onController controller: UIViewController) {
        
        var isCameraOption = true
        UtilityClass.showActionSheetWithTitle(title: "Choose Option", message: "Choose option for media", onViewController: controller, withButtonArray: ["Camera", "Photos Library"])
        {[weak controller] (buttonIndex) in
            
            if buttonIndex == LONG_MAX
            {
                return
            }
            
            guard let `controller` = controller else {return}
            
            if buttonIndex == 1 {
                isCameraOption = false
            }
            
            let imagePicker = UIImagePickerController ()
            imagePicker.navigationBar.isTranslucent = false
            imagePicker.delegate = controller as? UIImagePickerControllerDelegate & UINavigationControllerDelegate
            imagePicker.sourceType = isCameraOption ? .camera : .savedPhotosAlbum
            imagePicker.mediaTypes = isCameraOption ? UIImagePickerController.availableMediaTypes(for: .camera)! : UIImagePickerController.availableMediaTypes(for: .savedPhotosAlbum)!
            controller.present(imagePicker, animated: true, completion: nil)
        }
    }
    
    class func getUniqueString(usingString: String) -> String {
        var uniqueString = ""
        
        let uniqueProcessID = ProcessInfo ().globallyUniqueString
        
        uniqueString = usingString + Date.getString() + "-" + uniqueProcessID
        
        return uniqueString
    }
    
    class func compressVideo(inputURL: URL, outputURL: URL, handler: @escaping (_ exportSession: AVAssetExportSession?)->Void) {
        
        let urlAsset = AVURLAsset(url: inputURL, options: nil)
        
        guard let exportSession = AVAssetExportSession (asset: urlAsset, presetName: AVAssetExportPresetLowQuality) else {
            return
        }

        exportSession.outputURL = outputURL
        exportSession.outputFileType = AVFileTypeMPEG4
        exportSession.shouldOptimizeForNetworkUse = true
        
        exportSession.exportAsynchronously {
            handler (exportSession)
        }
    }
    
    static func getCoordinatDistance(location1: CLLocation, location2: CLLocation) -> Double {
        let distanceInMeters = location1.distance(from: location2) // result is in meters
        return distanceInMeters
    }
}


extension UINavigationController : SFSafariViewControllerDelegate
{
    public func safariViewControllerDidFinish(_ controller: SFSafariViewController) {
        controller.dismiss(animated: true, completion: nil)
    }
}

//MARK: -
//MARK: - extension -> Documents Directory Handling
extension UtilityClass
{
    class func getDocumentsDirectoryPath() -> URL {
        return try! FileManager.default.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: true)
    }
    
    class func getDocumentsFolder(withName folderName: String) -> URL? {
        let documentsPath = UtilityClass.getDocumentsDirectoryPath()
        print(documentsPath)
        
        let newDirectoryPath = documentsPath.appendingPathComponent(folderName) //documentsPath.appending("/"+folderName)
        
        let fileManager = FileManager.default
        
        if !fileManager.fileExists(atPath: newDirectoryPath.path) {
            do {
                try fileManager.createDirectory(atPath: newDirectoryPath.path, withIntermediateDirectories: true, attributes: nil)
            } catch {
                return nil
            }
        }
        return newDirectoryPath
    }
    
    class func addFileToFolder(_ folder: String, fileName: String, fileData: Data) -> Bool {
        guard let folderDirectory = UtilityClass.getDocumentsFolder(withName: folder) else {
            return false
        }
        
        let fileManager = FileManager.default
        
        let newFilePath = folderDirectory.appendingPathComponent(fileName) //folderDirectory.appending("/"+fileName)
        
        let isSaved = fileManager.createFile(atPath: newFilePath.path, contents: fileData, attributes: nil)
        
        return isSaved
    }
    
    class func getFileFromFolder(_ folder: String, fileName: String) -> Data? {
        guard let folderDirectory = UtilityClass.getDocumentsFolder(withName: folder) else {
            return nil
        }
        
        let fileManager = FileManager.default
        
        let newFilePath = folderDirectory.appendingPathComponent(fileName)
        let newData = fileManager.contents(atPath: newFilePath.path)
//        if fileManager.fileExists(atPath: newFilePath.path) {
            return newData
//        }
//        return nil
    }
    
    class func getContents(inFolder: String) -> [URL]? {
        guard let folderDirectory = UtilityClass.getDocumentsFolder(withName: inFolder) else {
            return nil
        }
        var contentArray: [URL]?
        do {
            contentArray = try FileManager.default.contentsOfDirectory(at: folderDirectory, includingPropertiesForKeys: nil, options: .skipsHiddenFiles)
        } catch {
            print(error)
        }
        return contentArray?.sorted(by: { (url1, url2) -> Bool in
            return url1.deletingPathExtension().lastPathComponent.localizedCaseInsensitiveCompare(url2.deletingPathExtension().lastPathComponent) == ComparisonResult.orderedAscending
        })
    }
}

//MARK: -
//MARK: - extension ->
extension UtilityClass
{
    class func getThumbnailFromVideo(_ videoURL: URL) -> UIImage? {
        
        let asset = AVURLAsset (url: videoURL)
        
        let generate = AVAssetImageGenerator (asset: asset)
        generate.appliesPreferredTrackTransform = true
        
        let time = CMTimeMake(1, 60)
        
        let imgRef: CGImage!
        
        do {
            imgRef = try generate.copyCGImage(at: time, actualTime: nil)
        } catch {
            return nil
        }
        
        return UIImage (cgImage: imgRef)
    }
}
