//
//  UITableViewCell-Extension.swift
//  OOTTBusinessApp
//
//  Created by Santosh on 11/08/17.
//  Copyright © 2017 KonstantInfoSolutions. All rights reserved.
//

import Foundation
import UIKit

extension UITableViewCell
{
    func disbaleSelection()
    {
        self.selectionStyle = .none
    }
}
