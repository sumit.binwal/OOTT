//
//  PieChart.swift
//  OOTTBusinessApp
//
//  Created by Santosh on 15/01/18.
//  Copyright © 2018 KonstantInfoSolutions. All rights reserved.
//

import UIKit

extension CGFloat {
    func toRadians() -> CGFloat {
        return self * CGFloat.pi / 180.0
    }
}

class PieChartItem {
    var value: CGFloat
    var color: UIColor
    
    init(_ color: UIColor, value: CGFloat) {
        self.color = color
        self.value = value
    }
}

//@IBDesignable
class PieChart: UIView {

    var chartItems = [PieChartItem]()
    
    var degreeFactor: CGFloat = 360/100
    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.backgroundColor = UIColor.clear
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.backgroundColor = UIColor.clear
    }
    
    func addChartItem(chartItem: PieChartItem) {
        chartItems.append(chartItem)
    }
    
    func removeAllItems() {
        chartItems.removeAll()
        setNeedsDisplay()
    }
    
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        
        let path = UIBezierPath (ovalIn: rect)
        UIColor.black.setFill()
        path.fill()
        
        // Drawing code
        if chartItems.count > 0 {
            // draw the chart...
            
            let radius = min(frame.width, frame.height) * 0.5
            
            let viewCenter = CGPoint(x: bounds.size.width * 0.5, y: bounds.size.height * 0.5)
            
            var startAngle = CGFloat (270).toRadians()
            
            for segment in chartItems {
                
                let valueFactor: CGFloat = segment.value * degreeFactor
                let endAngle = startAngle - valueFactor.toRadians()

                let path = UIBezierPath ()
                path.move(to: viewCenter)
                path.addArc(withCenter: viewCenter, radius: radius, startAngle: startAngle, endAngle: endAngle, clockwise: false)
                
                segment.color.setFill()
                
                path.fill()
                
                startAngle = endAngle
            }
        }
    }
}
