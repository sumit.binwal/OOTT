//
//  UserDefaultsExtension.swift
//  OOTTBusinessApp
//
//  Created by Santosh on 17/01/18.
//  Copyright © 2018 KonstantInfoSolutions. All rights reserved.
//

import Foundation

extension UserDefaults
{
    struct MyApp {
        static let isCheckinPopupVisible = "isCheckinPopupVisible"
        static let checkinLatitude = "checkinLatitude"
        static let checkinLongitude = "checkinLongitude"
    }
}
