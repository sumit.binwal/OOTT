//
//  String+Extension.swift
//  OOTTBusinessApp
//
//  Created by Santosh on 19/06/17.
//  Copyright © 2017 KonstantInfoSolutions. All rights reserved.
//

import Foundation
import UIKit

extension String
{
    func isEmptyString() -> Bool
    {
        let newString = self.trimmingCharacters(in: .whitespacesAndNewlines)
        if newString.isEmpty
        {
            return true
        }
        return false
    }
    
    func base64EncodedString() -> String {
        var encodedString = ""
        let trimmedText = getTrimmedText()
        if let data = trimmedText.data(using: .utf8) {
            encodedString = data.base64EncodedString()
        }
        return encodedString
    }
    
    func decodeStringFromBase64Encoding() -> String {
        var decodedString = ""
        if let data = Data (base64Encoded: self) {
            decodedString = String (data: data, encoding: .utf8) ?? ""
        }
        return decodedString
    }
    
    func getTrimmedText() -> String
    {
        let newString = self.trimmingCharacters(in: .whitespacesAndNewlines)
        return newString
    }
    
    func isValidEmailAddress() -> Bool {
        
        var returnValue = true
        let emailRegEx = "[A-Z0-9a-z.-_]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,3}"
        
        do {
            let regex = try NSRegularExpression(pattern: emailRegEx)
            let nsString = self as NSString
            let results = regex.matches(in: self, range: NSRange(location: 0, length: nsString.length))
            
            if results.count == 0
            {
                returnValue = false
            }
            
        } catch let error as NSError {
            print("invalid regex: \(error.localizedDescription)")
            returnValue = false
        }
        return  returnValue
    }
    
    func getLastString(seperatedBy:String) -> String
    {
        if self.contains(seperatedBy)
        {
            let strArray = self.components(separatedBy: seperatedBy)
            if !strArray.isEmpty
            {
                var lastString = strArray.last!
                lastString = lastString.replacingOccurrences(of: "#", with: "")
                lastString = lastString.trimmingCharacters(in: .whitespaces)
                return lastString
            }
            return self
        }
        return self
    }
    
    static func getDateTimeString(fromString:String, inputFormat:String, outputFormat:String ) -> String
        {
                //String to Date Convert
            
                let dateString = fromString
                let dateFormatter = DateFormatter()
                dateFormatter.dateFormat = inputFormat
                let s = dateFormatter.date(from:dateString)
            
                //CONVERT FROM NSDate to String
            
                let dateFormatter1 = DateFormatter()
                dateFormatter1.dateFormat = outputFormat
                let dateString1 = dateFormatter1.string(from:s!)
                print(dateString1)
            return dateString1
            
        }
    
    func getAttributedString(_ withLineSpacing : CGFloat, font: UIFont) -> NSAttributedString
    {
        let attrString = NSMutableAttributedString(string: self)
        let style = NSMutableParagraphStyle()
        
        style.lineSpacing = withLineSpacing // change line spacing between paragraph like 36 or 48
//        style.lineBreakMode = .byTruncatingTail

        attrString.addAttribute(NSParagraphStyleAttributeName, value: style, range: NSRange(location: 0, length: self.count))
//        attrString.addAttribute(NSFontAttributeName, value: font, range: NSRange(location: 0, length: self.characters.count))
        return attrString
    }
    
//    func isValidWebsite() -> Bool {
//        if !self.isEmptyString() {
//            if let url = URL (string: self) {
//                if UIApplication.shared.canOpenURL(url) {
//                    return true
//                }
//            }
//        }
//        return false
//    }
    
    func isValidWebsite() -> Bool {
        if !isEmptyString() {
            if let url = URL (string: self) {
                if url.scheme != nil {
                    print("1")
                    return UIApplication.shared.canOpenURL(url)
                }
                if let newURL = URL (string: "http://"+url.absoluteString) {
                    print("2")
                    return UIApplication.shared.canOpenURL(newURL)
                }
                else {
                    print("url is not proper")
                }
            }
            else {
                print("url is not proper")
            }
        }
        else {
            print("empty string")
        }
        return false
    }
}
