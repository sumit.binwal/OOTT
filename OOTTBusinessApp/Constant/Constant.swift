//
//  Constant.swift
//  OOTTUserApp
//
//  Created by Santosh on 06/06/17.
//  Copyright © 2017 KonstantInfoSolutions. All rights reserved.
//

//MARK:- Importing Modules
import Foundation
import UIKit

//MARK:- Application Constants
let App_Name = "OOTT"

let App_Global_Msg = "This is an alert"
let App_Global_Error_Msg = "We encountered some error, please try again later"


let FONT_PROXIMA_LIGHT = "ProximaNova-Light"
let FONT_PROXIMA_REGULAR = "ProximaNova-Regular"
let FONT_PROXIMA_BOLD = "ProximaNova-Bold"
let FONT_PROXIMA_SEMIBOLD = "ProximaNova-Semibold"



let scaleFactorX = UIScreen.main.bounds.size.width/375
let scaleFactorY = UIScreen.main.bounds.size.height/667
let userDefaults = UserDefaults.standard

let color_light_blue = UIColor(red: 164/255, green: 198/255, blue: 246/255, alpha: 1.0)

let color_pink = UIColor(red: 237/255, green: 43/255, blue: 93/255, alpha: 1.0)
let color_dividerLine = UIColor(red: 56/255, green: 73/255, blue: 87/255, alpha: 1.0)

let color_placeholderColor = UIColor(white: 1.0, alpha: 0.6)

let color_cellImageBorder = UIColor(red: 29.0/255, green: 158.0/255, blue: 240.0/255, alpha: 1.0)

var appDeviceToken : String?

enum ImageSelectType : Int {
    case photos
    case camera
}

//let Google_Api_Key = "AIzaSyDTpNIzTtm33oDwg8db_RgP6u6nMk7Ms1k"
let Google_Api_Key = "AIzaSyDE2190ZsW_eqZCcZ3BMbmNgspoeP-fDhY"


