//
//  AmazonServiceManager.swift
//  OOTTBusinessApp
//
//  Created by Santosh on 27/12/17.
//  Copyright © 2017 KonstantInfoSolutions. All rights reserved.
//

import Foundation
import AWSCognito

class AmazonServiceManager: NSObject {
    
    
    //MARK:- Instance Creation (Private)
    private static var instance : AmazonServiceManager =
    {
        let newInstance = AmazonServiceManager.init()
        
        return newInstance
    }()
    
    //MARK:- init (Private)
    override init() {
        super.init()
    }
    
    //MARK:- Shared Instance(Global)
    class func defaultServiceManager() -> AmazonServiceManager {
        return instance
    }
    
    func setDefaultServiceConfiguration(forRegion region: AWSRegionType, poolID: String)
    {
        let credentialsProvider = AWSCognitoCredentialsProvider (regionType: region, identityPoolId: poolID)
        
        let configuration = AWSServiceConfiguration (region: region, credentialsProvider: credentialsProvider)
        
        AWSServiceManager.default().defaultServiceConfiguration = configuration
    }
}
