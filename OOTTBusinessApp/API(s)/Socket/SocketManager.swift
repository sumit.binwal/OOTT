//
//  SocketManager.swift
//  SocketManagerSwift
//
//  Created by Sumit Sharma on 26/07/2017.
//  Copyright © 2017 sumit. All rights reserved.
//

import Foundation
import SocketIO
import PKHUD

//MARK:- Socket Listener Keywords
enum SocketListenerKeywords:String {
    case authenticate = "authenticate"
    case checkin = "checkin"
    case error = "error"
    case newMessage = "newMessage"
    case chatHistroy = "chatHistroy"
    case chatList = "chatList"
    case friendStatus = "friendsStatus"
    case isInBackground = "isBackground"
    
    // Group chat
    case groupChatList = "groupList"
    case groupHistroy = "groupChatHistroy"
    case groupChatLikesHistroy = "messageLikes"
    case newGroupMessage = "newGroupMessage"
    case leaveGroup = "leaveGroup"
    case messageLike = "messageLike"
    case leaveChatWindow = "leaveChatWindow"
    
    // Block user
    case blockUser = "blockUser"
    case unBlockUser = "unblockUser"
    
    //Group Delete
    case groupRemoved = "groupRemoved"
    
    // Session expired (multiple login)
    case sessionExpired = "sessionExpired"
    
    //Checkin Submit
    case checkinDone = "checkinDone"
}

enum ErrorCodes : Int {
    case SocketNotConnected = 001
}

//MARK:- Notification Keys
let NOTIFICATION_SOCKET_CONNECTED : String = "socketConnected"

////MARK:-
////MARK:- Socket Host
////MARK: -> Dev
////var SocketHost = "http://192.168.0.131:7471"
//
////MARK: -> Client
//var SocketHost = "http://202.157.76.19:7472"

//MARK:-
//MARK:- SocketManager Class
class SocketManager: NSObject {
    
    //MARK: Listener block
    typealias ListenerHandler = (_ data:[Any], _ ack:SocketAckEmitter) -> ()
    
    typealias MessageHandler = (_ messageDict:[String:Any] , _ statusCode:Int) -> ()
    
    typealias ChatHistoryHandler = (_ chatHistoryArray:[[String:Any]] , _ statusCode:Int) -> ()
    
    typealias SingleChatHistoryHandler = (_ chatHistoryArray:[[String:Any]], _ isBlocked: Bool , _ statusCode:Int) -> ()
    
    typealias GroupChatHistoryHandler = (_ chatHistoryArray:[[String:Any]], _ position: String , _ statusCode:Int) -> ()
    
    typealias GroupChatLikesHistoryHandler = (_ chatHistoryArray:[[String:Any]], _ statusCode:Int) -> ()
    
    //MARK:- SocketClient (Private)
    private var socket : SocketIOClient?
    
    //MARK:- Instance Creation (Private)
    private static var instance : SocketManager =
    {
        let newInstance = SocketManager.init()
        
        return newInstance
    }()
    
    //MARK:- init (Private)
    private override init() {
        
        super.init()
        
        // Initializing the socket client for socket host
        socket = SocketIOClient (socketURL: URL (string: baseURLString)!, config:[.log(true), .reconnects(true), .reconnectAttempts(-1), .reconnectWait(1), .compress, .forceNew(true)])
        setupErrorListner()
        setupDisconnectListner()
        sessionExpiredListener()
    }
    
    //MARK:- SSID Var (updates the configuration of socket, Only set before connection)
    var userSID : String!
    {
        didSet
        {
            let socketconfig = SocketIOClientConfiguration (arrayLiteral: .log(true), .reconnects(true), .reconnectAttempts(-1), .reconnectWait(1), .compress, .connectParams(["ssid" : userSID!]), .forceNew(true), .path("/socket.io/"))
            
            socket?.config = socketconfig
            
            print("logged in sid is : ",userSID)
        }
    }
    
    
    //MARK:- Shared Instance(Global)
    class func sharedInstance() -> SocketManager {
        return instance
    }
    
    //MARK:- Connect Socket Method
    func connectSocket(callback:@escaping ListenerHandler)
    {
        socket?.on(clientEvent: .connect, callback:
            { (dataArray, ack) in
                print("Connect listner : ", dataArray)
                callback (dataArray, ack)
        })
        socket?.connect()
    }
    
    //MARK:- Disconnect Socket Method
    func disconnectSocket()
    {
        socket?.disconnect()
    }
    
    //MARK:- Socket Connection Status
    func isSocketConnected() -> Bool {
        return socket?.status == SocketIOClientStatus.connected
    }
    
    //MARK:- Update user location(for Check in)
    func updateUserLocation(withLatitude latitude:Double,longitude:Double, loginToken:String, messageHandler : MessageHandler?) {
        print("------------------------------------------------------------------------------------------------------------------")
        socket?.emitWithAck(SocketListenerKeywords.checkin.rawValue, with: [["latitude":String(latitude),"longitude":String(longitude),"ssid":loginToken]]).timingOut(after: 0, callback: { (dataArray) in
            print("Check-In Ack : ", dataArray)
            // If handler is set as nil, then return (no need to move further)
            if messageHandler == nil
            {
                return
            }
            
            // If handler is implemented
            // If returned response array has data in it, then retreiving the dictionary object
            if let responseDictionary = dataArray[0] as? [String:Any]
            {
                // Fetching the status code
                let statusCode = responseDictionary["status"] as! Int
                
                if let dict = responseDictionary["data"] as? [String:Any]
                {
                    messageHandler!(dict,statusCode)
                }
            }
        })
    }
    
    //MARK:- Error Listener
    func setupErrorListner()
    {
        socket?.on(clientEvent: .error, callback: { (dataArray, ack) in
            print("Error listner : ", dataArray)
            checkInTimer.invalidate()
        })
    }
    
    //MARK:- Disconnect Listener
    func setupDisconnectListner()
    {
        socket?.on(clientEvent: .disconnect, callback:
            { (dataArray, ack) in
//                guard let `self` = self else {return}
//                self.socket?.off(clientEvent: .connect)
            print("Disconnect listner : ", dataArray)
        })
    }
    
    //MARK:- Session Expired Listener
    func sessionExpiredListener()
    {
        socket?.on(SocketListenerKeywords.sessionExpired.rawValue, callback: { (dataArray, ack) in
            print(dataArray)
            
            if let appdele = appDelegate as? AppDelegate
            {
                appdele.handleSessionExpiredSituation()
            }
        })
    }
    
    //MARK:- Send Message (Emit)
    func sendMessage(_ messageModel : SocketModelSendMessage, messageHandler : MessageHandler?)
    {
        
        // Generating Dictionary from Model
        let params = messageModel.getDictionary()
        
        // Emitting the 'newMessage' event and handling the response
        socket?.emitWithAck(SocketListenerKeywords.newMessage.rawValue, with: [params]).timingOut(after: 0, callback:
            { (dataArray) in
                
                
                print(dataArray)
                
                // If handler is set as nil, then return (no need to move further)
                if messageHandler == nil
                {
                    return
                }
                
                // If handler is implemented
                // If returned response array has data in it, then retreiving the dictionary object
                if let responseDictionary = dataArray[0] as? [String:Any]
                {
                    // Fetching the status code
                    let statusCode = responseDictionary["status"] as! Int
                    
                    // Fetching the data dictionary
                    let messageDict = responseDictionary["data"] as! [String:Any]
                    
                    // Calling the handler
                    messageHandler!(messageDict,statusCode)
                }
        })
    }
    
    //MARK:- Receive Message (Listener)
    func receiveIncomingMessages(_ messageHandler : MessageHandler?)
    {
        self.removeIncomingMessagesListener()
        if messageHandler == nil
        {
            return;
        }
        
        socket?.on(SocketListenerKeywords.newMessage.rawValue, callback: { (dataArray, ack) in
            print(dataArray)
            
            // If handler is implemented
            // If returned response array has data in it, then retreiving the dictionary object
            if let responseDictionary = dataArray[0] as? [String:Any]
            {
                // Fetching the status code
                let statusCode = responseDictionary["status"] as! Int
                
                // Fetching the data dictionary
                let messageDict = responseDictionary["data"] as! [String:Any]
                
                // Calling the handler
                messageHandler!(messageDict,statusCode)
            }
        })
    }
    
    func removeIncomingMessagesListener()
    {
        if isSocketConnected() {
            socket?.off(SocketListenerKeywords.newMessage.rawValue)
        }
    }
    
    //MARK:- Get User Chat History (Emit)
    func getUserChatHistory(_ chatHistoryModel : SocketModelGetHistory, chatHistoryHandler : SingleChatHistoryHandler?)
    {
        if !isSocketConnected() {
            HUD.hide()
            return
        }
        // Generating Dictionary from Model
        let params = chatHistoryModel.getDictionary()
        
        // Emitting the 'newMessage' event and handling the response
        socket?.emitWithAck(SocketListenerKeywords.chatHistroy.rawValue, with: [params]).timingOut(after: 0, callback:
            { (dataArray) in
                
                print(dataArray)
                
                // If handler is set as nil, then return (no need to move further)
                if chatHistoryHandler == nil
                {
                    return
                }
                
                // If handler is implemented
                // If returned response array has data in it, then retreiving the dictionary object
                if let responseDictionary = dataArray[0] as? [String:Any]
                {
                    // Fetching the status code
                    let statusCode = responseDictionary["status"] as! Int
                    
                    // Fetching the data dictionary
                    
                    let dataDict = responseDictionary["data"] as! [String:Any]
                    let messageArray = dataDict["history"] as! [[String:Any]]
                    let isBlocked = dataDict["isBlocked"] as! Bool
                    
                    // Calling the handler
                    chatHistoryHandler!(messageArray,isBlocked,statusCode)
                }
        })
    }
    
    //MARK:- Get User Recent Chat List (Emit)
    func getUserChatList(_ chatHistoryHandler : ChatHistoryHandler?)
    {
        if !isSocketConnected() {
            HUD.hide()
            return
        }
        
        // Emitting the 'chatList' event and handling the response
        socket?.emitWithAck(SocketListenerKeywords.chatList.rawValue, with: [""]).timingOut(after: 0, callback:
            { (dataArray) in
                
                print(dataArray)
                
                // If handler is set as nil, then return (no need to move further)
                if chatHistoryHandler == nil
                {
                    return
                }
                
                // If handler is implemented
                // If returned response array has data in it, then retreiving the dictionary object
                if let responseDictionary = dataArray[0] as? [String:Any]
                {
                    // Fetching the status code
                    let statusCode = responseDictionary["status"] as! Int
                    
                    // Fetching the data dictionary
                    let messageArray = responseDictionary["data"] as! [[String:Any]]
                    
                    // Calling the handler
                    chatHistoryHandler!(messageArray,statusCode)
                }
        })
    }
    
    func setIsInBackground(_ isInBackground : Bool)
    {
        socket?.emit(SocketListenerKeywords.isInBackground.rawValue, with: [["isBackground" : isInBackground]])
    }
    
    //MARK:- Receive Message (Listener)
    func getFriendOnlineStatus(_ messageHandler : MessageHandler?)
    {
        if messageHandler == nil
        {
            return;
        }
        
//        if isSocketConnected()
//        {
            socket?.on(SocketListenerKeywords.friendStatus.rawValue, callback: { (dataArray, ack) in
                print(dataArray)
                
                // If handler is implemented
                // If returned response array has data in it, then retreiving the dictionary object
                if let responseDictionary = dataArray[0] as? [String:Any]
                {
                    // Fetching the status code
                    let statusCode = responseDictionary["status"] as! Int
                    
                    // Fetching the data dictionary
                    let messageDict = responseDictionary["data"] as! [String:Any]
                    
                    // Calling the handler
                    messageHandler!(messageDict,statusCode)
                }
            })
//        }
    }
    
    func removeFriendOnlineStatusListener()
    {
        socket?.off(SocketListenerKeywords.friendStatus.rawValue)
    }
    
    //MARK:- Get Group Chat History (Emit)
    func getGroupChatList(_ chatHistoryHandler : ChatHistoryHandler?)
    {
        if !isSocketConnected() {
            HUD.hide()
            return
        }
        
        // Emitting the 'chatList' event and handling the response
        socket?.emitWithAck(SocketListenerKeywords.groupChatList.rawValue, with: [""]).timingOut(after: 0, callback:
            { (dataArray) in
                
                print(dataArray)
                
                // If handler is set as nil, then return (no need to move further)
                if chatHistoryHandler == nil
                {
                    return
                }
                
                // If handler is implemented
                // If returned response array has data in it, then retreiving the dictionary object
                if let responseDictionary = dataArray[0] as? [String:Any]
                {
                    // Fetching the status code
                    let statusCode = responseDictionary["status"] as! Int
                    
                    // Fetching the data dictionary
                    let messageArray = responseDictionary["data"] as! [[String:Any]]
                    
                    // Calling the handler
                    chatHistoryHandler!(messageArray,statusCode)
                }
        })
    }
    
    //MARK:- Get Group Chat History (Emit)
    func getGroupChatHistory(_ chatHistoryModel : SocketModelGetHistory, chatHistoryHandler : GroupChatHistoryHandler?)
    {
        if !isSocketConnected() {
            HUD.hide()
            return
        }
        // Generating Dictionary from Model
        let params = chatHistoryModel.getDictionary()
        
        // Emitting the 'newMessage' event and handling the response
        socket?.emitWithAck(SocketListenerKeywords.groupHistroy.rawValue, with: [params]).timingOut(after: 0, callback:
            { (dataArray) in
                
                print(dataArray)
                
                // If handler is set as nil, then return (no need to move further)
                if chatHistoryHandler == nil
                {
                    return
                }
                
                // If handler is implemented
                // If returned response array has data in it, then retreiving the dictionary object
                if let responseDictionary = dataArray[0] as? [String:Any]
                {
                    // Fetching the status code
                    let statusCode = responseDictionary["status"] as! Int
                    
                    // Fetching the data dictionary
                    let dataDict = responseDictionary["data"] as! [String:Any]
                    let messageArray = dataDict["history"] as! [[String:Any]]
                    let position = dataDict["position"] as! String
                    
                    // Calling the handler
                    chatHistoryHandler!(messageArray, position, statusCode)
                }
        })
    }
    
    //MARK:- Get Group Chat Likes History (Emit)
    func getGroupChatLikesHistory(_ chatLikesHistoryModel : SocketModelGetLikesHistory, chatHistoryHandler : GroupChatLikesHistoryHandler?)
    {
        if !isSocketConnected() {
            HUD.hide()
            return
        }
        
        // Generating Dictionary from Model
        let params = chatLikesHistoryModel.getDictionary()
        
        // Emitting the 'newMessage' event and handling the response
        socket?.emitWithAck(SocketListenerKeywords.groupChatLikesHistroy.rawValue, with: [params]).timingOut(after: 0, callback:
            { (dataArray) in
                
                print(dataArray)
                
                // If handler is set as nil, then return (no need to move further)
                if chatHistoryHandler == nil
                {
                    return
                }
                
                // If handler is implemented
                // If returned response array has data in it, then retreiving the dictionary object
                if let responseDictionary = dataArray[0] as? [String:Any]
                {
                    // Fetching the status code
                    let statusCode = responseDictionary["status"] as! Int
                    
                    // Fetching the data dictionary
                    let messageArray = responseDictionary["results"] as! [[String:Any]]
                    
                    // Calling the handler
                    chatHistoryHandler!(messageArray, statusCode)
                }
        })
    }
    
    //MARK:- Send Group Message (Emit)
    func sendGroupMessage(_ messageModel : SocketModelSendGroupMessage, messageDict: [String:Any]? = nil, messageHandler : MessageHandler?)
    {
        
        // Generating Dictionary from Model
        var params: [String:Any]!
        if let newParams = messageDict {
            params = newParams
        }
        else {
            params = messageModel.messageType == .normal ? messageModel.getDictionary() : messageModel.getDictionaryLeftUser()
        }
        
        // Emitting the 'newMessage' event and handling the response
        socket?.emitWithAck(SocketListenerKeywords.newGroupMessage.rawValue, with: [params]).timingOut(after: 0, callback:
            { (dataArray) in
                
                
                print(dataArray)
                
                // If handler is set as nil, then return (no need to move further)
                if messageHandler == nil
                {
                    return
                }
                
                // If handler is implemented
                // If returned response array has data in it, then retreiving the dictionary object
                if let responseDictionary = dataArray[0] as? [String:Any]
                {
                    // Fetching the status code
                    let statusCode = responseDictionary["status"] as! Int
                    
                    // Fetching the data dictionary
                    let messageDict = responseDictionary["data"] as! [String:Any]
                    
                    // Calling the handler
                    messageHandler!(messageDict,statusCode)
                }
        })
    }
    
    //MARK:- Receive Message (Listener)
    func receiveIncomingGroupMessages(_ messageHandler : MessageHandler?)
    {
        self.removeIncomingGroupMessagesListener()
        if messageHandler == nil
        {
            return;
        }
        
        socket?.on(SocketListenerKeywords.newGroupMessage.rawValue, callback: { (dataArray, ack) in
            print(dataArray)
            
            // If handler is implemented
            // If returned response array has data in it, then retreiving the dictionary object
            if let responseDictionary = dataArray[0] as? [String:Any]
            {
                // Fetching the status code
                let statusCode = responseDictionary["status"] as! Int
                
                // Fetching the data dictionary
                let messageDict = responseDictionary["data"] as! [String:Any]
                
                // Calling the handler
                messageHandler!(messageDict,statusCode)
            }
        })
    }
    
    func removeIncomingGroupMessagesListener()
    {
        if isSocketConnected() {
            socket?.off(SocketListenerKeywords.newGroupMessage.rawValue)
        }
    }
    
    //MARK:- Group Leave (EMIT)
    func leaveGroup(_ roomID : String, messageHandler : MessageHandler?)
    {
        
        // Generating Dictionary from Model
        let params = [
            "room_id" : roomID
        ]
        
        // Emitting the 'leaveGroup' event and handling the response
        socket?.emitWithAck(SocketListenerKeywords.leaveGroup.rawValue, with: [params]).timingOut(after: 0, callback:
            { (dataArray) in
                
                
                print(dataArray)
                
                // If handler is set as nil, then return (no need to move further)
                if messageHandler == nil
                {
                    return
                }
                
                // If handler is implemented
                // If returned response array has data in it, then retreiving the dictionary object
                if let responseDictionary = dataArray[0] as? [String:Any]
                {
                    // Fetching the status code
                    let statusCode = responseDictionary["status"] as! Int
                    
                    // Fetching the data dictionary
                    let messageDict = responseDictionary
                    
                    // Calling the handler
                    messageHandler!(messageDict,statusCode)
                }
        })
    }
    
    //MARK:- Like/Unlike Message (EMIT)
    func likeUnLikeMessage(_ likeUnLikeModel : SocketModelLikeMessage, messageHandler : MessageHandler?)
    {
        // Generating Dictionary from Model
        let params = likeUnLikeModel.getDictionary()
        
        print("like params :",params)
        
        // Emitting the 'messageLike' event and handling the response
        socket?.emitWithAck(SocketListenerKeywords.messageLike.rawValue, with: [params]).timingOut(after: 0, callback:
            { (dataArray) in
                
                print(dataArray)
                
                // If handler is set as nil, then return (no need to move further)
                if messageHandler == nil
                {
                    return
                }
                
                // If handler is implemented
                // If returned response array has data in it, then retreiving the dictionary object
                if let responseDictionary = dataArray[0] as? [String:Any]
                {
                    // Fetching the status code
                    let statusCode = responseDictionary["status"] as! Int
                    
                    // Fetching the data dictionary
                    let messageDict = responseDictionary
                    
                    // Calling the handler
                    messageHandler!(messageDict,statusCode)
                }
        })
    }
    
    //MARK:- User Moving Away From Chat Screen (EMIT)
    func userDisappearingFromChatRoom(_ roomID : String)
    {
        // Generating Dictionary from Model
        let params = [
            "room_id" : roomID
        ]
        
        // Emitting the 'leaveChatWindow' event and handling the response
        socket?.emitWithAck(SocketListenerKeywords.leaveChatWindow.rawValue, with: [params]).timingOut(after: 0, callback:
            { (dataArray) in
                print(dataArray)
        })
    }
    
    //MARK:- Block User (EMIT)
    func block(_ userID : String, messageHandler : MessageHandler?)
    {
        // Generating Dictionary from Model
        let params = [
            "user_id" : userID
        ]
        
        // Emitting the 'blockUser' event and handling the response
        socket?.emitWithAck(SocketListenerKeywords.blockUser.rawValue, with: [params]).timingOut(after: 0, callback:
            { (dataArray) in
                print(dataArray)
                
                // If handler is set as nil, then return (no need to move further)
                if messageHandler == nil
                {
                    return
                }
                
                // If handler is implemented
                // If returned response array has data in it, then retreiving the dictionary object
                if let responseDictionary = dataArray[0] as? [String:Any]
                {
                    // Fetching the status code
                    let statusCode = responseDictionary["status"] as! Int
                    
                    // Fetching the data dictionary
                    let messageDict = responseDictionary["data"] as! [String:Any]
                    
                    // Calling the handler
                    messageHandler!(messageDict,statusCode)
                }
        })
    }
    
    //MARK:- UnBlock User (EMIT)
    func unBlock(_ userID : String, messageHandler : MessageHandler?)
    {
        // Generating Dictionary from Model
        let params = [
            "user_id" : userID
        ]
        
        // Emitting the 'unBlockUser' event and handling the response
        socket?.emitWithAck(SocketListenerKeywords.unBlockUser.rawValue, with: [params]).timingOut(after: 0, callback:
            { (dataArray) in
                print(dataArray)
                
                // If handler is set as nil, then return (no need to move further)
                if messageHandler == nil
                {
                    return
                }
                
                // If handler is implemented
                // If returned response array has data in it, then retreiving the dictionary object
                if let responseDictionary = dataArray[0] as? [String:Any]
                {
                    // Fetching the status code
                    let statusCode = responseDictionary["status"] as! Int
                    
                    // Fetching the data dictionary
                    let messageDict = responseDictionary["data"] as! [String:Any]
                    
                    // Calling the handler
                    messageHandler!(messageDict,statusCode)
                }
        })
    }
    
    //MARK:- Group Deleted (Listener)
    func groupDeletedListener(_ messageHandler : MessageHandler?)
    {
        self.removeGroupDeletedListener()
        if messageHandler == nil
        {
            return;
        }
        
        socket?.on(SocketListenerKeywords.groupRemoved.rawValue, callback: { (dataArray, ack) in
            print(dataArray)
            
            // If handler is implemented
            // If returned response array has data in it, then retreiving the dictionary object
            if let responseDictionary = dataArray[0] as? [String:Any]
            {
                // Fetching the status code
                let statusCode = responseDictionary["status"] as! Int
                
                // Fetching the data dictionary
                let messageDict = responseDictionary["data"] as! [String:Any]
                
                // Calling the handler
                messageHandler!(messageDict,statusCode)
            }
        })
    }
    
    func removeGroupDeletedListener()
    {
        socket?.off(SocketListenerKeywords.groupRemoved.rawValue)
    }
    
    
    //MARK:- Checkin Submit (EMIT)
    func checkinSubmitOptions(_ optionsDict : ModelCheckinPopup, messageHandler : MessageHandler?)
    {
        // Generating Dictionary from Model
        let params = optionsDict.getDictionary()
        
        // Emitting the 'checkinDone' event and handling the response
        socket?.emitWithAck(SocketListenerKeywords.checkinDone.rawValue, with: [params]).timingOut(after: 0, callback:
            { (dataArray) in
                print(dataArray)
                
                // If handler is set as nil, then return (no need to move further)
                if messageHandler == nil
                {
                    return
                }
                
                // If handler is implemented
                // If returned response array has data in it, then retreiving the dictionary object
                if let responseDictionary = dataArray[0] as? [String:Any]
                {
                    // Fetching the status code
                    let statusCode = responseDictionary["status"] as! Int
                    
                    // Calling the handler
                    messageHandler!(["":""],statusCode)
                }
        })
    }
}
